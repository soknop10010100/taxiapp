/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.github.florent37.singledateandtimepicker;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.github.florent37.singledateandtimepicker";
  public static final String BUILD_TYPE = "release";
}
