/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.fxn.pix;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String LIBRARY_PACKAGE_NAME = "com.fxn.pix";
  public static final String BUILD_TYPE = "release";
}
