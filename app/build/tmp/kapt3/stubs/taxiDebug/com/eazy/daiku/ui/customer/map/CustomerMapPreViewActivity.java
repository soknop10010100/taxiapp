package com.eazy.daiku.ui.customer.map;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00b8\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u000f\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u0018\u0000 W2\u00020\u00012\u00020\u0002:\u0001WB\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u00102\u001a\u000203H\u0002J\u0010\u00104\u001a\u0002032\u0006\u00105\u001a\u00020\rH\u0002J\u0010\u00106\u001a\u0002032\u0006\u00105\u001a\u00020\rH\u0002J\b\u00107\u001a\u000203H\u0002J\b\u00108\u001a\u000203H\u0002J\b\u00109\u001a\u000203H\u0002J\b\u0010:\u001a\u000203H\u0002J\b\u0010;\u001a\u00020\u0014H\u0002J\b\u0010<\u001a\u000203H\u0002J\b\u0010=\u001a\u000203H\u0002J\u0010\u0010>\u001a\u0002032\u0006\u00105\u001a\u00020\rH\u0002J\u0010\u0010?\u001a\u0002032\u0006\u00105\u001a\u00020\rH\u0002J\b\u0010@\u001a\u000203H\u0002J\u0012\u0010A\u001a\u0002032\b\u0010B\u001a\u0004\u0018\u00010CH\u0014J\b\u0010D\u001a\u000203H\u0014J\u0010\u0010E\u001a\u0002032\u0006\u0010F\u001a\u00020!H\u0016J\u0010\u0010G\u001a\u00020\u00142\u0006\u0010H\u001a\u00020IH\u0016J\b\u0010J\u001a\u000203H\u0014J\b\u0010K\u001a\u000203H\u0014J\b\u0010L\u001a\u000203H\u0002J\b\u0010M\u001a\u000203H\u0002J\u0006\u0010N\u001a\u000203J$\u0010O\u001a\u000203*\u00020P2\u0006\u0010Q\u001a\u00020R2\u0006\u0010S\u001a\u00020T2\u0006\u0010U\u001a\u00020VH\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0019\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u001c\u001a\u0016\u0012\u0004\u0012\u00020\u001e\u0018\u00010\u001dj\n\u0012\u0004\u0012\u00020\u001e\u0018\u0001`\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010 \u001a\u0004\u0018\u00010!X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\"\u001a\u0004\u0018\u00010\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010#\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010$\u001a\u00020%X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u0014X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\'\u001a\u0004\u0018\u00010(X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010)\u001a\u0004\u0018\u00010*X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010+\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010,\u001a\u00020-8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b0\u00101\u001a\u0004\b.\u0010/\u00a8\u0006X"}, d2 = {"Lcom/eazy/daiku/ui/customer/map/CustomerMapPreViewActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "()V", "address", "", "addressIndex", "Landroid/location/Address;", "binding", "Lcom/eazy/daiku/databinding/ActivityCustomerMapPreViewBinding;", "currentGpsLocation", "Landroid/location/Location;", "currentLatLngKiosk", "Lcom/google/android/gms/maps/model/LatLng;", "descriptionFromSearch", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "intentFilter", "Landroid/content/IntentFilter;", "isNightTheme", "", "isRotateGesture", "isShowTraffic", "kioskMarker", "Lcom/google/android/gms/maps/model/Marker;", "latLngMarkerClick", "listKioskModel", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "listMarkerOption", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/MarkerOption;", "Lkotlin/collections/ArrayList;", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "mapScreenShotPath", "marker", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "needReloadData", "qrCodeRespond", "Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "termModel", "Lcom/eazy/daiku/ui/customer/model/Terms;", "titleFromSearch", "viewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "getViewModel", "()Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "doAction", "", "draMarkerStationKiosk", "latLng", "drawMarker", "fetchCurrentLocation", "initGoogleMap", "initObserver", "initView", "justPreviewOnly", "loadGpsLocationUser", "mapScreenShot", "moveCameraToCurrentLocation", "moveCameraToKioskLocation", "moveToCurrentGPS", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMapReady", "googleMap", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onResume", "onStart", "openMapSettings", "showAllMarker", "submitCheckoutTaxi", "writeBitmap", "Ljava/io/File;", "bitmap", "Landroid/graphics/Bitmap;", "format", "Landroid/graphics/Bitmap$CompressFormat;", "quality", "", "Companion", "app_taxiDebug"})
public final class CustomerMapPreViewActivity extends com.eazy.daiku.utility.base.BaseActivity implements com.google.android.gms.maps.OnMapReadyCallback {
    private final kotlin.Lazy viewModel$delegate = null;
    private com.eazy.daiku.databinding.ActivityCustomerMapPreViewBinding binding;
    private com.google.android.gms.maps.GoogleMap mMap;
    private com.eazy.daiku.data.model.server_model.QrCodeRespond qrCodeRespond;
    private android.location.Location currentGpsLocation;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private boolean needReloadData = false;
    private com.google.android.gms.maps.model.Marker marker;
    private com.google.android.gms.maps.model.Marker kioskMarker;
    private com.google.android.gms.maps.model.LatLng currentLatLngKiosk;
    private boolean isShowTraffic = false;
    private boolean isRotateGesture = false;
    private boolean isNightTheme = false;
    private com.eazy.daiku.ui.customer.model.ListKioskModel listKioskModel;
    private com.eazy.daiku.ui.customer.model.Terms termModel;
    private java.lang.String address;
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.MarkerOption> listMarkerOption;
    private com.google.android.gms.maps.model.LatLng latLngMarkerClick;
    private java.lang.String mapScreenShotPath;
    private android.location.Address addressIndex;
    private final android.content.IntentFilter intentFilter = null;
    private java.lang.String titleFromSearch = "";
    private java.lang.String descriptionFromSearch = "";
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.map.CustomerMapPreViewActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String listKioskModelKey = "listKioskModelKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String termModelKey = "termModelKey";
    private com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver;
    
    public CustomerMapPreViewActivity() {
        super();
    }
    
    private final com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initObserver() {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.GoogleMap googleMap) {
    }
    
    private final void writeBitmap(java.io.File $this$writeBitmap, android.graphics.Bitmap bitmap, android.graphics.Bitmap.CompressFormat format, int quality) {
    }
    
    private final void doAction() {
    }
    
    private final void mapScreenShot() {
    }
    
    public final void submitCheckoutTaxi() {
    }
    
    private final void openMapSettings() {
    }
    
    private final void initGoogleMap() {
    }
    
    private final boolean justPreviewOnly() {
        return false;
    }
    
    private final void loadGpsLocationUser() {
    }
    
    private final void fetchCurrentLocation() {
    }
    
    private final void drawMarker(com.google.android.gms.maps.model.LatLng latLng) {
    }
    
    private final void draMarkerStationKiosk(com.google.android.gms.maps.model.LatLng latLng) {
    }
    
    private final void moveCameraToCurrentLocation(com.google.android.gms.maps.model.LatLng latLng) {
    }
    
    private final void moveCameraToKioskLocation(com.google.android.gms.maps.model.LatLng latLng) {
    }
    
    private final void moveToCurrentGPS() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void showAllMarker() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/ui/customer/map/CustomerMapPreViewActivity$Companion;", "", "()V", "listKioskModelKey", "", "termModelKey", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}