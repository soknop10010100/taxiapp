package com.eazy.daiku.ui.customer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u000f\u0012\b\b\u0002\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/ui/customer/model/CarBookingModel;", "", "nameCar", "", "(Ljava/lang/String;)V", "getNameCar", "()Ljava/lang/String;", "app_taxiDebug"})
public final class CarBookingModel {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String nameCar = null;
    
    public CarBookingModel() {
        super();
    }
    
    public CarBookingModel(@org.jetbrains.annotations.NotNull()
    java.lang.String nameCar) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getNameCar() {
        return null;
    }
}