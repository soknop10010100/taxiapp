package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000*\u0004\b\u0000\u0010\u00012\u0014\u0012\u0004\u0012\u0002H\u0001\u0012\n\u0012\b\u0012\u0004\u0012\u0002H\u00010\u00030\u0002:\u0001\u001aB9\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\f\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\t\u0012\u0014\u0010\n\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00028\u00000\u00030\u000b\u00a2\u0006\u0002\u0010\fJ\u001e\u0010\u0012\u001a\u00020\u000f2\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u0014\u001a\u00020\u0007H\u0016J\u001e\u0010\u0015\u001a\b\u0012\u0004\u0012\u00028\u00000\u00032\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0007H\u0016J\u001a\u0010\u0019\u001a\u00020\u000f2\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f0\u000eR\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00028\u00000\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0010\u0012\f\b\u0001\u0012\b\u0012\u0004\u0012\u00028\u00000\u00030\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00020\u000f0\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001b"}, d2 = {"Lcom/eazy/daiku/utility/base/BasePaginationAdapter;", "ITEM", "Landroidx/paging/PagedListAdapter;", "Lcom/eazy/daiku/utility/base/BasePaginationAdapter$BasePaginationViewHolder;", "self", "", "itemLayoutId", "", "diffCallback", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "holderClass", "Ljava/lang/Class;", "(Ljava/lang/Object;ILandroidx/recyclerview/widget/DiffUtil$ItemCallback;Ljava/lang/Class;)V", "paginationAdapterListener", "Lkotlin/Function1;", "", "getSelf", "()Ljava/lang/Object;", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "setPaginationAdapterListener", "BasePaginationViewHolder", "app_taxiDebug"})
public final class BasePaginationAdapter<ITEM extends java.lang.Object> extends androidx.paging.PagedListAdapter<ITEM, com.eazy.daiku.utility.base.BasePaginationAdapter.BasePaginationViewHolder<ITEM>> {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object self = null;
    private final int itemLayoutId = 0;
    private final androidx.recyclerview.widget.DiffUtil.ItemCallback<ITEM> diffCallback = null;
    private final java.lang.Class<? extends com.eazy.daiku.utility.base.BasePaginationAdapter.BasePaginationViewHolder<ITEM>> holderClass = null;
    private kotlin.jvm.functions.Function1<? super ITEM, kotlin.Unit> paginationAdapterListener;
    
    public BasePaginationAdapter(@org.jetbrains.annotations.NotNull()
    java.lang.Object self, int itemLayoutId, @org.jetbrains.annotations.NotNull()
    androidx.recyclerview.widget.DiffUtil.ItemCallback<ITEM> diffCallback, @org.jetbrains.annotations.NotNull()
    java.lang.Class<? extends com.eazy.daiku.utility.base.BasePaginationAdapter.BasePaginationViewHolder<ITEM>> holderClass) {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getSelf() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.utility.base.BasePaginationAdapter.BasePaginationViewHolder<ITEM> onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BasePaginationAdapter.BasePaginationViewHolder<ITEM> holder, int position) {
    }
    
    public final void setPaginationAdapterListener(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super ITEM, kotlin.Unit> paginationAdapterListener) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b&\u0018\u0000*\u0004\b\u0001\u0010\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J)\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00028\u00012\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00028\u0001\u0012\u0004\u0012\u00020\u00070\nH&\u00a2\u0006\u0002\u0010\u000b\u00a8\u0006\f"}, d2 = {"Lcom/eazy/daiku/utility/base/BasePaginationAdapter$BasePaginationViewHolder;", "DATA", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "onBind", "", "data", "listener", "Lkotlin/Function1;", "(Ljava/lang/Object;Lkotlin/jvm/functions/Function1;)V", "app_taxiDebug"})
    public static abstract class BasePaginationViewHolder<DATA extends java.lang.Object> extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        
        public BasePaginationViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public abstract void onBind(DATA data, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super DATA, kotlin.Unit> listener);
    }
}