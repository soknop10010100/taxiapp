package com.eazy.daiku.ui.customer.map;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00fc\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0007\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0013\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u008f\u00012\u00020\u00012\u00020\u0002:\u0002\u008f\u0001B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0016\u0010R\u001a\b\u0012\u0004\u0012\u00020\u00130S2\u0006\u0010T\u001a\u00020\u0007H\u0002J \u0010U\u001a\u00020V2\u0006\u0010W\u001a\u00020\u00072\u0006\u0010X\u001a\u00020\u00132\u0006\u0010Y\u001a\u00020\u0007H\u0002J \u0010Z\u001a\u00020V2\u0006\u0010X\u001a\u00020\u00132\u0006\u0010[\u001a\u00020D2\u0006\u0010\\\u001a\u00020\u0005H\u0002J \u0010]\u001a\u00020V2\u0016\u0010^\u001a\u0012\u0012\u0004\u0012\u00020_0-j\b\u0012\u0004\u0012\u00020_`/H\u0002J\b\u0010`\u001a\u00020VH\u0002J\u0010\u0010a\u001a\u00020V2\u0006\u0010b\u001a\u00020\u001fH\u0002J\b\u0010c\u001a\u00020VH\u0002J\b\u0010d\u001a\u00020VH\u0003J\b\u0010e\u001a\u00020VH\u0002J\b\u0010f\u001a\u00020VH\u0002J\b\u0010g\u001a\u00020VH\u0002J\b\u0010h\u001a\u00020VH\u0002J(\u0010i\u001a\u00020\u001f2\u0006\u0010C\u001a\u00020D2\u0006\u0010j\u001a\u00020D2\u0006\u0010E\u001a\u00020D2\u0006\u0010k\u001a\u00020DH\u0002J\u0010\u0010l\u001a\u00020V2\u0006\u0010m\u001a\u00020\u0007H\u0002J \u0010n\u001a\u00020V2\u0006\u0010W\u001a\u00020\u00072\u0006\u0010X\u001a\u00020\u00132\u0006\u0010Y\u001a\u00020\u0007H\u0002J\u0010\u0010o\u001a\u00020V2\u0006\u0010X\u001a\u00020\u0013H\u0002J\u0018\u0010p\u001a\u00020V2\u0006\u0010X\u001a\u00020\u00132\u0006\u0010b\u001a\u00020\u001fH\u0002J\u0012\u0010q\u001a\u00020V2\b\u0010r\u001a\u0004\u0018\u00010sH\u0014J\u0012\u0010t\u001a\u00020\u001f2\b\u0010u\u001a\u0004\u0018\u00010vH\u0016J\b\u0010w\u001a\u00020VH\u0014J\u0012\u0010x\u001a\u00020V2\b\u0010y\u001a\u0004\u0018\u000103H\u0016J\u0010\u0010z\u001a\u00020\u001f2\u0006\u0010{\u001a\u00020|H\u0016J\b\u0010}\u001a\u00020VH\u0014J\b\u0010~\u001a\u00020VH\u0014J\b\u0010\u007f\u001a\u00020VH\u0002J\t\u0010\u0080\u0001\u001a\u00020VH\u0002J+\u0010\u0081\u0001\u001a\u00020V2\u0006\u0010)\u001a\u00020\u00132\u0006\u0010*\u001a\u00020\u00132\u0007\u0010\u0082\u0001\u001a\u00020\u00072\u0007\u0010\u0083\u0001\u001a\u00020\u0007H\u0002J\t\u0010\u0084\u0001\u001a\u00020VH\u0002J\u0013\u0010\u0085\u0001\u001a\u00020V2\b\u0010\u0086\u0001\u001a\u00030\u0087\u0001H\u0002J+\u0010\u0088\u0001\u001a\u00020V*\u00030\u0089\u00012\b\u0010\u008a\u0001\u001a\u00030\u008b\u00012\b\u0010\u008c\u0001\u001a\u00030\u008d\u00012\u0007\u0010\u008e\u0001\u001a\u00020\u0005H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\n\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u0016\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0018R\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0018R\u000e\u0010\u001a\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001b\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010!\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\"\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010%\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010&\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010(\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010)\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010*\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010+\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010,\u001a\u0016\u0012\u0004\u0012\u00020.\u0018\u00010-j\n\u0012\u0004\u0012\u00020.\u0018\u0001`/X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00100\u001a\u0004\u0018\u000101X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00102\u001a\u0004\u0018\u000103X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00104\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00105\u001a\u0004\u0018\u000106X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00107\u001a\u0004\u0018\u000106X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u00108\u001a\u0004\u0018\u000106X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u00109\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u0002060:X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010;\u001a\u00020<X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010=\u001a\u00020\u001fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010>\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010?\u001a\u0004\u0018\u00010@X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010A\u001a\u00020BX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010C\u001a\u00020DX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010E\u001a\u00020DX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010F\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0018R\u0012\u0010G\u001a\u0004\u0018\u00010\u0017X\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0018R\u000e\u0010H\u001a\u00020IX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010J\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010K\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010L\u001a\u00020M8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\bP\u0010Q\u001a\u0004\bN\u0010O\u00a8\u0006\u0090\u0001"}, d2 = {"Lcom/eazy/daiku/ui/customer/map/PickUpLocationActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "()V", "CLICK_ACTION_THRESHOLD", "", "address", "", "addressIndex", "Landroid/location/Address;", "addressPointA", "addressPointB", "binding", "Lcom/eazy/daiku/databinding/ActivityPickUpLocationBinding;", "bookingPreviewDoCheckoutModel", "Lcom/eazy/daiku/data/model/server_model/BookingPreviewDoCheckoutModel;", "carBookingModel", "Lcom/eazy/daiku/data/model/server_model/Vehicle;", "currentLatLng", "Lcom/google/android/gms/maps/model/LatLng;", "currentLatLngA", "currentLatLngB", "currentLatitude", "", "Ljava/lang/Double;", "currentLongtitude", "descriptionFromSearch", "fromTitle", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "hasAddressA", "", "hasAddressB", "hasPointOnMapA", "hasPointOnMapB", "intentFilter", "Landroid/content/IntentFilter;", "isCanNotBack", "isNightTheme", "isRotateGesture", "isShowTraffic", "latLngA", "latLngB", "latLngMarkerClick", "listMarkerOption", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/MarkerOption;", "Lkotlin/collections/ArrayList;", "locationKioskNearBy", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "mapScreenShotPath", "marker", "Lcom/google/android/gms/maps/model/Marker;", "markerA", "markerB", "markerHasMap", "Ljava/util/HashMap;", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "needReloadData", "outTradeNo", "previewCheckoutModel", "Lcom/eazy/daiku/ui/customer/model/PreviewCheckoutModel;", "selectCarBookingAdapter", "Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment$SelectCarBookingAdapter;", "startX", "", "startY", "taxiLatitude", "taxiLongtitude", "timer", "Landroid/os/CountDownTimer;", "titleFromSearch", "toTitle", "viewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "getViewModel", "()Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "decodePoly", "", "encoded", "drawMarker", "", "titleMarker", "latLng", "title", "drawMarkerTaxi", "gpsHeading", "driverId", "drawMarkerTaxiWithLatLng", "driver", "Lcom/eazy/daiku/data/model/server_model/Drivers;", "enableBottonBookNow", "fetchCurrentLocation", "isSetPointOnMap", "getApiMap", "initAction", "initGoogleMap", "initObserver", "initRecyclerview", "initView", "isClickShimmer", "endX", "endY", "mapScreenShot", "previewCheckout", "moveCameraWhenSearch", "moveLocation", "moveToCurrentLocation", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateOptionsMenu", "menu", "Landroid/view/Menu;", "onDestroy", "onMapReady", "googleMap", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onResume", "onStart", "openMapSettings", "showAllMarker", "submitBookingPreviewDoCheckout", "titleA", "titleB", "submitCheckout", "submitCheckoutTaxi", "terms", "Lcom/eazy/daiku/ui/customer/model/Terms;", "writeBitmap", "Ljava/io/File;", "bitmap", "Landroid/graphics/Bitmap;", "format", "Landroid/graphics/Bitmap$CompressFormat;", "quality", "Companion", "app_taxiDebug"})
public final class PickUpLocationActivity extends com.eazy.daiku.utility.base.BaseActivity implements com.google.android.gms.maps.OnMapReadyCallback {
    private com.eazy.daiku.databinding.ActivityPickUpLocationBinding binding;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.google.android.gms.maps.GoogleMap mMap;
    private boolean needReloadData = false;
    private com.google.android.gms.maps.model.LatLng currentLatLng;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private java.lang.Double currentLatitude;
    private java.lang.Double currentLongtitude;
    private java.lang.Double taxiLatitude;
    private java.lang.Double taxiLongtitude;
    private com.google.android.gms.maps.model.Marker marker;
    private com.eazy.daiku.ui.customer.model.ListKioskModel locationKioskNearBy;
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.MarkerOption> listMarkerOption;
    private final android.content.IntentFilter intentFilter = null;
    private com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment.SelectCarBookingAdapter selectCarBookingAdapter;
    private android.location.Address addressIndex;
    private java.lang.String mapScreenShotPath;
    private java.lang.String address;
    private com.google.android.gms.maps.model.LatLng latLngMarkerClick;
    private com.eazy.daiku.data.model.server_model.Vehicle carBookingModel;
    private java.lang.String titleFromSearch = "";
    private java.lang.String descriptionFromSearch = "";
    private com.eazy.daiku.ui.customer.model.PreviewCheckoutModel previewCheckoutModel;
    private com.eazy.daiku.data.model.server_model.BookingPreviewDoCheckoutModel bookingPreviewDoCheckoutModel;
    private com.google.android.gms.maps.model.Marker markerA;
    private com.google.android.gms.maps.model.Marker markerB;
    private boolean hasPointOnMapA = false;
    private boolean hasPointOnMapB = false;
    private com.google.android.gms.maps.model.LatLng latLngA;
    private com.google.android.gms.maps.model.LatLng latLngB;
    private com.google.android.gms.maps.model.LatLng currentLatLngA;
    private com.google.android.gms.maps.model.LatLng currentLatLngB;
    private boolean hasAddressA = false;
    private boolean hasAddressB = false;
    private java.lang.String addressPointA;
    private java.lang.String addressPointB;
    private float startX = 0.0F;
    private float startY = 0.0F;
    private final int CLICK_ACTION_THRESHOLD = 200;
    private final java.util.HashMap<java.lang.String, com.google.android.gms.maps.model.Marker> markerHasMap = null;
    private java.lang.String fromTitle = "";
    private java.lang.String toTitle = "";
    private java.lang.String outTradeNo;
    private boolean isCanNotBack = false;
    private android.os.CountDownTimer timer;
    private boolean isShowTraffic = false;
    private boolean isRotateGesture = false;
    private boolean isNightTheme = false;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.map.PickUpLocationActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String searchLocationKioskNearByKey = "searchLocationKioskNearByKey";
    private com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver;
    
    public PickUpLocationActivity() {
        super();
    }
    
    private final com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initObserver() {
    }
    
    @android.annotation.SuppressLint(value = {"ClickableViewAccessibility"})
    private final void initAction() {
    }
    
    private final void drawMarkerTaxiWithLatLng(java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> driver) {
    }
    
    private final boolean isClickShimmer(float startX, float endX, float startY, float endY) {
        return false;
    }
    
    private final void mapScreenShot(java.lang.String previewCheckout) {
    }
    
    private final void writeBitmap(java.io.File $this$writeBitmap, android.graphics.Bitmap bitmap, android.graphics.Bitmap.CompressFormat format, int quality) {
    }
    
    private final void submitCheckoutTaxi(com.eazy.daiku.ui.customer.model.Terms terms) {
    }
    
    private final void openMapSettings() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public boolean onCreateOptionsMenu(@org.jetbrains.annotations.Nullable()
    android.view.Menu menu) {
        return false;
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap googleMap) {
    }
    
    private final void initGoogleMap() {
    }
    
    private final void initRecyclerview() {
    }
    
    private final void fetchCurrentLocation(boolean isSetPointOnMap) {
    }
    
    private final void moveToCurrentLocation(com.google.android.gms.maps.model.LatLng latLng, boolean isSetPointOnMap) {
    }
    
    private final void moveCameraWhenSearch(java.lang.String titleMarker, com.google.android.gms.maps.model.LatLng latLng, java.lang.String title) {
    }
    
    private final void drawMarker(java.lang.String titleMarker, com.google.android.gms.maps.model.LatLng latLng, java.lang.String title) {
    }
    
    private final void getApiMap() {
    }
    
    private final java.util.List<com.google.android.gms.maps.model.LatLng> decodePoly(java.lang.String encoded) {
        return null;
    }
    
    private final void drawMarkerTaxi(com.google.android.gms.maps.model.LatLng latLng, float gpsHeading, int driverId) {
    }
    
    private final void moveLocation(com.google.android.gms.maps.model.LatLng latLng) {
    }
    
    private final void showAllMarker() {
    }
    
    private final void submitCheckout() {
    }
    
    private final void submitBookingPreviewDoCheckout(com.google.android.gms.maps.model.LatLng latLngA, com.google.android.gms.maps.model.LatLng latLngB, java.lang.String titleA, java.lang.String titleB) {
    }
    
    private final void enableBottonBookNow() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/customer/map/PickUpLocationActivity$Companion;", "", "()V", "searchLocationKioskNearByKey", "", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}