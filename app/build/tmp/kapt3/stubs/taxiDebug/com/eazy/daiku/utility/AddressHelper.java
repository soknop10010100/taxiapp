package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J)\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\nJ+\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\b\u00a2\u0006\u0002\u0010\r\u00a8\u0006\u000e"}, d2 = {"Lcom/eazy/daiku/utility/AddressHelper;", "", "()V", "getStringAddress", "", "context", "Landroid/content/Context;", "latitude", "", "longitude", "(Landroid/content/Context;Ljava/lang/Double;Ljava/lang/Double;)Ljava/lang/String;", "getStringAddressByIndex", "Landroid/location/Address;", "(Landroid/content/Context;Ljava/lang/Double;Ljava/lang/Double;)Landroid/location/Address;", "app_taxiDebug"})
public final class AddressHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.AddressHelper INSTANCE = null;
    
    private AddressHelper() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStringAddress(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.Double latitude, @org.jetbrains.annotations.Nullable()
    java.lang.Double longitude) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.location.Address getStringAddressByIndex(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.Double latitude, @org.jetbrains.annotations.Nullable()
    java.lang.Double longitude) {
        return null;
    }
}