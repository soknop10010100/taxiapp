package com.eazy.daiku.utility.redirect;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b&\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\t\u001a\u00020\nJ\u001e\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\nJ&\u0010\b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\nJ\u0018\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u000f\u001a\u0004\u0018\u00010\fJ\u001c\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J\u000e\u0010\u0014\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J2\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\"\u0010\u0017\u001a\u001e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00190\u0018j\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0019`\u001aJN\u0010\u001b\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\f2\u0006\u0010\u001d\u001a\u00020\f2\u0006\u0010\u001e\u001a\u00020\f2\u0006\u0010\u001f\u001a\u00020\f2\u0006\u0010 \u001a\u00020\f2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\f2\u0006\u0010$\u001a\u00020\fJ\u0016\u0010%\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\fJ\u000e\u0010\'\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010(\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010)\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J \u0010*\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\f2\b\b\u0002\u0010+\u001a\u00020,J\u0016\u0010-\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010.\u001a\u00020\fJ&\u0010/\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u001c\u001a\u0004\u0018\u00010\f2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J\u0016\u00100\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u00101\u001a\u00020\fJ^\u00102\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u00103\u001a\u00020\f2\u0006\u00104\u001a\u00020\f2\u0006\u00105\u001a\u00020\f2\u0006\u00106\u001a\u00020\f2\u0006\u00107\u001a\u00020\f2\u0006\u00108\u001a\u00020\f2\u0006\u00109\u001a\u00020\f2\u0006\u0010:\u001a\u00020\f2\u0006\u0010;\u001a\u00020\f2\u0006\u0010<\u001a\u00020\fJ\u000e\u0010=\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010>\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u001c\u0010?\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J\u000e\u0010@\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010A\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010B\u001a\u00020,J@\u0010A\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\"\u0010C\u001a\u001e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00190\u0018j\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0019`\u001a2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012Jh\u0010A\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\"\u0010C\u001a\u001e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u00190\u0018j\u000e\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0019`\u001a2&\u0010D\u001a\"\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0019\u0018\u00010\u0018j\u0010\u0012\u0004\u0012\u00020\f\u0012\u0004\u0012\u00020\u0019\u0018\u0001`\u001a2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J(\u0010E\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\b\u0002\u0010F\u001a\u00020,2\u0006\u0010\t\u001a\u00020\n2\u0006\u0010G\u001a\u00020\fJ\u001e\u0010E\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010H\u001a\u00020\f2\u0006\u0010\t\u001a\u00020\nJ\u0016\u0010I\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\fJ$\u0010J\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010&\u001a\u00020\f2\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012J\u0016\u0010K\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010L\u001a\u00020\fJ\u001e\u0010M\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\f2\u0006\u0010N\u001a\u00020\fJ\u001e\u0010O\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\f2\u0006\u0010P\u001a\u00020\fJ\u0016\u0010Q\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010R\u001a\u00020S\u00a8\u0006T"}, d2 = {"Lcom/eazy/daiku/utility/redirect/RedirectClass;", "Lcom/eazy/daiku/utility/redirect/BaseRedirect;", "()V", "gotSampleActivity", "", "activity", "Landroid/app/Activity;", "gotoAboutUsActivity", "gotoChangeNewPasswordActivity", "verifyPinEnum", "Lcom/eazy/daiku/utility/enumerable/VerifyPinEnum;", "pinCode", "", "otpToken", "gotoCustomerMapPreView", "listKioskJsonData", "gotoEditProfileActivity", "activityResult", "Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;", "Landroidx/activity/result/ActivityResult;", "gotoForgetPwdActivity", "gotoHistoryTripActivity", "gotoIdentityVerificationActivity", "kycHm", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "gotoListTaxiDriverActivity", "jsonData", "lat", "long", "address", "pathMapScreenShotPath", "addressByIndex", "Landroid/location/Address;", "descriptionFromSearch", "titleFromSearch", "gotoLocationKioskBookingTaxi", "dataJson", "gotoLoginActivity", "gotoMainActivity", "gotoMainWalletActivity", "gotoMapPreviewActivity", "isPreview", "", "gotoPaymentCompleteActivity", "qrCodeUrl", "gotoPickUpLocation", "gotoPlayStore", "applicationId", "gotoPreviewCheckoutActivity", "dataJsonPreview", "carId", "deviceId", "pathMapScreenShot", "adminArea", "countryName", "getAddressLine", "lattitude", "longtitude", "vechicle", "gotoProfileActivity", "gotoScanQRCodeActivity", "gotoSearchMapActivity", "gotoSignUpActivity", "gotoUploadDocsActivity", "reUploadKyc", "bodyHm", "kycDocTemporary", "gotoVerificationPinActivity", "hasVerifyBiometric", "bookingCode", "title", "gotoVerifyOtpCodeActivity", "gotoWebPayActivity", "gotoWebViewGoogleMapActivity", "googleMapData", "gotoWithdrawMoneyActivity", "jsonDataListBank", "gotoWithdrawMoneyWebViewActivity", "verifyOtpUrl", "openDeepLink", "uri", "Landroid/net/Uri;", "app_taxiDebug"})
public final class RedirectClass extends com.eazy.daiku.utility.redirect.BaseRedirect {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.redirect.RedirectClass INSTANCE = null;
    
    private RedirectClass() {
        super();
    }
    
    public final void gotoLoginActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoMainActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoProfileActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoEditProfileActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    public final void gotoSignUpActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoUploadDocsActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyHm, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    public final void gotoUploadDocsActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyHm, @org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.String, java.lang.Object> kycDocTemporary, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    public final void gotoUploadDocsActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, boolean reUploadKyc) {
    }
    
    public final void gotoScanQRCodeActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoMapPreviewActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonData, boolean isPreview) {
    }
    
    public final void gotoCustomerMapPreView(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.Nullable()
    java.lang.String listKioskJsonData) {
    }
    
    public final void gotoLocationKioskBookingTaxi(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String dataJson) {
    }
    
    public final void gotoListTaxiDriverActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonData, @org.jetbrains.annotations.NotNull()
    java.lang.String lat, @org.jetbrains.annotations.NotNull()
    java.lang.String p3_1663806, @org.jetbrains.annotations.NotNull()
    java.lang.String address, @org.jetbrains.annotations.NotNull()
    java.lang.String pathMapScreenShotPath, @org.jetbrains.annotations.NotNull()
    android.location.Address addressByIndex, @org.jetbrains.annotations.NotNull()
    java.lang.String descriptionFromSearch, @org.jetbrains.annotations.NotNull()
    java.lang.String titleFromSearch) {
    }
    
    public final void gotoSearchMapActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    public final void gotoPickUpLocation(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.Nullable()
    java.lang.String jsonData, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    public final void gotoChangeNewPasswordActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String pinCode, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum) {
    }
    
    public final void gotoChangeNewPasswordActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String pinCode, @org.jetbrains.annotations.NotNull()
    java.lang.String otpToken, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum) {
    }
    
    public final void gotoChangeNewPasswordActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum) {
    }
    
    public final void gotoVerifyOtpCodeActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String otpToken) {
    }
    
    public final void gotoVerificationPinActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String title, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum) {
    }
    
    public final void gotoVerificationPinActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, boolean hasVerifyBiometric, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum, @org.jetbrains.annotations.NotNull()
    java.lang.String bookingCode) {
    }
    
    public final void gotoHistoryTripActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoMainWalletActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoWebViewGoogleMapActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String googleMapData) {
    }
    
    public final void gotoIdentityVerificationActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> kycHm) {
    }
    
    public final void gotoWithdrawMoneyActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonData, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonDataListBank) {
    }
    
    public final void gotoWithdrawMoneyWebViewActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonData, @org.jetbrains.annotations.NotNull()
    java.lang.String verifyOtpUrl) {
    }
    
    public final void gotoForgetPwdActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotoPreviewCheckoutActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String dataJsonPreview, @org.jetbrains.annotations.NotNull()
    java.lang.String carId, @org.jetbrains.annotations.NotNull()
    java.lang.String deviceId, @org.jetbrains.annotations.NotNull()
    java.lang.String pathMapScreenShot, @org.jetbrains.annotations.NotNull()
    java.lang.String adminArea, @org.jetbrains.annotations.NotNull()
    java.lang.String countryName, @org.jetbrains.annotations.NotNull()
    java.lang.String getAddressLine, @org.jetbrains.annotations.NotNull()
    java.lang.String lattitude, @org.jetbrains.annotations.NotNull()
    java.lang.String longtitude, @org.jetbrains.annotations.NotNull()
    java.lang.String vechicle) {
    }
    
    public final void gotoWebPayActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String dataJson, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    public final void openDeepLink(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.net.Uri uri) {
    }
    
    public final void gotoPlayStore(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String applicationId) {
    }
    
    public final void gotoPaymentCompleteActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    java.lang.String qrCodeUrl) {
    }
    
    public final void gotoAboutUsActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
    
    public final void gotSampleActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
    }
}