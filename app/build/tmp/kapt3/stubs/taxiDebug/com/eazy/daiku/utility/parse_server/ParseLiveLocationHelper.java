package com.eazy.daiku.utility.parse_server;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0007\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u0000 >2\u00020\u0001:\u0001>B\u0005\u00a2\u0006\u0002\u0010\u0002JN\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001bJ\u0006\u0010\u0012\u001a\u00020\u0004J\f\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00110\u001fJ\u001c\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010 \u001a\u00020\u0004J\u0014\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010\u0013\u001a\u00020\u0014J\u001c\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010#\u001a\u00020\u0004J\u0014\u0010$\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010\u0013\u001a\u00020\u0014J\u0014\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010\u0013\u001a\u00020\u0014J\u0014\u0010&\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010\u0013\u001a\u00020\u0014J\u0014\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00110\u001f2\u0006\u0010(\u001a\u00020\u0004J\u0006\u0010)\u001a\u00020\u0004J\u0010\u0010*\u001a\u0004\u0018\u00010+2\u0006\u0010,\u001a\u00020-Jf\u0010.\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u0002012\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001b2\u0006\u00102\u001a\u00020\u00042\u0006\u00103\u001a\u00020\u00042\u0006\u00104\u001a\u00020\u00042\u0006\u00105\u001a\u00020\u0004J^\u00106\u001a\u00020\u00112\u0006\u00107\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u0002012\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001b2\u0006\u00108\u001a\u00020\u0017JV\u00106\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u00042\u0006\u0010\u0013\u001a\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010/\u001a\u00020\u00042\u0006\u00100\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u0002012\u0006\u0010\u001c\u001a\u00020\u001b2\u0006\u0010\u001d\u001a\u00020\u001b2\u0006\u00108\u001a\u00020\u0017J\u0016\u00109\u001a\u00020:2\u0006\u0010,\u001a\u00020-2\u0006\u00102\u001a\u00020\u0004J\u0018\u0010;\u001a\u00020:2\u0006\u0010,\u001a\u00020-2\b\u0010<\u001a\u0004\u0018\u00010\u000bJ\u000e\u0010\u0015\u001a\u00020\u00042\u0006\u0010=\u001a\u00020+R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0006\u001a\n \b*\u0004\u0018\u00010\u00070\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006?"}, d2 = {"Lcom/eazy/daiku/utility/parse_server/ParseLiveLocationHelper;", "", "()V", "appServer", "", "appType", "currentCalendar", "Ljava/util/Calendar;", "kotlin.jvm.PlatformType", "deviceType", "lastLocation", "Landroid/location/Location;", "getLastLocation", "()Landroid/location/Location;", "setLastLocation", "(Landroid/location/Location;)V", "addAttendanceUser", "Lcom/parse/ParseObject;", "appInfo", "userId", "", "userInfo", "isEmployee", "", "loginDate", "loginTime", "gpsHeading", "", "latitude", "longtitude", "checkAssignTaxiIsSelf", "Lcom/parse/ParseQuery;", "status", "checkAttendanceUser", "checkCodeTaxiIsSelf", "code", "checkLiveMapUserParseServer", "checkLiveUserCustomerParseServer", "checkLiveUserParseServer", "checkStatusAssignTaxi", "outTradeNo", "deviceInfo", "getUser", "Lcom/eazy/daiku/data/model/server_model/User;", "context", "Landroid/content/Context;", "liveMapUserParseServer", "lastUpdatedDate", "lastUpdatedTime", "", "bookingCode", "startTripDate", "startTripTime", "destinationInfo", "liveUserParseServer", "parseObject", "displayOnMap", "liveUserUpdateBookingCode", "", "submitLocationParseServer", "location", "user", "Companion", "app_taxiDebug"})
public final class ParseLiveLocationHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.parse_server.ParseLiveLocationHelper.Companion Companion = null;
    private static com.eazy.daiku.utility.parse_server.ParseLiveLocationHelper ourInstance;
    private java.lang.String appServer;
    private java.lang.String appType;
    private java.lang.String deviceType = "android";
    private java.util.Calendar currentCalendar;
    @org.jetbrains.annotations.Nullable()
    private android.location.Location lastLocation;
    
    public ParseLiveLocationHelper() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final com.eazy.daiku.utility.parse_server.ParseLiveLocationHelper getNewInstance() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.location.Location getLastLocation() {
        return null;
    }
    
    public final void setLastLocation(@org.jetbrains.annotations.Nullable()
    android.location.Location p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String deviceInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.User getUser(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String userInfo(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.server_model.User user) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String appInfo() {
        return null;
    }
    
    public final void submitLocationParseServer(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.location.Location location) {
    }
    
    /**
     * live attendance
     */
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseObject addAttendanceUser(@org.jetbrains.annotations.NotNull()
    java.lang.String appInfo, int userId, @org.jetbrains.annotations.NotNull()
    java.lang.String userInfo, boolean isEmployee, @org.jetbrains.annotations.NotNull()
    java.lang.String loginDate, @org.jetbrains.annotations.NotNull()
    java.lang.String loginTime, double gpsHeading, double latitude, double longtitude) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkAttendanceUser(int userId) {
        return null;
    }
    
    /**
     * live user to parse server
     */
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkLiveUserParseServer(int userId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkLiveUserCustomerParseServer(int userId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseObject liveUserParseServer(@org.jetbrains.annotations.NotNull()
    java.lang.String appInfo, int userId, @org.jetbrains.annotations.NotNull()
    java.lang.String userInfo, boolean isEmployee, @org.jetbrains.annotations.NotNull()
    java.lang.String lastUpdatedDate, @org.jetbrains.annotations.NotNull()
    java.lang.String lastUpdatedTime, float gpsHeading, double latitude, double longtitude, boolean displayOnMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseObject liveUserParseServer(@org.jetbrains.annotations.NotNull()
    com.parse.ParseObject parseObject, @org.jetbrains.annotations.NotNull()
    java.lang.String appInfo, int userId, @org.jetbrains.annotations.NotNull()
    java.lang.String userInfo, boolean isEmployee, @org.jetbrains.annotations.NotNull()
    java.lang.String lastUpdatedDate, @org.jetbrains.annotations.NotNull()
    java.lang.String lastUpdatedTime, float gpsHeading, double latitude, double longtitude, boolean displayOnMap) {
        return null;
    }
    
    public final void liveUserUpdateBookingCode(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String bookingCode) {
    }
    
    /**
     * live map to parse server
     */
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkLiveMapUserParseServer(int userId) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseObject liveMapUserParseServer(@org.jetbrains.annotations.NotNull()
    java.lang.String appInfo, int userId, @org.jetbrains.annotations.NotNull()
    java.lang.String userInfo, @org.jetbrains.annotations.NotNull()
    java.lang.String lastUpdatedDate, @org.jetbrains.annotations.NotNull()
    java.lang.String lastUpdatedTime, float gpsHeading, double latitude, double longtitude, @org.jetbrains.annotations.NotNull()
    java.lang.String bookingCode, @org.jetbrains.annotations.NotNull()
    java.lang.String startTripDate, @org.jetbrains.annotations.NotNull()
    java.lang.String startTripTime, @org.jetbrains.annotations.NotNull()
    java.lang.String destinationInfo) {
        return null;
    }
    
    /**
     * booking Taxi parse server
     */
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkAssignTaxiIsSelf(int userId, @org.jetbrains.annotations.NotNull()
    java.lang.String status) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkCodeTaxiIsSelf(int userId, @org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkAssignTaxiIsSelf() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.parse.ParseQuery<com.parse.ParseObject> checkStatusAssignTaxi(@org.jetbrains.annotations.NotNull()
    java.lang.String outTradeNo) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u00048FX\u0087\u0004\u00a2\u0006\f\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/parse_server/ParseLiveLocationHelper$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/parse_server/ParseLiveLocationHelper;", "getNewInstance$annotations", "getNewInstance", "()Lcom/eazy/daiku/utility/parse_server/ParseLiveLocationHelper;", "ourInstance", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @kotlin.jvm.JvmStatic()
        @java.lang.Deprecated()
        public static void getNewInstance$annotations() {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.eazy.daiku.utility.parse_server.ParseLiveLocationHelper getNewInstance() {
            return null;
        }
    }
}