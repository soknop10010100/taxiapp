package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\r\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u000e\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0003\u001a\u00020\u0004J\u000e\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0006\u0010\t\u001a\u00020\nJ\u001c\u0010\u000b\u001a\u0004\u0018\u00010\f2\b\u0010\r\u001a\u0004\u0018\u00010\u000e2\b\u0010\u000f\u001a\u0004\u0018\u00010\fJ\u0006\u0010\u0010\u001a\u00020\u0011J\u0016\u0010\u0012\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\fJ\u000e\u0010\u0014\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\u0015\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\fJ\u000e\u0010\u0016\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\u0017\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\u0018\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\fJ\u0016\u0010\u0019\u001a\u00020\f2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\fJ\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\u001c\u001a\u00020\u00062\u0006\u0010\u001d\u001a\u00020\u001eJ\u0010\u0010\u001f\u001a\u00020\u001b2\u0006\u0010\u0007\u001a\u00020\bH\u0002J\u0016\u0010 \u001a\u00020\u001b2\u0006\u0010!\u001a\u00020\f2\u0006\u0010\"\u001a\u00020#J\u0010\u0010$\u001a\u00020\u001b2\b\u0010%\u001a\u0004\u0018\u00010&J\u0016\u0010\'\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010(\u001a\u00020\fJ\u000e\u0010)\u001a\u00020\u001b2\u0006\u0010*\u001a\u00020\fJ\u000e\u0010+\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ,\u0010,\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010-\u001a\b\u0012\u0004\u0012\u00020/0.2\u0006\u00100\u001a\u00020/2\u0006\u00101\u001a\u00020\u0011J$\u00102\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u00103\u001a\b\u0012\u0004\u0012\u0002040.2\u0006\u00105\u001a\u000204J,\u00106\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u00107\u001a\b\u0012\u0004\u0012\u00020/0.2\u0006\u00108\u001a\u00020/2\u0006\u00101\u001a\u00020\u0011J\u0016\u00109\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010:\u001a\u00020\fJ\u0016\u0010;\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010:\u001a\u00020\fJ\u0016\u0010<\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010:\u001a\u00020\fJ\u001e\u0010=\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010>\u001a\u00020\fJ\u001e\u0010?\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010>\u001a\u00020\fJ\u001e\u0010@\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010>\u001a\u00020\fJ\u001e\u0010A\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0013\u001a\u00020\f2\u0006\u0010>\u001a\u00020\fJ \u0010B\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020C2\u0006\u0010D\u001a\u00020\f2\b\b\u0002\u0010E\u001a\u00020\u001bJ\u001e\u0010F\u001a\u00020\u00062\u0006\u0010G\u001a\u00020H2\u0006\u0010:\u001a\u00020&2\u0006\u0010I\u001a\u00020\f\u00a8\u0006J"}, d2 = {"Lcom/eazy/daiku/utility/EazyTaxiHelper;", "", "()V", "KessLogDataGson", "Lcom/eazy/daiku/data/network/CustomHttpLogging;", "clearSession", "", "context", "Landroid/content/Context;", "configurePagedListConfig", "Landroidx/paging/PagedList$Config;", "formatDate", "", "date", "Ljava/util/Date;", "pattern", "getBatteryPercentage", "", "getDeviceTokenPreference", "PREF_NAME", "getFirstBankAccount", "getFirstBankAccountSharePreference", "getSaveListAccountNumber", "getSearchHistoryMap", "getSearchHistoryMapSharePreference", "getSharePreference", "hasNotNetworkAvailable", "", "hideKeyboard", "view", "Landroid/view/View;", "isNetworkAvailable", "isPackageInstalled", "packageName", "packageManager", "Landroid/content/pm/PackageManager;", "isValidEmail", "target", "", "makeCall", "number", "phoneNumberValidate", "phone", "removeFirstBankAccount", "removeListBankAccount", "listBankAccount", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "saveBankAccount", "userId", "removeListSearchHistoryMap", "listSearchHistoryMap", "Lcom/eazy/daiku/ui/customer/model/SearchMapHistoryModel;", "searchMapHistoryModel", "removeListSharePreference", "bankList", "bankAccount", "saveFirstBankAccount", "data", "saveListBankAccountNumber", "saveListSearchHistoryMap", "setDeviceTokenPreference", "VALUE", "setFirstBankAccountSharePreference", "setListSearchHistoryMapSharePreference", "setSharePreference", "setUpTitleAppBar", "Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "appBarTitle", "showSymbolBack", "validateField", "fieldLayout", "Lcom/google/android/material/textfield/TextInputLayout;", "msgError", "app_taxiDebug"})
public final class EazyTaxiHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.EazyTaxiHelper INSTANCE = null;
    
    private EazyTaxiHelper() {
        super();
    }
    
    public final void setUpTitleAppBar(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseCoreActivity context, @org.jetbrains.annotations.NotNull()
    java.lang.String appBarTitle, boolean showSymbolBack) {
    }
    
    public final void validateField(@org.jetbrains.annotations.NotNull()
    com.google.android.material.textfield.TextInputLayout fieldLayout, @org.jetbrains.annotations.NotNull()
    java.lang.CharSequence data, @org.jetbrains.annotations.NotNull()
    java.lang.String msgError) {
    }
    
    public final void makeCall(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String number) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.paging.PagedList.Config configurePagedListConfig() {
        return null;
    }
    
    public final boolean isPackageInstalled(@org.jetbrains.annotations.NotNull()
    java.lang.String packageName, @org.jetbrains.annotations.NotNull()
    android.content.pm.PackageManager packageManager) {
        return false;
    }
    
    public final boolean phoneNumberValidate(@org.jetbrains.annotations.NotNull()
    java.lang.String phone) {
        return false;
    }
    
    public final void setSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME, @org.jetbrains.annotations.NotNull()
    java.lang.String VALUE) {
    }
    
    public final void setDeviceTokenPreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME, @org.jetbrains.annotations.NotNull()
    java.lang.String VALUE) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDeviceTokenPreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME) {
        return null;
    }
    
    public final void removeListSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> bankList, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.SaveBankAccount bankAccount, int userId) {
    }
    
    public final void setFirstBankAccountSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME, @org.jetbrains.annotations.NotNull()
    java.lang.String VALUE) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFirstBankAccountSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME) {
        return null;
    }
    
    public final void removeFirstBankAccount(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void setListSearchHistoryMapSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME, @org.jetbrains.annotations.NotNull()
    java.lang.String VALUE) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSearchHistoryMapSharePreference(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String PREF_NAME) {
        return null;
    }
    
    private final boolean isNetworkAvailable(android.content.Context context) {
        return false;
    }
    
    public final boolean hasNotNetworkAvailable(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final void clearSession(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String formatDate(@org.jetbrains.annotations.Nullable()
    java.util.Date date, @org.jetbrains.annotations.Nullable()
    java.lang.String pattern) {
        return null;
    }
    
    public final boolean isValidEmail(@org.jetbrains.annotations.Nullable()
    java.lang.CharSequence target) {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.network.CustomHttpLogging KessLogDataGson() {
        return null;
    }
    
    public final void saveListBankAccountNumber(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSaveListAccountNumber(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    public final void removeListBankAccount(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> listBankAccount, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.SaveBankAccount saveBankAccount, int userId) {
    }
    
    public final void saveFirstBankAccount(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFirstBankAccount(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    public final void saveListSearchHistoryMap(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String data) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSearchHistoryMap(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    public final void removeListSearchHistoryMap(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel> listSearchHistoryMap, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.model.SearchMapHistoryModel searchMapHistoryModel) {
    }
    
    public final void hideKeyboard(@org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
    
    public final int getBatteryPercentage() {
        return 0;
    }
}