package com.eazy.daiku.ui.map;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 ;2\u00020\u00012\u00020\u0002:\u0001;B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0016\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020 0\u001f2\u0006\u0010!\u001a\u00020\u000bH\u0002J\b\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020#H\u0002J\b\u0010%\u001a\u00020#H\u0002J\b\u0010&\u001a\u00020#H\u0002J\b\u0010\'\u001a\u00020#H\u0002J\b\u0010(\u001a\u00020#H\u0002J\b\u0010)\u001a\u00020#H\u0002J\b\u0010*\u001a\u00020\u0011H\u0002J\b\u0010+\u001a\u00020#H\u0002J\u0012\u0010,\u001a\u00020#2\b\u0010-\u001a\u0004\u0018\u00010.H\u0014J\b\u0010/\u001a\u00020#H\u0014J\u0010\u00100\u001a\u00020#2\u0006\u00101\u001a\u00020\u0015H\u0016J\u0010\u00102\u001a\u00020\u00112\u0006\u00103\u001a\u000204H\u0016J\b\u00105\u001a\u00020#H\u0014J\b\u00106\u001a\u00020#H\u0014J\b\u00107\u001a\u00020#H\u0002J\b\u00108\u001a\u00020#H\u0002J\b\u00109\u001a\u00020#H\u0002J\b\u0010:\u001a\u00020#H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\n\u001a\u00020\u000b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000e\u0010\u000f\u001a\u0004\b\f\u0010\rR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0019\u001a\u00020\u001a8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001d\u0010\u000f\u001a\u0004\b\u001b\u0010\u001c\u00a8\u0006<"}, d2 = {"Lcom/eazy/daiku/ui/map/MapPreviewActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityMapPreviewBinding;", "currentGpsLocation", "Landroid/location/Location;", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "googleMapApplication", "", "getGoogleMapApplication", "()Ljava/lang/String;", "googleMapApplication$delegate", "Lkotlin/Lazy;", "isNightTheme", "", "isRotateGesture", "isShowTraffic", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "needReloadData", "qrCodeRespond", "Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "qrCodeVm", "Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "getQrCodeVm", "()Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "qrCodeVm$delegate", "decodePoly", "", "Lcom/google/android/gms/maps/model/LatLng;", "encoded", "doAction", "", "fetchGpsLocation", "initGoogleMap", "initObserved", "initQrCodeRespond", "initUI", "intiView", "justPreviewOnly", "loadGpsLocationUser", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onMapReady", "googleMap", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onResume", "onStop", "openMapSettings", "redirectToGoogleMap", "startLoadNavigationMap", "updateUIGoogleMap", "Companion", "app_taxiDebug"})
public final class MapPreviewActivity extends com.eazy.daiku.utility.base.BaseActivity implements com.google.android.gms.maps.OnMapReadyCallback {
    private com.eazy.daiku.databinding.ActivityMapPreviewBinding binding;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private com.google.android.gms.maps.GoogleMap mMap;
    private com.eazy.daiku.data.model.server_model.QrCodeRespond qrCodeRespond;
    private android.location.Location currentGpsLocation;
    private final kotlin.Lazy googleMapApplication$delegate = null;
    private final kotlin.Lazy qrCodeVm$delegate = null;
    private boolean needReloadData = false;
    private boolean isShowTraffic = false;
    private boolean isRotateGesture = false;
    private boolean isNightTheme = false;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.map.MapPreviewActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String QrCodeFieldKey = "QrCodeFieldKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PreviewMapOnly = "PreviewMapOnly";
    
    public MapPreviewActivity() {
        super();
    }
    
    private final java.lang.String getGoogleMapApplication() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.QrCodeVm getQrCodeVm() {
        return null;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.NotNull()
    com.google.android.gms.maps.GoogleMap googleMap) {
    }
    
    private final java.util.List<com.google.android.gms.maps.model.LatLng> decodePoly(java.lang.String encoded) {
        return null;
    }
    
    private final void initObserved() {
    }
    
    private final void initQrCodeRespond() {
    }
    
    private final void intiView() {
    }
    
    private final void doAction() {
    }
    
    private final void redirectToGoogleMap() {
    }
    
    private final void startLoadNavigationMap() {
    }
    
    private final void initGoogleMap() {
    }
    
    private final void initUI() {
    }
    
    private final void updateUIGoogleMap() {
    }
    
    private final void fetchGpsLocation() {
    }
    
    private final boolean justPreviewOnly() {
        return false;
    }
    
    private final void loadGpsLocationUser() {
    }
    
    private final void openMapSettings() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/ui/map/MapPreviewActivity$Companion;", "", "()V", "PreviewMapOnly", "", "QrCodeFieldKey", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}