package com.eazy.daiku.ui.customer.step_booking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 (2\u00020\u00012\u00020\u0002:\u0001(B\u0005\u00a2\u0006\u0002\u0010\u0003J\u0012\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0002J\u0018\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0019H\u0002J\b\u0010\u001a\u001a\u00020\u0015H\u0002J\b\u0010\u001b\u001a\u00020\u0015H\u0002J\u0006\u0010\u001c\u001a\u00020\u0015J\u0010\u0010\u001d\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\u0012\u0010\u001e\u001a\u00020\u00152\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\u0012\u0010!\u001a\u00020\u00152\b\u0010\"\u001a\u0004\u0018\u00010\rH\u0016J\u0010\u0010#\u001a\u00020$2\u0006\u0010%\u001a\u00020&H\u0016J\b\u0010\'\u001a\u00020\u0015H\u0002R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\"\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\n\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\n\u0018\u0001`\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/LocationKioskBookingTaxiActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityLocationKioskBookingTaxiBinding;", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "listLocationKiosk", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "Lkotlin/collections/ArrayList;", "mMap", "Lcom/google/android/gms/maps/GoogleMap;", "marker", "Lcom/google/android/gms/maps/model/Marker;", "bitmapDescriptorFromVector", "Lcom/google/android/gms/maps/model/BitmapDescriptor;", "vectorResId", "", "drawMarkerTaxi", "", "latLng", "Lcom/google/android/gms/maps/model/LatLng;", "location", "", "fetchCurrentLocation", "initGoogleMap", "initView", "moveToCurrentLocation", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onMapReady", "googleMap", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "showAllMarker", "Companion", "app_taxiDebug"})
public final class LocationKioskBookingTaxiActivity extends com.eazy.daiku.utility.base.BaseActivity implements com.google.android.gms.maps.OnMapReadyCallback {
    private com.eazy.daiku.databinding.ActivityLocationKioskBookingTaxiBinding binding;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private com.google.android.gms.maps.GoogleMap mMap;
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.ListKioskModel> listLocationKiosk;
    private com.google.android.gms.maps.model.Marker marker;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.step_booking.LocationKioskBookingTaxiActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String dataListLocationKioskKey = "dataListLocationKioskKey";
    
    public LocationKioskBookingTaxiActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initView() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap googleMap) {
    }
    
    private final void initGoogleMap() {
    }
    
    private final void fetchCurrentLocation() {
    }
    
    private final void moveToCurrentLocation(com.google.android.gms.maps.model.LatLng latLng) {
    }
    
    private final void showAllMarker() {
    }
    
    private final void drawMarkerTaxi(com.google.android.gms.maps.model.LatLng latLng, java.lang.String location) {
    }
    
    private final com.google.android.gms.maps.model.BitmapDescriptor bitmapDescriptorFromVector(int vectorResId) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/LocationKioskBookingTaxiActivity$Companion;", "", "()V", "dataListLocationKioskKey", "", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}