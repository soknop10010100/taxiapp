package com.eazy.daiku.utility.pagin.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000l\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B)\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0002\u0010\fJ*\u0010\u001f\u001a\u00020 2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020\"2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030$H\u0016J*\u0010%\u001a\u00020 2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020\"2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030$H\u0016J*\u0010&\u001a\u00020 2\f\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00020\'2\u0012\u0010#\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030(H\u0016R \u0010\r\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00100\u000f0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R#\u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00030\u00100\u000f0\u00168F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u00020\u00168F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0018R\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u00130\u00168F\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u0018R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u00130\u00168F\u00a2\u0006\u0006\u001a\u0004\b\u001e\u0010\u0018R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006)"}, d2 = {"Lcom/eazy/daiku/utility/pagin/history/HistoryPaginDataSource;", "Landroidx/paging/PageKeyedDataSource;", "", "Lcom/eazy/daiku/data/model/server_model/History;", "context", "Landroid/content/Context;", "baseViewModel", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "queryKey", "Ljava/util/HashMap;", "", "", "(Landroid/content/Context;Lcom/eazy/daiku/utility/base/BaseViewModel;Ljava/util/HashMap;)V", "_errorPaginationMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "", "_hasHistoryListsMutableLiveData", "_loadingMutableLiveData", "", "_loadingPaginationMutableLiveData", "errorPaginationMutableLiveData", "Landroidx/lifecycle/LiveData;", "getErrorPaginationMutableLiveData", "()Landroidx/lifecycle/LiveData;", "hasHistoryListsMutableLiveData", "getHasHistoryListsMutableLiveData", "loadingMutableLiveData", "getLoadingMutableLiveData", "loadingPaginationMutableLiveData", "getLoadingPaginationMutableLiveData", "loadAfter", "", "params", "Landroidx/paging/PageKeyedDataSource$LoadParams;", "callback", "Landroidx/paging/PageKeyedDataSource$LoadCallback;", "loadBefore", "loadInitial", "Landroidx/paging/PageKeyedDataSource$LoadInitialParams;", "Landroidx/paging/PageKeyedDataSource$LoadInitialCallback;", "app_taxiDebug"})
public final class HistoryPaginDataSource extends androidx.paging.PageKeyedDataSource<java.lang.Integer, com.eazy.daiku.data.model.server_model.History> {
    private android.content.Context context;
    private com.eazy.daiku.utility.base.BaseViewModel baseViewModel;
    private java.util.HashMap<java.lang.String, java.lang.Object> queryKey;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingMutableLiveData;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingPaginationMutableLiveData;
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> _hasHistoryListsMutableLiveData;
    private androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.History>>> _errorPaginationMutableLiveData;
    
    public HistoryPaginDataSource(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseViewModel baseViewModel, @org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> queryKey) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getLoadingMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getLoadingPaginationMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getHasHistoryListsMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.History>>> getErrorPaginationMutableLiveData() {
        return null;
    }
    
    @java.lang.Override()
    public void loadInitial(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadInitialParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadInitialCallback<java.lang.Integer, com.eazy.daiku.data.model.server_model.History> callback) {
    }
    
    @java.lang.Override()
    public void loadBefore(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadCallback<java.lang.Integer, com.eazy.daiku.data.model.server_model.History> callback) {
    }
    
    @java.lang.Override()
    public void loadAfter(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadCallback<java.lang.Integer, com.eazy.daiku.data.model.server_model.History> callback) {
    }
}