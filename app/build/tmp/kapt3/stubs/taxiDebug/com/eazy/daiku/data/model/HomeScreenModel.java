package com.eazy.daiku.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\u0018\u00002\u00020\u0001B/\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0003\u0012\b\b\u0002\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u00a2\u0006\u0002\u0010\u000bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u000fR\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000fR\u001a\u0010\u0007\u001a\u00020\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\u0011\"\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0015\u00a8\u0006\u0016"}, d2 = {"Lcom/eazy/daiku/data/model/HomeScreenModel;", "", "id", "", "name", "", "icon", "isEnable", "", "actionEnum", "Lcom/eazy/daiku/utility/enumerable/HomeScreenActionEnum;", "(ILjava/lang/String;IZLcom/eazy/daiku/utility/enumerable/HomeScreenActionEnum;)V", "getActionEnum", "()Lcom/eazy/daiku/utility/enumerable/HomeScreenActionEnum;", "getIcon", "()I", "getId", "()Z", "setEnable", "(Z)V", "getName", "()Ljava/lang/String;", "app_taxiDebug"})
public final class HomeScreenModel {
    private final int id = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String name = null;
    private final int icon = 0;
    private boolean isEnable;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.enumerable.HomeScreenActionEnum actionEnum = null;
    
    public HomeScreenModel(int id, @org.jetbrains.annotations.NotNull()
    java.lang.String name, int icon, boolean isEnable, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.HomeScreenActionEnum actionEnum) {
        super();
    }
    
    public final int getId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    public final int getIcon() {
        return 0;
    }
    
    public final boolean isEnable() {
        return false;
    }
    
    public final void setEnable(boolean p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.enumerable.HomeScreenActionEnum getActionEnum() {
        return null;
    }
}