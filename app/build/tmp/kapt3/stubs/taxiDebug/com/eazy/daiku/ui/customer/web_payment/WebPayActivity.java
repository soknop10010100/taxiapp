package com.eazy.daiku.ui.customer.web_payment;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 &2\u00020\u0001:\u0002&\'B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\b\u0010\u0018\u001a\u00020\u0017H\u0002J\u001f\u0010\u0019\u001a\u0004\u0018\u00010\n2\u0006\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u001b\u001a\u00020\u001cH\u0002\u00a2\u0006\u0002\u0010\u001dJ\u0012\u0010\u001e\u001a\u00020\u00172\b\u0010\u001f\u001a\u0004\u0018\u00010 H\u0014J\b\u0010!\u001a\u00020\u0017H\u0014J\u0010\u0010\"\u001a\u00020\n2\u0006\u0010#\u001a\u00020$H\u0016J\b\u0010%\u001a\u00020\u0017H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006("}, d2 = {"Lcom/eazy/daiku/ui/customer/web_payment/WebPayActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "JAVASCRIPT_OBJ", "", "binding", "Lcom/eazy/daiku/databinding/ActivityWebPayBinding;", "dataWebViewRespond", "Lcom/eazy/daiku/ui/customer/model/WebPayRespondModel;", "hasAbaInstall", "", "hasAcledaInstall", "hasKessChatInstall", "hasSpnInstall", "intentFilter", "Landroid/content/IntentFilter;", "linearProgressIndicator", "Lcom/google/android/material/progressindicator/LinearProgressIndicator;", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "webChromeClient", "Landroid/webkit/WebChromeClient;", "initView", "", "injectJavaScriptFunction", "isAppInstalled", "packageName", "packageManager", "Landroid/content/pm/PackageManager;", "(Ljava/lang/String;Landroid/content/pm/PackageManager;)Ljava/lang/Boolean;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "onStart", "Companion", "JavaScriptInterface", "app_taxiDebug"})
public final class WebPayActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityWebPayBinding binding;
    private com.eazy.daiku.ui.customer.model.WebPayRespondModel dataWebViewRespond;
    private com.google.android.material.progressindicator.LinearProgressIndicator linearProgressIndicator;
    private boolean hasKessChatInstall = false;
    private boolean hasAcledaInstall = false;
    private boolean hasAbaInstall = false;
    private boolean hasSpnInstall = false;
    private final java.lang.String JAVASCRIPT_OBJ = "javascript_obj";
    private final android.content.IntentFilter intentFilter = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.web_payment.WebPayActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String dataJsonWebViewKey = "dataJsonWebViewKey";
    private final android.webkit.WebChromeClient webChromeClient = null;
    private com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver;
    
    public WebPayActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final java.lang.Boolean isAppInstalled(java.lang.String packageName, android.content.pm.PackageManager packageManager) {
        return null;
    }
    
    private final void injectJavaScriptFunction() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\u0006H\u0007\u00a8\u0006\b"}, d2 = {"Lcom/eazy/daiku/ui/customer/web_payment/WebPayActivity$JavaScriptInterface;", "", "(Lcom/eazy/daiku/ui/customer/web_payment/WebPayActivity;)V", "textFromWeb", "", "method", "", "data", "app_taxiDebug"})
    public final class JavaScriptInterface {
        
        public JavaScriptInterface() {
            super();
        }
        
        @android.webkit.JavascriptInterface()
        public final void textFromWeb(@org.jetbrains.annotations.Nullable()
        java.lang.String method, @org.jetbrains.annotations.Nullable()
        java.lang.String data) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/customer/web_payment/WebPayActivity$Companion;", "", "()V", "dataJsonWebViewKey", "", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}