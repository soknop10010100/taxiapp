package com.eazy.daiku.utility.other;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0010\u0018\u0000 32\u00020\u0001:\u000234B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u00020\u0004H\u0002J\u0010\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u0004H\u0002J\n\u0010\r\u001a\u0004\u0018\u00010\u000eH\u0002J\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0004H\u0002J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0011\u001a\u00020\u0004H\u0002J\b\u0010\u0014\u001a\u00020\u0004H\u0002J\u0010\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0016\u001a\u00020\u0017H\u0002J\b\u0010\u0018\u001a\u00020\u0004H\u0002J\b\u0010\u0019\u001a\u00020\u0004H\u0002J\b\u0010\u001a\u001a\u00020\u001bH\u0002J\b\u0010\u001c\u001a\u00020\u0004H\u0002J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\n\u0010!\u001a\u0004\u0018\u00010\"H\u0002J\n\u0010#\u001a\u0004\u0018\u00010\u0004H\u0002J\u0018\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u000e2\u0006\u0010\'\u001a\u00020\u0004H\u0002J0\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020\u00042\u0006\u0010*\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u00042\u0006\u0010,\u001a\u00020\u00042\b\b\u0002\u0010-\u001a\u00020\u0004J\u0010\u0010.\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0002J\u0010\u0010/\u001a\u00020%2\u0006\u0010\t\u001a\u00020\u0004H\u0002J\b\u00100\u001a\u00020\u0004H\u0002J\b\u00101\u001a\u00020\u0004H\u0002J\b\u00102\u001a\u00020\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00065"}, d2 = {"Lcom/eazy/daiku/utility/other/TelegramSentErrorLogHelper;", "", "()V", "fullBatterySymbol", "", "locationSymbol", "lowBatterySymbol", "networkSymbol", "robotSymbol", "text", "appNameVersion", "capitalize", "str", "captureScreen", "Ljava/io/File;", "createMultiPartRetrofit", "Lcom/eazy/daiku/data/remote/TelegramApi;", "baseUrl", "createRetrofitService", "Lretrofit2/Retrofit;", "currentDateTime", "formatFileSize", "size", "", "getAndroidApi", "getDeviceName", "getMultiPathOkHttpClient", "Lokhttp3/OkHttpClient;", "getPublicIPAddress", "getWifiLevel", "", "context", "Landroid/content/Context;", "hasActivityAlive", "Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "networkType", "sentCaptionAndPhoto", "", "file", "caption", "sentLogErrorToTelegram", "titleHeader", "header", "bodyRequest", "bodyRespond", "respondTimer", "speedInternet", "submitToTelegramChecked", "timeZoneUser", "toGetDeviceName", "userError", "Companion", "MultiPathInterceptor", "app_taxiDebug"})
public final class TelegramSentErrorLogHelper {
    private final java.lang.String networkSymbol = "\ud83d\udcf6";
    private final java.lang.String locationSymbol = "\ud83d\udccd";
    private final java.lang.String robotSymbol = "\ud83e\udd16";
    private final java.lang.String fullBatterySymbol = "\ud83d\udd0b";
    private final java.lang.String lowBatterySymbol = "\ud83e\udeab";
    private java.lang.String text = "";
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.other.TelegramSentErrorLogHelper.Companion Companion = null;
    private static com.eazy.daiku.utility.other.TelegramSentErrorLogHelper ourInstance;
    private static final long CHAT_ID_DEV = -1001580343972L;
    private static final long CHAT_ID_PROD = -1001767489584L;
    
    private TelegramSentErrorLogHelper() {
        super();
    }
    
    public final void sentLogErrorToTelegram(@org.jetbrains.annotations.NotNull()
    java.lang.String titleHeader, @org.jetbrains.annotations.NotNull()
    java.lang.String header, @org.jetbrains.annotations.NotNull()
    java.lang.String bodyRequest, @org.jetbrains.annotations.NotNull()
    java.lang.String bodyRespond, @org.jetbrains.annotations.NotNull()
    java.lang.String respondTimer) {
    }
    
    private final void submitToTelegramChecked(java.lang.String text) {
    }
    
    /**
     * current date time
     */
    private final java.lang.String currentDateTime() {
        return null;
    }
    
    private final java.lang.String timeZoneUser() {
        return null;
    }
    
    private final java.lang.String userError() {
        return null;
    }
    
    private final java.lang.String appNameVersion() {
        return null;
    }
    
    private final java.lang.String toGetDeviceName() {
        return null;
    }
    
    private final java.lang.String getDeviceName() {
        return null;
    }
    
    private final java.lang.String getAndroidApi() {
        return null;
    }
    
    private final java.lang.String capitalize(java.lang.String str) {
        return null;
    }
    
    private final java.lang.String networkType() {
        return null;
    }
    
    private final java.lang.String getPublicIPAddress() {
        return null;
    }
    
    private final java.io.File captureScreen() {
        return null;
    }
    
    private final java.lang.String formatFileSize(double size) {
        return null;
    }
    
    private final int speedInternet(android.content.Context context) {
        return 0;
    }
    
    private final int getWifiLevel(android.content.Context context) {
        return 0;
    }
    
    private final com.eazy.daiku.utility.base.BaseCoreActivity hasActivityAlive() {
        return null;
    }
    
    private final void sentCaptionAndPhoto(java.io.File file, java.lang.String caption) {
    }
    
    private final com.eazy.daiku.data.remote.TelegramApi createMultiPartRetrofit(java.lang.String baseUrl) {
        return null;
    }
    
    private final okhttp3.OkHttpClient getMultiPathOkHttpClient() {
        return null;
    }
    
    private final retrofit2.Retrofit createRetrofitService(java.lang.String baseUrl) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final com.eazy.daiku.utility.other.TelegramSentErrorLogHelper getInstance() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\u0016\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/other/TelegramSentErrorLogHelper$MultiPathInterceptor;", "Lokhttp3/Interceptor;", "()V", "intercept", "Lokhttp3/Response;", "chain", "Lokhttp3/Interceptor$Chain;", "app_taxiDebug"})
    public static final class MultiPathInterceptor implements okhttp3.Interceptor {
        
        public MultiPathInterceptor() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.Throws(exceptionClasses = {java.io.IOException.class})
        @java.lang.Override()
        public okhttp3.Response intercept(@org.jetbrains.annotations.NotNull()
        okhttp3.Interceptor.Chain chain) throws java.io.IOException {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u00078FX\u0087\u0004\u00a2\u0006\f\u0012\u0004\b\b\u0010\u0002\u001a\u0004\b\t\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/eazy/daiku/utility/other/TelegramSentErrorLogHelper$Companion;", "", "()V", "CHAT_ID_DEV", "", "CHAT_ID_PROD", "instance", "Lcom/eazy/daiku/utility/other/TelegramSentErrorLogHelper;", "getInstance$annotations", "getInstance", "()Lcom/eazy/daiku/utility/other/TelegramSentErrorLogHelper;", "ourInstance", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @kotlin.jvm.JvmStatic()
        @java.lang.Deprecated()
        public static void getInstance$annotations() {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.eazy.daiku.utility.other.TelegramSentErrorLogHelper getInstance() {
            return null;
        }
    }
}