package com.eazy.daiku.ui.customer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001c\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001BA\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\fJ\u000b\u0010!\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\"\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010#\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010$\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010%\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003JE\u0010&\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001J\u0013\u0010\'\u001a\u00020(2\b\u0010)\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010*\u001a\u00020+H\u00d6\u0001J\t\u0010,\u001a\u00020-H\u00d6\u0001R \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR \u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 \u00a8\u0006."}, d2 = {"Lcom/eazy/daiku/ui/customer/model/CustomerFees;", "", "POSACLBKHPP", "Lcom/eazy/daiku/ui/customer/model/POSACLBKHPP;", "POSABAAKHPP", "Lcom/eazy/daiku/ui/customer/model/POSABAAKHPP;", "POSSBPLKHPP", "Lcom/eazy/daiku/ui/customer/model/POSSBPLKHPP;", "WECHAT", "Lcom/eazy/daiku/ui/customer/model/WECHAT;", "ALIPAY", "Lcom/eazy/daiku/ui/customer/model/ALIPAY;", "(Lcom/eazy/daiku/ui/customer/model/POSACLBKHPP;Lcom/eazy/daiku/ui/customer/model/POSABAAKHPP;Lcom/eazy/daiku/ui/customer/model/POSSBPLKHPP;Lcom/eazy/daiku/ui/customer/model/WECHAT;Lcom/eazy/daiku/ui/customer/model/ALIPAY;)V", "getALIPAY", "()Lcom/eazy/daiku/ui/customer/model/ALIPAY;", "setALIPAY", "(Lcom/eazy/daiku/ui/customer/model/ALIPAY;)V", "getPOSABAAKHPP", "()Lcom/eazy/daiku/ui/customer/model/POSABAAKHPP;", "setPOSABAAKHPP", "(Lcom/eazy/daiku/ui/customer/model/POSABAAKHPP;)V", "getPOSACLBKHPP", "()Lcom/eazy/daiku/ui/customer/model/POSACLBKHPP;", "setPOSACLBKHPP", "(Lcom/eazy/daiku/ui/customer/model/POSACLBKHPP;)V", "getPOSSBPLKHPP", "()Lcom/eazy/daiku/ui/customer/model/POSSBPLKHPP;", "setPOSSBPLKHPP", "(Lcom/eazy/daiku/ui/customer/model/POSSBPLKHPP;)V", "getWECHAT", "()Lcom/eazy/daiku/ui/customer/model/WECHAT;", "setWECHAT", "(Lcom/eazy/daiku/ui/customer/model/WECHAT;)V", "component1", "component2", "component3", "component4", "component5", "copy", "equals", "", "other", "hashCode", "", "toString", "", "app_taxiDebug"})
public final class CustomerFees {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "POS_ACLBKHPP")
    private com.eazy.daiku.ui.customer.model.POSACLBKHPP POSACLBKHPP;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "POS_ABAAKHPP")
    private com.eazy.daiku.ui.customer.model.POSABAAKHPP POSABAAKHPP;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "POS_SBPLKHPP")
    private com.eazy.daiku.ui.customer.model.POSSBPLKHPP POSSBPLKHPP;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "WECHAT")
    private com.eazy.daiku.ui.customer.model.WECHAT WECHAT;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "ALIPAY")
    private com.eazy.daiku.ui.customer.model.ALIPAY ALIPAY;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.ui.customer.model.CustomerFees copy(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSACLBKHPP POSACLBKHPP, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSABAAKHPP POSABAAKHPP, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSSBPLKHPP POSSBPLKHPP, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.WECHAT WECHAT, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.ALIPAY ALIPAY) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public CustomerFees() {
        super();
    }
    
    public CustomerFees(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSACLBKHPP POSACLBKHPP, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSABAAKHPP POSABAAKHPP, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSSBPLKHPP POSSBPLKHPP, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.WECHAT WECHAT, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.ALIPAY ALIPAY) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.POSACLBKHPP component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.POSACLBKHPP getPOSACLBKHPP() {
        return null;
    }
    
    public final void setPOSACLBKHPP(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSACLBKHPP p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.POSABAAKHPP component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.POSABAAKHPP getPOSABAAKHPP() {
        return null;
    }
    
    public final void setPOSABAAKHPP(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSABAAKHPP p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.POSSBPLKHPP component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.POSSBPLKHPP getPOSSBPLKHPP() {
        return null;
    }
    
    public final void setPOSSBPLKHPP(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.POSSBPLKHPP p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.WECHAT component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.WECHAT getWECHAT() {
        return null;
    }
    
    public final void setWECHAT(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.WECHAT p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.ALIPAY component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.ALIPAY getALIPAY() {
        return null;
    }
    
    public final void setALIPAY(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.ALIPAY p0) {
    }
}