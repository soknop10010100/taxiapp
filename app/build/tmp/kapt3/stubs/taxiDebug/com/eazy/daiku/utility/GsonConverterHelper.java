package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0019\u0010\u0003\u001a\u00020\u0004\"\u0004\b\u0000\u0010\u00052\u0006\u0010\u0006\u001a\u0002H\u0005\u00a2\u0006\u0002\u0010\u0007J\u0014\u0010\b\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u000b\u001a\u00020\u0004J\u000e\u0010\f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u0004J \u0010\u000e\u001a\u0002H\u0005\"\u0006\b\u0000\u0010\u0005\u0018\u00012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0010J\"\u0010\u0011\u001a\u0004\u0018\u0001H\u0005\"\u0006\b\u0000\u0010\u0005\u0018\u00012\b\u0010\u000f\u001a\u0004\u0018\u00010\u0004H\u0086\b\u00a2\u0006\u0002\u0010\u0010J\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\n0\t2\u0006\u0010\u0013\u001a\u00020\u0004J\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\t2\u0006\u0010\u0016\u001a\u00020\u0004J\u001c\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\n0\tJ\u001c\u0010\u001c\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\f\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\n0\tJ\u001c\u0010\u001e\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001a2\f\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00150\t\u00a8\u0006 "}, d2 = {"Lcom/eazy/daiku/utility/GsonConverterHelper;", "", "()V", "convertGenericClassToJson", "", "T", "data", "(Ljava/lang/Object;)Ljava/lang/String;", "getBankAccountLists", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "BankAccountJson", "getFirstBankAccount", "bankAccount", "getJsonObjectToGenericClass", "jsonData", "(Ljava/lang/String;)Ljava/lang/Object;", "getJsonObjectToGenericClassValidate", "getListMyBankAccount", "myBankAccountJson", "getListMySearchHistoryMap", "Lcom/eazy/daiku/ui/customer/model/SearchMapHistoryModel;", "myHistoryMapJson", "saveBankAccountJson", "", "context", "Landroid/content/Context;", "BankLists", "saveListMyBankAccount", "myBankAccountList", "saveListSearchHistoryMap", "mySearchHistoryMapList", "app_taxiDebug"})
public final class GsonConverterHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.GsonConverterHelper INSTANCE = null;
    
    private GsonConverterHelper() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> getBankAccountLists(@org.jetbrains.annotations.NotNull()
    java.lang.String BankAccountJson) {
        return null;
    }
    
    public final void saveBankAccountJson(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> BankLists) {
    }
    
    public final void saveListMyBankAccount(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> myBankAccountList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.model.SaveBankAccount getFirstBankAccount(@org.jetbrains.annotations.NotNull()
    java.lang.String bankAccount) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> getListMyBankAccount(@org.jetbrains.annotations.NotNull()
    java.lang.String myBankAccountJson) {
        return null;
    }
    
    public final void saveListSearchHistoryMap(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel> mySearchHistoryMapList) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel> getListMySearchHistoryMap(@org.jetbrains.annotations.NotNull()
    java.lang.String myHistoryMapJson) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final <T extends java.lang.Object>java.lang.String convertGenericClassToJson(T data) {
        return null;
    }
}