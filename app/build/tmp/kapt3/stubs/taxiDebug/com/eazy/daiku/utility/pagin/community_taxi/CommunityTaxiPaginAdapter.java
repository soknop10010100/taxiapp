package com.eazy.daiku.utility.pagin.community_taxi;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 \u001b2\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u001b\u001cB\u0005\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0002\u0018\u00010\u000fJ\u0018\u0010\u0010\u001a\u00020\t2\u0006\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u0006H\u0016J\u0018\u0010\u0013\u001a\u00020\u00032\u0006\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u0006H\u0016J\u001c\u0010\u0017\u001a\u00020\t2\f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00020\u000f2\u0006\u0010\u0019\u001a\u00020\u0006J\u000e\u0010\u001a\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u001d"}, d2 = {"Lcom/eazy/daiku/utility/pagin/community_taxi/CommunityTaxiPaginAdapter;", "Landroidx/paging/PagedListAdapter;", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "Lcom/eazy/daiku/utility/pagin/community_taxi/CommunityTaxiPaginAdapter$MyCommunityTaxiPaginViewHolder;", "()V", "selectCommunityTaxiId", "", "selectRow", "Lkotlin/Function1;", "", "getSelectRow", "()Lkotlin/jvm/functions/Function1;", "setSelectRow", "(Lkotlin/jvm/functions/Function1;)V", "getCommunityTaxiLists", "Landroidx/paging/PagedList;", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "submitData", "pagedList", "communityTaxiId", "updateItem", "Companion", "MyCommunityTaxiPaginViewHolder", "app_taxiDebug"})
public final class CommunityTaxiPaginAdapter extends androidx.paging.PagedListAdapter<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, com.eazy.daiku.utility.pagin.community_taxi.CommunityTaxiPaginAdapter.MyCommunityTaxiPaginViewHolder> {
    private int selectCommunityTaxiId = -1;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.pagin.community_taxi.CommunityTaxiPaginAdapter.Companion Companion = null;
    private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond> USER_COMPARATOR = null;
    public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> selectRow;
    
    public CommunityTaxiPaginAdapter() {
        super(null);
    }
    
    public final void submitData(@org.jetbrains.annotations.NotNull()
    androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond> pagedList, int communityTaxiId) {
    }
    
    public final void updateItem(int communityTaxiId) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond> getCommunityTaxiLists() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.utility.pagin.community_taxi.CommunityTaxiPaginAdapter.MyCommunityTaxiPaginViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.pagin.community_taxi.CommunityTaxiPaginAdapter.MyCommunityTaxiPaginViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> getSelectRow() {
        return null;
    }
    
    public final void setSelectRow(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> p0) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/eazy/daiku/utility/pagin/community_taxi/CommunityTaxiPaginAdapter$MyCommunityTaxiPaginViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "communityCardRow", "Lcom/google/android/material/card/MaterialCardView;", "titleTv", "Landroid/widget/TextView;", "bind", "", "communityTaxi", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "selectItem", "", "app_taxiDebug"})
    public static final class MyCommunityTaxiPaginViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final android.widget.TextView titleTv = null;
        private final com.google.android.material.card.MaterialCardView communityCardRow = null;
        
        public MyCommunityTaxiPaginViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.server_model.CommunityTaxiRespond communityTaxi) {
        }
        
        public final void selectItem(boolean selectItem) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/pagin/community_taxi/CommunityTaxiPaginAdapter$Companion;", "", "()V", "USER_COMPARATOR", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}