package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00d0\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001c\b\u0016\u0018\u0000 \u0080\u00012\u00020\u0001:\u0002\u0080\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010;\u001a\u00020<J\u0010\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J\u0010\u0010A\u001a\u00020>2\u0006\u0010?\u001a\u00020@H\u0002J\u0010\u0010B\u001a\u00020>2\u0006\u0010C\u001a\u00020DH\u0016J\u0018\u0010B\u001a\u00020>2\u0006\u0010C\u001a\u00020D2\u0006\u0010E\u001a\u00020\u0016H\u0016J\u0010\u0010F\u001a\u00020>2\u0006\u0010G\u001a\u00020HH\u0002J\u0010\u0010I\u001a\u00020>2\b\u0010J\u001a\u0004\u0018\u00010&J\u0010\u0010K\u001a\u00020>2\u0006\u0010L\u001a\u00020MH\u0016J\u0006\u0010N\u001a\u00020>J.\u0010O\u001a\u00020>2\u0006\u0010P\u001a\u00020<2\b\u0010Q\u001a\u0004\u0018\u00010R2\u0012\u0010S\u001a\u000e\u0012\u0004\u0012\u00020\u0016\u0012\u0004\u0012\u00020>0TH\u0004J\u0010\u0010U\u001a\u00020>2\b\b\u0002\u0010V\u001a\u00020\u0016J\u000e\u0010W\u001a\u00020>2\u0006\u0010X\u001a\u00020\u0016J\b\u0010Y\u001a\u00020>H\u0002J\b\u0010Z\u001a\u00020>H\u0002J\b\u0010[\u001a\u00020>H\u0004J\b\u0010\\\u001a\u00020>H\u0002J\"\u0010]\u001a\u00020>2\u0006\u0010^\u001a\u00020_2\u0006\u0010`\u001a\u00020_2\b\u0010a\u001a\u0004\u0018\u00010bH\u0014J\u0012\u0010c\u001a\u00020>2\b\u0010d\u001a\u0004\u0018\u00010eH\u0014J\b\u0010f\u001a\u00020>H\u0014J\b\u0010g\u001a\u00020>H\u0014J\u0006\u0010h\u001a\u00020>J\u0006\u0010i\u001a\u00020>J\b\u0010j\u001a\u00020>H\u0014J\b\u0010k\u001a\u00020>H\u0014J\b\u0010l\u001a\u00020>H\u0014J\b\u0010m\u001a\u00020>H\u0002J\b\u0010n\u001a\u00020>H\u0002J\u0006\u0010o\u001a\u00020>J\b\u0010p\u001a\u00020>H\u0002J\u0018\u0010q\u001a\u00020>2\u0006\u0010r\u001a\u00020<2\b\b\u0002\u0010s\u001a\u00020<J\u0018\u0010t\u001a\u00020>2\u0006\u0010r\u001a\u00020<2\b\u0010u\u001a\u0004\u0018\u00010&J\b\u0010v\u001a\u00020>H\u0004J\u0010\u0010w\u001a\u00020>2\u0006\u0010P\u001a\u00020\u0016H\u0004J\u0006\u0010x\u001a\u00020>J\u000e\u0010y\u001a\u00020>2\u0006\u0010z\u001a\u00020<J\u0006\u0010{\u001a\u00020>J\u0006\u0010|\u001a\u00020>J\u0006\u0010}\u001a\u00020>J\b\u0010~\u001a\u00020>H\u0002J\u000e\u0010\u007f\u001a\u00020<2\u0006\u0010G\u001a\u00020HR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\u0016X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\u0014\u0010\u001b\u001a\u00020\u001cX\u0084\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0010\u0010\u001f\u001a\u0004\u0018\u00010 X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010!\u001a\u0004\u0018\u00010\"X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010#\u001a\u00020$X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010%\u001a\u0004\u0018\u00010&X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\'\u001a\u00020(X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010)\u001a\u0012\u0012\u0004\u0012\u00020&0*j\b\u0012\u0004\u0012\u00020&`+X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010,\u001a\u0004\u0018\u00010-X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010/\"\u0004\b0\u00101R\u000e\u00102\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00103\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00104\u001a\u000205X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00106\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u00107\u001a\u000208X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u00109\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010:\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0081\u0001"}, d2 = {"Lcom/eazy/daiku/utility/base/BaseActivity;", "Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "()V", "confirmBookingAlertDialog", "Lcom/eazy/daiku/utility/custom/ConfirmBookingAlertDialog;", "delayTimer", "", "easyImage", "Lpl/aprilapps/easyphotopicker/EasyImage;", "foregroundOnlyBroadcastReceiver", "Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyBroadcastReceiver;", "getForegroundOnlyBroadcastReceiver", "()Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyBroadcastReceiver;", "setForegroundOnlyBroadcastReceiver", "(Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyBroadcastReceiver;)V", "foregroundOnlyLocationService", "Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;", "getForegroundOnlyLocationService", "()Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;", "setForegroundOnlyLocationService", "(Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;)V", "foregroundOnlyLocationServiceBound", "", "getForegroundOnlyLocationServiceBound", "()Z", "setForegroundOnlyLocationServiceBound", "(Z)V", "foregroundOnlyServiceConnection", "Landroid/content/ServiceConnection;", "getForegroundOnlyServiceConnection", "()Landroid/content/ServiceConnection;", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "handler", "Landroid/os/Handler;", "intentFilter", "Landroid/content/IntentFilter;", "lastLocation", "Landroid/location/Location;", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "locationsTemp", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "mEazyTaxiApplication", "Lcom/eazy/daiku/EazyTaxiApplication;", "getMEazyTaxiApplication", "()Lcom/eazy/daiku/EazyTaxiApplication;", "setMEazyTaxiApplication", "(Lcom/eazy/daiku/EazyTaxiApplication;)V", "mIsStateAlreadySaved", "mPendingShowDialog", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "removeHandler", "runnableCode", "Ljava/lang/Runnable;", "shouldShow", "startLocationFlag", "appInfo", "", "assignedFilterBookingTaxi", "", "parseServer", "Lcom/eazy/daiku/utility/parse_server/ParseLiveLocationHelper;", "assignedStopMovingClass", "buildEasyImage", "chooserType", "Lpl/aprilapps/easyphotopicker/ChooserType;", "isMultiple", "checkingAttendanceUserInParseServer", "user", "Lcom/eazy/daiku/data/model/server_model/User;", "checkingStopMovingDriver", "locationData", "chooseGalleryImage", "easyImageCallbacks", "Lpl/aprilapps/easyphotopicker/EasyImage$Callbacks;", "clearTripSession", "confirmBooking", "status", "parseObject", "Lcom/parse/ParseObject;", "completedUpdated", "Lkotlin/Function1;", "disableDisplayOnMapLiveUser", "displayOnMap", "globalLoadingViewEnable", "showLoading", "initFcmToken", "initSubscribeParseServer", "needToCheckPermissionLocation", "nextStepAfterHasLocation", "onActivityResult", "requestCode", "", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onPause", "onRegisterBindServiceForegroundConnection", "onRegisterForegroundService", "onResume", "onResumeFragments", "onStart", "removeLiveDriverInParseServer", "removeLiveTripHandler", "removeTrackLocation", "showBookingDialog", "startBroadcastData", "key", "jsonModel", "startBroadcastLocationData", "location", "startUpdateLocationToParseServer", "stopMovingLocationParseServer", "subscribeTaxiBooking", "subscribeTaxiBookingByCode", "code", "unRegisterForegroundBroadcastService", "unRegisterLocationLiveUser", "unSubscribeLocationForeground", "updateLiveUserParseServer", "userInfo", "Companion", "app_taxiDebug"})
public class BaseActivity extends com.eazy.daiku.utility.base.BaseCoreActivity {
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private com.eazy.daiku.utility.custom.ConfirmBookingAlertDialog confirmBookingAlertDialog;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.EazyTaxiApplication mEazyTaxiApplication;
    private pl.aprilapps.easyphotopicker.EasyImage easyImage;
    private boolean shouldShow = true;
    private android.location.Location lastLocation;
    private boolean startLocationFlag = true;
    private android.os.Handler handler;
    private boolean removeHandler = false;
    private final long delayTimer = 60000L;
    private final java.util.ArrayList<android.location.Location> locationsTemp = null;
    private final android.content.IntentFilter intentFilter = null;
    private boolean mIsStateAlreadySaved = false;
    private boolean mPendingShowDialog = false;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.base.BaseActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String bookingCode = "";
    @org.jetbrains.annotations.NotNull()
    private final android.content.ServiceConnection foregroundOnlyServiceConnection = null;
    private boolean foregroundOnlyLocationServiceBound = false;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyLocationService foregroundOnlyLocationService;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyBroadcastReceiver foregroundOnlyBroadcastReceiver;
    private final java.lang.Runnable runnableCode = null;
    private com.google.android.gms.location.LocationCallback locationCallback;
    private final com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver = null;
    
    public BaseActivity() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.eazy.daiku.EazyTaxiApplication getMEazyTaxiApplication() {
        return null;
    }
    
    protected final void setMEazyTaxiApplication(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.EazyTaxiApplication p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    public void chooseGalleryImage(@org.jetbrains.annotations.NotNull()
    pl.aprilapps.easyphotopicker.EasyImage.Callbacks easyImageCallbacks) {
    }
    
    public void buildEasyImage(@org.jetbrains.annotations.NotNull()
    pl.aprilapps.easyphotopicker.ChooserType chooserType) {
    }
    
    public void buildEasyImage(@org.jetbrains.annotations.NotNull()
    pl.aprilapps.easyphotopicker.ChooserType chooserType, boolean isMultiple) {
    }
    
    public final void startBroadcastData(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.NotNull()
    java.lang.String jsonModel) {
    }
    
    public final void startBroadcastLocationData(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    android.location.Location location) {
    }
    
    private final void nextStepAfterHasLocation() {
    }
    
    private final void initFcmToken() {
    }
    
    protected final void needToCheckPermissionLocation() {
    }
    
    public final void clearTripSession() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String userInfo(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.server_model.User user) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String appInfo() {
        return null;
    }
    
    private final void checkingAttendanceUserInParseServer(com.eazy.daiku.data.model.server_model.User user) {
    }
    
    private final void updateLiveUserParseServer() {
    }
    
    public final void disableDisplayOnMapLiveUser(boolean displayOnMap) {
    }
    
    protected final void startUpdateLocationToParseServer() {
    }
    
    public final void unSubscribeLocationForeground() {
    }
    
    public final void unRegisterLocationLiveUser() {
    }
    
    private final void removeLiveDriverInParseServer() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @org.jetbrains.annotations.NotNull()
    protected final android.content.ServiceConnection getForegroundOnlyServiceConnection() {
        return null;
    }
    
    protected final boolean getForegroundOnlyLocationServiceBound() {
        return false;
    }
    
    protected final void setForegroundOnlyLocationServiceBound(boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyLocationService getForegroundOnlyLocationService() {
        return null;
    }
    
    protected final void setForegroundOnlyLocationService(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyLocationService p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    protected final com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyBroadcastReceiver getForegroundOnlyBroadcastReceiver() {
        return null;
    }
    
    protected final void setForegroundOnlyBroadcastReceiver(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyBroadcastReceiver p0) {
    }
    
    public final void onRegisterBindServiceForegroundConnection() {
    }
    
    public final void onRegisterForegroundService() {
    }
    
    public final void unRegisterForegroundBroadcastService() {
    }
    
    public final void removeTrackLocation() {
    }
    
    private final void initSubscribeParseServer() {
    }
    
    private final void assignedStopMovingClass(com.eazy.daiku.utility.parse_server.ParseLiveLocationHelper parseServer) {
    }
    
    private final void assignedFilterBookingTaxi(com.eazy.daiku.utility.parse_server.ParseLiveLocationHelper parseServer) {
    }
    
    public final void subscribeTaxiBooking() {
    }
    
    public final void subscribeTaxiBookingByCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
    }
    
    @java.lang.Override()
    protected void onResumeFragments() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    private final void showBookingDialog() {
    }
    
    protected final void confirmBooking(@org.jetbrains.annotations.NotNull()
    java.lang.String status, @org.jetbrains.annotations.Nullable()
    com.parse.ParseObject parseObject, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> completedUpdated) {
    }
    
    protected final void stopMovingLocationParseServer(boolean status) {
    }
    
    public final void checkingStopMovingDriver(@org.jetbrains.annotations.Nullable()
    android.location.Location locationData) {
    }
    
    public final void globalLoadingViewEnable(boolean showLoading) {
    }
    
    private final void removeLiveTripHandler() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/base/BaseActivity$Companion;", "", "()V", "bookingCode", "", "getBookingCode", "()Ljava/lang/String;", "setBookingCode", "(Ljava/lang/String;)V", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getBookingCode() {
            return null;
        }
        
        public final void setBookingCode(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
    }
}