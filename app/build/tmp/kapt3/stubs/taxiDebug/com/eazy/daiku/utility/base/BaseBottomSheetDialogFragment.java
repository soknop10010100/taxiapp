package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018J\u0010\u0010\u0019\u001a\u00020\u00162\u0006\u0010\u001a\u001a\u00020\u0010H\u0016J\u0019\u0010\u001b\u001a\u0002H\u001c\"\n\b\u0000\u0010\u001c*\u0004\u0018\u00010\u0000H\u0004\u00a2\u0006\u0002\u0010\u001dR\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\u000f\u001a\u0004\u0018\u00010\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006\u001e"}, d2 = {"Lcom/eazy/daiku/utility/base/BaseBottomSheetDialogFragment;", "Lcom/google/android/material/bottomsheet/BottomSheetDialogFragment;", "()V", "activityLauncher", "Lcom/eazy/daiku/utility/BetterActivityResult;", "Landroid/content/Intent;", "Landroidx/activity/result/ActivityResult;", "getActivityLauncher", "()Lcom/eazy/daiku/utility/BetterActivityResult;", "fContextBase", "Landroidx/fragment/app/FragmentActivity;", "getFContextBase", "()Landroidx/fragment/app/FragmentActivity;", "setFContextBase", "(Landroidx/fragment/app/FragmentActivity;)V", "mContextBase", "Landroid/content/Context;", "getMContextBase", "()Landroid/content/Context;", "setMContextBase", "(Landroid/content/Context;)V", "globalShowError", "", "text", "", "onAttach", "context", "self", "T", "()Lcom/eazy/daiku/utility/base/BaseBottomSheetDialogFragment;", "app_taxiDebug"})
public class BaseBottomSheetDialogFragment extends com.google.android.material.bottomsheet.BottomSheetDialogFragment {
    @org.jetbrains.annotations.Nullable()
    private android.content.Context mContextBase;
    @org.jetbrains.annotations.Nullable()
    private androidx.fragment.app.FragmentActivity fContextBase;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.BetterActivityResult<android.content.Intent, androidx.activity.result.ActivityResult> activityLauncher = null;
    
    public BaseBottomSheetDialogFragment() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.content.Context getMContextBase() {
        return null;
    }
    
    public final void setMContextBase(@org.jetbrains.annotations.Nullable()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final androidx.fragment.app.FragmentActivity getFContextBase() {
        return null;
    }
    
    public final void setFContextBase(@org.jetbrains.annotations.Nullable()
    androidx.fragment.app.FragmentActivity p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.BetterActivityResult<android.content.Intent, androidx.activity.result.ActivityResult> getActivityLauncher() {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    protected final <T extends com.eazy.daiku.utility.base.BaseBottomSheetDialogFragment>T self() {
        return null;
    }
    
    public final void globalShowError(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
}