package com.eazy.daiku.ui.wallet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000R\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u001dH\u0002J\b\u0010\u001f\u001a\u00020\u001dH\u0002J\b\u0010 \u001a\u00020\u001dH\u0002J\u0012\u0010!\u001a\u00020\u001d2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0014J\b\u0010$\u001a\u00020\u001dH\u0014J\b\u0010%\u001a\u00020\u001dH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0017\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001b\u0010\u0014\u001a\u0004\b\u0019\u0010\u001a\u00a8\u0006&"}, d2 = {"Lcom/eazy/daiku/ui/wallet/MainWalletActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityMainWalletBinding;", "blockAmount", "", "intentFilter", "Landroid/content/IntentFilter;", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "totalAmount", "totalBalance", "transactionRespond", "Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "transactionVm", "Lcom/eazy/daiku/utility/view_model/TransactionVm;", "getTransactionVm", "()Lcom/eazy/daiku/utility/view_model/TransactionVm;", "transactionVm$delegate", "Lkotlin/Lazy;", "trxAdapter", "Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginAdapter;", "withdrawViewModel", "Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "getWithdrawViewModel", "()Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "withdrawViewModel$delegate", "doAction", "", "initData", "initObserved", "initUi", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onStart", "app_taxiDebug"})
public final class MainWalletActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private final android.content.IntentFilter intentFilter = null;
    private double totalBalance = 0.0;
    private double blockAmount = 0.0;
    private double totalAmount = 0.0;
    private com.eazy.daiku.data.model.server_model.TransactionRespond transactionRespond;
    private com.eazy.daiku.databinding.ActivityMainWalletBinding binding;
    private com.eazy.daiku.utility.pagin.transaction.TransactionPaginAdapter trxAdapter;
    private final kotlin.Lazy transactionVm$delegate = null;
    private final kotlin.Lazy withdrawViewModel$delegate = null;
    private final com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver = null;
    
    public MainWalletActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.TransactionVm getTransactionVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.withdraw.WithdrawViewModel getWithdrawViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initData() {
    }
    
    private final void initUi() {
    }
    
    private final void initObserved() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final void doAction() {
    }
}