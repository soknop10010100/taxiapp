package com.eazy.daiku.ui.login;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0018\u001a\u00020\u0019H\u0002J\u0012\u0010\u001a\u001a\u0004\u0018\u00010\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\b\u0010\u001e\u001a\u00020\u0019H\u0002J\b\u0010\u001f\u001a\u00020\u0019H\u0002J\b\u0010 \u001a\u00020\u0019H\u0002J\u0012\u0010!\u001a\u00020\u00192\b\u0010\"\u001a\u0004\u0018\u00010#H\u0014R\u0014\u0010\u0003\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\bX\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\u0012\u001a\u0004\b\u000f\u0010\u0010R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0017\u0010\u0012\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006$"}, d2 = {"Lcom/eazy/daiku/ui/login/LoginActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "TAG", "", "getTAG", "()Ljava/lang/String;", "binding", "Lcom/eazy/daiku/databinding/ActivityLoginBinding;", "getBinding$app_taxiDebug", "()Lcom/eazy/daiku/databinding/ActivityLoginBinding;", "setBinding$app_taxiDebug", "(Lcom/eazy/daiku/databinding/ActivityLoginBinding;)V", "loginViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "getLoginViewModel", "()Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "loginViewModel$delegate", "Lkotlin/Lazy;", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "doAction", "", "getUserFromGson", "Lcom/eazy/daiku/data/model/server_model/User;", "jsonObject", "Lcom/google/gson/JsonObject;", "initConfigureLanguage", "initObserved", "initView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_taxiDebug"})
public final class LoginActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String TAG = "LoginActivity__________";
    private final kotlin.Lazy loginViewModel$delegate = null;
    private final kotlin.Lazy userInfoVM$delegate = null;
    public com.eazy.daiku.databinding.ActivityLoginBinding binding;
    
    public LoginActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getTAG() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.LoginViewModel getLoginViewModel() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.databinding.ActivityLoginBinding getBinding$app_taxiDebug() {
        return null;
    }
    
    public final void setBinding$app_taxiDebug(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.databinding.ActivityLoginBinding p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    private final com.eazy.daiku.data.model.server_model.User getUserFromGson(com.google.gson.JsonObject jsonObject) {
        return null;
    }
    
    private final void initConfigureLanguage() {
    }
}