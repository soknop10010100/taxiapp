package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006J\u0018\u0010\t\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u0006\u00a8\u0006\n"}, d2 = {"Lcom/eazy/daiku/utility/MapHelper;", "", "()V", "bitmapDescriptorFromVector", "Lcom/google/android/gms/maps/model/BitmapDescriptor;", "context", "Landroid/content/Context;", "vectorResId", "", "bitmapFromVector", "app_taxiDebug"})
public final class MapHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.MapHelper INSTANCE = null;
    
    private MapHelper() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.BitmapDescriptor bitmapDescriptorFromVector(int vectorResId, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.BitmapDescriptor bitmapFromVector(int vectorResId, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.BitmapDescriptor bitmapDescriptorFromVector(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int vectorResId) {
        return null;
    }
}