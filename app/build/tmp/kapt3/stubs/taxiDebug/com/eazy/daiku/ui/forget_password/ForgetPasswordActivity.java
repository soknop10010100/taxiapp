package com.eazy.daiku.ui.forget_password;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\fH\u0002J\b\u0010\r\u001a\u00020\fH\u0002J\b\u0010\u000e\u001a\u00020\fH\u0002J\u0012\u0010\u000f\u001a\u00020\f2\b\u0010\u0010\u001a\u0004\u0018\u00010\u0011H\u0014J\u0010\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\b\u00a8\u0006\u0016"}, d2 = {"Lcom/eazy/daiku/ui/forget_password/ForgetPasswordActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityForgetPasswordBinding;", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "Lkotlin/Lazy;", "doAction", "", "initObserved", "intiView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "app_taxiDebug"})
public final class ForgetPasswordActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    private com.eazy.daiku.databinding.ActivityForgetPasswordBinding binding;
    private final kotlin.Lazy userInfoVM$delegate = null;
    
    public ForgetPasswordActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void intiView() {
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
}