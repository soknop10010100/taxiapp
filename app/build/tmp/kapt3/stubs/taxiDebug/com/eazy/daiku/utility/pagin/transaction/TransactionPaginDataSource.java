package com.eazy.daiku.utility.pagin.transaction;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\n\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B)\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0012\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\t\u00a2\u0006\u0002\u0010\fJ*\u0010%\u001a\u00020&2\f\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00020(2\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030*H\u0016J*\u0010+\u001a\u00020&2\f\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00020(2\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030*H\u0016J*\u0010,\u001a\u00020&2\f\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u00020-2\u0012\u0010)\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030.H\u0016J\u0016\u0010/\u001a\b\u0012\u0004\u0012\u00020\u0003002\u0006\u00101\u001a\u00020\u0010H\u0002R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00130\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00100\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u0017\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00020\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u0019R\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u00130\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u0019R\u0017\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u00130\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u001f\u0010\u0019R\u001a\u0010 \u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\n0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\b\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010!\u001a\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\"0\tX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00100\u00178F\u00a2\u0006\u0006\u001a\u0004\b$\u0010\u0019\u00a8\u00062"}, d2 = {"Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginDataSource;", "Landroidx/paging/PageKeyedDataSource;", "", "Lcom/eazy/daiku/data/model/MyTransaction;", "context", "Landroid/content/Context;", "baseViewModel", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "queryKey", "Ljava/util/HashMap;", "", "", "(Landroid/content/Context;Lcom/eazy/daiku/utility/base/BaseViewModel;Ljava/util/HashMap;)V", "_errorPaginationMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "_hasTrxListsMutableLiveData", "_loadingMutableLiveData", "", "_loadingPaginationMutableLiveData", "_totalBalanceMutableLiveData", "errorPaginationMutableLiveData", "Landroidx/lifecycle/LiveData;", "getErrorPaginationMutableLiveData", "()Landroidx/lifecycle/LiveData;", "hasTrxListsMutableLiveData", "getHasTrxListsMutableLiveData", "loadingMutableLiveData", "getLoadingMutableLiveData", "loadingPaginationMutableLiveData", "getLoadingPaginationMutableLiveData", "mapDateHeaderHm", "totalAmountByDate", "", "totalBalanceMutableLiveData", "getTotalBalanceMutableLiveData", "loadAfter", "", "params", "Landroidx/paging/PageKeyedDataSource$LoadParams;", "callback", "Landroidx/paging/PageKeyedDataSource$LoadCallback;", "loadBefore", "loadInitial", "Landroidx/paging/PageKeyedDataSource$LoadInitialParams;", "Landroidx/paging/PageKeyedDataSource$LoadInitialCallback;", "migrateTrx", "Ljava/util/ArrayList;", "trxRespond", "app_taxiDebug"})
public final class TransactionPaginDataSource extends androidx.paging.PageKeyedDataSource<java.lang.Integer, com.eazy.daiku.data.model.MyTransaction> {
    private android.content.Context context;
    private com.eazy.daiku.utility.base.BaseViewModel baseViewModel;
    private java.util.HashMap<java.lang.String, java.lang.Object> queryKey;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingMutableLiveData;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingPaginationMutableLiveData;
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> _hasTrxListsMutableLiveData;
    private androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.TransactionRespond>> _errorPaginationMutableLiveData;
    private androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.server_model.TransactionRespond> _totalBalanceMutableLiveData;
    private final java.util.HashMap<java.lang.String, java.lang.String> mapDateHeaderHm = null;
    private final java.util.HashMap<java.lang.String, java.lang.Double> totalAmountByDate = null;
    
    public TransactionPaginDataSource(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseViewModel baseViewModel, @org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> queryKey) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getLoadingMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getLoadingPaginationMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getHasTrxListsMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.TransactionRespond>> getErrorPaginationMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.server_model.TransactionRespond> getTotalBalanceMutableLiveData() {
        return null;
    }
    
    @java.lang.Override()
    public void loadInitial(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadInitialParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadInitialCallback<java.lang.Integer, com.eazy.daiku.data.model.MyTransaction> callback) {
    }
    
    @java.lang.Override()
    public void loadBefore(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadCallback<java.lang.Integer, com.eazy.daiku.data.model.MyTransaction> callback) {
    }
    
    @java.lang.Override()
    public void loadAfter(@org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadParams<java.lang.Integer> params, @org.jetbrains.annotations.NotNull()
    androidx.paging.PageKeyedDataSource.LoadCallback<java.lang.Integer, com.eazy.daiku.data.model.MyTransaction> callback) {
    }
    
    private final java.util.ArrayList<com.eazy.daiku.data.model.MyTransaction> migrateTrx(com.eazy.daiku.data.model.server_model.TransactionRespond trxRespond) {
        return null;
    }
}