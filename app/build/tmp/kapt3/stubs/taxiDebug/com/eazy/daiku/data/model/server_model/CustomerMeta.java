package com.eazy.daiku.data.model.server_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0015\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BQ\u0012\u0018\b\u0002\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\u0018\b\u0002\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0003j\b\u0012\u0004\u0012\u00020\t`\u0005\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\fJ\u0019\u0010\u001b\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u0019\u0010\u001d\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0003j\b\u0012\u0004\u0012\u00020\t`\u0005H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003JU\u0010\u001f\u001a\u00020\u00002\u0018\b\u0002\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0018\b\u0002\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0003j\b\u0012\u0004\u0012\u00020\t`\u00052\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001J\u0013\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010#\u001a\u00020$H\u00d6\u0001J\t\u0010%\u001a\u00020\u000bH\u00d6\u0001R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R.\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R.\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\t0\u0003j\b\u0012\u0004\u0012\u00020\t`\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0012\"\u0004\b\u0016\u0010\u0014R \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006&"}, d2 = {"Lcom/eazy/daiku/data/model/server_model/CustomerMeta;", "", "kessFees", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/CustomerKessFees;", "Lkotlin/collections/ArrayList;", "destination", "Lcom/eazy/daiku/data/model/server_model/CustomerDestination;", "paymentTypeFees", "Lcom/eazy/daiku/data/model/server_model/CustomerPaymentTypeFees;", "qrcodeurl", "", "(Ljava/util/ArrayList;Lcom/eazy/daiku/data/model/server_model/CustomerDestination;Ljava/util/ArrayList;Ljava/lang/String;)V", "getDestination", "()Lcom/eazy/daiku/data/model/server_model/CustomerDestination;", "setDestination", "(Lcom/eazy/daiku/data/model/server_model/CustomerDestination;)V", "getKessFees", "()Ljava/util/ArrayList;", "setKessFees", "(Ljava/util/ArrayList;)V", "getPaymentTypeFees", "setPaymentTypeFees", "getQrcodeurl", "()Ljava/lang/String;", "setQrcodeurl", "(Ljava/lang/String;)V", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_taxiDebug"})
public final class CustomerMeta {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "kess_fees")
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerKessFees> kessFees;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "destination")
    private com.eazy.daiku.data.model.server_model.CustomerDestination destination;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "payment_type_fees")
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerPaymentTypeFees> paymentTypeFees;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "qr_code_url")
    private java.lang.String qrcodeurl;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.model.server_model.CustomerMeta copy(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerKessFees> kessFees, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.CustomerDestination destination, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerPaymentTypeFees> paymentTypeFees, @org.jetbrains.annotations.Nullable()
    java.lang.String qrcodeurl) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public CustomerMeta() {
        super();
    }
    
    public CustomerMeta(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerKessFees> kessFees, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.CustomerDestination destination, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerPaymentTypeFees> paymentTypeFees, @org.jetbrains.annotations.Nullable()
    java.lang.String qrcodeurl) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerKessFees> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerKessFees> getKessFees() {
        return null;
    }
    
    public final void setKessFees(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerKessFees> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.CustomerDestination component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.CustomerDestination getDestination() {
        return null;
    }
    
    public final void setDestination(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.CustomerDestination p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerPaymentTypeFees> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerPaymentTypeFees> getPaymentTypeFees() {
        return null;
    }
    
    public final void setPaymentTypeFees(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.CustomerPaymentTypeFees> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getQrcodeurl() {
        return null;
    }
    
    public final void setQrcodeurl(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}