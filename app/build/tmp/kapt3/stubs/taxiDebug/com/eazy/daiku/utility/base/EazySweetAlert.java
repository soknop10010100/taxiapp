package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0007\u001a\u00020\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH\u0014\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/utility/base/EazySweetAlert;", "Lcn/pedant/SweetAlert/SweetAlertDialog;", "context", "Landroid/content/Context;", "alertType", "", "(Landroid/content/Context;I)V", "onCreate", "", "savedInstanceState", "Landroid/os/Bundle;", "app_taxiDebug"})
public final class EazySweetAlert extends cn.pedant.SweetAlert.SweetAlertDialog {
    
    public EazySweetAlert(@org.jetbrains.annotations.Nullable()
    android.content.Context context, int alertType) {
        super(null);
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
}