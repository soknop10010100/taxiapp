package com.eazy.daiku.utility.view_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\r\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dJ\u0006\u0010\u001e\u001a\u00020\u001bJ*\u0010\u001f\u001a\u00020\u001b2\u0006\u0010 \u001a\u00020!2\u0006\u0010\u001c\u001a\u00020\u001d2\u0012\u0010\"\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u001b0#R\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\r0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0010\u0010\u0011R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\r0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0011R\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\r0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0015\u0010\u0011R\u001d\u0010\u0016\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0011R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019\u00a8\u0006$"}, d2 = {"Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_dataMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "_fetchProcessingMutableLiveData", "_loadingMutableLiveData", "", "_loadingScanQrMutableLiveData", "fetchProcessingMutableLiveData", "getFetchProcessingMutableLiveData", "()Landroidx/lifecycle/MutableLiveData;", "loadingMutableLiveData", "getLoadingMutableLiveData", "loadingScanQrMutableLiveData", "getLoadingScanQrMutableLiveData", "qrCodeDataMutableLiveData", "getQrCodeDataMutableLiveData", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "fetchDataQrCode", "", "code", "", "fetchTripProcessing", "requestTripToServer", "tipEnum", "Lcom/eazy/daiku/utility/enumerable/TripEnum;", "tripListener", "Lkotlin/Function1;", "app_taxiDebug"})
public final class QrCodeVm extends com.eazy.daiku.utility.base.BaseViewModel {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingScanQrMutableLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> _dataMutableLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingMutableLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> _fetchProcessingMutableLiveData = null;
    
    @javax.inject.Inject()
    public QrCodeVm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingScanQrMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> getQrCodeDataMutableLiveData() {
        return null;
    }
    
    public final void fetchDataQrCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
    }
    
    public final void requestTripToServer(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.TripEnum tipEnum, @org.jetbrains.annotations.NotNull()
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> tripListener) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> getFetchProcessingMutableLiveData() {
        return null;
    }
    
    public final void fetchTripProcessing() {
    }
}