package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000~\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\u0010$\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0006\n\u0002\b\u000e\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0014J\u0016\u0010(\u001a\u00020%2\u0006\u0010)\u001a\u00020*2\u0006\u0010+\u001a\u00020\u000bJ\b\u0010,\u001a\u0004\u0018\u00010\u000bJ\b\u0010-\u001a\u0004\u0018\u00010\u0000J\b\u0010.\u001a\u0004\u0018\u00010\u000bJ\b\u0010/\u001a\u0004\u0018\u000100J\u0006\u00101\u001a\u00020%J\u000e\u00102\u001a\u00020%2\u0006\u00103\u001a\u00020\u000bJ*\u00104\u001a\u00020%2\u0006\u00105\u001a\u00020\'2\u0006\u00103\u001a\u00020\u000b2\u0012\u00106\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020%07J\u0006\u00108\u001a\u00020\rJ\u0006\u00109\u001a\u00020\u000bJ\b\u0010:\u001a\u00020%H\u0014J\u000e\u0010;\u001a\u00020%2\u0006\u0010;\u001a\u00020\u000bJ\u0016\u0010<\u001a\u00020%2\u0006\u0010=\u001a\u00020>2\u0006\u0010?\u001a\u00020>J\u000e\u0010@\u001a\u00020%2\u0006\u0010A\u001a\u00020\u000bJ\u0010\u0010B\u001a\u00020%2\u0006\u0010C\u001a\u00020\u000bH\u0002J\u000e\u0010D\u001a\u00020%2\u0006\u0010E\u001a\u000200J\u0017\u0010F\u001a\u0002HG\"\n\b\u0000\u0010G*\u0004\u0018\u00010\u0000\u00a2\u0006\u0002\u0010HJ\u0018\u0010I\u001a\u00020%2\u0006\u0010J\u001a\u00020\u00002\b\b\u0001\u0010K\u001a\u00020\u000bR\u001d\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR/\u0010\t\u001a \u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\r0\f0\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\bR#\u0010\u000f\u001a\u0014\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\u0004\u0012\u00020\u00100\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\bR)\u0010\u0012\u001a\u001a\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u00130\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\bR\u001d\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\r0\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\bR\u001c\u0010\u0017\u001a\u0004\u0018\u00010\u0018X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001e\u0010\u001d\u001a\u00020\u001e8\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u0010\u0010#\u001a\u0004\u0018\u00010\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006L"}, d2 = {"Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "Ldagger/android/support/DaggerAppCompatActivity;", "()V", "activityLauncher", "Lcom/eazy/daiku/utility/BetterActivityResult;", "Landroid/content/Intent;", "Landroidx/activity/result/ActivityResult;", "getActivityLauncher", "()Lcom/eazy/daiku/utility/BetterActivityResult;", "activityMultiPermission", "", "", "", "", "getActivityMultiPermission", "activityOpenDocument", "Landroid/net/Uri;", "getActivityOpenDocument", "activityOpenMultiDocuments", "", "getActivityOpenMultiDocuments", "activityPermission", "getActivityPermission", "easyImageCallbacks", "Lpl/aprilapps/easyphotopicker/EasyImage$Callbacks;", "getEasyImageCallbacks", "()Lpl/aprilapps/easyphotopicker/EasyImage$Callbacks;", "setEasyImageCallbacks", "(Lpl/aprilapps/easyphotopicker/EasyImage$Callbacks;)V", "factory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "getFactory", "()Landroidx/lifecycle/ViewModelProvider$Factory;", "setFactory", "(Landroidx/lifecycle/ViewModelProvider$Factory;)V", "globalCurrentCoreActivity", "attachBaseContext", "", "base", "Landroid/content/Context;", "convertAsBitmap", "imageBitmap", "Landroid/widget/ImageView;", "image", "getBiometric", "getGlobalCurrentCoreActivity", "getPassWordUser", "getUserUserToSharePreference", "Lcom/eazy/daiku/data/model/server_model/User;", "globalRequestDeviceGps", "globalShowError", "text", "globalShowSuccessWithDismiss", "context", "onDismiss", "Lkotlin/Function1;", "hasInternetConnection", "hasUserInSharePreference", "onResume", "saveBiometric", "saveCurrentGpsUser", "lat", "", "lng", "savePassWordUser", "passWord", "savePhoneNumberUser", "phoneNumber", "saveUserToSharePreference", "user", "self", "T", "()Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "setNewLocale", "mContext", "language", "app_taxiDebug"})
public class BaseCoreActivity extends dagger.android.support.DaggerAppCompatActivity {
    @javax.inject.Inject()
    public androidx.lifecycle.ViewModelProvider.Factory factory;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.BetterActivityResult<android.content.Intent, androidx.activity.result.ActivityResult> activityLauncher = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.BetterActivityResult<java.lang.String, java.lang.Boolean> activityPermission = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.BetterActivityResult<java.lang.String[], java.util.Map<java.lang.String, java.lang.Boolean>> activityMultiPermission = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.BetterActivityResult<java.lang.String[], java.util.List<android.net.Uri>> activityOpenMultiDocuments = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.utility.BetterActivityResult<java.lang.String[], android.net.Uri> activityOpenDocument = null;
    @org.jetbrains.annotations.Nullable()
    private pl.aprilapps.easyphotopicker.EasyImage.Callbacks easyImageCallbacks;
    private com.eazy.daiku.utility.base.BaseCoreActivity globalCurrentCoreActivity;
    
    public BaseCoreActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.ViewModelProvider.Factory getFactory() {
        return null;
    }
    
    public final void setFactory(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.BetterActivityResult<android.content.Intent, androidx.activity.result.ActivityResult> getActivityLauncher() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.BetterActivityResult<java.lang.String, java.lang.Boolean> getActivityPermission() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.BetterActivityResult<java.lang.String[], java.util.Map<java.lang.String, java.lang.Boolean>> getActivityMultiPermission() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.BetterActivityResult<java.lang.String[], java.util.List<android.net.Uri>> getActivityOpenMultiDocuments() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.BetterActivityResult<java.lang.String[], android.net.Uri> getActivityOpenDocument() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final pl.aprilapps.easyphotopicker.EasyImage.Callbacks getEasyImageCallbacks() {
        return null;
    }
    
    public final void setEasyImageCallbacks(@org.jetbrains.annotations.Nullable()
    pl.aprilapps.easyphotopicker.EasyImage.Callbacks p0) {
    }
    
    public final <T extends com.eazy.daiku.utility.base.BaseCoreActivity>T self() {
        return null;
    }
    
    @java.lang.Override()
    protected void attachBaseContext(@org.jetbrains.annotations.Nullable()
    android.content.Context base) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.utility.base.BaseCoreActivity getGlobalCurrentCoreActivity() {
        return null;
    }
    
    public final void globalShowError(@org.jetbrains.annotations.NotNull()
    java.lang.String text) {
    }
    
    public final void globalRequestDeviceGps() {
    }
    
    public final boolean hasInternetConnection() {
        return false;
    }
    
    public final void globalShowSuccessWithDismiss(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String text, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> onDismiss) {
    }
    
    public final void setNewLocale(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseCoreActivity mContext, @org.jetbrains.annotations.NotNull()
    @com.eazy.daiku.utility.other.LocaleManager.LocaleDef()
    java.lang.String language) {
    }
    
    public final void saveUserToSharePreference(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.server_model.User user) {
    }
    
    private final void savePhoneNumberUser(java.lang.String phoneNumber) {
    }
    
    public final void savePassWordUser(@org.jetbrains.annotations.NotNull()
    java.lang.String passWord) {
    }
    
    public final void saveCurrentGpsUser(double lat, double lng) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.User getUserUserToSharePreference() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPassWordUser() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String hasUserInSharePreference() {
        return null;
    }
    
    public final void saveBiometric(@org.jetbrains.annotations.NotNull()
    java.lang.String saveBiometric) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBiometric() {
        return null;
    }
    
    public final void convertAsBitmap(@org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageBitmap, @org.jetbrains.annotations.NotNull()
    java.lang.String image) {
    }
}