package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\f\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0002\u0011\u0012B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0007X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/eazy/daiku/utility/Constants;", "", "()V", "EAZYTAXI_PREF_NAME", "", "biometricKey", "endTrip", "", "passWordKey", "phoneNumberKey", "pretty_gson", "saveFirstBankAccount", "saveListAccountNumberBank", "saveListSearchHistoryMap", "startTrip", "tripStateKey", "userDetailKey", "PhoneConfig", "Token", "app_taxiDebug"})
public final class Constants {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.Constants INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EAZYTAXI_PREF_NAME = "EAZYTAXI_PREF_NAME";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String pretty_gson = "eazyLog";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String userDetailKey = "userDetailKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String biometricKey = "biometricKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String phoneNumberKey = "phoneNumberKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String passWordKey = "passWordKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String tripStateKey = "tripStateKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String saveListAccountNumberBank = "saveABAAccountNumberBank";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String saveFirstBankAccount = "saveFirstBankAccount";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String saveListSearchHistoryMap = "saveListSearchHistoryMap";
    public static final int startTrip = 1;
    public static final int endTrip = 0;
    
    private Constants() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/utility/Constants$Token;", "", "()V", "API_TOKEN", "", "app_taxiDebug"})
    public static final class Token {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Constants.Token INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String API_TOKEN = "api_token";
        
        private Token() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/Constants$PhoneConfig;", "", "()V", "PHONE_CONFIRMATION_CODE", "", "PHONE_MAX_LENGTH", "PHONE_MIN_LENGTH", "app_taxiDebug"})
    public static final class PhoneConfig {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Constants.PhoneConfig INSTANCE = null;
        public static final int PHONE_MIN_LENGTH = 9;
        public static final int PHONE_MAX_LENGTH = 19;
        public static final int PHONE_CONFIRMATION_CODE = 6;
        
        private PhoneConfig() {
            super();
        }
    }
}