package com.eazy.daiku.utility.permission_media;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010$\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J=\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0018\u0010\n\u001a\u0014\u0012\u0010\u0012\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\r0\f0\u000bH\u0002\u00a2\u0006\u0002\u0010\u000eJ\u000e\u0010\u000f\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0006J\u000e\u0010\u0011\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0006J\u000e\u0010\u0012\u001a\u00020\r2\u0006\u0010\u0005\u001a\u00020\u0013J\u000e\u0010\u0014\u001a\u00020\r2\u0006\u0010\u0010\u001a\u00020\u0006J\"\u0010\u0015\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00062\u0012\u0010\u0016\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u0017J5\u0010\u0018\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00062\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\b2\u0012\u0010\u0019\u001a\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00040\u0017\u00a2\u0006\u0002\u0010\u001a\u00a8\u0006\u001b"}, d2 = {"Lcom/eazy/daiku/utility/permission_media/PermissionHelper;", "", "()V", "checkedRequestMultiPermission", "", "activity", "Landroid/content/Context;", "param", "", "", "activityResult", "Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;", "", "", "(Landroid/content/Context;[Ljava/lang/String;Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;)V", "hasCOARSEAndFINELocationPermission", "context", "hasCameraAndExternalStoragePermission", "hasDeviceGpsAndNetwork", "Landroid/app/Activity;", "hasExternalStoragePermission", "requestCameraAndWriteExternalStorageCamera", "permissionListenerCallBack", "Lkotlin/Function1;", "requestMultiPermission", "callBack", "(Landroid/content/Context;[Ljava/lang/String;Lkotlin/jvm/functions/Function1;)V", "app_taxiDebug"})
public final class PermissionHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.permission_media.PermissionHelper INSTANCE = null;
    
    private PermissionHelper() {
        super();
    }
    
    /**
     * request all permission not available
     */
    public final void requestCameraAndWriteExternalStorageCamera(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> permissionListenerCallBack) {
    }
    
    public final void requestMultiPermission(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String[] param, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> callBack) {
    }
    
    private final void checkedRequestMultiPermission(android.content.Context activity, java.lang.String[] param, com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<java.util.Map<java.lang.String, java.lang.Boolean>> activityResult) {
    }
    
    /**
     * checking all permission are available
     */
    public final boolean hasCameraAndExternalStoragePermission(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final boolean hasExternalStoragePermission(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final boolean hasCOARSEAndFINELocationPermission(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    public final boolean hasDeviceGpsAndNetwork(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity) {
        return false;
    }
}