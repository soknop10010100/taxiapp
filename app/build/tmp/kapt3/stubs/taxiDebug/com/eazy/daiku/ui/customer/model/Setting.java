package com.eazy.daiku.ui.customer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0018\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\u0018\b\u0002\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0007j\b\u0012\u0004\u0012\u00020\u0003`\b\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u001a\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0010\u0010\u001b\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0016J\u0019\u0010\u001c\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0007j\b\u0012\u0004\u0012\u00020\u0003`\bH\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003JL\u0010\u001e\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\u0018\b\u0002\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0007j\b\u0012\u0004\u0012\u00020\u0003`\b2\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003H\u00c6\u0001\u00a2\u0006\u0002\u0010\u001fJ\u0013\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010#\u001a\u00020\u0005H\u00d6\u0001J\t\u0010$\u001a\u00020\u0003H\u00d6\u0001R.\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0007j\b\u0012\u0004\u0012\u00020\u0003`\b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R \u0010\t\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0010\"\u0004\b\u0014\u0010\u0012R\"\u0010\u0004\u001a\u0004\u0018\u00010\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0019\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018\u00a8\u0006%"}, d2 = {"Lcom/eazy/daiku/ui/customer/model/Setting;", "", "paymentType", "", "zoomScreen", "", "enabledPaymentMethods", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "template", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/util/ArrayList;Ljava/lang/String;)V", "getEnabledPaymentMethods", "()Ljava/util/ArrayList;", "setEnabledPaymentMethods", "(Ljava/util/ArrayList;)V", "getPaymentType", "()Ljava/lang/String;", "setPaymentType", "(Ljava/lang/String;)V", "getTemplate", "setTemplate", "getZoomScreen", "()Ljava/lang/Integer;", "setZoomScreen", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "component1", "component2", "component3", "component4", "copy", "(Ljava/lang/String;Ljava/lang/Integer;Ljava/util/ArrayList;Ljava/lang/String;)Lcom/eazy/daiku/ui/customer/model/Setting;", "equals", "", "other", "hashCode", "toString", "app_taxiDebug"})
public final class Setting {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "payment_type")
    private java.lang.String paymentType;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "zoom_screen")
    private java.lang.Integer zoomScreen;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "enabled_payment_methods")
    private java.util.ArrayList<java.lang.String> enabledPaymentMethods;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "template")
    private java.lang.String template;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.ui.customer.model.Setting copy(@org.jetbrains.annotations.Nullable()
    java.lang.String paymentType, @org.jetbrains.annotations.Nullable()
    java.lang.Integer zoomScreen, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> enabledPaymentMethods, @org.jetbrains.annotations.Nullable()
    java.lang.String template) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Setting() {
        super();
    }
    
    public Setting(@org.jetbrains.annotations.Nullable()
    java.lang.String paymentType, @org.jetbrains.annotations.Nullable()
    java.lang.Integer zoomScreen, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> enabledPaymentMethods, @org.jetbrains.annotations.Nullable()
    java.lang.String template) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPaymentType() {
        return null;
    }
    
    public final void setPaymentType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getZoomScreen() {
        return null;
    }
    
    public final void setZoomScreen(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getEnabledPaymentMethods() {
        return null;
    }
    
    public final void setEnabledPaymentMethods(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTemplate() {
        return null;
    }
    
    public final void setTemplate(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}