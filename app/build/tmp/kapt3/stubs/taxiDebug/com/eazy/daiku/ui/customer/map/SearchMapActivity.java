package com.eazy.daiku.ui.customer.map;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000V\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u0000 %2\u00020\u0001:\u0001%B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\u0012\u0010\u001a\u001a\u00020\u00162\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020\u0016H\u0002J\u0018\u0010\"\u001a\u00020\u00162\u0006\u0010#\u001a\u00020\u001e2\u0006\u0010$\u001a\u00020\u001eH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006&"}, d2 = {"Lcom/eazy/daiku/ui/customer/map/SearchMapActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivitySearchMapBinding;", "historySearMapAdapter", "Lcom/eazy/daiku/ui/customer/adapter/HistorySearMapAdapter;", "listSearchMapHistory", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/SearchMapHistoryModel;", "Lkotlin/collections/ArrayList;", "searchMapAdapter", "Lcom/eazy/daiku/ui/customer/adapter/SearchMapAdapter;", "userId", "", "viewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "getViewModel", "()Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "initAction", "", "initObserver", "initSearchHistoryMap", "initView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "saveTextSearchMap", "switchView", "haveDataSearch", "haveHistory", "Companion", "app_taxiDebug"})
public final class SearchMapActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivitySearchMapBinding binding;
    private int userId = -100;
    private final kotlin.Lazy viewModel$delegate = null;
    private com.eazy.daiku.ui.customer.adapter.SearchMapAdapter searchMapAdapter;
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel> listSearchMapHistory;
    private com.eazy.daiku.ui.customer.adapter.HistorySearMapAdapter historySearMapAdapter;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.map.SearchMapActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String geometyModelKey = "geometyModelKey";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String keySearch = "";
    
    public SearchMapActivity() {
        super();
    }
    
    private final com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initAction() {
    }
    
    private final void initSearchHistoryMap() {
    }
    
    private final void initObserver() {
    }
    
    private final void switchView(boolean haveDataSearch, boolean haveHistory) {
    }
    
    private final void saveTextSearchMap() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\b\u00a8\u0006\f"}, d2 = {"Lcom/eazy/daiku/ui/customer/map/SearchMapActivity$Companion;", "", "()V", "geometyModelKey", "", "getGeometyModelKey", "()Ljava/lang/String;", "setGeometyModelKey", "(Ljava/lang/String;)V", "keySearch", "getKeySearch", "setKeySearch", "app_taxiDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getGeometyModelKey() {
            return null;
        }
        
        public final void setGeometyModelKey(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getKeySearch() {
            return null;
        }
        
        public final void setKeySearch(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
    }
}