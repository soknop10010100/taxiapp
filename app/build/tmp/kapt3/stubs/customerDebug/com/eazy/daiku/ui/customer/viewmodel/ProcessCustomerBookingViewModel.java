package com.eazy.daiku.ui.customer.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b5\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u000e\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J*\u0010Q\u001a\u00020R2\"\u0010S\u001a\u001e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V0Tj\u000e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V`WJ\u0006\u0010X\u001a\u00020RJ\u000e\u0010Y\u001a\u00020R2\u0006\u0010Z\u001a\u00020UJ*\u0010[\u001a\u00020R2\"\u0010S\u001a\u001e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V0Tj\u000e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V`WJ*\u0010\\\u001a\u00020R2\"\u0010S\u001a\u001e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V0Tj\u000e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V`WJ*\u0010]\u001a\u00020R2\"\u0010^\u001a\u001e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V0Tj\u000e\u0012\u0004\u0012\u00020U\u0012\u0004\u0012\u00020V`WJ\u000e\u0010_\u001a\u00020R2\u0006\u0010`\u001a\u00020\u001cJ\u0006\u0010a\u001a\u00020RJ\u000e\u0010b\u001a\u00020R2\u0006\u0010c\u001a\u00020UJ\u000e\u0010d\u001a\u00020R2\u0006\u0010c\u001a\u00020UR\u001a\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0013\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u0014\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\u00190\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00190\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001e\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010 \u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010!\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010#\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010$\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010%\u001a\b\u0012\u0004\u0012\u00020\u001c0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\'\u0010(R\u001d\u0010)\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b*\u0010(R\u001d\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b,\u0010(R\u001d\u0010-\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b.\u0010(R\u001d\u0010/\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b0\u0010(R\u001d\u00101\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b2\u0010(R#\u00103\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00150\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b4\u0010(R\u001d\u00105\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b6\u0010(R\u0017\u00107\u001a\b\u0012\u0004\u0012\u00020\u00190\b8F\u00a2\u0006\u0006\u001a\u0004\b8\u0010(R\u0017\u00109\u001a\b\u0012\u0004\u0012\u00020\u00190\b8F\u00a2\u0006\u0006\u001a\u0004\b:\u0010(R\u0017\u0010;\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\b<\u0010(R\u0017\u0010=\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\b>\u0010(R\u0017\u0010?\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\b@\u0010(R\u0017\u0010A\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bB\u0010(R\u0017\u0010C\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bD\u0010(R\u0017\u0010E\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bF\u0010(R\u0017\u0010G\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bH\u0010(R\u0017\u0010I\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bJ\u0010(R\u0017\u0010K\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bL\u0010(R\u0017\u0010M\u001a\b\u0012\u0004\u0012\u00020\u001c0\b8F\u00a2\u0006\u0006\u001a\u0004\bN\u0010(R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\bO\u0010P\u00a8\u0006e"}, d2 = {"Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_dataBookingPreviewDoCheckoutLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/BookingPreviewDoCheckoutModel;", "_dataBookingProcessingLiveData", "Lcom/example/example/BookingProcessingModel;", "_dataCancelBookingLiveData", "Lcom/google/gson/JsonElement;", "_dataCheckoutLiveData", "Lcom/eazy/daiku/ui/customer/model/WebPayRespondModel;", "_dataCheckoutTaxiLiveData", "Lcom/eazy/daiku/ui/customer/model/PreviewCheckoutModel;", "_dataGetTaxiDriverLiveData", "_dataListAllLocationKioskLiveData", "", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "_dataSearchLocationKioskLiveData", "_dataSearchMapLiveData", "Lcom/eazy/daiku/ui/customer/model/sear_map/SearchMapModel;", "_dataSearchMapLocalLiveData", "_loadingBookingPreviewDoCheckoutLiveData", "", "_loadingBookingProcessingLiveData", "_loadingCancelBookingLiveData", "_loadingCheckoutLiveData", "_loadingCheckoutTaxiLiveData", "_loadingGetTaxiDriverLiveData", "_loadingListAllLocationKioskLiveData", "_loadingSearchLocationKioskLiveData", "_loadingSearchMapLiveData", "_loadingSearchMapLocalLiveData", "dataBookingPreviewDoCheckoutLiveData", "getDataBookingPreviewDoCheckoutLiveData", "()Landroidx/lifecycle/MutableLiveData;", "dataBookingProcessingLiveData", "getDataBookingProcessingLiveData", "dataCancelBookingLiveData", "getDataCancelBookingLiveData", "dataCheckoutLiveData", "getDataCheckoutLiveData", "dataCheckoutTaxiLiveData", "getDataCheckoutTaxiLiveData", "dataGetTaxiDriverLiveData", "getDataGetTaxiDriverLiveData", "dataListAllLocationKioskLiveData", "getDataListAllLocationKioskLiveData", "dataSearchLocationKioskLiveData", "getDataSearchLocationKioskLiveData", "dataSearchMapLiveData", "getDataSearchMapLiveData", "dataSearchMapLocalLiveData", "getDataSearchMapLocalLiveData", "loadingBookingPreviewDoCheckoutLiveData", "getLoadingBookingPreviewDoCheckoutLiveData", "loadingBookingProcessingLiveData", "getLoadingBookingProcessingLiveData", "loadingCancelBookingLiveData", "getLoadingCancelBookingLiveData", "loadingCheckoutLiveData", "getLoadingCheckoutLiveData", "loadingCheckoutTaxiLiveData", "getLoadingCheckoutTaxiLiveData", "loadingGetTaxiDriverLiveData", "getLoadingGetTaxiDriverLiveData", "loadingListAllLocationKioskLiveData", "getLoadingListAllLocationKioskLiveData", "loadingSearchLocationKioskLiveData", "getLoadingSearchLocationKioskLiveData", "loadingSearchMapLiveData", "getLoadingSearchMapLiveData", "loadingSearchMapLocalLiveData", "getLoadingSearchMapLocalLiveData", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "submitBookingPreviewDoCheckout", "", "body", "Ljava/util/HashMap;", "", "", "Lkotlin/collections/HashMap;", "submitBookingProcessing", "submitCancelBooingTaxi", "code", "submitCheckout", "submitCheckoutTaxi", "submitGetTaxiDriver", "map", "submitListAllLocationKiosk", "allDevice", "submitSearchLocationKioskNearBy", "submitSearchMap", "textSearchMap", "submitSearchMapLocal", "app_customerDebug"})
public final class ProcessCustomerBookingViewModel extends com.eazy.daiku.utility.base.BaseViewModel {
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingSearchMapLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> _dataSearchMapLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingSearchMapLocalLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> _dataSearchMapLocalLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingSearchLocationKioskLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.ListKioskModel>> _dataSearchLocationKioskLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingGetTaxiDriverLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _dataGetTaxiDriverLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingCheckoutTaxiLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.PreviewCheckoutModel>> _dataCheckoutTaxiLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingListAllLocationKioskLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.ui.customer.model.ListKioskModel>>> _dataListAllLocationKioskLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingCheckoutLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.WebPayRespondModel>> _dataCheckoutLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingBookingPreviewDoCheckoutLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.BookingPreviewDoCheckoutModel>> _dataBookingPreviewDoCheckoutLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingBookingProcessingLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.example.example.BookingProcessingModel>> _dataBookingProcessingLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingCancelBookingLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _dataCancelBookingLiveData = null;
    
    @javax.inject.Inject()
    public ProcessCustomerBookingViewModel(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingSearchMapLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> getDataSearchMapLiveData() {
        return null;
    }
    
    public final void submitSearchMap(@org.jetbrains.annotations.NotNull()
    java.lang.String textSearchMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingSearchMapLocalLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> getDataSearchMapLocalLiveData() {
        return null;
    }
    
    public final void submitSearchMapLocal(@org.jetbrains.annotations.NotNull()
    java.lang.String textSearchMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingSearchLocationKioskLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.ListKioskModel>> getDataSearchLocationKioskLiveData() {
        return null;
    }
    
    public final void submitSearchLocationKioskNearBy() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingGetTaxiDriverLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getDataGetTaxiDriverLiveData() {
        return null;
    }
    
    public final void submitGetTaxiDriver(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> map) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingCheckoutTaxiLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.PreviewCheckoutModel>> getDataCheckoutTaxiLiveData() {
        return null;
    }
    
    public final void submitCheckoutTaxi(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> body) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingListAllLocationKioskLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.ui.customer.model.ListKioskModel>>> getDataListAllLocationKioskLiveData() {
        return null;
    }
    
    public final void submitListAllLocationKiosk(boolean allDevice) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingCheckoutLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.WebPayRespondModel>> getDataCheckoutLiveData() {
        return null;
    }
    
    public final void submitCheckout(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> body) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingBookingPreviewDoCheckoutLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.BookingPreviewDoCheckoutModel>> getDataBookingPreviewDoCheckoutLiveData() {
        return null;
    }
    
    public final void submitBookingPreviewDoCheckout(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> body) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingBookingProcessingLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.example.example.BookingProcessingModel>> getDataBookingProcessingLiveData() {
        return null;
    }
    
    public final void submitBookingProcessing() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingCancelBookingLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getDataCancelBookingLiveData() {
        return null;
    }
    
    public final void submitCancelBooingTaxi(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
    }
}