package com.eazy.daiku.utility.view_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0000\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J&\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\u001e0\u00122\u0012\u0010\u001f\u001a\u000e\u0012\u0004\u0012\u00020!\u0012\u0004\u0012\u00020\"0 R \u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u000f0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0010\u001a\b\u0012\u0004\u0012\u00020\u000f0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0011\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t0\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u0017\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\r0\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0016\u0010\u0014R\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0014R\u0017\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\u000f0\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0014R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001c\u00a8\u0006#"}, d2 = {"Lcom/eazy/daiku/utility/view_model/CustomerHistoryVm;", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_customerErrorPaginationMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "", "Lcom/eazy/daiku/data/model/server_model/CustomerHistoryModel;", "_customerHasHistoryListsMutableLiveData", "", "_customerLoadingMutableLiveData", "", "_customerLoadingPaginationMutableLiveData", "customerErrorPaginationMutableLiveData", "Landroidx/lifecycle/LiveData;", "getCustomerErrorPaginationMutableLiveData", "()Landroidx/lifecycle/LiveData;", "customerHasHistoryListsMutableLiveData", "getCustomerHasHistoryListsMutableLiveData", "customerLoadingMutableLiveData", "getCustomerLoadingMutableLiveData", "customerLoadingPaginationMutableLiveData", "getCustomerLoadingPaginationMutableLiveData", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "submitCustomerHistory", "Landroidx/paging/PagedList;", "bodyQuery", "Ljava/util/HashMap;", "", "", "app_customerDebug"})
public final class CustomerHistoryVm extends com.eazy.daiku.utility.base.BaseViewModel {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _customerLoadingMutableLiveData;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _customerLoadingPaginationMutableLiveData;
    private androidx.lifecycle.MutableLiveData<java.lang.Integer> _customerHasHistoryListsMutableLiveData;
    private androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.CustomerHistoryModel>>> _customerErrorPaginationMutableLiveData;
    
    @javax.inject.Inject()
    public CustomerHistoryVm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getCustomerLoadingMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Boolean> getCustomerLoadingPaginationMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<java.lang.Integer> getCustomerHasHistoryListsMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.CustomerHistoryModel>>> getCustomerErrorPaginationMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CustomerHistoryModel>> submitCustomerHistory(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyQuery) {
        return null;
    }
}