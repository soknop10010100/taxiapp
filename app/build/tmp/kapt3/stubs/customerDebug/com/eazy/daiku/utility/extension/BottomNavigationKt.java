package com.eazy.daiku.utility.extension;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\u000e\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\n\u0010\u0003\u001a\u00020\u0001*\u00020\u0002\u00a8\u0006\u0004"}, d2 = {"hide", "", "Lcom/google/android/material/bottomnavigation/BottomNavigationView;", "show", "app_customerDebug"})
public final class BottomNavigationKt {
    
    /**
     * Potentially animate showing a [BottomNavigationView].
     *
     * Abruptly changing the visibility leads to a re-layout of main content, animating
     * `translationY` leaves a gap where the view was that content does not fill.
     *
     * Instead, take a snapshot of the view, and animate this in, only changing the visibility (and
     * thus layout) when the animation completes.
     */
    public static final void show(@org.jetbrains.annotations.NotNull()
    com.google.android.material.bottomnavigation.BottomNavigationView $this$show) {
    }
    
    /**
     * Potentially animate hiding a [BottomNavigationView].
     *
     * Abruptly changing the visibility leads to a re-layout of main content, animating
     * `translationY` leaves a gap where the view was that content does not fill.
     *
     * Instead, take a snapshot, instantly hide the view (so content lays out to fill), then animate
     * out the snapshot.
     */
    public static final void hide(@org.jetbrains.annotations.NotNull()
    com.google.android.material.bottomnavigation.BottomNavigationView $this$hide) {
    }
}