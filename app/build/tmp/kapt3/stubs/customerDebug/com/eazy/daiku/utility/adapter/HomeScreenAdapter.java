package com.eazy.daiku.utility.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\u000e\u001a\u00020\t2\u0006\u0010\u0004\u001a\u00020\u0006J\u0014\u0010\u000e\u001a\u00020\t2\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005J\b\u0010\u000f\u001a\u00020\u0010H\u0016J\u0018\u0010\u0011\u001a\u00020\t2\u0006\u0010\u0012\u001a\u00020\u00022\u0006\u0010\u0013\u001a\u00020\u0010H\u0016J\u0018\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0010H\u0016J\u000e\u0010\u0018\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001aJ\u0016\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001c\u001a\u00020\u001dR\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R*\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\u0006\u0012\u0006\u0012\u0004\u0018\u00010\t\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u001f"}, d2 = {"Lcom/eazy/daiku/utility/adapter/HomeScreenAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/utility/adapter/HomeScreenAdapter$HomeScreenHolder;", "()V", "homeScreenModel", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/HomeScreenModel;", "selectedRow", "Lkotlin/Function1;", "", "getSelectedRow", "()Lkotlin/jvm/functions/Function1;", "setSelectedRow", "(Lkotlin/jvm/functions/Function1;)V", "addHomeScreen", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "removeView", "homeActionEnum", "Lcom/eazy/daiku/utility/enumerable/HomeScreenActionEnum;", "updateView", "isEnableUi", "", "HomeScreenHolder", "app_customerDebug"})
public final class HomeScreenAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.utility.adapter.HomeScreenAdapter.HomeScreenHolder> {
    private java.util.ArrayList<com.eazy.daiku.data.model.HomeScreenModel> homeScreenModel;
    @org.jetbrains.annotations.Nullable()
    private kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.HomeScreenModel, kotlin.Unit> selectedRow;
    
    public HomeScreenAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.HomeScreenModel, kotlin.Unit> getSelectedRow() {
        return null;
    }
    
    public final void setSelectedRow(@org.jetbrains.annotations.Nullable()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.HomeScreenModel, kotlin.Unit> p0) {
    }
    
    public final void addHomeScreen(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.HomeScreenModel> homeScreenModel) {
    }
    
    public final void addHomeScreen(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.HomeScreenModel homeScreenModel) {
    }
    
    public final void updateView(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.HomeScreenActionEnum homeActionEnum, boolean isEnableUi) {
    }
    
    public final void removeView(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.HomeScreenActionEnum homeActionEnum) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.utility.adapter.HomeScreenAdapter.HomeScreenHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.adapter.HomeScreenAdapter.HomeScreenHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0011"}, d2 = {"Lcom/eazy/daiku/utility/adapter/HomeScreenAdapter$HomeScreenHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "homeScreenContainer", "Landroidx/cardview/widget/CardView;", "getHomeScreenContainer", "()Landroidx/cardview/widget/CardView;", "iconImg", "Landroid/widget/ImageView;", "titleTv", "Landroid/widget/TextView;", "bind", "", "homeScreenModel", "Lcom/eazy/daiku/data/model/HomeScreenModel;", "app_customerDebug"})
    public static final class HomeScreenHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final android.widget.ImageView iconImg = null;
        private final android.widget.TextView titleTv = null;
        @org.jetbrains.annotations.NotNull()
        private final androidx.cardview.widget.CardView homeScreenContainer = null;
        
        public HomeScreenHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final androidx.cardview.widget.CardView getHomeScreenContainer() {
            return null;
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.HomeScreenModel homeScreenModel) {
        }
    }
}