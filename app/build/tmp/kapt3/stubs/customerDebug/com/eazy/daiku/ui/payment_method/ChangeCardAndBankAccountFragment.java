package com.eazy.daiku.ui.payment_method;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 *2\u00020\u0001:\u0002*+B\u0005\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0002J\b\u0010\u0019\u001a\u00020\u0006H\u0002J\b\u0010\u001a\u001a\u00020\u0006H\u0002J\u0010\u0010\u001b\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00062\u0006\u0010\u001f\u001a\u00020 H\u0016J\u0012\u0010!\u001a\u00020\u00062\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J$\u0010$\u001a\u00020\u001d2\u0006\u0010%\u001a\u00020&2\b\u0010\'\u001a\u0004\u0018\u00010(2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016J\u001a\u0010)\u001a\u00020\u00062\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0016R\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\rR\u001e\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00100\u000fj\b\u0012\u0004\u0012\u00020\u0010`\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0013X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006,"}, d2 = {"Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment;", "Lcom/eazy/daiku/utility/base/BaseFragment;", "()V", "_saveBankAccountListsListener", "Lkotlin/Function1;", "", "", "binding", "Lcom/eazy/daiku/databinding/FragmentChangeCardAndBankAccountBinding;", "fContext", "Landroidx/fragment/app/FragmentActivity;", "idTickBankAccount", "", "Ljava/lang/Integer;", "mySaveBankAccounts", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "Lkotlin/collections/ArrayList;", "navController", "Landroidx/navigation/NavController;", "saveBankAccount", "saveBankAccountAdapter", "Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment$SaveBankAccountAdapter;", "accessParentFragment", "Lcom/eazy/daiku/utility/bottom_sheet/PaymentMethodBtsFragment;", "doAction", "initMySaveBankAccount", "initView", "view", "Landroid/view/View;", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onViewCreated", "Companion", "SaveBankAccountAdapter", "app_customerDebug"})
public final class ChangeCardAndBankAccountFragment extends com.eazy.daiku.utility.base.BaseFragment {
    private androidx.fragment.app.FragmentActivity fContext;
    private androidx.navigation.NavController navController;
    private com.eazy.daiku.databinding.FragmentChangeCardAndBankAccountBinding binding;
    private com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment.SaveBankAccountAdapter saveBankAccountAdapter;
    private java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> mySaveBankAccounts;
    private kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> _saveBankAccountListsListener;
    private com.eazy.daiku.data.model.SaveBankAccount saveBankAccount;
    private java.lang.Integer idTickBankAccount;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String SaveBankAccountKey = "SaveBankAccountKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String firstSaveBankAccountKey = "firstSaveBankAccountKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String idTickBankAccountKey = "idTickBankAccountKey";
    
    public ChangeCardAndBankAccountFragment() {
        super();
    }
    
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddABAAccountNumberFragment.
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment newInstance(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> saveAbaAccountListsListener) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView(android.view.View view) {
    }
    
    private final void doAction() {
    }
    
    private final com.eazy.daiku.utility.bottom_sheet.PaymentMethodBtsFragment accessParentFragment() {
        return null;
    }
    
    private final void initMySaveBankAccount() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0003J.\u0010\u0013\u001a\u00020\u000e2\u0016\u0010\b\u001a\u0012\u0012\u0004\u0012\u00020\u00050\tj\b\u0012\u0004\u0012\u00020\u0005`\n2\u0006\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007J\u0006\u0010\u0015\u001a\u00020\u000eJ\b\u0010\u0016\u001a\u00020\u0007H\u0016J\u0018\u0010\u0017\u001a\u00020\u000e2\u0006\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u0007H\u0016J\u0018\u0010\u001a\u001a\u00020\u00022\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u0007H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\b\u001a\u0016\u0012\u0004\u0012\u00020\u0005\u0018\u00010\tj\n\u0012\u0004\u0012\u00020\u0005\u0018\u0001`\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R,\u0010\u000b\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u000e0\fX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012\u00a8\u0006\u001f"}, d2 = {"Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment$SaveBankAccountAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment$SaveBankAccountAdapter$SaveBankAccountHolder;", "()V", "firstBankAccount", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "idTickBankAccount", "", "saveBankAccount", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "selectedRow", "Lkotlin/Function2;", "", "", "getSelectedRow", "()Lkotlin/jvm/functions/Function2;", "setSelectedRow", "(Lkotlin/jvm/functions/Function2;)V", "add", "myBankAccount", "clear", "getItemCount", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "SaveBankAccountHolder", "app_customerDebug"})
    static final class SaveBankAccountAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment.SaveBankAccountAdapter.SaveBankAccountHolder> {
        private java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> saveBankAccount;
        private com.eazy.daiku.data.model.SaveBankAccount firstBankAccount;
        public kotlin.jvm.functions.Function2<? super com.eazy.daiku.data.model.SaveBankAccount, ? super java.lang.Boolean, kotlin.Unit> selectedRow;
        private int idTickBankAccount = -100;
        
        public SaveBankAccountAdapter() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function2<com.eazy.daiku.data.model.SaveBankAccount, java.lang.Boolean, kotlin.Unit> getSelectedRow() {
            return null;
        }
        
        public final void setSelectedRow(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function2<? super com.eazy.daiku.data.model.SaveBankAccount, ? super java.lang.Boolean, kotlin.Unit> p0) {
        }
        
        public final void add(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> saveBankAccount, @org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.SaveBankAccount myBankAccount, int idTickBankAccount) {
        }
        
        public final void clear() {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment.SaveBankAccountAdapter.SaveBankAccountHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment.SaveBankAccountAdapter.SaveBankAccountHolder holder, int position) {
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0015\u001a\u00020\u00162\u0006\u0010\u0017\u001a\u00020\u0018R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\bR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u000f\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u000eR\u0011\u0010\u0011\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u000eR\u0011\u0010\u0013\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u000e\u00a8\u0006\u0019"}, d2 = {"Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment$SaveBankAccountAdapter$SaveBankAccountHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "accountNameTv", "Landroid/widget/TextView;", "getAccountNameTv", "()Landroid/widget/TextView;", "accountNumberTv", "getAccountNumberTv", "actionDelete", "Landroid/widget/ImageView;", "getActionDelete", "()Landroid/widget/ImageView;", "logoImg", "getLogoImg", "viewTickBankAccount", "getViewTickBankAccount", "viewUnTickBankAccount", "getViewUnTickBankAccount", "bind", "", "saveBankAccount", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "app_customerDebug"})
        public static final class SaveBankAccountHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private final android.widget.ImageView logoImg = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.TextView accountNameTv = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.TextView accountNumberTv = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.ImageView actionDelete = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.ImageView viewTickBankAccount = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.ImageView viewUnTickBankAccount = null;
            
            public SaveBankAccountHolder(@org.jetbrains.annotations.NotNull()
            android.view.View itemView) {
                super(null);
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.ImageView getLogoImg() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getAccountNameTv() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getAccountNumberTv() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.ImageView getActionDelete() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.ImageView getViewTickBankAccount() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.ImageView getViewUnTickBankAccount() {
                return null;
            }
            
            public final void bind(@org.jetbrains.annotations.NotNull()
            com.eazy.daiku.data.model.SaveBankAccount saveBankAccount) {
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0007\u001a\u00020\b2\u0012\u0010\t\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\f0\nH\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment$Companion;", "", "()V", "SaveBankAccountKey", "", "firstSaveBankAccountKey", "idTickBankAccountKey", "newInstance", "Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment;", "saveAbaAccountListsListener", "Lkotlin/Function1;", "", "", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddABAAccountNumberFragment.
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment newInstance(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super java.lang.Boolean, kotlin.Unit> saveAbaAccountListsListener) {
            return null;
        }
    }
}