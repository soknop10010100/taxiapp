package com.eazy.daiku.ui.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0016H\u0002J\b\u0010\u0018\u001a\u00020\u0016H\u0002J\b\u0010\u0019\u001a\u00020\u0016H\u0002J\b\u0010\u001a\u001a\u00020\u0016H\u0002J\b\u0010\u001b\u001a\u00020\u0016H\u0002J\b\u0010\u001c\u001a\u00020\u0016H\u0002J\u0012\u0010\u001d\u001a\u00020\u00162\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0014J\u0010\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#H\u0016J\b\u0010$\u001a\u00020\u0016H\u0002J\u0010\u0010%\u001a\u00020\u00162\u0006\u0010&\u001a\u00020!H\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0080\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001b\u0010\u000f\u001a\u00020\u00108@X\u0080\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006\'"}, d2 = {"Lcom/eazy/daiku/ui/profile/MyProfileActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityMprofileBinding;", "getBinding$app_customerDebug", "()Lcom/eazy/daiku/databinding/ActivityMprofileBinding;", "setBinding$app_customerDebug", "(Lcom/eazy/daiku/databinding/ActivityMprofileBinding;)V", "user", "Lcom/eazy/daiku/data/model/server_model/User;", "getUser$app_customerDebug", "()Lcom/eazy/daiku/data/model/server_model/User;", "setUser$app_customerDebug", "(Lcom/eazy/daiku/data/model/server_model/User;)V", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM$app_customerDebug", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "Lkotlin/Lazy;", "doAction", "", "doInitData", "drawDataInUI", "initObserved", "initUserVerified", "initView", "initViewTaxiWithCustomer", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "reloadData", "visibleViewTaxiWithCustomer", "isCustomer", "app_customerDebug"})
public final class MyProfileActivity extends com.eazy.daiku.utility.base.BaseActivity {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy userInfoVM$delegate = null;
    public com.eazy.daiku.databinding.ActivityMprofileBinding binding;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.server_model.User user;
    
    public MyProfileActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM$app_customerDebug() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.databinding.ActivityMprofileBinding getBinding$app_customerDebug() {
        return null;
    }
    
    public final void setBinding$app_customerDebug(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.databinding.ActivityMprofileBinding p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.User getUser$app_customerDebug() {
        return null;
    }
    
    public final void setUser$app_customerDebug(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.User p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void doInitData() {
    }
    
    private final void initView() {
    }
    
    private final void initViewTaxiWithCustomer() {
    }
    
    private final void visibleViewTaxiWithCustomer(boolean isCustomer) {
    }
    
    private final void initUserVerified() {
    }
    
    private final void initObserved() {
    }
    
    private final void drawDataInUI() {
    }
    
    private final void reloadData() {
    }
    
    private final void doAction() {
    }
}