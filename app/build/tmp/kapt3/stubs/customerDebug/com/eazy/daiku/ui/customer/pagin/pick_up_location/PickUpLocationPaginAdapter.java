package com.eazy.daiku.ui.customer.pagin.pick_up_location;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u001c2\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0002\u001c\u001dB\u0005\u00a2\u0006\u0002\u0010\u0003J\u001e\u0010\u000e\u001a\u00020\t2\u0016\u0010\u000f\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0010J\u0006\u0010\u0011\u001a\u00020\tJ\b\u0010\u0012\u001a\u00020\u0013H\u0016J\u0018\u0010\u0014\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u0013H\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u0013H\u0016J\u0010\u0010\u001b\u001a\u00020\t2\u0006\u0010\u0015\u001a\u00020\u0002H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\t0\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u001e"}, d2 = {"Lcom/eazy/daiku/ui/customer/pagin/pick_up_location/PickUpLocationPaginAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/ui/customer/pagin/pick_up_location/PickUpLocationPaginAdapter$MyPickUpLocationPaginViewHolder;", "()V", "listPickUpLocation", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/PickUpLocationModel;", "selectRowPickUpLocation", "Lkotlin/Function1;", "", "getSelectRowPickUpLocation", "()Lkotlin/jvm/functions/Function1;", "setSelectRowPickUpLocation", "(Lkotlin/jvm/functions/Function1;)V", "add", "pickUpLocation", "Lkotlin/collections/ArrayList;", "clear", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "onViewRecycled", "Companion", "MyPickUpLocationPaginViewHolder", "app_customerDebug"})
public final class PickUpLocationPaginAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.ui.customer.pagin.pick_up_location.PickUpLocationPaginAdapter.MyPickUpLocationPaginViewHolder> {
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.PickUpLocationModel> listPickUpLocation;
    public kotlin.jvm.functions.Function1<? super com.eazy.daiku.ui.customer.model.PickUpLocationModel, kotlin.Unit> selectRowPickUpLocation;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.pagin.pick_up_location.PickUpLocationPaginAdapter.Companion Companion = null;
    private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<com.eazy.daiku.ui.customer.model.PickUpLocationModel> USER_COMPARATOR = null;
    
    public PickUpLocationPaginAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.ui.customer.model.PickUpLocationModel, kotlin.Unit> getSelectRowPickUpLocation() {
        return null;
    }
    
    public final void setSelectRowPickUpLocation(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.ui.customer.model.PickUpLocationModel, kotlin.Unit> p0) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.PickUpLocationModel> pickUpLocation) {
    }
    
    public final void clear() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.ui.customer.pagin.pick_up_location.PickUpLocationPaginAdapter.MyPickUpLocationPaginViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.pagin.pick_up_location.PickUpLocationPaginAdapter.MyPickUpLocationPaginViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public void onViewRecycled(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.pagin.pick_up_location.PickUpLocationPaginAdapter.MyPickUpLocationPaginViewHolder holder) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000e"}, d2 = {"Lcom/eazy/daiku/ui/customer/pagin/pick_up_location/PickUpLocationPaginAdapter$MyPickUpLocationPaginViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "description", "Landroid/widget/TextView;", "imageView", "Landroid/widget/ImageView;", "title", "bind", "", "pickUp", "Lcom/eazy/daiku/ui/customer/model/PickUpLocationModel;", "app_customerDebug"})
    public static final class MyPickUpLocationPaginViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private android.widget.ImageView imageView;
        private android.widget.TextView title;
        private android.widget.TextView description;
        
        public MyPickUpLocationPaginViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.customer.model.PickUpLocationModel pickUp) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/ui/customer/pagin/pick_up_location/PickUpLocationPaginAdapter$Companion;", "", "()V", "USER_COMPARATOR", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/eazy/daiku/ui/customer/model/PickUpLocationModel;", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}