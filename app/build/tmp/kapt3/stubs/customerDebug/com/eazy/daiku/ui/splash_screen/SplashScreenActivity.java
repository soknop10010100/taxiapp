package com.eazy.daiku.ui.splash_screen;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0013\u001a\u0004\u0018\u00010\u00142\u0006\u0010\u0015\u001a\u00020\u0016H\u0002J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\u0006\u0010\u0019\u001a\u00020\u001aJ\u0012\u0010\u001b\u001a\u00020\u001a2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u000e\u0010\r\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\u000f8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0012\u0010\f\u001a\u0004\b\u0010\u0010\u0011\u00a8\u0006\u001e"}, d2 = {"Lcom/eazy/daiku/ui/splash_screen/SplashScreenActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivitySplashScreenBinding;", "bookingCode", "", "loginViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "getLoginViewModel", "()Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "loginViewModel$delegate", "Lkotlin/Lazy;", "pinCode", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "getUserFromGson", "Lcom/eazy/daiku/data/model/server_model/User;", "jsonObject", "Lcom/google/gson/JsonObject;", "hasLogin", "", "initObserver", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_customerDebug"})
public final class SplashScreenActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    private com.eazy.daiku.databinding.ActivitySplashScreenBinding binding;
    private final kotlin.Lazy userInfoVM$delegate = null;
    private final kotlin.Lazy loginViewModel$delegate = null;
    private java.lang.String pinCode = "";
    private java.lang.String bookingCode = "";
    
    public SplashScreenActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.LoginViewModel getLoginViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initObserver() {
    }
    
    private final com.eazy.daiku.data.model.server_model.User getUserFromGson(com.google.gson.JsonObject jsonObject) {
        return null;
    }
    
    private final boolean hasLogin() {
        return false;
    }
}