package com.eazy.daiku.utility.service;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0016\u0018\u0000 \t2\u00020\u0001:\u0001\tB\u0005\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\bH\u0016\u00a8\u0006\n"}, d2 = {"Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "Landroid/content/BroadcastReceiver;", "()V", "onReceive", "", "p0", "Landroid/content/Context;", "p1", "Landroid/content/Intent;", "Companion", "app_customerDebug"})
public class MyBroadcastReceiver extends android.content.BroadcastReceiver {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.service.MyBroadcastReceiver.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String customBroadcastKey = "com.eazy.daikou.customer.broadcast.activity.CUSTOM_CHT_BROADCAST";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String reloadPaymentSuccessKey = "payment_successfully";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String confirmBookingKey = "confirm_booking_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String stopMovingLocationDriverKey = "stop_moving_location_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String hasReloadWalletKey = "has_reload_wallet_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String hasProcessingTripKey = "has_processing_trip_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String reloadProfileUserKey = "reload_profile_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String Test = "test";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String reloadMainWalletWhenWithdrawSuccessKey = "reloadMainWalletWhenWithdrawSuccessKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String whenWithdrawMoneySuccessFinishScreenWithdrawMoneyKey = "whenWithdrawMoneySuccessFinishScreenWithdrawMoneyKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String finishWhenPaymentCompletedKey = "finishWhenPaymentCompletedKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String reloadProcessingBookingKey = "reloadProcessingBookingKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String updateLocationTrackerKey = "updateLocationTrackerKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String dataKey = "data_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String locationDataKey = "location_data_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String notificationAcceptOrder = "notificationAcceptOrder";
    
    public MyBroadcastReceiver() {
        super();
    }
    
    @java.lang.Override()
    public void onReceive(@org.jetbrains.annotations.Nullable()
    android.content.Context p0, @org.jetbrains.annotations.Nullable()
    android.content.Intent p1) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0010\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/eazy/daiku/utility/service/MyBroadcastReceiver$Companion;", "", "()V", "Test", "", "confirmBookingKey", "customBroadcastKey", "dataKey", "finishWhenPaymentCompletedKey", "hasProcessingTripKey", "hasReloadWalletKey", "locationDataKey", "notificationAcceptOrder", "reloadMainWalletWhenWithdrawSuccessKey", "reloadPaymentSuccessKey", "reloadProcessingBookingKey", "reloadProfileUserKey", "stopMovingLocationDriverKey", "updateLocationTrackerKey", "whenWithdrawMoneySuccessFinishScreenWithdrawMoneyKey", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}