package com.eazy.daiku.utility.call_back;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\bf\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002J!\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00018\u00002\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007H&\u00a2\u0006\u0002\u0010\b\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/call_back/MessageEditTextListener;", "T", "", "onResult", "", "data", "dialogInterface", "Landroid/content/DialogInterface;", "(Ljava/lang/Object;Landroid/content/DialogInterface;)V", "app_customerDebug"})
public abstract interface MessageEditTextListener<T extends java.lang.Object> {
    
    public abstract void onResult(@org.jetbrains.annotations.Nullable()
    T data, @org.jetbrains.annotations.Nullable()
    android.content.DialogInterface dialogInterface);
}