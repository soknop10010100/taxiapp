package com.eazy.daiku.utility.pagin.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \u00152\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0015\u0016B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\f\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000fH\u0016J\u0010\u0010\u0014\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u0003H\u0016R&\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\u0017"}, d2 = {"Lcom/eazy/daiku/utility/pagin/history/HistoryPaginAdapter;", "Landroidx/paging/PagedListAdapter;", "Lcom/eazy/daiku/data/model/server_model/History;", "Lcom/eazy/daiku/utility/pagin/history/HistoryPaginAdapter$MyHistoryPaginViewHolder;", "()V", "selectRowTrx", "Lkotlin/Function1;", "", "getSelectRowTrx", "()Lkotlin/jvm/functions/Function1;", "setSelectRowTrx", "(Lkotlin/jvm/functions/Function1;)V", "onBindViewHolder", "holder", "position", "", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "onViewRecycled", "Companion", "MyHistoryPaginViewHolder", "app_customerDebug"})
public final class HistoryPaginAdapter extends androidx.paging.PagedListAdapter<com.eazy.daiku.data.model.server_model.History, com.eazy.daiku.utility.pagin.history.HistoryPaginAdapter.MyHistoryPaginViewHolder> {
    public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.History, kotlin.Unit> selectRowTrx;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.pagin.history.HistoryPaginAdapter.Companion Companion = null;
    private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<com.eazy.daiku.data.model.server_model.History> USER_COMPARATOR = null;
    
    public HistoryPaginAdapter() {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.server_model.History, kotlin.Unit> getSelectRowTrx() {
        return null;
    }
    
    public final void setSelectRowTrx(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.History, kotlin.Unit> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.utility.pagin.history.HistoryPaginAdapter.MyHistoryPaginViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.pagin.history.HistoryPaginAdapter.MyHistoryPaginViewHolder holder, int position) {
    }
    
    @java.lang.Override()
    public void onViewRecycled(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.pagin.history.HistoryPaginAdapter.MyHistoryPaginViewHolder holder) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\u0018\u00002\u00020\u00012\u00020\u0002B\r\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0011J\u0012\u0010\u001c\u001a\u00020\u001a2\b\u0010\u001d\u001a\u0004\u0018\u00010\u000bH\u0016R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001c\u0010\n\u001a\u0004\u0018\u00010\u000bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0013\u0010\u0013\u001a\u0004\u0018\u00010\u0014\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u000e\u0010\u0017\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001e"}, d2 = {"Lcom/eazy/daiku/utility/pagin/history/HistoryPaginAdapter$MyHistoryPaginViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "amountTv", "Landroid/widget/TextView;", "dateHistoryTv", "distanceHistoryTv", "gMap", "Lcom/google/android/gms/maps/GoogleMap;", "getGMap", "()Lcom/google/android/gms/maps/GoogleMap;", "setGMap", "(Lcom/google/android/gms/maps/GoogleMap;)V", "historyGlobal", "Lcom/eazy/daiku/data/model/server_model/History;", "hotelNameHistoryTv", "map", "Lcom/google/android/gms/maps/MapView;", "getMap", "()Lcom/google/android/gms/maps/MapView;", "statusTv", "timeHistoryTv", "bind", "", "history", "onMapReady", "p0", "app_customerDebug"})
    public static final class MyHistoryPaginViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder implements com.google.android.gms.maps.OnMapReadyCallback {
        private final android.widget.TextView dateHistoryTv = null;
        private final android.widget.TextView timeHistoryTv = null;
        private final android.widget.TextView hotelNameHistoryTv = null;
        private final android.widget.TextView amountTv = null;
        private final android.widget.TextView distanceHistoryTv = null;
        private final android.widget.TextView statusTv = null;
        private com.eazy.daiku.data.model.server_model.History historyGlobal;
        @org.jetbrains.annotations.Nullable()
        private final com.google.android.gms.maps.MapView map = null;
        @org.jetbrains.annotations.Nullable()
        private com.google.android.gms.maps.GoogleMap gMap;
        
        public MyHistoryPaginViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.android.gms.maps.MapView getMap() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.google.android.gms.maps.GoogleMap getGMap() {
            return null;
        }
        
        public final void setGMap(@org.jetbrains.annotations.Nullable()
        com.google.android.gms.maps.GoogleMap p0) {
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.server_model.History history) {
        }
        
        @java.lang.Override()
        public void onMapReady(@org.jetbrains.annotations.Nullable()
        com.google.android.gms.maps.GoogleMap p0) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/pagin/history/HistoryPaginAdapter$Companion;", "", "()V", "USER_COMPARATOR", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/eazy/daiku/data/model/server_model/History;", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}