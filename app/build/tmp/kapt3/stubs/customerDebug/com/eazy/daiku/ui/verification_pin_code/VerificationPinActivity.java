package com.eazy.daiku.ui.verification_pin_code;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0017\u001a\u00020\u0018H\u0002J\u0012\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0002J\b\u0010\u001d\u001a\u00020\u0018H\u0002J\b\u0010\u001e\u001a\u00020\u0018H\u0002J\u0012\u0010\u001f\u001a\u00020\u00182\b\u0010 \u001a\u0004\u0018\u00010!H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u000f\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0010\u001a\u00020\u00118BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0014\u0010\u000e\u001a\u0004\b\u0012\u0010\u0013R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/eazy/daiku/ui/verification_pin_code/VerificationPinActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityVerificationPinBinding;", "code", "", "faceBiometricListener", "Lcom/eazy/daiku/utility/call_back/BiometricListener;", "loginViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "getLoginViewModel", "()Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "loginViewModel$delegate", "Lkotlin/Lazy;", "pinCode", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "verifyPinEnum", "Lcom/eazy/daiku/utility/enumerable/VerifyPinEnum;", "doAction", "", "getUserFromGson", "Lcom/eazy/daiku/data/model/server_model/User;", "jsonObject", "Lcom/google/gson/JsonObject;", "initObserved", "initView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_customerDebug"})
public final class VerificationPinActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    private com.eazy.daiku.databinding.ActivityVerificationPinBinding binding;
    private final kotlin.Lazy loginViewModel$delegate = null;
    private final kotlin.Lazy userInfoVM$delegate = null;
    private com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum = com.eazy.daiku.utility.enumerable.VerifyPinEnum.Other;
    private java.lang.String pinCode = "";
    private java.lang.String code = "";
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.verification_pin_code.VerificationPinActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHANGE_TITLE = "change_title";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String whoCallActivity = "who_call_activity";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String biometricKey = "biometricKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String bookingCode = "bookingCode";
    private com.eazy.daiku.utility.call_back.BiometricListener faceBiometricListener;
    
    public VerificationPinActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.LoginViewModel getLoginViewModel() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initObserved() {
    }
    
    private final com.eazy.daiku.data.model.server_model.User getUserFromGson(com.google.gson.JsonObject jsonObject) {
        return null;
    }
    
    private final void initView() {
    }
    
    private final void doAction() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/eazy/daiku/ui/verification_pin_code/VerificationPinActivity$Companion;", "", "()V", "CHANGE_TITLE", "", "biometricKey", "bookingCode", "whoCallActivity", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}