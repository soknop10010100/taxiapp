package com.eazy.daiku.ui.customer.step_booking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \n2\u00020\u0001:\u0001\nB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\u0012\u0010\u0007\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\tH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/PaymentCompleteActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityPaymentCompleteBinding;", "initView", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "Companion", "app_customerDebug"})
public final class PaymentCompleteActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityPaymentCompleteBinding binding;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.step_booking.PaymentCompleteActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String qrcodeUrlKey = "qrcodeUrlKey";
    
    public PaymentCompleteActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/PaymentCompleteActivity$Companion;", "", "()V", "qrcodeUrlKey", "", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}