package com.eazy.daiku.ui.customer.step_booking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u001e2\u00020\u0001:\u0001\u001eB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0013\u001a\u00020\u0014H\u0002J\u0012\u0010\u0015\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014J\b\u0010\u0018\u001a\u00020\u0014H\u0014J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\b\u0010\u001d\u001a\u00020\u0014H\u0014R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001f"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/ListCarTaxiActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "address", "", "addressByIndex", "Landroid/location/Address;", "adminArea", "countryName", "getAddressLine", "intentFilter", "Landroid/content/IntentFilter;", "latitude", "listKioskModel", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "longitude", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "pathScreenShotMap", "initView", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onStart", "Companion", "app_customerDebug"})
public final class ListCarTaxiActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.ui.customer.model.ListKioskModel listKioskModel;
    private java.lang.String latitude;
    private java.lang.String longitude;
    private java.lang.String address;
    private java.lang.String pathScreenShotMap;
    private android.location.Address addressByIndex;
    private java.lang.String adminArea;
    private java.lang.String countryName;
    private java.lang.String getAddressLine;
    private final android.content.IntentFilter intentFilter = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.step_booking.ListCarTaxiActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String dataListCarTaxiJsonObjectKey = "dataListCarTaxiJsonObjectKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String latKey = "latKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String longKey = "longKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String addressKey = "addressKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String screenShotMapPathKey = "screenShotMapPathKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String addressByIndexKey = "addressByIndexKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String adminAreaKey = "adminAreaKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String countryNameKey = "countryNameKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String getAddressLineKey = "getAddressLineKey";
    private com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver;
    
    public ListCarTaxiActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/ListCarTaxiActivity$Companion;", "", "()V", "addressByIndexKey", "", "addressKey", "adminAreaKey", "countryNameKey", "dataListCarTaxiJsonObjectKey", "getAddressLineKey", "latKey", "longKey", "screenShotMapPathKey", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}