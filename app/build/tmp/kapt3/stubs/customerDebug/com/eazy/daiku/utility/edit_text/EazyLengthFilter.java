package com.eazy.daiku.utility.edit_text;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\r\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J:\u0010\u0006\u001a\u0004\u0018\u00010\u00072\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\u00032\u0006\u0010\n\u001a\u00020\u00032\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u0003H\u0016J\u0006\u0010\u000f\u001a\u00020\u0003J\u000e\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0003R\u000e\u0010\u0005\u001a\u00020\u0003X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lcom/eazy/daiku/utility/edit_text/EazyLengthFilter;", "Landroid/text/InputFilter;", "int", "", "(I)V", "mMax", "filter", "", "source", "start", "end", "dest", "Landroid/text/Spanned;", "dstart", "dend", "getMax", "setMax", "", "max", "app_customerDebug"})
public final class EazyLengthFilter implements android.text.InputFilter {
    private int mMax;
    
    public EazyLengthFilter(int p0_52215) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @java.lang.Override()
    public java.lang.CharSequence filter(@org.jetbrains.annotations.NotNull()
    java.lang.CharSequence source, int start, int end, @org.jetbrains.annotations.NotNull()
    android.text.Spanned dest, int dstart, int dend) {
        return null;
    }
    
    /**
     * @return the maximum length enforced by this input filter
     */
    public final int getMax() {
        return 0;
    }
    
    public final void setMax(int max) {
    }
}