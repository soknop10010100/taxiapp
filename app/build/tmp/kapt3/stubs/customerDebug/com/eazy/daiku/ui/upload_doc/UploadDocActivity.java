package com.eazy.daiku.ui.upload_doc;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000x\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 92\u00020\u0001:\u00019B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001e\u001a\u00020\u001fH\u0002J\u0012\u0010 \u001a\u0004\u0018\u00010!2\u0006\u0010\"\u001a\u00020#H\u0002J\b\u0010$\u001a\u00020%H\u0002J\b\u0010&\u001a\u00020\u001fH\u0002J\b\u0010\'\u001a\u00020\u001fH\u0002J\b\u0010(\u001a\u00020\u001fH\u0002J\"\u0010)\u001a\u00020\u001f2\u0006\u0010*\u001a\u00020\u00162\u0006\u0010+\u001a\u00020\u00162\b\u0010,\u001a\u0004\u0018\u00010-H\u0014J\u0012\u0010.\u001a\u00020\u001f2\b\u0010/\u001a\u0004\u0018\u000100H\u0014J\u0010\u00101\u001a\u00020%2\u0006\u00102\u001a\u000203H\u0016J\b\u00104\u001a\u00020\u001fH\u0002J\b\u00105\u001a\u00020\u001fH\u0002J\b\u00106\u001a\u00020\u001fH\u0002J\b\u00107\u001a\u00020\u001fH\u0002J\b\u00108\u001a\u00020\u001fH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\n\u001a\u0004\b\u000e\u0010\bR\u001a\u0010\u0010\u001a\u000e\u0012\u0004\u0012\u00020\u0012\u0012\u0004\u0012\u00020\u00130\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0012X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0017\u001a\u00020\u00188BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001b\u0010\n\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001c\u001a\u00020\u001dX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006:"}, d2 = {"Lcom/eazy/daiku/ui/upload_doc/UploadDocActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityUploadDocBinding;", "communityTaxi", "Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "getCommunityTaxi", "()Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "communityTaxi$delegate", "Lkotlin/Lazy;", "communityTaxiAdapter", "Lcom/eazy/daiku/utility/pagin/community_taxi/CommunityTaxiPaginAdapter;", "communityTaxiTemporaryRequestOneTimeVm", "getCommunityTaxiTemporaryRequestOneTimeVm", "communityTaxiTemporaryRequestOneTimeVm$delegate", "docKycTemporary", "Ljava/util/HashMap;", "", "", "hasBiometric", "selectVehicleTypeId", "", "uploadDocVm", "Lcom/eazy/daiku/utility/view_model/uplaod_doc_vm/UploadDocVM;", "getUploadDocVm", "()Lcom/eazy/daiku/utility/view_model/uplaod_doc_vm/UploadDocVM;", "uploadDocVm$delegate", "vehicleAdapter", "Lcom/eazy/daiku/utility/adapter/VehicleSelectAdapter;", "doAction", "", "getUserFromGson", "Lcom/eazy/daiku/data/model/server_model/User;", "jsonObject", "Lcom/google/gson/JsonObject;", "hasReUploadKyc", "", "initDataSever", "initObserved", "intiView", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "item", "Landroid/view/MenuItem;", "openCommunityTaxi", "openIdCard", "openPlateNumber", "openSelectVehicle", "openSelfiePicture", "Companion", "app_customerDebug"})
public final class UploadDocActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    private com.eazy.daiku.databinding.ActivityUploadDocBinding binding;
    private com.eazy.daiku.utility.adapter.VehicleSelectAdapter vehicleAdapter;
    private com.eazy.daiku.utility.pagin.community_taxi.CommunityTaxiPaginAdapter communityTaxiAdapter;
    private java.util.HashMap<java.lang.String, java.lang.Object> docKycTemporary;
    private int selectVehicleTypeId = -1;
    private java.lang.String hasBiometric;
    private final kotlin.Lazy uploadDocVm$delegate = null;
    private final kotlin.Lazy communityTaxi$delegate = null;
    private final kotlin.Lazy communityTaxiTemporaryRequestOneTimeVm$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.upload_doc.UploadDocActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String reUploadKycKey = "reUploadKycKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String RequestBodyKey = "request_body_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String hasKycDraftKey = "hasKycDraftKey";
    public static final int RequestIdCardCode = 167;
    public static final int RequestSelfieProfileCode = 168;
    public static final int RequestVehiclePictureCode = 169;
    public static final int RequestPlateNumberCode = 170;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TAG = "UploadDocActivity";
    
    public UploadDocActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.uplaod_doc_vm.UploadDocVM getUploadDocVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.community.CommunityVm getCommunityTaxi() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.community.CommunityVm getCommunityTaxiTemporaryRequestOneTimeVm() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void intiView() {
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    private final void initDataSever() {
    }
    
    private final boolean hasReUploadKyc() {
        return false;
    }
    
    private final void openIdCard() {
    }
    
    private final void openSelfiePicture() {
    }
    
    private final void openSelectVehicle() {
    }
    
    private final void openPlateNumber() {
    }
    
    private final void openCommunityTaxi() {
    }
    
    private final com.eazy.daiku.data.model.server_model.User getUserFromGson(com.google.gson.JsonObject jsonObject) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\u0007\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/eazy/daiku/ui/upload_doc/UploadDocActivity$Companion;", "", "()V", "RequestBodyKey", "", "RequestIdCardCode", "", "RequestPlateNumberCode", "RequestSelfieProfileCode", "RequestVehiclePictureCode", "TAG", "hasKycDraftKey", "reUploadKycKey", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}