package com.eazy.daiku.utility.view_model.user_case.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0013\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0013\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\nJ\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003\u00a2\u0006\u0002\u0010\rJ>\u0010\u0015\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010\u0016J\u0013\u0010\u0017\u001a\u00020\u00072\b\u0010\u0018\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0019\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u001a\u001a\u00020\u001bH\u00d6\u0001R\u0015\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\t\u0010\nR\u0015\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\n\n\u0002\u0010\u000e\u001a\u0004\b\f\u0010\rR\u0015\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\u000f\u0010\nR\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\n\n\u0002\u0010\u000b\u001a\u0004\b\u0010\u0010\n\u00a8\u0006\u001c"}, d2 = {"Lcom/eazy/daiku/utility/view_model/user_case/model/changePwdState;", "", "newPwd", "", "confirmPwd", "pwdNotMatch", "hasDoneValidate", "", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;)V", "getConfirmPwd", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getHasDoneValidate", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getNewPwd", "getPwdNotMatch", "component1", "component2", "component3", "component4", "copy", "(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Boolean;)Lcom/eazy/daiku/utility/view_model/user_case/model/changePwdState;", "equals", "other", "hashCode", "toString", "", "app_customerDebug"})
public final class changePwdState {
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer newPwd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer confirmPwd = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer pwdNotMatch = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Boolean hasDoneValidate = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.view_model.user_case.model.changePwdState copy(@org.jetbrains.annotations.Nullable()
    java.lang.Integer newPwd, @org.jetbrains.annotations.Nullable()
    java.lang.Integer confirmPwd, @org.jetbrains.annotations.Nullable()
    java.lang.Integer pwdNotMatch, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean hasDoneValidate) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public changePwdState() {
        super();
    }
    
    public changePwdState(@org.jetbrains.annotations.Nullable()
    java.lang.Integer newPwd, @org.jetbrains.annotations.Nullable()
    java.lang.Integer confirmPwd, @org.jetbrains.annotations.Nullable()
    java.lang.Integer pwdNotMatch, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean hasDoneValidate) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getNewPwd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getConfirmPwd() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getPwdNotMatch() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getHasDoneValidate() {
        return null;
    }
}