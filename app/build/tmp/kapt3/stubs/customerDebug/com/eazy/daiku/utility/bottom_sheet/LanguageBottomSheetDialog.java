package com.eazy.daiku.utility.bottom_sheet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u001c2\u00020\u0001:\u0001\u001cB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000b\u001a\u00020\bH\u0002J\b\u0010\f\u001a\u00020\bH\u0002J\b\u0010\r\u001a\u00020\bH\u0002J\u0010\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\u0010H\u0016J\u0012\u0010\u0011\u001a\u00020\b2\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J$\u0010\u0014\u001a\u00020\u00152\u0006\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\u00192\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016J\u001a\u0010\u001a\u001a\u00020\b2\u0006\u0010\u001b\u001a\u00020\u00152\b\u0010\u0012\u001a\u0004\u0018\u00010\u0013H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001d"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/LanguageBottomSheetDialog;", "Lcom/eazy/daiku/utility/base/BaseBottomSheetDialogFragment;", "()V", "binding", "Lcom/eazy/daiku/databinding/LanguageMenuBottomSheetDialogLayoutBinding;", "chooseLanguage", "Lkotlin/Function1;", "Lcom/eazy/daiku/utility/enumerable/LanguageSettings;", "", "fContext", "Landroidx/fragment/app/FragmentActivity;", "doAction", "initConfigure", "initView", "onAttach", "context", "Landroid/content/Context;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onViewCreated", "view", "Companion", "app_customerDebug"})
public final class LanguageBottomSheetDialog extends com.eazy.daiku.utility.base.BaseBottomSheetDialogFragment {
    private com.eazy.daiku.databinding.LanguageMenuBottomSheetDialogLayoutBinding binding;
    private androidx.fragment.app.FragmentActivity fContext;
    private kotlin.jvm.functions.Function1<? super com.eazy.daiku.utility.enumerable.LanguageSettings, kotlin.Unit> chooseLanguage;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.bottom_sheet.LanguageBottomSheetDialog.Companion Companion = null;
    
    public LanguageBottomSheetDialog() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.utility.bottom_sheet.LanguageBottomSheetDialog newInstance(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.utility.enumerable.LanguageSettings, kotlin.Unit> chooseLanguage) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void doAction() {
    }
    
    private final void initConfigure() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001c\u0010\u0003\u001a\u00020\u00042\u0012\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\b0\u0006H\u0007\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/LanguageBottomSheetDialog$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/bottom_sheet/LanguageBottomSheetDialog;", "chooseLanguage", "Lkotlin/Function1;", "Lcom/eazy/daiku/utility/enumerable/LanguageSettings;", "", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.utility.bottom_sheet.LanguageBottomSheetDialog newInstance(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.eazy.daiku.utility.enumerable.LanguageSettings, kotlin.Unit> chooseLanguage) {
            return null;
        }
    }
}