package com.eazy.daiku.data.repository;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00c6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J6\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u001a\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\b2\u0006\u0010\u0011\u001a\u00020\rJ<\u0010\u0012\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00140\u00130\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ<\u0010\u0015\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00160\u00130\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u001a\u0010\u0017\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\b2\u0006\u0010\u0011\u001a\u00020\rJ6\u0010\u0018\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00190\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u0012\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\bJ*\u0010\u001b\u001a\u001e\u0012\u001a\u0012\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u001d0\u001cj\b\u0012\u0004\u0012\u00020\u001d`\u001e0\t0\b2\u0006\u0010\u001f\u001a\u00020\rJ6\u0010 \u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u0012\u0010\"\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\bJ6\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u001a\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\t0\b2\u0006\u0010\u0011\u001a\u00020\rJ6\u0010&\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\'0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u0012\u0010(\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020)0\t0\bJ\u001a\u0010*\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\u0006\u0010\u0011\u001a\u00020\rJ6\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010,\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010-\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020.0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010/\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002000\t0\b2\"\u00101\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u00102\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ<\u00103\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002040\u00130\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u00105\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\"\u00106\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u0018\u00107\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002080\u00130\t0\bJ \u00109\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020:0\u00130\t0\b2\u0006\u0010;\u001a\u00020<J6\u0010=\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u0012\u0010>\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020:0\t0\bJ\u0014\u0010?\u001a\b\u0012\u0004\u0012\u00020@0\b2\u0006\u0010A\u001a\u00020\rJ\u0014\u0010B\u001a\b\u0012\u0004\u0012\u00020@0\b2\u0006\u0010A\u001a\u00020\rJ6\u0010C\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020D0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010E\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020!0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010F\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020G0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010H\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0\b2\"\u0010I\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ6\u0010J\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eJ\u0012\u0010K\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020$0\t0\bJ6\u0010L\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020G0\t0\b2\"\u0010\u000b\u001a\u001e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u00010\fj\u000e\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0001`\u000eR\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006M"}, d2 = {"Lcom/eazy/daiku/data/repository/Repository;", "", "context", "Landroid/content/Context;", "apiService", "Lcom/eazy/daiku/data/remote/EazyTaxiApi;", "(Landroid/content/Context;Lcom/eazy/daiku/data/remote/EazyTaxiApi;)V", "createUser", "Lkotlinx/coroutines/flow/Flow;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/CreateUserRespond;", "bodyMap", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "endTrip", "Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "code", "fetchCommunityTaxi", "", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "fetchHistory", "Lcom/eazy/daiku/data/model/server_model/History;", "fetchQrCode", "fetchTransaction", "Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "fetchTripProcessing", "fetchVehicleType", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "Lkotlin/collections/ArrayList;", "vehicleType", "login", "Lcom/google/gson/JsonElement;", "logout", "reUploadKycDoc", "Lcom/eazy/daiku/data/model/server_model/User;", "startTrip", "submitBookingPreviewDoCheckout", "Lcom/eazy/daiku/data/model/server_model/BookingPreviewDoCheckoutModel;", "submitBookingProcessing", "Lcom/example/example/BookingProcessingModel;", "submitCancelBookingTaxi", "submitChangePasswordByOtp", "submitChangePwd", "submitCheckout", "Lcom/eazy/daiku/ui/customer/model/WebPayRespondModel;", "submitCheckoutTaxi", "Lcom/eazy/daiku/ui/customer/model/PreviewCheckoutModel;", "body", "submitCreateUserCustomer", "submitCustomerHistory", "Lcom/eazy/daiku/data/model/server_model/CustomerHistoryModel;", "submitGetTaxiDriver", "map", "submitListAllBank", "Lcom/eazy/daiku/data/model/ListAllBankAccountModel;", "submitListAllLocationKiosk", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "allDevice", "", "submitRequestOtp", "submitSearchLocationKioskNearBy", "submitSearchMap", "Lcom/eazy/daiku/ui/customer/model/sear_map/SearchMapModel;", "textSearch", "submitSearchMapLocal", "submitVerifyBankAccount", "Lcom/eazy/daiku/data/model/VerifyBankAccountModel;", "submitVerifyOtp", "submitWithdraw", "Lcom/eazy/daiku/data/model/WithdrawMoneyRespondModel;", "updateAvailableTaxi", "bodyRequest", "updateUserInfo", "userInfo", "userSubmitWithdraw", "app_customerDebug"})
public final class Repository {
    private final android.content.Context context = null;
    private final com.eazy.daiku.data.remote.EazyTaxiApi apiService = null;
    
    @javax.inject.Inject()
    public Repository(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.remote.EazyTaxiApi apiService) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> login(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> logout() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> userInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> updateAvailableTaxi(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyRequest) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.CreateUserRespond>> createUser(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond>>> fetchVehicleType(@org.jetbrains.annotations.NotNull()
    java.lang.String vehicleType) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> fetchQrCode(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> startTrip(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> endTrip(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> fetchTripProcessing() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> reUploadKycDoc(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.TransactionRespond>> fetchTransaction(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.History>>> fetchHistory(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.CustomerHistoryModel>>> submitCustomerHistory(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> submitWithdraw(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> submitChangePwd(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> submitRequestOtp(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> submitVerifyOtp(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> submitChangePasswordByOtp(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond>>> fetchCommunityTaxi(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> updateUserInfo(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.VerifyBankAccountModel>> submitVerifyBankAccount(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> submitSearchMap(@org.jetbrains.annotations.NotNull()
    java.lang.String textSearch) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> submitSearchMapLocal(@org.jetbrains.annotations.NotNull()
    java.lang.String textSearch) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.CreateUserRespond>> submitCreateUserCustomer(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.ListKioskModel>> submitSearchLocationKioskNearBy() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> submitGetTaxiDriver(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> map) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.PreviewCheckoutModel>> submitCheckoutTaxi(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> body) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.ui.customer.model.ListKioskModel>>> submitListAllLocationKiosk(boolean allDevice) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.WebPayRespondModel>> submitCheckout(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.ListAllBankAccountModel>>> submitListAllBank() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> userSubmitWithdraw(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.BookingPreviewDoCheckoutModel>> submitBookingPreviewDoCheckout(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.example.example.BookingProcessingModel>> submitBookingProcessing() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlinx.coroutines.flow.Flow<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> submitCancelBookingTaxi(@org.jetbrains.annotations.NotNull()
    java.lang.String code) {
        return null;
    }
}