package com.eazy.daiku.utility.bottom_sheet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 *2\u00020\u0001:\u0001*B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001a\u001a\u00020\u0007H\u0002J\b\u0010\u001b\u001a\u00020\u000fH\u0016J\b\u0010\u001c\u001a\u00020\u0007H\u0002J\u0010\u0010\u001d\u001a\u00020\u00072\u0006\u0010\u001e\u001a\u00020\u001fH\u0016J$\u0010 \u001a\u00020!2\u0006\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u001a\u0010(\u001a\u00020\u00072\u0006\u0010)\u001a\u00020!2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016R \u0010\u0003\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u0010R\"\u0010\u0011\u001a\u0016\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0012j\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0015X\u0082.\u00a2\u0006\u0002\n\u0000R#\u0010\u0016\u001a\u0014\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u00048F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u000e\u0010\u0019\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/PaymentMethodBtsFragment;", "Lcom/eazy/daiku/utility/base/BaseBottomSheetDialogFragment;", "()V", "_saveAbaAccountListsListener", "Lkotlin/Function2;", "", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "", "actionCardEnum", "Lcom/eazy/daiku/utility/enumerable/ActionCard;", "binding", "Lcom/eazy/daiku/databinding/PaymentMethodBtsFragmentBinding;", "fContext", "Landroidx/fragment/app/FragmentActivity;", "idTickBankAccount", "", "Ljava/lang/Integer;", "mySaveBankAccount", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "navController", "Landroidx/navigation/NavController;", "saveAbaAccountListsListener", "getSaveAbaAccountListsListener", "()Lkotlin/jvm/functions/Function2;", "saveBankAccount", "doAction", "getTheme", "initView", "onAttach", "context", "Landroid/content/Context;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "Companion", "app_customerDebug"})
public final class PaymentMethodBtsFragment extends com.eazy.daiku.utility.base.BaseBottomSheetDialogFragment {
    private androidx.fragment.app.FragmentActivity fContext;
    private com.eazy.daiku.databinding.PaymentMethodBtsFragmentBinding binding;
    private androidx.navigation.NavController navController;
    private kotlin.jvm.functions.Function2<? super java.lang.Boolean, ? super com.eazy.daiku.data.model.SaveBankAccount, kotlin.Unit> _saveAbaAccountListsListener;
    private com.eazy.daiku.utility.enumerable.ActionCard actionCardEnum;
    private java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> mySaveBankAccount;
    private com.eazy.daiku.data.model.SaveBankAccount saveBankAccount;
    private java.lang.Integer idTickBankAccount;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.bottom_sheet.PaymentMethodBtsFragment.Companion Companion = null;
    
    public PaymentMethodBtsFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.utility.bottom_sheet.PaymentMethodBtsFragment newInstance(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.SaveBankAccount saveBankAccount, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.ActionCard actionCardEnum, @org.jetbrains.annotations.Nullable()
    java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> mySaveBankAccount, int idTickBankAccount, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super java.lang.Boolean, ? super com.eazy.daiku.data.model.SaveBankAccount, kotlin.Unit> saveAbaAccountListsListener) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void doAction() {
    }
    
    @java.lang.Override()
    public int getTheme() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function2<java.lang.Boolean, com.eazy.daiku.data.model.SaveBankAccount, kotlin.Unit> getSaveAbaAccountListsListener() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002JX\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u001c\b\u0002\u0010\t\u001a\u0016\u0012\u0004\u0012\u00020\u0006\u0018\u00010\nj\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u000b2\u0006\u0010\f\u001a\u00020\r2\u0018\u0010\u000e\u001a\u0014\u0012\u0004\u0012\u00020\u0010\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00110\u000fH\u0007\u00a8\u0006\u0012"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/PaymentMethodBtsFragment$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/bottom_sheet/PaymentMethodBtsFragment;", "saveBankAccount", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "actionCardEnum", "Lcom/eazy/daiku/utility/enumerable/ActionCard;", "mySaveBankAccount", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "idTickBankAccount", "", "saveAbaAccountListsListener", "Lkotlin/Function2;", "", "", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.utility.bottom_sheet.PaymentMethodBtsFragment newInstance(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.SaveBankAccount saveBankAccount, @org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.enumerable.ActionCard actionCardEnum, @org.jetbrains.annotations.Nullable()
        java.util.ArrayList<com.eazy.daiku.data.model.SaveBankAccount> mySaveBankAccount, int idTickBankAccount, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function2<? super java.lang.Boolean, ? super com.eazy.daiku.data.model.SaveBankAccount, kotlin.Unit> saveAbaAccountListsListener) {
            return null;
        }
    }
}