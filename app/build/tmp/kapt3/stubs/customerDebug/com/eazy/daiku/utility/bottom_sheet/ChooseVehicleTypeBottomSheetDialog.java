package com.eazy.daiku.utility.bottom_sheet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 \"2\u00020\u0001:\u0002\"#B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0012\u001a\u00020\nH\u0002J\b\u0010\u0013\u001a\u00020\nH\u0002J\b\u0010\u0014\u001a\u00020\nH\u0002J\u0010\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u0017H\u0016J$\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001d2\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016J\u001a\u0010 \u001a\u00020\n2\u0006\u0010!\u001a\u00020\u00192\b\u0010\u001e\u001a\u0004\u0018\u00010\u001fH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u000f\u001a\u0016\u0012\u0004\u0012\u00020\t\u0018\u00010\u0010j\n\u0012\u0004\u0012\u00020\t\u0018\u0001`\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006$"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog;", "Lcom/eazy/daiku/utility/base/BaseBottomSheetDialogFragment;", "()V", "binding", "Lcom/eazy/daiku/databinding/ChooseVehicleTypeTaxiRowBottomSheetLayoutBinding;", "fContext", "Landroidx/fragment/app/FragmentActivity;", "selectVehicleTaxiListener", "Lkotlin/Function1;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "", "vehicleAdapter", "Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog$VehicleChooseAdapter;", "vehicleTermId", "", "vehicleTypeTaxis", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "doAction", "initData", "initView", "onAttach", "context", "Landroid/content/Context;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "Companion", "VehicleChooseAdapter", "app_customerDebug"})
public final class ChooseVehicleTypeBottomSheetDialog extends com.eazy.daiku.utility.base.BaseBottomSheetDialogFragment {
    private com.eazy.daiku.databinding.ChooseVehicleTypeTaxiRowBottomSheetLayoutBinding binding;
    private androidx.fragment.app.FragmentActivity fContext;
    private kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> selectVehicleTaxiListener;
    private com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog.VehicleChooseAdapter vehicleAdapter;
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleTypeTaxis;
    private int vehicleTermId = -1;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog.Companion Companion = null;
    
    public ChooseVehicleTypeBottomSheetDialog() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog newInstance(int vehicleTermId, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleTypeTaxis, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> selectVehicleTaxiListener) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void doAction() {
    }
    
    private final void initData() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0002\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0003J&\u0010\u0011\u001a\u00020\u00072\u0016\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u000fj\b\u0012\u0004\u0012\u00020\u0006`\u00102\u0006\u0010\u0012\u001a\u00020\rJ\b\u0010\u0013\u001a\u00020\rH\u0016J\u0018\u0010\u0014\u001a\u00020\u00072\u0006\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\rH\u0016J\u0018\u0010\u0017\u001a\u00020\u00022\u0006\u0010\u0018\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\rH\u0016R&\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\"\u0010\u000e\u001a\u0016\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000fj\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u0010X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog$VehicleChooseAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog$VehicleChooseAdapter$VehicleHolder;", "()V", "selectedRow", "Lkotlin/Function1;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "", "getSelectedRow", "()Lkotlin/jvm/functions/Function1;", "setSelectedRow", "(Lkotlin/jvm/functions/Function1;)V", "selectedVehicleTypeId", "", "vehicleModels", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "addVehicle", "vehicleTypeId", "getItemCount", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "VehicleHolder", "app_customerDebug"})
    static final class VehicleChooseAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog.VehicleChooseAdapter.VehicleHolder> {
        private java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleModels;
        public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> selectedRow;
        private int selectedVehicleTypeId = -1;
        
        public VehicleChooseAdapter() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> getSelectedRow() {
            return null;
        }
        
        public final void setSelectedRow(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> p0) {
        }
        
        public final void addVehicle(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleModels, int vehicleTypeId) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog.VehicleChooseAdapter.VehicleHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog.VehicleChooseAdapter.VehicleHolder holder, int position) {
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0017"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog$VehicleChooseAdapter$VehicleHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "cardVehicleCardView", "Lcom/google/android/material/card/MaterialCardView;", "getCardVehicleCardView", "()Lcom/google/android/material/card/MaterialCardView;", "imageView", "Landroid/widget/ImageView;", "getImageView", "()Landroid/widget/ImageView;", "textView", "Landroid/widget/TextView;", "getTextView", "()Landroid/widget/TextView;", "bind", "", "vehicleModel", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "selectItem", "", "app_customerDebug"})
        public static final class VehicleHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            @org.jetbrains.annotations.NotNull()
            private final android.widget.ImageView imageView = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.TextView textView = null;
            @org.jetbrains.annotations.NotNull()
            private final com.google.android.material.card.MaterialCardView cardVehicleCardView = null;
            
            public VehicleHolder(@org.jetbrains.annotations.NotNull()
            android.view.View itemView) {
                super(null);
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.ImageView getImageView() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.TextView getTextView() {
                return null;
            }
            
            @org.jetbrains.annotations.NotNull()
            public final com.google.android.material.card.MaterialCardView getCardVehicleCardView() {
                return null;
            }
            
            public final void bind(@org.jetbrains.annotations.NotNull()
            com.eazy.daiku.data.model.server_model.VehicleTypeRespond vehicleModel) {
            }
            
            public final void selectItem(boolean selectItem) {
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J<\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0016\u0010\u0007\u001a\u0012\u0012\u0004\u0012\u00020\t0\bj\b\u0012\u0004\u0012\u00020\t`\n2\u0012\u0010\u000b\u001a\u000e\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\r0\fH\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/bottom_sheet/ChooseVehicleTypeBottomSheetDialog;", "vehicleTermId", "", "vehicleTypeTaxis", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "Lkotlin/collections/ArrayList;", "selectVehicleTaxiListener", "Lkotlin/Function1;", "", "app_customerDebug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.utility.bottom_sheet.ChooseVehicleTypeBottomSheetDialog newInstance(int vehicleTermId, @org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleTypeTaxis, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> selectVehicleTaxiListener) {
            return null;
        }
    }
}