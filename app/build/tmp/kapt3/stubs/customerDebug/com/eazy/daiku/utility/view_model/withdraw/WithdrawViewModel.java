package com.eazy.daiku.utility.view_model.withdraw;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\\\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0017\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010)\u001a\u00020*J*\u0010+\u001a\u00020*2\"\u0010,\u001a\u001e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/0-j\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/`0J*\u00101\u001a\u00020*2\"\u0010,\u001a\u001e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/0-j\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/`0J*\u00102\u001a\u00020*2\"\u0010,\u001a\u001e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/0-j\u000e\u0012\u0004\u0012\u00020.\u0012\u0004\u0012\u00020/`0R \u0010\u0007\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0010\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00120\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00120\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00120\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R#\u0010\u0016\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0018R\u001d\u0010\u001b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u001c\u0010\u0018R\u001d\u0010\u001d\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000f0\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u001e\u0010\u0018R\u0017\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u00120\b8F\u00a2\u0006\u0006\u001a\u0004\b \u0010\u0018R\u0017\u0010!\u001a\b\u0012\u0004\u0012\u00020\u00120\b8F\u00a2\u0006\u0006\u001a\u0004\b\"\u0010\u0018R\u0017\u0010#\u001a\b\u0012\u0004\u0012\u00020\u00120\b8F\u00a2\u0006\u0006\u001a\u0004\b$\u0010\u0018R\u0017\u0010%\u001a\b\u0012\u0004\u0012\u00020\u00120\b8F\u00a2\u0006\u0006\u001a\u0004\b&\u0010\u0018R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010(\u00a8\u00063"}, d2 = {"Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_dataListAllBankLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "", "Lcom/eazy/daiku/data/model/ListAllBankAccountModel;", "_dataRespondVerifyBankAccount", "Lcom/eazy/daiku/data/model/VerifyBankAccountModel;", "_dataUserWithdrawLiveData", "Lcom/eazy/daiku/data/model/WithdrawMoneyRespondModel;", "_dataWithdrawLiveData", "_loadingListAllBankLiveData", "", "_loadingUserWithdrawLiveData", "_loadingVerifyBankAccount", "_loadingWithdrawLiveData", "dataListAllBankLiveData", "getDataListAllBankLiveData", "()Landroidx/lifecycle/MutableLiveData;", "dataRespondVerifyBankAccount", "getDataRespondVerifyBankAccount", "dataUserWithdrawLiveData", "getDataUserWithdrawLiveData", "dataWithdrawLiveData", "getDataWithdrawLiveData", "loadingListAllBankLiveData", "getLoadingListAllBankLiveData", "loadingUserWithdrawLiveData", "getLoadingUserWithdrawLiveData", "loadingVerifyBankAccount", "getLoadingVerifyBankAccount", "loadingWithdrawLiveData", "getLoadingWithdrawLiveData", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "submitListAllBank", "", "submitVerifyBankAccount", "bodyMap", "Ljava/util/HashMap;", "", "", "Lkotlin/collections/HashMap;", "submitWithdraw", "userSubmitWithdraw", "app_customerDebug"})
public final class WithdrawViewModel extends com.eazy.daiku.utility.base.BaseViewModel {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingWithdrawLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> _dataWithdrawLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingVerifyBankAccount = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.VerifyBankAccountModel>> _dataRespondVerifyBankAccount = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingListAllBankLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.ListAllBankAccountModel>>> _dataListAllBankLiveData = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingUserWithdrawLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> _dataUserWithdrawLiveData = null;
    
    @javax.inject.Inject()
    public WithdrawViewModel(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingWithdrawLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> getDataWithdrawLiveData() {
        return null;
    }
    
    public final void submitWithdraw(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingVerifyBankAccount() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.VerifyBankAccountModel>> getDataRespondVerifyBankAccount() {
        return null;
    }
    
    public final void submitVerifyBankAccount(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingListAllBankLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.ListAllBankAccountModel>>> getDataListAllBankLiveData() {
        return null;
    }
    
    public final void submitListAllBank() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingUserWithdrawLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> getDataUserWithdrawLiveData() {
        return null;
    }
    
    public final void userSubmitWithdraw(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
    }
}