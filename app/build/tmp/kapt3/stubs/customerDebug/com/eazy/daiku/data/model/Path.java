package com.eazy.daiku.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\b$\b\u0086\b\u0018\u00002\u00020\u0001Bs\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0016\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\t\u0012\u0006\u0010\n\u001a\u00020\u000b\u0012\u0006\u0010\f\u001a\u00020\r\u0012\u0006\u0010\u000e\u001a\u00020\u000b\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\u0006\u0010\u0011\u001a\u00020\u0003\u0012\f\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0013\u0012\u0006\u0010\u0014\u001a\u00020\u000b\u00a2\u0006\u0002\u0010\u0015J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00030\u0013H\u00c6\u0003J\t\u0010)\u001a\u00020\u000bH\u00c6\u0003J\t\u0010*\u001a\u00020\u0003H\u00c6\u0003J\t\u0010+\u001a\u00020\u0003H\u00c6\u0003J\u0019\u0010,\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\tH\u00c6\u0003J\t\u0010-\u001a\u00020\u000bH\u00c6\u0003J\t\u0010.\u001a\u00020\rH\u00c6\u0003J\t\u0010/\u001a\u00020\u000bH\u00c6\u0003J\t\u00100\u001a\u00020\u0010H\u00c6\u0003J\t\u00101\u001a\u00020\u0003H\u00c6\u0003J\u008d\u0001\u00102\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00032\b\b\u0002\u0010\u0005\u001a\u00020\u00032\u0018\b\u0002\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\t2\b\b\u0002\u0010\n\u001a\u00020\u000b2\b\b\u0002\u0010\f\u001a\u00020\r2\b\b\u0002\u0010\u000e\u001a\u00020\u000b2\b\b\u0002\u0010\u000f\u001a\u00020\u00102\b\b\u0002\u0010\u0011\u001a\u00020\u00032\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u00132\b\b\u0002\u0010\u0014\u001a\u00020\u000bH\u00c6\u0001J\u0013\u00103\u001a\u00020\r2\b\u00104\u001a\u0004\u0018\u00010\bH\u00d6\u0003J\t\u00105\u001a\u00020\u0010H\u00d6\u0001J\t\u00106\u001a\u00020\u000bH\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0016\u0010\u0017R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00030\u0013\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0019R\u0011\u0010\u0004\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0017R\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0017R!\u0010\u0006\u001a\u0012\u0012\u0004\u0012\u00020\b0\u0007j\b\u0012\u0004\u0012\u00020\b`\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u001dR\u0011\u0010\n\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001e\u0010\u001fR\u0011\u0010\f\u001a\u00020\r\u00a2\u0006\b\n\u0000\u001a\u0004\b \u0010!R\u0011\u0010\u000e\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010\u001fR\u0011\u0010\u0014\u001a\u00020\u000b\u00a2\u0006\b\n\u0000\u001a\u0004\b#\u0010\u001fR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010%R\u0011\u0010\u0011\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u0017\u00a8\u00067"}, d2 = {"Lcom/eazy/daiku/data/model/Path;", "Ljava/io/Serializable;", "ascend", "", "descend", "distance", "legs", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "points", "", "points_encoded", "", "snapped_waypoints", "transfers", "", "weight", "bbox", "", "street_name", "(DDDLjava/util/ArrayList;Ljava/lang/String;ZLjava/lang/String;IDLjava/util/List;Ljava/lang/String;)V", "getAscend", "()D", "getBbox", "()Ljava/util/List;", "getDescend", "getDistance", "getLegs", "()Ljava/util/ArrayList;", "getPoints", "()Ljava/lang/String;", "getPoints_encoded", "()Z", "getSnapped_waypoints", "getStreet_name", "getTransfers", "()I", "getWeight", "component1", "component10", "component11", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "other", "hashCode", "toString", "app_customerDebug"})
public final class Path implements java.io.Serializable {
    private final double ascend = 0.0;
    private final double descend = 0.0;
    private final double distance = 0.0;
    @org.jetbrains.annotations.NotNull()
    private final java.util.ArrayList<java.lang.Object> legs = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String points = null;
    private final boolean points_encoded = false;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String snapped_waypoints = null;
    private final int transfers = 0;
    private final double weight = 0.0;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.Double> bbox = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String street_name = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.model.Path copy(double ascend, double descend, double distance, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Object> legs, @org.jetbrains.annotations.NotNull()
    java.lang.String points, boolean points_encoded, @org.jetbrains.annotations.NotNull()
    java.lang.String snapped_waypoints, int transfers, double weight, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Double> bbox, @org.jetbrains.annotations.NotNull()
    java.lang.String street_name) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Path(double ascend, double descend, double distance, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Object> legs, @org.jetbrains.annotations.NotNull()
    java.lang.String points, boolean points_encoded, @org.jetbrains.annotations.NotNull()
    java.lang.String snapped_waypoints, int transfers, double weight, @org.jetbrains.annotations.NotNull()
    java.util.List<java.lang.Double> bbox, @org.jetbrains.annotations.NotNull()
    java.lang.String street_name) {
        super();
    }
    
    public final double component1() {
        return 0.0;
    }
    
    public final double getAscend() {
        return 0.0;
    }
    
    public final double component2() {
        return 0.0;
    }
    
    public final double getDescend() {
        return 0.0;
    }
    
    public final double component3() {
        return 0.0;
    }
    
    public final double getDistance() {
        return 0.0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Object> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Object> getLegs() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPoints() {
        return null;
    }
    
    public final boolean component6() {
        return false;
    }
    
    public final boolean getPoints_encoded() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSnapped_waypoints() {
        return null;
    }
    
    public final int component8() {
        return 0;
    }
    
    public final int getTransfers() {
        return 0;
    }
    
    public final double component9() {
        return 0.0;
    }
    
    public final double getWeight() {
        return 0.0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Double> component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Double> getBbox() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getStreet_name() {
        return null;
    }
}