package com.eazy.daiku.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u001b\u0018\u00002\u00020\u0001B\u0091\u0001\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\b\u0010\n\u001a\u0004\u0018\u00010\u0007\u0012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0007\u0012\u0016\b\u0002\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007\u0018\u00010\r\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\n\b\u0002\u0010\u0012\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\u0013R\u001e\u0010\u000b\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001c\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u001e\u0010\u000e\u001a\u0004\u0018\u00010\u000fX\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010%\u001a\u0004\b!\u0010\"\"\u0004\b#\u0010$R(\u0010\f\u001a\u0010\u0012\u0004\u0012\u00020\u0003\u0012\u0004\u0012\u00020\u0007\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R\u001e\u0010\n\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\u0018\u001a\u0004\b*\u0010\u0015\"\u0004\b+\u0010\u0017\u00a8\u0006,"}, d2 = {"Lcom/eazy/daiku/data/model/MyTransaction;", "Lcom/eazy/daiku/data/model/server_model/Transaction;", "bookingCode", "", "kessTransactionId", "payBy", "total", "", "walletTransactionStatus", "createdAt", "totalBalance", "blockedBalance", "totalAmountByDate", "Ljava/util/HashMap;", "showHeaderDate", "", "mUserWithdraw", "Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;", "mType", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/Double;Ljava/util/HashMap;Ljava/lang/Boolean;Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;Ljava/lang/String;)V", "getBlockedBalance", "()Ljava/lang/Double;", "setBlockedBalance", "(Ljava/lang/Double;)V", "Ljava/lang/Double;", "getMType", "()Ljava/lang/String;", "setMType", "(Ljava/lang/String;)V", "getMUserWithdraw", "()Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;", "setMUserWithdraw", "(Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;)V", "getShowHeaderDate", "()Ljava/lang/Boolean;", "setShowHeaderDate", "(Ljava/lang/Boolean;)V", "Ljava/lang/Boolean;", "getTotalAmountByDate", "()Ljava/util/HashMap;", "setTotalAmountByDate", "(Ljava/util/HashMap;)V", "getTotalBalance", "setTotalBalance", "app_customerDebug"})
public final class MyTransaction extends com.eazy.daiku.data.model.server_model.Transaction {
    @org.jetbrains.annotations.Nullable()
    private java.lang.Double totalBalance;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Double blockedBalance;
    @org.jetbrains.annotations.Nullable()
    private java.util.HashMap<java.lang.String, java.lang.Double> totalAmountByDate;
    @org.jetbrains.annotations.Nullable()
    private java.lang.Boolean showHeaderDate;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.server_model.WithdrawRespond mUserWithdraw;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String mType;
    
    public MyTransaction(@org.jetbrains.annotations.Nullable()
    java.lang.String bookingCode, @org.jetbrains.annotations.Nullable()
    java.lang.String kessTransactionId, @org.jetbrains.annotations.Nullable()
    java.lang.String payBy, @org.jetbrains.annotations.Nullable()
    java.lang.Double total, @org.jetbrains.annotations.Nullable()
    java.lang.String walletTransactionStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String createdAt, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalBalance, @org.jetbrains.annotations.Nullable()
    java.lang.Double blockedBalance, @org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.String, java.lang.Double> totalAmountByDate, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean showHeaderDate, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.WithdrawRespond mUserWithdraw, @org.jetbrains.annotations.Nullable()
    java.lang.String mType) {
        super(null, null, null, null, null, null, null, null);
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotalBalance() {
        return null;
    }
    
    public final void setTotalBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getBlockedBalance() {
        return null;
    }
    
    public final void setBlockedBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.HashMap<java.lang.String, java.lang.Double> getTotalAmountByDate() {
        return null;
    }
    
    public final void setTotalAmountByDate(@org.jetbrains.annotations.Nullable()
    java.util.HashMap<java.lang.String, java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getShowHeaderDate() {
        return null;
    }
    
    public final void setShowHeaderDate(@org.jetbrains.annotations.Nullable()
    java.lang.Boolean p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.WithdrawRespond getMUserWithdraw() {
        return null;
    }
    
    public final void setMUserWithdraw(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.WithdrawRespond p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMType() {
        return null;
    }
    
    public final void setMType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}