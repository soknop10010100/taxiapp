package com.eazy.daiku.ui.signup;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000N\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0011\n\u0002\b\u0003\n\u0002\u0010\b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001e\u001a\u00020\u001fH\u0002J\b\u0010 \u001a\u00020\u001fH\u0002J\b\u0010!\u001a\u00020\u001fH\u0002J\b\u0010\"\u001a\u00020\u001fH\u0002J\u0012\u0010#\u001a\u00020\u001f2\b\u0010$\u001a\u0004\u0018\u00010%H\u0014J\b\u0010&\u001a\u00020\u001fH\u0002J\u0010\u0010\'\u001a\u00020\u001f2\u0006\u0010(\u001a\u00020\fH\u0002J\b\u0010)\u001a\u00020\u001fH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0010\u0012\u0004\u0012\u00020\u000f\u0012\u0004\u0012\u00020\u0010\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0012X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0013R\u0016\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u000f0\u0012X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0013R+\u0010\u0017\u001a\u00020\u00162\u0006\u0010\u0015\u001a\u00020\u00168B@BX\u0082\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u001c\u0010\u001d\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001b\u00a8\u0006*"}, d2 = {"Lcom/eazy/daiku/ui/signup/SignUpActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityCreateUserBinding;", "createUserViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/CreateUserViewModel;", "getCreateUserViewModel", "()Lcom/eazy/daiku/utility/view_model/user_case/CreateUserViewModel;", "createUserViewModel$delegate", "Lkotlin/Lazy;", "dobDate", "Ljava/util/Date;", "docKycTemporary", "Ljava/util/HashMap;", "", "", "genderKey", "", "[Ljava/lang/String;", "genderScreenValue", "<set-?>", "", "selectGenderIndex", "getSelectGenderIndex", "()I", "setSelectGenderIndex", "(I)V", "selectGenderIndex$delegate", "Lkotlin/properties/ReadWriteProperty;", "doAction", "", "initObserved", "initView", "initViewTaxiWithCustomer", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onValidateInformationUser", "updateUIDob", "date", "validateTextChangedEditText", "app_customerDebug"})
public final class SignUpActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    private final kotlin.Lazy createUserViewModel$delegate = null;
    private com.eazy.daiku.databinding.ActivityCreateUserBinding binding;
    private java.util.Date dobDate;
    private java.lang.String[] genderScreenValue;
    private java.lang.String[] genderKey;
    private final kotlin.properties.ReadWriteProperty selectGenderIndex$delegate = null;
    private java.util.HashMap<java.lang.String, java.lang.Object> docKycTemporary;
    
    public SignUpActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.CreateUserViewModel getCreateUserViewModel() {
        return null;
    }
    
    private final int getSelectGenderIndex() {
        return 0;
    }
    
    private final void setSelectGenderIndex(int p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void validateTextChangedEditText() {
    }
    
    private final void initObserved() {
    }
    
    private final void initViewTaxiWithCustomer() {
    }
    
    private final void onValidateInformationUser() {
    }
    
    private final void updateUIDob(java.util.Date date) {
    }
    
    private final void doAction() {
    }
}