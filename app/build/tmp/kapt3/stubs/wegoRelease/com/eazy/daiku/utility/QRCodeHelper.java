package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u0004\u0018\u00010\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/QRCodeHelper;", "", "()V", "generateQrImage", "Landroid/graphics/Bitmap;", "str", "", "app_wegoRelease"})
public final class QRCodeHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.QRCodeHelper INSTANCE = null;
    
    private QRCodeHelper() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.graphics.Bitmap generateQrImage(@org.jetbrains.annotations.Nullable()
    java.lang.String str) {
        return null;
    }
}