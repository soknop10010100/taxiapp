package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0016\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0012\b\t\n\u000b\f\r\u000e\u000f\u0010\u0011\u0012\u0013\u0014\u0015\u0016\u0017\u0018\u0019B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/eazy/daiku/utility/Config;", "", "()V", "BASE_URL", "", "EAZYTAXI_PREF_NAME", "FCM_TOKEN", "LOG_CRASH_MSG", "BankBic", "HistoryStatus", "HttpStatusCode", "InternetCon", "KeySearch", "KeyServer", "KycDocKey", "KycDocStatus", "ParseServerKey", "QrStatus", "StatusAssignKey", "TaxiAppType", "TypeDeepLink", "TypeMapMarker", "TypeMarkerOptions", "TypeScheme", "UserDeviceInfo", "WeGoBusiness", "app_wegoRelease"})
public final class Config {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.Config INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String BASE_URL = "https://eazybooking.asia/";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EAZYTAXI_PREF_NAME = "EAZYTAXI_PREF_NAME";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LOG_CRASH_MSG = "LOG_CRASH_MSG";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String FCM_TOKEN = "fcm_token";
    
    private Config() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/Config$TypeMarkerOptions;", "", "()V", "kioskMarkerType", "", "userMarkerType", "app_wegoRelease"})
    public static final class TypeMarkerOptions {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.TypeMarkerOptions INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String userMarkerType = "userMarkerType";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String kioskMarkerType = "kioskMarkerType";
        
        private TypeMarkerOptions() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/eazy/daiku/utility/Config$TypeDeepLink;", "", "()V", "abaDeepLink", "", "acledaDeepLink", "kessChatDeepLink", "sathapanaDeepLink", "app_wegoRelease"})
    public static final class TypeDeepLink {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.TypeDeepLink INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String kessChatDeepLink = "io.kessinnovation.kesschat";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String acledaDeepLink = "com.domain.acledabankqr";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String sathapanaDeepLink = "kh.com.sathapana.consumer";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String abaDeepLink = "com.paygo24.ibank";
        
        private TypeDeepLink() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/eazy/daiku/utility/Config$TypeScheme;", "", "()V", "abaScheme", "", "acledaScheme", "kessChatScheme", "spnScheme", "app_wegoRelease"})
    public static final class TypeScheme {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.TypeScheme INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String abaScheme = "abamobilebank";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String acledaScheme = "market";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String kessChatScheme = "kesspay.io";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String spnScheme = "spnb";
        
        private TypeScheme() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/utility/Config$KeyServer;", "", "()V", "Slug", "", "app_wegoRelease"})
    public static final class KeyServer {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.KeyServer INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Slug = "car-type";
        
        private KeyServer() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/Config$QrStatus;", "", "()V", "processingTrip", "", "startTrip", "app_wegoRelease"})
    public static final class QrStatus {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.QrStatus INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String processingTrip = "processing_trip";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String startTrip = "start_trip";
        
        private QrStatus() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/Config$KycDocStatus;", "", "()V", "Pending", "", "Refused", "Verified", "app_wegoRelease"})
    public static final class KycDocStatus {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.KycDocStatus INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Pending = "PENDING";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Verified = "VERIFIED";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Refused = "REFUSED";
        
        private KycDocStatus() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/eazy/daiku/utility/Config$HistoryStatus;", "", "()V", "Blocked", "", "Failed", "Paid", "Received", "app_wegoRelease"})
    public static final class HistoryStatus {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.HistoryStatus INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Paid = "paid";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Failed = "failed";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Blocked = "blocked";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Received = "received";
        
        private HistoryStatus() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/Config$BankBic;", "", "()V", "bicABA", "", "bicACL", "app_wegoRelease"})
    public static final class BankBic {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.BankBic INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String bicABA = "ABAAKHPP";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String bicACL = "ACLBKHPP";
        
        private BankBic() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/Config$InternetCon;", "", "()V", "ConnectException", "", "LocalizeException", "SSLHandshakeException", "SSLProtocolException", "UnknownHostException", "app_wegoRelease"})
    public static final class InternetCon {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.InternetCon INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LocalizeException = "LocalizeException";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String UnknownHostException = "UnknownHostException";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String ConnectException = "ConnectException";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SSLHandshakeException = "SSLHandshakeException";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String SSLProtocolException = "SSLProtocolException";
        
        private InternetCon() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0007\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/utility/Config$HttpStatusCode;", "", "()V", "BAD_GATEWAY", "", "GATE_WAY_TIMEOUT", "INTERNAL_SERVER_ERROR", "SERVER_DOWN", "SERVICE_UNAVAILABLE", "SUCCESSED", "UNAUTHORIZED", "app_wegoRelease"})
    public static final class HttpStatusCode {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.HttpStatusCode INSTANCE = null;
        public static final int UNAUTHORIZED = 401;
        public static final int BAD_GATEWAY = 502;
        public static final int GATE_WAY_TIMEOUT = 504;
        public static final int SERVICE_UNAVAILABLE = 503;
        public static final int SERVER_DOWN = 521;
        public static final int INTERNAL_SERVER_ERROR = 500;
        public static final int SUCCESSED = 200;
        
        private HttpStatusCode() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/eazy/daiku/utility/Config$KycDocKey;", "", "()V", "idCard", "", "organizationCode", "plateNumberImg", "plateNumberText", "selfie", "vehiclePicture", "app_wegoRelease"})
    public static final class KycDocKey {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.KycDocKey INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String selfie = "selfie";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String idCard = "idCard";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String vehiclePicture = "vehiclePicture";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String plateNumberImg = "plateNumberImg";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String plateNumberText = "plateNumberText";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String organizationCode = "organizationCode";
        
        private KycDocKey() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/Config$WeGoBusiness;", "", "()V", "Employee", "", "Freelance", "app_wegoRelease"})
    public static final class WeGoBusiness {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.WeGoBusiness INSTANCE = null;
        public static final int Employee = 1;
        public static final int Freelance = 0;
        
        private WeGoBusiness() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/eazy/daiku/utility/Config$ParseServerKey;", "", "()V", "BookingTaxiKey", "", "LiveAttendanceKey", "LiveMapKey", "LiveUserKey", "app_wegoRelease"})
    public static final class ParseServerKey {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.ParseServerKey INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LiveMapKey = "LiveUser";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LiveAttendanceKey = "LiveAttendance";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String LiveUserKey = "LiveUser";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String BookingTaxiKey = "BookingTaxi";
        
        private ParseServerKey() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/Config$TaxiAppType;", "", "()V", "EazyTaxiCustomer", "", "EazyTaxiDriver", "WegoTaxiDriver", "app_wegoRelease"})
    public static final class TaxiAppType {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.TaxiAppType INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EazyTaxiDriver = "eazy_taxi_driver";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String EazyTaxiCustomer = "eazy_taxi_customer";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String WegoTaxiDriver = "wego_taxi_driver";
        
        private TaxiAppType() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0012\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0013\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0005\u0010\u0006R\u001a\u0010\u0007\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\u0006\"\u0004\b\t\u0010\nR\u0011\u0010\u000b\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\u0006R\u0014\u0010\r\u001a\u00020\u0004X\u0086D\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\u0006R\u0011\u0010\u000f\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0006R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u0011\u0010\u0012\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0006R\u0011\u0010\u0014\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0006\u00a8\u0006\u0016"}, d2 = {"Lcom/eazy/daiku/utility/Config$UserDeviceInfo;", "", "()V", "Connection", "", "getConnection", "()Ljava/lang/String;", "appServer", "getAppServer", "setAppServer", "(Ljava/lang/String;)V", "appType", "getAppType", "device", "getDevice", "deviceName", "getDeviceName", "deviceType", "modelName", "getModelName", "version", "getVersion", "app_wegoRelease"})
    public static final class UserDeviceInfo {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.UserDeviceInfo INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        private static java.lang.String appServer;
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String appType = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String deviceType = "android";
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String device = "Android";
        @org.jetbrains.annotations.Nullable()
        private static final java.lang.String Connection = null;
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String modelName = null;
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String deviceName = null;
        @org.jetbrains.annotations.NotNull()
        private static final java.lang.String version = null;
        
        private UserDeviceInfo() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getAppServer() {
            return null;
        }
        
        public final void setAppServer(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getAppType() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDevice() {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getConnection() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getModelName() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getDeviceName() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getVersion() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\n"}, d2 = {"Lcom/eazy/daiku/utility/Config$StatusAssignKey;", "", "()V", "Accepted", "", "Assigned", "Cancelled", "New", "NoResponse", "Rejected", "app_wegoRelease"})
    public static final class StatusAssignKey {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.StatusAssignKey INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Accepted = "Accepted";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Rejected = "Rejected";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Cancelled = "Cancelled";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String NoResponse = "No-Response";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String New = "New";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String Assigned = "Assigned";
        
        private StatusAssignKey() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/Config$TypeMapMarker;", "", "()V", "markerA", "", "markerB", "app_wegoRelease"})
    public static final class TypeMapMarker {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.TypeMapMarker INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String markerA = "markerA";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String markerB = "markerB";
        
        private TypeMapMarker() {
            super();
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/Config$KeySearch;", "", "()V", "currentGPS", "", "pointA", "pointB", "setPointOnMapA", "setPointOnMapB", "app_wegoRelease"})
    public static final class KeySearch {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.Config.KeySearch INSTANCE = null;
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String pointA = "pointA";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String pointB = "pointB";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String currentGPS = "currentGPS";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String setPointOnMapA = "setPointOnMapA";
        @org.jetbrains.annotations.NotNull()
        public static final java.lang.String setPointOnMapB = "setPointOnMapB";
        
        private KeySearch() {
            super();
        }
    }
}