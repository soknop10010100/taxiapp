package com.eazy.daiku.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000|\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006H\'J\u0010\u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\tH\'J\u0010\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\fH\'J\u0010\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u000fH\'J\u0010\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012H\'J\u0010\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0014\u001a\u00020\u0015H\'J\u0010\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u0018H\'J\u0010\u0010\u0019\u001a\u00020\u00042\u0006\u0010\u001a\u001a\u00020\u001bH\'J\u0010\u0010\u001c\u001a\u00020\u00042\u0006\u0010\u001d\u001a\u00020\u001eH\'J\u0010\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020!H\'J\u0010\u0010\"\u001a\u00020\u00042\u0006\u0010#\u001a\u00020$H\'J\u0010\u0010%\u001a\u00020&2\u0006\u0010\'\u001a\u00020(H\'J\u0010\u0010)\u001a\u00020\u00042\u0006\u0010*\u001a\u00020+H\'\u00a8\u0006,"}, d2 = {"Lcom/eazy/daiku/di/module/ViewModelModule;", "", "()V", "bindBaseViewModel", "Landroidx/lifecycle/ViewModel;", "baseViewModel", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "bindCommunityVm", "forgetPwdVm", "Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "bindCreateUserViewModel", "createUserViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/CreateUserViewModel;", "bindCustomerHistoryViewModel", "customerHistoryVm", "Lcom/eazy/daiku/utility/view_model/CustomerHistoryVm;", "bindHistoryVmViewModel", "historyVm", "Lcom/eazy/daiku/utility/view_model/HistoryVm;", "bindLoginViewModel", "loginViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "bindMUserViewModel", "mUserInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "bindProcessCustomerBooking", "processCustomerBookingViewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "bindScanQrVm", "scanQrVm", "Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "bindTransactionViewModel", "transactionVm", "Lcom/eazy/daiku/utility/view_model/TransactionVm;", "bindUploadDocVM", "uploadDocVM", "Lcom/eazy/daiku/utility/view_model/uplaod_doc_vm/UploadDocVM;", "bindViewModelFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "factory", "Lcom/eazy/daiku/utility/view_model/ViewModelFactory;", "bindWithdrawViewModel", "withdrawViewModel", "Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "app_wegoRelease"})
@dagger.Module()
public abstract class ViewModelModule {
    
    public ViewModelModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.base.BaseViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindBaseViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseViewModel baseViewModel);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.user_case.CreateUserViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindCreateUserViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.user_case.CreateUserViewModel createUserViewModel);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.user_case.LoginViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindLoginViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.user_case.LoginViewModel loginViewModel);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.uplaod_doc_vm.UploadDocVM.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindUploadDocVM(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.uplaod_doc_vm.UploadDocVM uploadDocVM);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.QrCodeVm.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindScanQrVm(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.QrCodeVm scanQrVm);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.user_case.UseCaseVm.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindMUserViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.user_case.UseCaseVm mUserInfoVM);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.TransactionVm.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindTransactionViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.TransactionVm transactionVm);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.HistoryVm.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindHistoryVmViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.HistoryVm historyVm);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.withdraw.WithdrawViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindWithdrawViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.withdraw.WithdrawViewModel withdrawViewModel);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.community.CommunityVm.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindCommunityVm(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.community.CommunityVm forgetPwdVm);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindProcessCustomerBooking(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel processCustomerBookingViewModel);
    
    @org.jetbrains.annotations.NotNull()
    @ViewModelKey(value = com.eazy.daiku.utility.view_model.CustomerHistoryVm.class)
    @dagger.multibindings.IntoMap()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModel bindCustomerHistoryViewModel(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.CustomerHistoryVm customerHistoryVm);
    
    @org.jetbrains.annotations.NotNull()
    @dagger.Binds()
    public abstract androidx.lifecycle.ViewModelProvider.Factory bindViewModelFactory(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.ViewModelFactory factory);
}