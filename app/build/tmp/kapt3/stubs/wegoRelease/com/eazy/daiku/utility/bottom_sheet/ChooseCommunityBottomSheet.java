package com.eazy.daiku.utility.bottom_sheet;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000v\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\r\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\u0018\u0000 72\u00020\u0001:\u000267B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u001d\u001a\u00020\u0013H\u0002J\b\u0010\u001e\u001a\u00020\u0013H\u0002J\b\u0010\u001f\u001a\u00020\u0013H\u0002J\b\u0010 \u001a\u00020\u0013H\u0002J\u0010\u0010!\u001a\u00020\u00132\u0006\u0010\"\u001a\u00020#H\u0016J\u0012\u0010$\u001a\u00020%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J$\u0010(\u001a\u00020)2\u0006\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010-2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\b\u0010.\u001a\u00020\u0013H\u0016J\u001a\u0010/\u001a\u00020\u00132\u0006\u00100\u001a\u00020)2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u0010\u00101\u001a\u00020\u00132\u0006\u00102\u001a\u000203H\u0002J\u0010\u00104\u001a\u00020\u00132\u0006\u00105\u001a\u00020\u0015H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0010\u0012\n\u0012\b\u0012\u0004\u0012\u00020\b0\u0007\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u000fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0010\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00130\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R+\u0010\u0016\u001a\u00020\u00152\u0006\u0010\u0014\u001a\u00020\u00158B@BX\u0082\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u001b\u0010\u001c\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u00068"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet;", "Lcom/eazy/daiku/utility/base/BaseBottomSheetDialogFragment;", "()V", "binding", "Lcom/eazy/daiku/databinding/ChooseCommunityTaxiLayoutBinding;", "communityLiveData", "Landroidx/lifecycle/LiveData;", "Landroidx/paging/PagedList;", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "communityTaxiAdapter", "Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet$CommunityTaxiAdapter;", "communityTaxiTemporaryVm", "Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "communityTaxiVm", "fContext", "Landroidx/fragment/app/FragmentActivity;", "oldLists", "selectCommunityTaxListener", "Lkotlin/Function1;", "", "<set-?>", "", "termId", "getTermId", "()I", "setTermId", "(I)V", "termId$delegate", "Lkotlin/properties/ReadWriteProperty;", "doAction", "initData", "initObserved", "initView", "onAttach", "context", "Landroid/content/Context;", "onCreateDialog", "Landroid/app/Dialog;", "savedInstanceState", "Landroid/os/Bundle;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "onStart", "onViewCreated", "view", "searchCommunityTaxi", "searchText", "", "showEmptyData", "size", "CommunityTaxiAdapter", "Companion", "app_wegoRelease"})
public final class ChooseCommunityBottomSheet extends com.eazy.daiku.utility.base.BaseBottomSheetDialogFragment {
    private com.eazy.daiku.databinding.ChooseCommunityTaxiLayoutBinding binding;
    private androidx.fragment.app.FragmentActivity fContext;
    private com.eazy.daiku.utility.view_model.community.CommunityVm communityTaxiVm;
    private kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> selectCommunityTaxListener;
    private com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet.CommunityTaxiAdapter communityTaxiAdapter;
    private androidx.lifecycle.LiveData<androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond>> communityLiveData;
    private com.eazy.daiku.utility.view_model.community.CommunityVm communityTaxiTemporaryVm;
    private androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond> oldLists;
    private final kotlin.properties.ReadWriteProperty termId$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet.Companion Companion = null;
    
    public ChooseCommunityBottomSheet() {
        super();
    }
    
    private final int getTermId() {
        return 0;
    }
    
    private final void setTermId(int p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet newInstance(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.community.CommunityVm communityTaxiVm, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.view_model.community.CommunityVm communityTaxiTemporaryVm, @org.jetbrains.annotations.NotNull()
    androidx.lifecycle.LiveData<androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond>> communityLiveData, int termId, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> selectCommunityTaxListener) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @java.lang.Override()
    public void onStart() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.app.Dialog onCreateDialog(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    private final void initData() {
    }
    
    private final void searchCommunityTaxi(java.lang.String searchText) {
    }
    
    private final void showEmptyData(int size) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0002\u0018\u0000 \u00192\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0019\u001aB\u0005\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\u000e\u001a\u00020\t2\u0006\u0010\u000f\u001a\u00020\u00032\u0006\u0010\u0010\u001a\u00020\u0006H\u0016J\u0018\u0010\u0011\u001a\u00020\u00032\u0006\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0006H\u0016J\u001c\u0010\u0015\u001a\u00020\t2\f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u0006R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\t0\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u001b"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet$CommunityTaxiAdapter;", "Landroidx/paging/PagedListAdapter;", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet$CommunityTaxiAdapter$MyCommunityTaxiViewHolder;", "()V", "selectCommunityTaxiId", "", "selectRow", "Lkotlin/Function1;", "", "getSelectRow", "()Lkotlin/jvm/functions/Function1;", "setSelectRow", "(Lkotlin/jvm/functions/Function1;)V", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "submitData", "pagedList", "Landroidx/paging/PagedList;", "termId", "Companion", "MyCommunityTaxiViewHolder", "app_wegoRelease"})
    static final class CommunityTaxiAdapter extends androidx.paging.PagedListAdapter<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet.CommunityTaxiAdapter.MyCommunityTaxiViewHolder> {
        private int selectCommunityTaxiId = -1;
        public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> selectRow;
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet.CommunityTaxiAdapter.Companion Companion = null;
        private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond> USER_COMPARATOR = null;
        
        public CommunityTaxiAdapter() {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> getSelectRow() {
            return null;
        }
        
        public final void setSelectRow(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> p0) {
        }
        
        public final void submitData(@org.jetbrains.annotations.NotNull()
        androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond> pagedList, int termId) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet.CommunityTaxiAdapter.MyCommunityTaxiViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet.CommunityTaxiAdapter.MyCommunityTaxiViewHolder holder, int position) {
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fJ\u000e\u0010\r\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\u000eR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000f"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet$CommunityTaxiAdapter$MyCommunityTaxiViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "tickImg", "Landroid/widget/ImageView;", "titleTv", "Landroid/widget/TextView;", "bind", "", "communityTaxi", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "selectItem", "", "app_wegoRelease"})
        public static final class MyCommunityTaxiViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            private final android.widget.TextView titleTv = null;
            private final android.widget.ImageView tickImg = null;
            
            public MyCommunityTaxiViewHolder(@org.jetbrains.annotations.NotNull()
            android.view.View itemView) {
                super(null);
            }
            
            public final void bind(@org.jetbrains.annotations.NotNull()
            com.eazy.daiku.data.model.server_model.CommunityTaxiRespond communityTaxi) {
            }
            
            public final void selectItem(boolean selectItem) {
            }
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet$CommunityTaxiAdapter$Companion;", "", "()V", "USER_COMPARATOR", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "app_wegoRelease"})
        public static final class Companion {
            
            private Companion() {
                super();
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002JH\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00062\u0012\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\t2\u0006\u0010\f\u001a\u00020\r2\u0012\u0010\u000e\u001a\u000e\u0012\u0004\u0012\u00020\u000b\u0012\u0004\u0012\u00020\u00100\u000fH\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/bottom_sheet/ChooseCommunityBottomSheet;", "communityTaxiVm", "Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "communityTaxiTemporaryVm", "communityLiveData", "Landroidx/lifecycle/LiveData;", "Landroidx/paging/PagedList;", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "termId", "", "selectCommunityTaxListener", "Lkotlin/Function1;", "", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.utility.bottom_sheet.ChooseCommunityBottomSheet newInstance(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.view_model.community.CommunityVm communityTaxiVm, @org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.view_model.community.CommunityVm communityTaxiTemporaryVm, @org.jetbrains.annotations.NotNull()
        androidx.lifecycle.LiveData<androidx.paging.PagedList<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond>> communityLiveData, int termId, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.CommunityTaxiRespond, kotlin.Unit> selectCommunityTaxListener) {
            return null;
        }
    }
}