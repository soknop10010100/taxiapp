package com.eazy.daiku.ui.customer.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000>\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001aB\u0005\u00a2\u0006\u0002\u0010\u0003J\u001e\u0010\u000e\u001a\u00020\t2\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u000fJ\u0006\u0010\u0010\u001a\u00020\tJ\b\u0010\u0011\u001a\u00020\u0012H\u0016J\u0018\u0010\u0013\u001a\u00020\t2\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\u0012H\u0016J\u0018\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u0012H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R&\u0010\u0007\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\t0\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\r\u00a8\u0006\u001b"}, d2 = {"Lcom/eazy/daiku/ui/customer/adapter/SearchMapAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/ui/customer/adapter/SearchMapAdapter$SelectCarBookingHolder;", "()V", "listMap", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/sear_map/Results;", "onSelectRowSearchMap", "Lkotlin/Function1;", "", "getOnSelectRowSearchMap", "()Lkotlin/jvm/functions/Function1;", "setOnSelectRowSearchMap", "(Lkotlin/jvm/functions/Function1;)V", "add", "Lkotlin/collections/ArrayList;", "clear", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "SelectCarBookingHolder", "app_wegoRelease"})
public final class SearchMapAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.ui.customer.adapter.SearchMapAdapter.SelectCarBookingHolder> {
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.sear_map.Results> listMap;
    public kotlin.jvm.functions.Function1<? super com.eazy.daiku.ui.customer.model.sear_map.Results, kotlin.Unit> onSelectRowSearchMap;
    
    public SearchMapAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.ui.customer.model.sear_map.Results, kotlin.Unit> getOnSelectRowSearchMap() {
        return null;
    }
    
    public final void setOnSelectRowSearchMap(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.ui.customer.model.sear_map.Results, kotlin.Unit> p0) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.sear_map.Results> listMap) {
    }
    
    public final void clear() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.ui.customer.adapter.SearchMapAdapter.SelectCarBookingHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.adapter.SearchMapAdapter.SelectCarBookingHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bR\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/eazy/daiku/ui/customer/adapter/SearchMapAdapter$SelectCarBookingHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "description", "Landroid/widget/TextView;", "title", "bind", "", "results", "Lcom/eazy/daiku/ui/customer/model/sear_map/Results;", "app_wegoRelease"})
    public static final class SelectCarBookingHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final android.widget.TextView title = null;
        private final android.widget.TextView description = null;
        
        public SelectCarBookingHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.customer.model.sear_map.Results results) {
        }
    }
}