package com.eazy.daiku.utility.pagin.transaction;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001B9\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\"\u0010\b\u001a\u001e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b`\f\u00a2\u0006\u0002\u0010\rJ\u0014\u0010\u0015\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0016H\u0016R\u0014\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R*\u0010\b\u001a\u001e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b0\tj\u000e\u0012\u0004\u0012\u00020\n\u0012\u0004\u0012\u00020\u000b`\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00100\u00128F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014\u00a8\u0006\u0017"}, d2 = {"Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginDataSourceFactory;", "Landroidx/paging/DataSource$Factory;", "", "Lcom/eazy/daiku/data/model/MyTransaction;", "context", "Landroid/content/Context;", "baseViewModel", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "queryKey", "Ljava/util/HashMap;", "", "", "Lkotlin/collections/HashMap;", "(Landroid/content/Context;Lcom/eazy/daiku/utility/base/BaseViewModel;Ljava/util/HashMap;)V", "_trxDataSourceMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginDataSource;", "trxDataSourceMutableLiveData", "Landroidx/lifecycle/LiveData;", "getTrxDataSourceMutableLiveData", "()Landroidx/lifecycle/LiveData;", "create", "Landroidx/paging/DataSource;", "app_wegoRelease"})
public final class TransactionPaginDataSourceFactory extends androidx.paging.DataSource.Factory<java.lang.Integer, com.eazy.daiku.data.model.MyTransaction> {
    private android.content.Context context;
    private com.eazy.daiku.utility.base.BaseViewModel baseViewModel;
    private java.util.HashMap<java.lang.String, java.lang.Object> queryKey;
    private androidx.lifecycle.MutableLiveData<com.eazy.daiku.utility.pagin.transaction.TransactionPaginDataSource> _trxDataSourceMutableLiveData;
    
    public TransactionPaginDataSourceFactory(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseViewModel baseViewModel, @org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> queryKey) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.utility.pagin.transaction.TransactionPaginDataSource> getTrxDataSourceMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public androidx.paging.DataSource<java.lang.Integer, com.eazy.daiku.data.model.MyTransaction> create() {
        return null;
    }
}