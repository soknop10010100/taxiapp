package com.eazy.daiku.data.signature;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0016\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u00040\n2\u0006\u0010\u000b\u001a\u00020\fH\u0002J\u000e\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\fJ\u0010\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0010\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0005\u001a\u00020\u0006H\u0002J\u0018\u0010\u0013\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0014\u001a\u00020\u0004\u00a8\u0006\u0015"}, d2 = {"Lcom/eazy/daiku/data/signature/RSA2;", "", "()V", "encryptByPublicKey", "", "context", "Landroid/content/Context;", "data", "", "getKSort", "", "jsonObject", "Lcom/google/gson/JsonObject;", "makeKeySort", "body", "readPrivateKey", "Ljava/security/interfaces/RSAPrivateKey;", "readPublicKey", "Ljava/security/interfaces/RSAPublicKey;", "sign", "plainText", "app_wegoRelease"})
public final class RSA2 {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.data.signature.RSA2 INSTANCE = null;
    
    private RSA2() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.Exception.class})
    public final java.lang.String sign(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String plainText) throws java.lang.Exception {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String encryptByPublicKey(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    byte[] data) {
        return null;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.Exception.class})
    private final java.security.interfaces.RSAPrivateKey readPrivateKey(android.content.Context context) throws java.lang.Exception {
        return null;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.lang.Exception.class})
    private final java.security.interfaces.RSAPublicKey readPublicKey(android.content.Context context) throws java.lang.Exception {
        return null;
    }
    
    private final java.util.List<java.lang.String> getKSort(com.google.gson.JsonObject jsonObject) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String makeKeySort(@org.jetbrains.annotations.NotNull()
    com.google.gson.JsonObject body) {
        return null;
    }
}