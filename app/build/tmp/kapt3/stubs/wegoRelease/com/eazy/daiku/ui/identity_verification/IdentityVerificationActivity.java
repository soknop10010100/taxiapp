package com.eazy.daiku.ui.identity_verification;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0002J\b\u0010\u000b\u001a\u00020\nH\u0002J\u0006\u0010\f\u001a\u00020\nJ\u0012\u0010\r\u001a\u00020\n2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000fH\u0014J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0015"}, d2 = {"Lcom/eazy/daiku/ui/identity_verification/IdentityVerificationActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityIdentityVerificationBinding;", "getBinding", "()Lcom/eazy/daiku/databinding/ActivityIdentityVerificationBinding;", "setBinding", "(Lcom/eazy/daiku/databinding/ActivityIdentityVerificationBinding;)V", "doAction", "", "initCurrentStatusUser", "initView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "Companion", "app_wegoRelease"})
public final class IdentityVerificationActivity extends com.eazy.daiku.utility.base.BaseActivity {
    public com.eazy.daiku.databinding.ActivityIdentityVerificationBinding binding;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.identity_verification.IdentityVerificationActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KycDataKey = "kyc_data_key";
    
    public IdentityVerificationActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.databinding.ActivityIdentityVerificationBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.databinding.ActivityIdentityVerificationBinding p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    public final void initView() {
    }
    
    private final void initCurrentStatusUser() {
    }
    
    private final void doAction() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/identity_verification/IdentityVerificationActivity$Companion;", "", "()V", "KycDataKey", "", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}