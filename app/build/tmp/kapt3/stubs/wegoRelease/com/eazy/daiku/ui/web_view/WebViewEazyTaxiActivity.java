package com.eazy.daiku.ui.web_view;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 \u00102\u00020\u0001:\u0002\u0010\u0011B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0005\u001a\u00020\u0006H\u0002J\b\u0010\u0007\u001a\u00020\u0006H\u0002J\b\u0010\b\u001a\u00020\u0006H\u0016J\u0012\u0010\t\u001a\u00020\u00062\b\u0010\n\u001a\u0004\u0018\u00010\u000bH\u0014J\u0010\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/eazy/daiku/ui/web_view/WebViewEazyTaxiActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityWebViewEazyTaxiBinding;", "initGoogleDataLoad", "", "initView", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "Companion", "GeoWebChromeClient", "app_wegoRelease"})
public final class WebViewEazyTaxiActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityWebViewEazyTaxiBinding binding;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.web_view.WebViewEazyTaxiActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DATA_GOOGLE = "data_google";
    
    public WebViewEazyTaxiActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    private final void initView() {
    }
    
    private final void initGoogleDataLoad() {
    }
    
    /**
     * WebChromeClient subclass handles UI-related calls
     * Note: think chrome as in decoration, not the Chrome browser
     */
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0016\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/ui/web_view/WebViewEazyTaxiActivity$GeoWebChromeClient;", "Landroid/webkit/WebChromeClient;", "()V", "onGeolocationPermissionsShowPrompt", "", "origin", "", "callback", "Landroid/webkit/GeolocationPermissions$Callback;", "app_wegoRelease"})
    public static final class GeoWebChromeClient extends android.webkit.WebChromeClient {
        
        public GeoWebChromeClient() {
            super();
        }
        
        @java.lang.Override()
        public void onGeolocationPermissionsShowPrompt(@org.jetbrains.annotations.Nullable()
        java.lang.String origin, @org.jetbrains.annotations.NotNull()
        android.webkit.GeolocationPermissions.Callback callback) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/web_view/WebViewEazyTaxiActivity$Companion;", "", "()V", "DATA_GOOGLE", "", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}