package com.eazy.daiku.utility.pagin.transaction;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 \u00142\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00030\u0001:\u0002\u0014\u0015B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0018\u0010\f\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u00032\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0018\u0010\u0010\u001a\u00020\u00032\u0006\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u000fH\u0016R&\u0010\u0005\u001a\u000e\u0012\u0004\u0012\u00020\u0002\u0012\u0004\u0012\u00020\u00070\u0006X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000b\u00a8\u0006\u0016"}, d2 = {"Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginAdapter;", "Landroidx/paging/PagedListAdapter;", "Lcom/eazy/daiku/data/model/MyTransaction;", "Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginAdapter$MyTransactionPaginViewHolder;", "()V", "selectRowTrx", "Lkotlin/Function1;", "", "getSelectRowTrx", "()Lkotlin/jvm/functions/Function1;", "setSelectRowTrx", "(Lkotlin/jvm/functions/Function1;)V", "onBindViewHolder", "holder", "position", "", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "Companion", "MyTransactionPaginViewHolder", "app_wegoRelease"})
public final class TransactionPaginAdapter extends androidx.paging.PagedListAdapter<com.eazy.daiku.data.model.MyTransaction, com.eazy.daiku.utility.pagin.transaction.TransactionPaginAdapter.MyTransactionPaginViewHolder> {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.pagin.transaction.TransactionPaginAdapter.Companion Companion = null;
    private static final androidx.recyclerview.widget.DiffUtil.ItemCallback<com.eazy.daiku.data.model.MyTransaction> USER_COMPARATOR = null;
    public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.MyTransaction, kotlin.Unit> selectRowTrx;
    
    public TransactionPaginAdapter() {
        super(null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.utility.pagin.transaction.TransactionPaginAdapter.MyTransactionPaginViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.pagin.transaction.TransactionPaginAdapter.MyTransactionPaginViewHolder holder, int position) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.MyTransaction, kotlin.Unit> getSelectRowTrx() {
        return null;
    }
    
    public final void setSelectRowTrx(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.MyTransaction, kotlin.Unit> p0) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u0015R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0016"}, d2 = {"Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginAdapter$MyTransactionPaginViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "amountTv", "Landroid/widget/TextView;", "headerContainerView", "Landroid/widget/LinearLayout;", "remarkTv", "statusTrxImg", "Landroid/widget/ImageView;", "timeTv", "titleHeaderTv", "titleTv", "totalAmountDailyTv", "totalByDateCardView", "Lcom/google/android/material/card/MaterialCardView;", "bind", "", "transaction", "Lcom/eazy/daiku/data/model/MyTransaction;", "app_wegoRelease"})
    public static final class MyTransactionPaginViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        private final android.widget.TextView titleTv = null;
        private final android.widget.TextView remarkTv = null;
        private final android.widget.TextView timeTv = null;
        private final android.widget.TextView amountTv = null;
        private final android.widget.TextView titleHeaderTv = null;
        private final android.widget.LinearLayout headerContainerView = null;
        private final android.widget.TextView totalAmountDailyTv = null;
        private final android.widget.ImageView statusTrxImg = null;
        private final com.google.android.material.card.MaterialCardView totalByDateCardView = null;
        
        public MyTransactionPaginViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.MyTransaction transaction) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/utility/pagin/transaction/TransactionPaginAdapter$Companion;", "", "()V", "USER_COMPARATOR", "Landroidx/recyclerview/widget/DiffUtil$ItemCallback;", "Lcom/eazy/daiku/data/model/MyTransaction;", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}