package com.eazy.daiku.utility.service.location.foreground_;

import java.lang.System;

/**
 * Service tracks location when requested and updates Activity via binding. If Activity is
 * stopped/unbinds and tracking is enabled, the service promotes itself to a foreground service to
 * insure location updates aren't interrupted.
 *
 * For apps running in the background on O+ devices, location is computed much less than previous
 * versions. Please reference documentation for details.
 */
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000t\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 .2\u00020\u0001:\u0002./B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0016\u001a\u00020\u00172\b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u0002J\u0010\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016J\b\u0010!\u001a\u00020\u001eH\u0016J\b\u0010\"\u001a\u00020\u001eH\u0016J\u0010\u0010#\u001a\u00020\u001e2\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J \u0010$\u001a\u00020%2\u0006\u0010\u001b\u001a\u00020\u001c2\u0006\u0010&\u001a\u00020%2\u0006\u0010\'\u001a\u00020%H\u0016J\u0010\u0010(\u001a\u00020\u00072\u0006\u0010\u001b\u001a\u00020\u001cH\u0016J\u0006\u0010)\u001a\u00020\u001eJ\u0006\u0010*\u001a\u00020\u001eJ\u001a\u0010+\u001a\u00020\u001e2\u0006\u0010,\u001a\u00020-2\b\u0010\u0018\u001a\u0004\u0018\u00010\tH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u0012\u0010\r\u001a\u00060\u000eR\u00020\u0000X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0010X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0014X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00060"}, d2 = {"Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;", "Landroid/app/Service;", "()V", "appServer", "", "appType", "configurationChange", "", "currentLocation", "Landroid/location/Location;", "deviceType", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "localBinder", "Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService$LocalBinder;", "locationCallback", "Lcom/google/android/gms/location/LocationCallback;", "locationRequest", "Lcom/google/android/gms/location/LocationRequest;", "notificationManager", "Landroid/app/NotificationManager;", "serviceRunningInForeground", "generateNotification", "Landroid/app/Notification;", "location", "onBind", "Landroid/os/IBinder;", "intent", "Landroid/content/Intent;", "onConfigurationChanged", "", "newConfig", "Landroid/content/res/Configuration;", "onCreate", "onDestroy", "onRebind", "onStartCommand", "", "flags", "startId", "onUnbind", "subscribeToLocationUpdates", "unsubscribeToLocationUpdates", "uploadToParseServer", "context", "Landroid/content/Context;", "Companion", "LocalBinder", "app_wegoRelease"})
public final class ForegroundOnlyLocationService extends android.app.Service {
    private boolean configurationChange = false;
    private boolean serviceRunningInForeground = false;
    private final com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyLocationService.LocalBinder localBinder = null;
    private java.lang.String appServer;
    private java.lang.String appType;
    private java.lang.String deviceType = "android";
    private android.app.NotificationManager notificationManager;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private com.google.android.gms.location.LocationRequest locationRequest;
    private com.google.android.gms.location.LocationCallback locationCallback;
    private android.location.Location currentLocation;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyLocationService.Companion Companion = null;
    private static final java.lang.String TAG = "ForegroundOnlyLocationService";
    private static final java.lang.String PACKAGE_NAME = "com.eazy.wego.driver.taxi";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST = "com.eazy.wego.driver.taxi.action.FOREGROUND_ONLY_LOCATION_BROADCAST";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String EXTRA_LOCATION = "com.eazy.wego.driver.taxi.extra.LOCATION";
    private static final java.lang.String EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION = "com.eazy.wego.driver.taxi.extra.CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION";
    private static final int NOTIFICATION_ID = 12345678;
    private static final java.lang.String NOTIFICATION_CHANNEL_ID = "while_in_use_channel_01";
    
    public ForegroundOnlyLocationService() {
        super();
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    @java.lang.Override()
    public int onStartCommand(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent, int flags, int startId) {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.os.IBinder onBind(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
        return null;
    }
    
    @java.lang.Override()
    public void onRebind(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
    
    @java.lang.Override()
    public boolean onUnbind(@org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
        return false;
    }
    
    @java.lang.Override()
    public void onDestroy() {
    }
    
    @java.lang.Override()
    public void onConfigurationChanged(@org.jetbrains.annotations.NotNull()
    android.content.res.Configuration newConfig) {
    }
    
    public final void subscribeToLocationUpdates() {
    }
    
    public final void unsubscribeToLocationUpdates() {
    }
    
    private final android.app.Notification generateNotification(android.location.Location location) {
        return null;
    }
    
    private final void uploadToParseServer(android.content.Context context, android.location.Location location) {
    }
    
    /**
     * Class used for the client Binder.  Since this service runs in the same process as its
     * clients, we don't need to deal with IPC.
     */
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002R\u0014\u0010\u0003\u001a\u00020\u00048@X\u0080\u0004\u00a2\u0006\u0006\u001a\u0004\b\u0005\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService$LocalBinder;", "Landroid/os/Binder;", "(Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;)V", "service", "Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;", "getService$app_wegoRelease", "()Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService;", "app_wegoRelease"})
    public final class LocalBinder extends android.os.Binder {
        
        public LocalBinder() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyLocationService getService$app_wegoRelease() {
            return null;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0004\n\u0002\u0010\b\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0080T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\f"}, d2 = {"Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyLocationService$Companion;", "", "()V", "ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST", "", "EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION", "EXTRA_LOCATION", "NOTIFICATION_CHANNEL_ID", "NOTIFICATION_ID", "", "PACKAGE_NAME", "TAG", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}