package com.eazy.daiku.utility;

import java.lang.System;

/**
 * Provides access to SharedPreferences for location to Activities and Services.
 */
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\b\u00c0\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0006\u001a\u00020\u00072\u0006\u0010\b\u001a\u00020\tJ\u0016\u0010\n\u001a\u00020\u000b2\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\f\u001a\u00020\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\r"}, d2 = {"Lcom/eazy/daiku/utility/SharedPreferenceUtil;", "", "()V", "KEY_FOREGROUND_ENABLED", "", "preference_file_key", "getLocationTrackingPref", "", "context", "Landroid/content/Context;", "saveLocationTrackingPref", "", "requestingLocationUpdates", "app_wegoRelease"})
public final class SharedPreferenceUtil {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.SharedPreferenceUtil INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String preference_file_key = "com.eazy.wego.driver.taxi.PREFERENCE_FILE_KEY";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KEY_FOREGROUND_ENABLED = "tracking_foreground_location";
    
    private SharedPreferenceUtil() {
        super();
    }
    
    /**
     * Returns true if requesting location updates, otherwise returns false.
     *
     * @param context The [Context].
     */
    public final boolean getLocationTrackingPref(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * Stores the location updates state in SharedPreferences.
     * @param requestingLocationUpdates The location updates state.
     */
    public final void saveLocationTrackingPref(@org.jetbrains.annotations.NotNull()
    android.content.Context context, boolean requestingLocationUpdates) {
    }
}