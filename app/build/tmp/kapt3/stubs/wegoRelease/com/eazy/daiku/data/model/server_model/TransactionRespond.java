package com.eazy.daiku.data.model.server_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0012\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B7\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0018\b\u0002\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b\u00a2\u0006\u0002\u0010\tJ\u0010\u0010\u0015\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJ\u0010\u0010\u0016\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003\u00a2\u0006\u0002\u0010\u000bJ\u0019\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u00c6\u0003J@\u0010\u0018\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0018\b\u0002\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\bH\u00c6\u0001\u00a2\u0006\u0002\u0010\u0019J\u0013\u0010\u001a\u001a\u00020\u001b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u001d\u001a\u00020\u001eH\u00d6\u0001J\t\u0010\u001f\u001a\u00020 H\u00d6\u0001R\"\u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\n\u0010\u000b\"\u0004\b\f\u0010\rR\"\u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u000e\u001a\u0004\b\u000f\u0010\u000b\"\u0004\b\u0010\u0010\rR.\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00070\u0006j\b\u0012\u0004\u0012\u00020\u0007`\b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014\u00a8\u0006!"}, d2 = {"Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "", "blockedBalance", "", "totalBalance", "transactions", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/Transaction;", "Lkotlin/collections/ArrayList;", "(Ljava/lang/Double;Ljava/lang/Double;Ljava/util/ArrayList;)V", "getBlockedBalance", "()Ljava/lang/Double;", "setBlockedBalance", "(Ljava/lang/Double;)V", "Ljava/lang/Double;", "getTotalBalance", "setTotalBalance", "getTransactions", "()Ljava/util/ArrayList;", "setTransactions", "(Ljava/util/ArrayList;)V", "component1", "component2", "component3", "copy", "(Ljava/lang/Double;Ljava/lang/Double;Ljava/util/ArrayList;)Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "equals", "", "other", "hashCode", "", "toString", "", "app_wegoRelease"})
public final class TransactionRespond {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "blocked_balance")
    private java.lang.Double blockedBalance;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "total_balance")
    private java.lang.Double totalBalance;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "transactions")
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.Transaction> transactions;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.model.server_model.TransactionRespond copy(@org.jetbrains.annotations.Nullable()
    java.lang.Double blockedBalance, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalBalance, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Transaction> transactions) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public TransactionRespond() {
        super();
    }
    
    public TransactionRespond(@org.jetbrains.annotations.Nullable()
    java.lang.Double blockedBalance, @org.jetbrains.annotations.Nullable()
    java.lang.Double totalBalance, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Transaction> transactions) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getBlockedBalance() {
        return null;
    }
    
    public final void setBlockedBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotalBalance() {
        return null;
    }
    
    public final void setTotalBalance(@org.jetbrains.annotations.Nullable()
    java.lang.Double p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.Transaction> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.Transaction> getTransactions() {
        return null;
    }
    
    public final void setTransactions(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Transaction> p0) {
    }
}