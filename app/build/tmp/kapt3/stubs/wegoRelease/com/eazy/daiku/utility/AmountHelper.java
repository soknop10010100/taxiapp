package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010\u0006\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00042\u0006\u0010\u0006\u001a\u00020\u0007J\u0016\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u000e\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0007\u00a8\u0006\u0010"}, d2 = {"Lcom/eazy/daiku/utility/AmountHelper;", "", "()V", "amountFormat", "", "currencySymbol", "amount", "", "decimalFilter", "", "editText", "Landroid/widget/EditText;", "digit", "", "latLngFormat", "kilomater", "app_wegoRelease"})
public final class AmountHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.AmountHelper INSTANCE = null;
    
    private AmountHelper() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String amountFormat(@org.jetbrains.annotations.NotNull()
    java.lang.String currencySymbol, double amount) {
        return null;
    }
    
    public final void decimalFilter(@org.jetbrains.annotations.NotNull()
    android.widget.EditText editText, int digit) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String latLngFormat(double kilomater) {
        return null;
    }
}