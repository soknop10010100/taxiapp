package com.eazy.daiku.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00c6\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\'J\b\u0010\u0005\u001a\u00020\u0006H\'J\b\u0010\u0007\u001a\u00020\bH\'J\b\u0010\t\u001a\u00020\nH\'J\b\u0010\u000b\u001a\u00020\fH\'J\b\u0010\r\u001a\u00020\u000eH\'J\b\u0010\u000f\u001a\u00020\u0010H\'J\b\u0010\u0011\u001a\u00020\u0012H\'J\b\u0010\u0013\u001a\u00020\u0014H\'J\b\u0010\u0015\u001a\u00020\u0016H\'J\b\u0010\u0017\u001a\u00020\u0018H\'J\b\u0010\u0019\u001a\u00020\u001aH\'J\b\u0010\u001b\u001a\u00020\u001cH\'J\b\u0010\u001d\u001a\u00020\u001eH\'J\b\u0010\u001f\u001a\u00020 H\'J\b\u0010!\u001a\u00020\"H\'J\b\u0010#\u001a\u00020$H\'J\b\u0010%\u001a\u00020&H\'J\b\u0010\'\u001a\u00020(H\'J\b\u0010)\u001a\u00020*H\'J\b\u0010+\u001a\u00020,H\'J\b\u0010-\u001a\u00020.H\'J\b\u0010/\u001a\u000200H\'J\b\u00101\u001a\u000202H\'J\b\u00103\u001a\u000204H\'J\b\u00105\u001a\u000206H\'J\b\u00107\u001a\u000208H\'J\b\u00109\u001a\u00020:H\'J\b\u0010;\u001a\u00020<H\'J\b\u0010=\u001a\u00020>H\'J\b\u0010?\u001a\u00020@H\'\u00a8\u0006A"}, d2 = {"Lcom/eazy/daiku/di/module/ActivityBuilderModule;", "", "()V", "aboutUsActivity", "Lcom/eazy/daiku/ui/about_us/AboutUsActivity;", "baseActivity", "Lcom/eazy/daiku/utility/base/BaseActivity;", "baseCoreActivity", "Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "changePasswordActivity", "Lcom/eazy/daiku/ui/change_password/ChangePasswordActivity;", "customerMapPreViewActivity", "Lcom/eazy/daiku/ui/customer/map/CustomerMapPreViewActivity;", "eazyTaxiWithdrawWebViewActivity", "Lcom/eazy/daiku/ui/withdraw/EazyTaxiWithdrawWebviewActivity;", "editProfileActivity", "Lcom/eazy/daiku/ui/profile/EditProfileActivity;", "forgetPasswordActivity", "Lcom/eazy/daiku/ui/forget_password/ForgetPasswordActivity;", "forgotPasswordActivity", "Lcom/eazy/daiku/ui/forget_password/VerifyOtpCodeActivity;", "historyTripActivity", "Lcom/eazy/daiku/ui/history/HistoryTripActivity;", "identityVerificationActivity", "Lcom/eazy/daiku/ui/identity_verification/IdentityVerificationActivity;", "listCarTaxiActivity", "Lcom/eazy/daiku/ui/customer/step_booking/ListCarTaxiActivity;", "locationKioskBookingTaxiActivity", "Lcom/eazy/daiku/ui/customer/step_booking/LocationKioskBookingTaxiActivity;", "loginActivity", "Lcom/eazy/daiku/ui/login/LoginActivity;", "mProfileActivity", "Lcom/eazy/daiku/ui/profile/MyProfileActivity;", "mainActivity", "Lcom/eazy/daiku/ui/MainActivity;", "mainWalletActivity", "Lcom/eazy/daiku/ui/wallet/MainWalletActivity;", "mapPreviewActivity", "Lcom/eazy/daiku/ui/map/MapPreviewActivity;", "paymentCompleteActivity", "Lcom/eazy/daiku/ui/customer/step_booking/PaymentCompleteActivity;", "previewCheckoutActivity", "Lcom/eazy/daiku/ui/customer/step_booking/PreviewCheckoutActivity;", "prickUpLocationActivity", "Lcom/eazy/daiku/ui/customer/map/PickUpLocationActivity;", "scanQRCodeActivity", "Lcom/eazy/daiku/ui/scan_qr_code/ScanQRCodeActivity;", "searchMapActivity", "Lcom/eazy/daiku/ui/customer/map/SearchMapActivity;", "signUpActivity", "Lcom/eazy/daiku/ui/signup/SignUpActivity;", "simpleBaseActivity", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "splashScreenActivity", "Lcom/eazy/daiku/ui/splash_screen/SplashScreenActivity;", "uploadDocActivity", "Lcom/eazy/daiku/ui/upload_doc/UploadDocActivity;", "verificationPinActivity", "Lcom/eazy/daiku/ui/verification_pin_code/VerificationPinActivity;", "webPayActivity", "Lcom/eazy/daiku/ui/customer/web_payment/WebPayActivity;", "webViewEazyTaxiActivity", "Lcom/eazy/daiku/ui/web_view/WebViewEazyTaxiActivity;", "withdrawMoneyActivity", "Lcom/eazy/daiku/ui/withdraw/WithdrawMoneyActivity;", "app_wegoRelease"})
@dagger.Module()
public abstract class ActivityBuilderModule {
    
    public ActivityBuilderModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.splash_screen.SplashScreenActivity splashScreenActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.utility.base.BaseActivity baseActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.utility.base.BaseCoreActivity baseCoreActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.utility.base.SimpleBaseActivity simpleBaseActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.signup.SignUpActivity signUpActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.login.LoginActivity loginActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.verification_pin_code.VerificationPinActivity verificationPinActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.MainActivity mainActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.profile.MyProfileActivity mProfileActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.profile.EditProfileActivity editProfileActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.scan_qr_code.ScanQRCodeActivity scanQRCodeActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.upload_doc.UploadDocActivity uploadDocActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.map.MapPreviewActivity mapPreviewActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.forget_password.VerifyOtpCodeActivity forgotPasswordActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.change_password.ChangePasswordActivity changePasswordActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.history.HistoryTripActivity historyTripActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.wallet.MainWalletActivity mainWalletActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.web_view.WebViewEazyTaxiActivity webViewEazyTaxiActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.identity_verification.IdentityVerificationActivity identityVerificationActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.withdraw.WithdrawMoneyActivity withdrawMoneyActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.withdraw.EazyTaxiWithdrawWebviewActivity eazyTaxiWithdrawWebViewActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.forget_password.ForgetPasswordActivity forgetPasswordActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.map.CustomerMapPreViewActivity customerMapPreViewActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.map.PickUpLocationActivity prickUpLocationActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.map.SearchMapActivity searchMapActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.web_payment.WebPayActivity webPayActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.step_booking.ListCarTaxiActivity listCarTaxiActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.step_booking.LocationKioskBookingTaxiActivity locationKioskBookingTaxiActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.step_booking.PreviewCheckoutActivity previewCheckoutActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.step_booking.PaymentCompleteActivity paymentCompleteActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.about_us.AboutUsActivity aboutUsActivity();
}