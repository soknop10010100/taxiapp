package com.eazy.daiku.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u000e\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\'\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/data/remote/GeneralApi;", "", "getPublicIp", "Lretrofit2/Call;", "Lcom/google/gson/JsonElement;", "app_wegoRelease"})
public abstract interface GeneralApi {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "/")
    public abstract retrofit2.Call<com.google.gson.JsonElement> getPublicIp();
}