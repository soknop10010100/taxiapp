package com.eazy.daiku;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u0003B\u0005\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\u0005\u001a\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u001fH\u0014J\u0012\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010#H\u0014J\u0006\u0010$\u001a\u00020!J\n\u0010%\u001a\u0004\u0018\u00010&H\u0007J\r\u0010\'\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010(J\u000e\u0010)\u001a\u00020!2\u0006\u0010*\u001a\u00020\tJ\u0010\u0010+\u001a\u00020\t2\u0006\u0010,\u001a\u00020-H\u0002J\b\u0010.\u001a\u00020\tH\u0002J\u0010\u0010/\u001a\u00020!2\u0006\u00100\u001a\u000201H\u0016J\b\u00102\u001a\u00020!H\u0016J\b\u00103\u001a\u00020!H\u0007J\b\u00104\u001a\u00020!H\u0007J\b\u00105\u001a\u00020!H\u0007J\b\u00106\u001a\u00020!H\u0007J\b\u00107\u001a\u00020!H\u0007J\b\u00108\u001a\u00020!H\u0002J\u000e\u00109\u001a\u00020!2\u0006\u0010:\u001a\u00020\u000eJ\u000e\u0010;\u001a\u00020!2\u0006\u0010:\u001a\u00020\u0014R\u0010\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0004\n\u0002\u0010\u0007R\u0012\u0010\b\u001a\u0004\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\nR\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0013\u001a\u0004\u0018\u00010\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001c\u0010\u0019\u001a\u0004\u0018\u00010\u001aX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001e\u00a8\u0006<"}, d2 = {"Lcom/eazy/daiku/EazyTaxiApplication;", "Ldagger/android/DaggerApplication;", "Ldagger/android/HasAndroidInjector;", "Landroidx/lifecycle/LifecycleObserver;", "()V", "applicationInjector", "error/NonExistentClass", "Lerror/NonExistentClass;", "backgroundMode", "", "Ljava/lang/Boolean;", "globalDialog", "Landroid/app/Dialog;", "mCurrentBaseActivity", "Lcom/eazy/daiku/utility/base/BaseActivity;", "getMCurrentBaseActivity", "()Lcom/eazy/daiku/utility/base/BaseActivity;", "setMCurrentBaseActivity", "(Lcom/eazy/daiku/utility/base/BaseActivity;)V", "mCurrentMainActivity", "Lcom/eazy/daiku/ui/MainActivity;", "getMCurrentMainActivity", "()Lcom/eazy/daiku/ui/MainActivity;", "setMCurrentMainActivity", "(Lcom/eazy/daiku/ui/MainActivity;)V", "timerHandler", "Landroid/os/Handler;", "getTimerHandler", "()Landroid/os/Handler;", "setTimerHandler", "(Landroid/os/Handler;)V", "Ldagger/android/AndroidInjector;", "attachBaseContext", "", "base", "Landroid/content/Context;", "detectCrashApp", "deviceId", "", "getBackgroundMode", "()Ljava/lang/Boolean;", "globalLoadingView", "show", "hasNetworkGps", "activity", "Landroid/app/Activity;", "hasProcessingTrip", "onConfigurationChanged", "newConfig", "Landroid/content/res/Configuration;", "onCreate", "onMoveToBackground", "onMoveToForeground", "onMoveToTerminateApp", "onPauseMoveToForeground", "onResumeMoveToForeground", "parseServerConfig", "setCurrentBaseActivity", "mCurrentActivity", "setCurrentMainActivity", "app_wegoRelease"})
public final class EazyTaxiApplication extends dagger.android.DaggerApplication implements dagger.android.HasAndroidInjector, androidx.lifecycle.LifecycleObserver {
    private final error.NonExistentClass applicationInjector = null;
    private java.lang.Boolean backgroundMode = false;
    private android.app.Dialog globalDialog;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.ui.MainActivity mCurrentMainActivity;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.utility.base.BaseActivity mCurrentBaseActivity;
    @org.jetbrains.annotations.Nullable()
    private android.os.Handler timerHandler;
    
    public EazyTaxiApplication() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.MainActivity getMCurrentMainActivity() {
        return null;
    }
    
    public final void setMCurrentMainActivity(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.MainActivity p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.utility.base.BaseActivity getMCurrentBaseActivity() {
        return null;
    }
    
    public final void setMCurrentBaseActivity(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.base.BaseActivity p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.os.Handler getTimerHandler() {
        return null;
    }
    
    public final void setTimerHandler(@org.jetbrains.annotations.Nullable()
    android.os.Handler p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    @android.annotation.SuppressLint(value = {"HardwareIds"})
    public final java.lang.String deviceId() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    protected dagger.android.AndroidInjector<? extends dagger.android.DaggerApplication> applicationInjector() {
        return null;
    }
    
    @java.lang.Override()
    protected void attachBaseContext(@org.jetbrains.annotations.Nullable()
    android.content.Context base) {
    }
    
    @java.lang.Override()
    public void onConfigurationChanged(@org.jetbrains.annotations.NotNull()
    android.content.res.Configuration newConfig) {
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
    
    private final void parseServerConfig() {
    }
    
    public final void setCurrentMainActivity(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.MainActivity mCurrentActivity) {
    }
    
    public final void setCurrentBaseActivity(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseActivity mCurrentActivity) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean getBackgroundMode() {
        return null;
    }
    
    public final void detectCrashApp() {
    }
    
    public final void globalLoadingView(boolean show) {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_START)
    public final void onMoveToForeground() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_RESUME)
    public final void onResumeMoveToForeground() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_PAUSE)
    public final void onPauseMoveToForeground() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_STOP)
    public final void onMoveToBackground() {
    }
    
    @androidx.lifecycle.OnLifecycleEvent(value = androidx.lifecycle.Lifecycle.Event.ON_DESTROY)
    public final void onMoveToTerminateApp() {
    }
    
    private final boolean hasProcessingTrip() {
        return false;
    }
    
    private final boolean hasNetworkGps(android.app.Activity activity) {
        return false;
    }
}