package com.eazy.daiku.ui.splash_screen;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\u0006\u0010\u0018\u001a\u00020\u0019J\u0012\u0010\u001a\u001a\u00020\u00192\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0005\u001a\u00020\u00068BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\t\u0010\n\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\u000b\u001a\u00020\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\r\u001a\u00020\u000e8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0011\u0010\n\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u001d"}, d2 = {"Lcom/eazy/daiku/ui/splash_screen/SplashScreenActivity;", "Lcom/eazy/daiku/utility/base/SimpleBaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivitySplashScreenBinding;", "loginViewModel", "Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "getLoginViewModel", "()Lcom/eazy/daiku/utility/view_model/user_case/LoginViewModel;", "loginViewModel$delegate", "Lkotlin/Lazy;", "pinCode", "", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "getUserFromGson", "Lcom/eazy/daiku/data/model/server_model/User;", "jsonObject", "Lcom/google/gson/JsonObject;", "hasLogin", "", "initObserver", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_wegoRelease"})
public final class SplashScreenActivity extends com.eazy.daiku.utility.base.SimpleBaseActivity {
    private com.eazy.daiku.databinding.ActivitySplashScreenBinding binding;
    private final kotlin.Lazy userInfoVM$delegate = null;
    private final kotlin.Lazy loginViewModel$delegate = null;
    private java.lang.String pinCode = "";
    
    public SplashScreenActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.LoginViewModel getLoginViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void initObserver() {
    }
    
    private final com.eazy.daiku.data.model.server_model.User getUserFromGson(com.google.gson.JsonObject jsonObject) {
        return null;
    }
    
    private final boolean hasLogin() {
        return false;
    }
}