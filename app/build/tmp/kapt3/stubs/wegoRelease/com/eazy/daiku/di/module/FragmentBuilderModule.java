package com.eazy.daiku.di.module;

import java.lang.System;

@kotlin.Suppress(names = {"unused"})
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\'J\b\u0010\u0005\u001a\u00020\u0006H\'J\b\u0010\u0007\u001a\u00020\bH\'J\b\u0010\t\u001a\u00020\nH\'J\b\u0010\u000b\u001a\u00020\fH\'J\b\u0010\r\u001a\u00020\u000eH\'J\b\u0010\u000f\u001a\u00020\u0010H\'J\b\u0010\u0011\u001a\u00020\u0012H\'\u00a8\u0006\u0013"}, d2 = {"Lcom/eazy/daiku/di/module/FragmentBuilderModule;", "", "()V", "addABAAccountNumberFragment", "Lcom/eazy/daiku/ui/payment_method/AddABAAccountNumberFragment;", "baseFragment", "Lcom/eazy/daiku/utility/base/BaseFragment;", "changeCardAndBankAccountFragment", "Lcom/eazy/daiku/ui/payment_method/ChangeCardAndBankAccountFragment;", "contactFragment", "Lcom/eazy/daiku/ui/home/ContactFragment;", "mainWalletFragment", "Lcom/eazy/daiku/ui/home/MainWalletFragment;", "selectBankFragment", "Lcom/eazy/daiku/ui/payment_method/SelectBankFragment;", "selectCarBookingFragment", "Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment;", "tripDetailBookingFragment", "Lcom/eazy/daiku/ui/customer/step_booking/TripDetailBookingFragment;", "app_wegoRelease"})
@dagger.Module()
public abstract class FragmentBuilderModule {
    
    public FragmentBuilderModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.utility.base.BaseFragment baseFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.home.MainWalletFragment mainWalletFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.home.ContactFragment contactFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.payment_method.AddABAAccountNumberFragment addABAAccountNumberFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment changeCardAndBankAccountFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.payment_method.SelectBankFragment selectBankFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment selectCarBookingFragment();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract com.eazy.daiku.ui.customer.step_booking.TripDetailBookingFragment tripDetailBookingFragment();
}