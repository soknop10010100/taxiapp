package com.eazy.daiku.ui.customer.step_booking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 82\u00020\u0001:\u000289B\u0005\u00a2\u0006\u0002\u0010\u0002J\n\u0010!\u001a\u0004\u0018\u00010\"H\u0002J\b\u0010#\u001a\u00020$H\u0002J\b\u0010%\u001a\u00020$H\u0003J\b\u0010&\u001a\u00020$H\u0002J\b\u0010\'\u001a\u00020$H\u0002J\u0010\u0010(\u001a\u00020$2\u0006\u0010)\u001a\u00020*H\u0016J$\u0010+\u001a\u00020,2\u0006\u0010-\u001a\u00020.2\b\u0010/\u001a\u0004\u0018\u0001002\b\u00101\u001a\u0004\u0018\u000102H\u0016J\u001a\u00103\u001a\u00020$2\u0006\u00104\u001a\u00020,2\b\u00101\u001a\u0004\u0018\u000102H\u0016J\u0010\u00105\u001a\u00020$2\u0006\u00106\u001a\u000207H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0007X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u00020\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000fR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0013X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0017\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u001a\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u001b\u001a\u00020\u001c8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001f\u0010 \u001a\u0004\b\u001d\u0010\u001e\u00a8\u0006:"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment;", "Lcom/eazy/daiku/utility/base/BaseFragment;", "()V", "address", "", "adminArea", "binding", "Lcom/eazy/daiku/databinding/SelectCarBookingLayoutBinding;", "carId", "countryName", "fContext", "Landroidx/fragment/app/FragmentActivity;", "getFContext", "()Landroidx/fragment/app/FragmentActivity;", "setFContext", "(Landroidx/fragment/app/FragmentActivity;)V", "getAddressLine", "latitude", "listKioskModel", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "longitude", "navController", "Landroidx/navigation/NavController;", "pathScreenShotMap", "selectCarBookingAdapter", "Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment$SelectCarBookingAdapter;", "vechicle", "viewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "getViewModel", "()Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "accessParentFragment", "Lcom/eazy/daiku/ui/customer/step_booking/StepBookingFragmentBottomSheet;", "initAction", "", "initObserver", "initRecyclerView", "initView", "onAttach", "context", "Landroid/content/Context;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "view", "submitCheckoutTaxi", "terms", "Lcom/eazy/daiku/ui/customer/model/Terms;", "Companion", "SelectCarBookingAdapter", "app_wegoRelease"})
public final class SelectCarBookingFragment extends com.eazy.daiku.utility.base.BaseFragment {
    private com.eazy.daiku.databinding.SelectCarBookingLayoutBinding binding;
    public androidx.fragment.app.FragmentActivity fContext;
    private androidx.navigation.NavController navController;
    private com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment.SelectCarBookingAdapter selectCarBookingAdapter;
    private com.eazy.daiku.ui.customer.model.ListKioskModel listKioskModel;
    private java.lang.String latitude;
    private java.lang.String longitude;
    private java.lang.String address;
    private java.lang.String carId;
    private java.lang.String pathScreenShotMap;
    private java.lang.String adminArea;
    private java.lang.String countryName;
    private java.lang.String getAddressLine;
    private java.lang.String vechicle;
    private final kotlin.Lazy viewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String myListTaxiKey = "myListTaxiKey";
    
    public SelectCarBookingFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.fragment.app.FragmentActivity getFContext() {
        return null;
    }
    
    public final void setFContext(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity p0) {
    }
    
    private final com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment newInstance(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.model.ListKioskModel listKioskModel, @org.jetbrains.annotations.NotNull()
    java.lang.String latitude, @org.jetbrains.annotations.NotNull()
    java.lang.String longitude, @org.jetbrains.annotations.NotNull()
    java.lang.String address, @org.jetbrains.annotations.NotNull()
    java.lang.String pathScreenShotMap, @org.jetbrains.annotations.NotNull()
    java.lang.String adminArea, @org.jetbrains.annotations.NotNull()
    java.lang.String countryName, @org.jetbrains.annotations.NotNull()
    java.lang.String getAddressLine) {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initRecyclerView() {
    }
    
    private final void initView() {
    }
    
    @android.annotation.SuppressLint(value = {"FragmentLiveDataObserve"})
    private final void initObserver() {
    }
    
    private final void initAction() {
    }
    
    private final void submitCheckoutTaxi(com.eazy.daiku.ui.customer.model.Terms terms) {
    }
    
    private final com.eazy.daiku.ui.customer.step_booking.StepBookingFragmentBottomSheet accessParentFragment() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\"B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001e\u0010\u0013\u001a\u00020\f2\u0016\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0007J\u0006\u0010\u0014\u001a\u00020\fJ\b\u0010\u0015\u001a\u00020\u0012H\u0016J\u0010\u0010\u0016\u001a\u00020\f2\u0006\u0010\u0017\u001a\u00020\u0012H\u0002J\u0010\u0010\u0018\u001a\u00020\f2\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J\u0018\u0010\u001b\u001a\u00020\f2\u0006\u0010\u001c\u001a\u00020\u00022\u0006\u0010\u001d\u001a\u00020\u0012H\u0016J\u0018\u0010\u001e\u001a\u00020\u00022\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\u0012H\u0016R\"\u0010\u0004\u001a\u0016\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u0005j\n\u0012\u0004\u0012\u00020\u0006\u0018\u0001`\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R&\u0010\n\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\f0\u000bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment$SelectCarBookingAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment$SelectCarBookingAdapter$SelectCarBookingHolder;", "()V", "carBookingList", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/Vehicle;", "Lkotlin/collections/ArrayList;", "context", "Landroid/content/Context;", "onSelectRow", "Lkotlin/Function1;", "", "getOnSelectRow", "()Lkotlin/jvm/functions/Function1;", "setOnSelectRow", "(Lkotlin/jvm/functions/Function1;)V", "selected", "", "add", "clear", "getItemCount", "notifyItemByPosition", "selectedPosition", "onAttachedToRecyclerView", "recyclerView", "Landroidx/recyclerview/widget/RecyclerView;", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "SelectCarBookingHolder", "app_wegoRelease"})
    public static final class SelectCarBookingAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment.SelectCarBookingAdapter.SelectCarBookingHolder> {
        private java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> carBookingList;
        public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.Vehicle, kotlin.Unit> onSelectRow;
        private android.content.Context context;
        private final java.util.ArrayList<java.lang.Integer> selected = null;
        
        public SelectCarBookingAdapter() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.server_model.Vehicle, kotlin.Unit> getOnSelectRow() {
            return null;
        }
        
        public final void setOnSelectRow(@org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.Vehicle, kotlin.Unit> p0) {
        }
        
        @java.lang.Override()
        public void onAttachedToRecyclerView(@org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView recyclerView) {
        }
        
        public final void add(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> carBookingList) {
        }
        
        public final void clear() {
        }
        
        private final void notifyItemByPosition(int selectedPosition) {
        }
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment.SelectCarBookingAdapter.SelectCarBookingHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
        
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment.SelectCarBookingAdapter.SelectCarBookingHolder holder, int position) {
        }
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment$SelectCarBookingAdapter$SelectCarBookingHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "containerVehicle", "Landroid/widget/LinearLayout;", "getContainerVehicle", "()Landroid/widget/LinearLayout;", "imCar", "Landroid/widget/ImageView;", "nameCar", "Landroid/widget/TextView;", "tvPrice", "bind", "", "vehicle", "Lcom/eazy/daiku/data/model/server_model/Vehicle;", "app_wegoRelease"})
        public static final class SelectCarBookingHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
            private final android.widget.TextView nameCar = null;
            private final android.widget.ImageView imCar = null;
            private final android.widget.TextView tvPrice = null;
            @org.jetbrains.annotations.NotNull()
            private final android.widget.LinearLayout containerVehicle = null;
            
            public SelectCarBookingHolder(@org.jetbrains.annotations.NotNull()
            android.view.View itemView) {
                super(null);
            }
            
            @org.jetbrains.annotations.NotNull()
            public final android.widget.LinearLayout getContainerVehicle() {
                return null;
            }
            
            public final void bind(@org.jetbrains.annotations.NotNull()
            com.eazy.daiku.data.model.server_model.Vehicle vehicle) {
            }
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002JH\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u00042\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00042\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0004H\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0010"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment$Companion;", "", "()V", "myListTaxiKey", "", "newInstance", "Lcom/eazy/daiku/ui/customer/step_booking/SelectCarBookingFragment;", "listKioskModel", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "latitude", "longitude", "address", "pathScreenShotMap", "adminArea", "countryName", "getAddressLine", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment newInstance(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.customer.model.ListKioskModel listKioskModel, @org.jetbrains.annotations.NotNull()
        java.lang.String latitude, @org.jetbrains.annotations.NotNull()
        java.lang.String longitude, @org.jetbrains.annotations.NotNull()
        java.lang.String address, @org.jetbrains.annotations.NotNull()
        java.lang.String pathScreenShotMap, @org.jetbrains.annotations.NotNull()
        java.lang.String adminArea, @org.jetbrains.annotations.NotNull()
        java.lang.String countryName, @org.jetbrains.annotations.NotNull()
        java.lang.String getAddressLine) {
            return null;
        }
    }
}