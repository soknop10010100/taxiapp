package com.eazy.daiku.ui.customer.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0010\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u001cB\u0005\u00a2\u0006\u0002\u0010\u0003J\u001e\u0010\u000f\u001a\u00020\n2\u0016\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0011J\u0006\u0010\u0012\u001a\u00020\nJ\b\u0010\u0013\u001a\u00020\u0014H\u0016J\u0018\u0010\u0015\u001a\u00020\n2\u0006\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u0014H\u0016J\u0018\u0010\u0018\u001a\u00020\u00022\u0006\u0010\u0019\u001a\u00020\u001a2\u0006\u0010\u001b\u001a\u00020\u0014H\u0016R\u0014\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005X\u0082\u000e\u00a2\u0006\u0002\n\u0000R,\u0010\u0007\u001a\u0014\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\t\u0012\u0004\u0012\u00020\n0\bX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u001d"}, d2 = {"Lcom/eazy/daiku/ui/customer/adapter/HistorySearMapAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/ui/customer/adapter/HistorySearMapAdapter$HistorySearchMapHolder;", "()V", "listHistorySearchMap", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/ui/customer/model/SearchMapHistoryModel;", "onSelectRowHistorySearchMap", "Lkotlin/Function2;", "", "", "getOnSelectRowHistorySearchMap", "()Lkotlin/jvm/functions/Function2;", "setOnSelectRowHistorySearchMap", "(Lkotlin/jvm/functions/Function2;)V", "add", "listMap", "Lkotlin/collections/ArrayList;", "clear", "getItemCount", "", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "HistorySearchMapHolder", "app_wegoRelease"})
public final class HistorySearMapAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.ui.customer.adapter.HistorySearMapAdapter.HistorySearchMapHolder> {
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel> listHistorySearchMap;
    public kotlin.jvm.functions.Function2<? super com.eazy.daiku.ui.customer.model.SearchMapHistoryModel, ? super java.lang.Boolean, kotlin.Unit> onSelectRowHistorySearchMap;
    
    public HistorySearMapAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function2<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel, java.lang.Boolean, kotlin.Unit> getOnSelectRowHistorySearchMap() {
        return null;
    }
    
    public final void setOnSelectRowHistorySearchMap(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function2<? super com.eazy.daiku.ui.customer.model.SearchMapHistoryModel, ? super java.lang.Boolean, kotlin.Unit> p0) {
    }
    
    public final void add(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.SearchMapHistoryModel> listMap) {
    }
    
    public final void clear() {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.ui.customer.adapter.HistorySearMapAdapter.HistorySearchMapHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.adapter.HistorySearMapAdapter.HistorySearchMapHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0015"}, d2 = {"Lcom/eazy/daiku/ui/customer/adapter/HistorySearMapAdapter$HistorySearchMapHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "actionDelete", "Landroid/widget/ImageView;", "getActionDelete", "()Landroid/widget/ImageView;", "setActionDelete", "(Landroid/widget/ImageView;)V", "titleHistory", "Landroid/widget/TextView;", "getTitleHistory", "()Landroid/widget/TextView;", "setTitleHistory", "(Landroid/widget/TextView;)V", "bind", "", "searchMapHistoryModel", "Lcom/eazy/daiku/ui/customer/model/SearchMapHistoryModel;", "app_wegoRelease"})
    public static final class HistorySearchMapHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private android.widget.TextView titleHistory;
        @org.jetbrains.annotations.NotNull()
        private android.widget.ImageView actionDelete;
        
        public HistorySearchMapHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTitleHistory() {
            return null;
        }
        
        public final void setTitleHistory(@org.jetbrains.annotations.NotNull()
        android.widget.TextView p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getActionDelete() {
            return null;
        }
        
        public final void setActionDelete(@org.jetbrains.annotations.NotNull()
        android.widget.ImageView p0) {
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.customer.model.SearchMapHistoryModel searchMapHistoryModel) {
        }
    }
}