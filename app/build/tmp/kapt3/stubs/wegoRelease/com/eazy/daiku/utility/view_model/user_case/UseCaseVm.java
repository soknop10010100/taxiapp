package com.eazy.daiku.utility.view_model.user_case;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0016\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0006\u0010-\u001a\u00020.J\u0006\u0010/\u001a\u00020.J*\u00100\u001a\u00020.2\"\u00101\u001a\u001e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204`5J*\u00106\u001a\u00020.2\"\u00107\u001a\u001e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204`5J*\u00108\u001a\u00020.2\"\u00101\u001a\u001e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204`5J*\u00109\u001a\u00020.2\"\u00101\u001a\u001e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204`5J*\u0010:\u001a\u00020.2\"\u0010;\u001a\u001e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204`5J*\u0010<\u001a\u00020.2\"\u00107\u001a\u001e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u00020402j\u000e\u0012\u0004\u0012\u000203\u0012\u0004\u0012\u000204`5J\u0016\u0010=\u001a\u00020.2\u0006\u0010>\u001a\u0002032\u0006\u0010?\u001a\u000203R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u000f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0014\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000e0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0014\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\t0\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u0018\u0010\u0019R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\u001a\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00178F\u00a2\u0006\u0006\u001a\u0004\b\u001b\u0010\u0019R\u0017\u0010\u001c\u001a\b\u0012\u0004\u0012\u00020\u000e0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u001d\u0010\u001eR\u001d\u0010\u001f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\b8F\u00a2\u0006\u0006\u001a\u0004\b \u0010\u001eR\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u001d\u0010#\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\u00178F\u00a2\u0006\u0006\u001a\u0004\b$\u0010\u0019R\u001d\u0010%\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\u00178F\u00a2\u0006\u0006\u001a\u0004\b&\u0010\u0019R\u0017\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u000e0\b8F\u00a2\u0006\u0006\u001a\u0004\b(\u0010\u001eR\u001d\u0010)\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\b8F\u00a2\u0006\u0006\u001a\u0004\b*\u0010\u001eR\u001d\u0010+\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000b0\u00178F\u00a2\u0006\u0006\u001a\u0004\b,\u0010\u0019\u00a8\u0006@"}, d2 = {"Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_changePwdValidate", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/utility/view_model/user_case/model/changePwdState;", "_dataUserInLiveData", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/User;", "_loadingUserInfoLiveData", "", "_logoutUserInLiveData", "Lcom/google/gson/JsonElement;", "_requestOtpMutableLiveData", "_submitChangePasswordByOtpMutableLiveData", "_submitChangePasswordLoadingMutableLiveData", "_submitChangePasswordMutableLiveData", "_verifyOtpMutableLiveData", "changePwdValidate", "Landroidx/lifecycle/LiveData;", "getChangePwdValidate", "()Landroidx/lifecycle/LiveData;", "dataUserLiveData", "getDataUserLiveData", "loadingUserLiveData", "getLoadingUserLiveData", "()Landroidx/lifecycle/MutableLiveData;", "logoutUserInLiveData", "getLogoutUserInLiveData", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "requestOtpMutableLiveData", "getRequestOtpMutableLiveData", "submitChangePasswordByOtpMutableLiveData", "getSubmitChangePasswordByOtpMutableLiveData", "submitChangePasswordLoadingMutableLiveData", "getSubmitChangePasswordLoadingMutableLiveData", "submitChangePasswordMutableLiveData", "getSubmitChangePasswordMutableLiveData", "verifyOtpMutableLiveData", "getVerifyOtpMutableLiveData", "fetchUserInfo", "", "logout", "submitChangePasswordByOtp", "bodyMap", "Ljava/util/HashMap;", "", "", "Lkotlin/collections/HashMap;", "submitChangePwd", "requestBody", "submitRequestOtp", "submitVerifyOtp", "updateAvailableTaxi", "bodyRequest", "updateUser", "validateField", "newPwd", "oldPwd", "app_wegoRelease"})
public final class UseCaseVm extends com.eazy.daiku.utility.base.BaseViewModel {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    
    /**
     * fetch user information
     */
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingUserInfoLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> _dataUserInLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _logoutUserInLiveData = null;
    
    /**
     * request otp
     */
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _requestOtpMutableLiveData = null;
    
    /**
     * verify otp
     */
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _verifyOtpMutableLiveData = null;
    
    /**
     * change password with otp
     */
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _submitChangePasswordByOtpMutableLiveData = null;
    
    /**
     * change password
     */
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.utility.view_model.user_case.model.changePwdState> _changePwdValidate = null;
    private androidx.lifecycle.MutableLiveData<java.lang.Boolean> _submitChangePasswordLoadingMutableLiveData;
    private androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _submitChangePasswordMutableLiveData;
    
    @javax.inject.Inject()
    public UseCaseVm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingUserLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> getDataUserLiveData() {
        return null;
    }
    
    public final void fetchUserInfo() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getLogoutUserInLiveData() {
        return null;
    }
    
    public final void logout() {
    }
    
    public final void updateAvailableTaxi(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyRequest) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getRequestOtpMutableLiveData() {
        return null;
    }
    
    public final void submitRequestOtp(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getVerifyOtpMutableLiveData() {
        return null;
    }
    
    public final void submitVerifyOtp(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getSubmitChangePasswordByOtpMutableLiveData() {
        return null;
    }
    
    public final void submitChangePasswordByOtp(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> bodyMap) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.utility.view_model.user_case.model.changePwdState> getChangePwdValidate() {
        return null;
    }
    
    public final void validateField(@org.jetbrains.annotations.NotNull()
    java.lang.String newPwd, @org.jetbrains.annotations.NotNull()
    java.lang.String oldPwd) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getSubmitChangePasswordLoadingMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getSubmitChangePasswordMutableLiveData() {
        return null;
    }
    
    public final void submitChangePwd(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> requestBody) {
    }
    
    /**
     * update user
     */
    public final void updateUser(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> requestBody) {
    }
}