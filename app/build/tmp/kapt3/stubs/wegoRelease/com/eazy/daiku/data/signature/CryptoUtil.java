package com.eazy.daiku.data.signature;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0007\n\u0002\u0010$\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0007\u001a\u00020\u00062\b\u0010\b\u001a\u0004\u0018\u00010\u00062\u0006\u0010\t\u001a\u00020\u0006J\u0018\u0010\n\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u000b\u001a\u00020\u00062\u0006\u0010\f\u001a\u00020\u0006J\u0012\u0010\r\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00060\u000eJ\u0012\u0010\u000f\u001a\u0004\u0018\u00010\u00102\u0006\u0010\u0011\u001a\u00020\u0006H\u0002J\u0012\u0010\u0012\u001a\u0004\u0018\u00010\u00132\u0006\u0010\u0014\u001a\u00020\u0006H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082D\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/eazy/daiku/data/signature/CryptoUtil;", "", "()V", "CRYPTO_BITS", "", "CRYPTO_METHOD", "", "decrypt", "result", "pubk", "encrypt", "plain", "privk", "generateKeyPair", "", "stringToPrivateKey", "Ljava/security/PrivateKey;", "privateKeyString", "stringToPublicKey", "Ljava/security/PublicKey;", "publicKeyString", "app_wegoRelease"})
public final class CryptoUtil {
    private final java.lang.String CRYPTO_METHOD = "RSA";
    private final int CRYPTO_BITS = 2048;
    
    public CryptoUtil() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.Throws(exceptionClasses = {java.security.NoSuchAlgorithmException.class, javax.crypto.NoSuchPaddingException.class, java.security.InvalidKeyException.class, javax.crypto.IllegalBlockSizeException.class, javax.crypto.BadPaddingException.class})
    public final java.util.Map<java.lang.String, java.lang.String> generateKeyPair() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    @kotlin.jvm.Throws(exceptionClasses = {java.security.NoSuchAlgorithmException.class, javax.crypto.NoSuchPaddingException.class, java.security.InvalidKeyException.class, javax.crypto.IllegalBlockSizeException.class, javax.crypto.BadPaddingException.class, java.security.spec.InvalidKeySpecException.class})
    public final java.lang.String encrypt(@org.jetbrains.annotations.NotNull()
    java.lang.String plain, @org.jetbrains.annotations.NotNull()
    java.lang.String privk) throws java.security.NoSuchAlgorithmException, javax.crypto.NoSuchPaddingException, java.security.InvalidKeyException, javax.crypto.IllegalBlockSizeException, javax.crypto.BadPaddingException, java.security.spec.InvalidKeySpecException {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.Throws(exceptionClasses = {javax.crypto.NoSuchPaddingException.class, java.security.NoSuchAlgorithmException.class, javax.crypto.BadPaddingException.class, javax.crypto.IllegalBlockSizeException.class, java.security.spec.InvalidKeySpecException.class, java.security.InvalidKeyException.class})
    public final java.lang.String decrypt(@org.jetbrains.annotations.Nullable()
    java.lang.String result, @org.jetbrains.annotations.NotNull()
    java.lang.String pubk) throws javax.crypto.NoSuchPaddingException, java.security.NoSuchAlgorithmException, javax.crypto.BadPaddingException, javax.crypto.IllegalBlockSizeException, java.security.spec.InvalidKeySpecException, java.security.InvalidKeyException {
        return null;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.security.spec.InvalidKeySpecException.class, java.security.NoSuchAlgorithmException.class})
    private final java.security.PublicKey stringToPublicKey(java.lang.String publicKeyString) throws java.security.spec.InvalidKeySpecException, java.security.NoSuchAlgorithmException {
        return null;
    }
    
    @kotlin.jvm.Throws(exceptionClasses = {java.security.spec.InvalidKeySpecException.class, java.security.NoSuchAlgorithmException.class})
    private final java.security.PrivateKey stringToPrivateKey(java.lang.String privateKeyString) throws java.security.spec.InvalidKeySpecException, java.security.NoSuchAlgorithmException {
        return null;
    }
}