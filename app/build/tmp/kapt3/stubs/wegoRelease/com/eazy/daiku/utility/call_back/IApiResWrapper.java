package com.eazy.daiku.utility.call_back;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\bf\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00028\u0000H&\u00a2\u0006\u0002\u0010\u0006J \u0010\u0007\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rH&J\u0010\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010H&\u00a8\u0006\u0011"}, d2 = {"Lcom/eazy/daiku/utility/call_back/IApiResWrapper;", "T", "", "onData", "", "respondData", "(Ljava/lang/Object;)V", "onError", "message", "", "code", "", "errorHashMap", "Lcom/google/gson/JsonObject;", "onLoading", "hasLoading", "", "app_wegoRelease"})
public abstract interface IApiResWrapper<T extends java.lang.Object> {
    
    public abstract void onLoading(boolean hasLoading);
    
    public abstract void onData(T respondData);
    
    public abstract void onError(@org.jetbrains.annotations.NotNull()
    java.lang.String message, int code, @org.jetbrains.annotations.NotNull()
    com.google.gson.JsonObject errorHashMap);
}