package com.eazy.daiku.ui.customer;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J0\u0010\u0002\u001a\u00020\u00032\b\u0010\u0004\u001a\u0004\u0018\u00010\u00052\b\u0010\u0006\u001a\u0004\u0018\u00010\u00052\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\t\u001a\u0004\u0018\u00010\nH&\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/ui/customer/MyMapCallback;", "", "onConfirmed", "", "name", "", "address", "latLng", "Lcom/google/android/gms/maps/model/LatLng;", "bitmap", "Landroid/graphics/Bitmap;", "app_wegoRelease"})
public abstract interface MyMapCallback {
    
    public abstract void onConfirmed(@org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.String address, @org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.model.LatLng latLng, @org.jetbrains.annotations.Nullable()
    android.graphics.Bitmap bitmap);
}