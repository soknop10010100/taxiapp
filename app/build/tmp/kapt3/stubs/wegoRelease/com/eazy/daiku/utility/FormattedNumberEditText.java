package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000J\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\f\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001:\u0001*B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010\u0007B!\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0012\u0010%\u001a\u00020&2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0002J\u0018\u0010\'\u001a\u00020&2\u0006\u0010(\u001a\u00020\t2\u0006\u0010)\u001a\u00020\tH\u0014R\u000e\u0010\u000b\u001a\u00020\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001a\u0010\u0012\u001a\u00020\u0013X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u0015\"\u0004\b\u0016\u0010\u0017R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u001a\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001b\u0010\u000f\"\u0004\b\u001c\u0010\u0011R\u001e\u0010\u001e\u001a\u00020\t2\u0006\u0010\u001d\u001a\u00020\t@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010\u000fR\u001e\u0010!\u001a\u00020 2\u0006\u0010\u001d\u001a\u00020 @BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u000e\u0010$\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006+"}, d2 = {"Lcom/eazy/daiku/utility/FormattedNumberEditText;", "Landroidx/appcompat/widget/AppCompatEditText;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defStyleAttr", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "digitsKeyListener", "Landroid/text/method/DigitsKeyListener;", "groupLength", "getGroupLength", "()I", "setGroupLength", "(I)V", "groupSeparator", "", "getGroupSeparator", "()C", "setGroupSeparator", "(C)V", "initCompleted", "", "inputLength", "getInputLength", "setInputLength", "<set-?>", "numberOfGroups", "getNumberOfGroups", "", "prefix", "getPrefix", "()Ljava/lang/String;", "separatorAndDigitsKeyListener", "init", "", "onSelectionChanged", "start", "end", "TextChangeListener", "app_wegoRelease"})
public final class FormattedNumberEditText extends androidx.appcompat.widget.AppCompatEditText {
    @org.jetbrains.annotations.NotNull()
    private java.lang.String prefix = "";
    private char groupSeparator = ' ';
    private int numberOfGroups = 4;
    private int groupLength = 4;
    private int inputLength;
    private final android.text.method.DigitsKeyListener digitsKeyListener = null;
    private android.text.method.DigitsKeyListener separatorAndDigitsKeyListener;
    private boolean initCompleted = false;
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPrefix() {
        return null;
    }
    
    public final char getGroupSeparator() {
        return '\u0000';
    }
    
    public final void setGroupSeparator(char p0) {
    }
    
    public final int getNumberOfGroups() {
        return 0;
    }
    
    public final int getGroupLength() {
        return 0;
    }
    
    public final void setGroupLength(int p0) {
    }
    
    public final int getInputLength() {
        return 0;
    }
    
    public final void setInputLength(int p0) {
    }
    
    public FormattedNumberEditText(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
    
    public FormattedNumberEditText(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    public FormattedNumberEditText(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    android.util.AttributeSet attrs, int defStyleAttr) {
        super(null);
    }
    
    private final void init(android.util.AttributeSet attrs) {
    }
    
    @java.lang.Override()
    protected void onSelectionChanged(int start, int end) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\r\n\u0002\b\t\b\u0082\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0018\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0016J*\u0010\u001c\u001a\u00020\u00192\b\u0010\u001a\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u0010\u001f\u001a\u00020\u00042\u0006\u0010 \u001a\u00020\u0004H\u0016J\u000e\u0010!\u001a\u00020\u00192\u0006\u0010\u001a\u001a\u00020\u001bJ*\u0010\"\u001a\u00020\u00192\b\u0010#\u001a\u0004\u0018\u00010\u001d2\u0006\u0010\u001e\u001a\u00020\u00042\u0006\u0010$\u001a\u00020\u00042\u0006\u0010%\u001a\u00020\u0004H\u0016R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\u0010X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0015\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\f\"\u0004\b\u0017\u0010\u000e\u00a8\u0006&"}, d2 = {"Lcom/eazy/daiku/utility/FormattedNumberEditText$TextChangeListener;", "Landroid/text/TextWatcher;", "(Lcom/eazy/daiku/utility/FormattedNumberEditText;)V", "deletedChars", "", "getDeletedChars", "()I", "setDeletedChars", "(I)V", "enteredText", "", "getEnteredText", "()Ljava/lang/String;", "setEnteredText", "(Ljava/lang/String;)V", "listenerEnabled", "", "getListenerEnabled", "()Z", "setListenerEnabled", "(Z)V", "textBefore", "getTextBefore", "setTextBefore", "afterTextChanged", "", "s", "Landroid/text/Editable;", "beforeTextChanged", "", "start", "count", "after", "handleTextChange", "onTextChanged", "text", "lengthBefore", "lengthAfter", "app_wegoRelease"})
    final class TextChangeListener implements android.text.TextWatcher {
        @org.jetbrains.annotations.NotNull()
        private java.lang.String textBefore = "";
        @org.jetbrains.annotations.NotNull()
        private java.lang.String enteredText = "";
        private int deletedChars = 0;
        private boolean listenerEnabled = true;
        
        public TextChangeListener() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getTextBefore() {
            return null;
        }
        
        public final void setTextBefore(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getEnteredText() {
            return null;
        }
        
        public final void setEnteredText(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        public final int getDeletedChars() {
            return 0;
        }
        
        public final void setDeletedChars(int p0) {
        }
        
        public final boolean getListenerEnabled() {
            return false;
        }
        
        public final void setListenerEnabled(boolean p0) {
        }
        
        @java.lang.Override()
        public void beforeTextChanged(@org.jetbrains.annotations.Nullable()
        java.lang.CharSequence s, int start, int count, int after) {
        }
        
        @java.lang.Override()
        public void onTextChanged(@org.jetbrains.annotations.Nullable()
        java.lang.CharSequence text, int start, int lengthBefore, int lengthAfter) {
        }
        
        @java.lang.Override()
        public void afterTextChanged(@org.jetbrains.annotations.Nullable()
        android.text.Editable s) {
        }
        
        public final void handleTextChange(@org.jetbrains.annotations.NotNull()
        android.text.Editable s) {
        }
    }
}