package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u0012\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0002J(\u0010\u0017\u001a\u00020\u0018\"\u0004\b\u0000\u0010\u00192\f\u0010\u001a\u001a\b\u0012\u0004\u0012\u0002H\u00190\u001b2\f\u0010\u001c\u001a\b\u0012\u0004\u0012\u0002H\u00190\u001dR\u001d\u0010\u0007\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001d\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\n0\t0\u000e8F\u00a2\u0006\u0006\u001a\u0004\b\u000f\u0010\u0010R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006\u001e"}, d2 = {"Lcom/eazy/daiku/utility/base/BaseViewModel;", "Landroidx/lifecycle/ViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_globalErrorMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/google/gson/JsonElement;", "get_globalErrorMutableLiveData", "()Landroidx/lifecycle/MutableLiveData;", "globalErrorMutableLiveData", "Landroidx/lifecycle/LiveData;", "getGlobalErrorMutableLiveData", "()Landroidx/lifecycle/LiveData;", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "errorHandle", "", "respondError", "Lcom/google/gson/JsonObject;", "submit", "", "T", "flowRequest", "Lkotlinx/coroutines/flow/Flow;", "iApiResWrapper", "Lcom/eazy/daiku/utility/call_back/IApiResWrapper;", "app_wegoRelease"})
public class BaseViewModel extends androidx.lifecycle.ViewModel {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    @org.jetbrains.annotations.NotNull()
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> _globalErrorMutableLiveData = null;
    
    @javax.inject.Inject()
    public BaseViewModel(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> get_globalErrorMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> getGlobalErrorMutableLiveData() {
        return null;
    }
    
    private final java.lang.String errorHandle(com.google.gson.JsonObject respondError) {
        return null;
    }
    
    public final <T extends java.lang.Object>void submit(@org.jetbrains.annotations.NotNull()
    kotlinx.coroutines.flow.Flow<? extends T> flowRequest, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.call_back.IApiResWrapper<T> iApiResWrapper) {
    }
}