package com.eazy.daiku.ui.customer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u001d\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\u0002\u0010\u0006R\u0013\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0013\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\n\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/ui/customer/model/MarkerOption;", "", "latLng", "Lcom/google/android/gms/maps/model/LatLng;", "typeMarker", "", "(Lcom/google/android/gms/maps/model/LatLng;Ljava/lang/String;)V", "getLatLng", "()Lcom/google/android/gms/maps/model/LatLng;", "getTypeMarker", "()Ljava/lang/String;", "app_wegoRelease"})
public final class MarkerOption {
    @org.jetbrains.annotations.Nullable()
    private final com.google.android.gms.maps.model.LatLng latLng = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String typeMarker = null;
    
    public MarkerOption() {
        super();
    }
    
    public MarkerOption(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.model.LatLng latLng, @org.jetbrains.annotations.Nullable()
    java.lang.String typeMarker) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.google.android.gms.maps.model.LatLng getLatLng() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTypeMarker() {
        return null;
    }
}