package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\u0018\u0000 \u00142\u00020\u0001:\u0001\u0014B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\r\u001a\u00020\u000eH\u0002J\b\u0010\u000f\u001a\u00020\u000eH\u0002J\b\u0010\u0010\u001a\u00020\u000eH\u0002J\u0010\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00020\u0013H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0015"}, d2 = {"Lcom/eazy/daiku/utility/base/BiometricCheck;", "", "()V", "biometricPrompt", "Landroidx/biometric/BiometricPrompt;", "context", "Landroid/content/Context;", "executor", "Ljava/util/concurrent/Executor;", "listener", "Lcom/eazy/daiku/utility/call_back/BiometricListener;", "promptInfo", "Landroidx/biometric/BiometricPrompt$PromptInfo;", "checkBiometric", "", "initBiometrics", "showBiometricDialog", "showToast", "msg", "", "Companion", "app_wegoRelease"})
public final class BiometricCheck {
    private com.eazy.daiku.utility.call_back.BiometricListener listener;
    private android.content.Context context;
    private java.util.concurrent.Executor executor;
    private androidx.biometric.BiometricPrompt biometricPrompt;
    private androidx.biometric.BiometricPrompt.PromptInfo promptInfo;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.base.BiometricCheck.Companion Companion = null;
    
    public BiometricCheck() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.utility.base.BiometricCheck newInstance(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.call_back.BiometricListener listener) {
        return null;
    }
    
    private final void checkBiometric() {
    }
    
    private final void showBiometricDialog() {
    }
    
    private final void initBiometrics() {
    }
    
    private final void showToast(java.lang.String msg) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0007\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/utility/base/BiometricCheck$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/base/BiometricCheck;", "context", "Landroid/content/Context;", "listener", "Lcom/eazy/daiku/utility/call_back/BiometricListener;", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.utility.base.BiometricCheck newInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.call_back.BiometricListener listener) {
            return null;
        }
    }
}