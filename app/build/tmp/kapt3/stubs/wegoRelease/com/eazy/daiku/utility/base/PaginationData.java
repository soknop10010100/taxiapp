package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b#\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002BU\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\u0004\u0012\u0006\u0010\t\u001a\u00020\u0004\u0012\u0006\u0010\n\u001a\u00020\u0007\u0012\u0006\u0010\u000b\u001a\u00020\u0007\u0012\u0006\u0010\f\u001a\u00020\u0007\u0012\u0006\u0010\r\u001a\u00020\u0004\u0012\u0006\u0010\u000e\u001a\u00020\u0004\u00a2\u0006\u0002\u0010\u000fJ\t\u0010\u001e\u001a\u00020\u0004H\u00c6\u0003J\t\u0010\u001f\u001a\u00020\u0004H\u00c6\u0003J\u000e\u0010 \u001a\u00028\u0000H\u00c6\u0003\u00a2\u0006\u0002\u0010\u0013J\t\u0010!\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\"\u001a\u00020\u0004H\u00c6\u0003J\t\u0010#\u001a\u00020\u0004H\u00c6\u0003J\t\u0010$\u001a\u00020\u0007H\u00c6\u0003J\t\u0010%\u001a\u00020\u0007H\u00c6\u0003J\t\u0010&\u001a\u00020\u0007H\u00c6\u0003J\t\u0010\'\u001a\u00020\u0004H\u00c6\u0003Jx\u0010(\u001a\b\u0012\u0004\u0012\u00028\u00000\u00002\b\b\u0002\u0010\u0003\u001a\u00020\u00042\b\b\u0002\u0010\u0005\u001a\u00028\u00002\b\b\u0002\u0010\u0006\u001a\u00020\u00072\b\b\u0002\u0010\b\u001a\u00020\u00042\b\b\u0002\u0010\t\u001a\u00020\u00042\b\b\u0002\u0010\n\u001a\u00020\u00072\b\b\u0002\u0010\u000b\u001a\u00020\u00072\b\b\u0002\u0010\f\u001a\u00020\u00072\b\b\u0002\u0010\r\u001a\u00020\u00042\b\b\u0002\u0010\u000e\u001a\u00020\u0004H\u00c6\u0001\u00a2\u0006\u0002\u0010)J\u0013\u0010*\u001a\u00020+2\b\u0010,\u001a\u0004\u0018\u00010\u0002H\u00d6\u0003J\t\u0010-\u001a\u00020\u0004H\u00d6\u0001J\t\u0010.\u001a\u00020\u0007H\u00d6\u0001R\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0013\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\u0014\u001a\u0004\b\u0012\u0010\u0013R\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0016R\u0011\u0010\b\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0011R\u0011\u0010\t\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0018\u0010\u0011R\u0011\u0010\n\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u0016R\u0011\u0010\u000b\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001a\u0010\u0016R\u0011\u0010\f\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u0016R\u0011\u0010\r\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001c\u0010\u0011R\u0011\u0010\u000e\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u0011\u00a8\u0006/"}, d2 = {"Lcom/eazy/daiku/utility/base/PaginationData;", "T", "", "current_page", "", "data", "first_page_url", "", "from", "last_page", "last_page_url", "path", "per_page", "to", "total", "(ILjava/lang/Object;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V", "getCurrent_page", "()I", "getData", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getFirst_page_url", "()Ljava/lang/String;", "getFrom", "getLast_page", "getLast_page_url", "getPath", "getPer_page", "getTo", "getTotal", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(ILjava/lang/Object;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Lcom/eazy/daiku/utility/base/PaginationData;", "equals", "", "other", "hashCode", "toString", "app_wegoRelease"})
public final class PaginationData<T extends java.lang.Object> {
    private final int current_page = 0;
    private final T data = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String first_page_url = null;
    private final int from = 0;
    private final int last_page = 0;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String last_page_url = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String path = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String per_page = null;
    private final int to = 0;
    private final int total = 0;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.base.PaginationData<T> copy(int current_page, T data, @org.jetbrains.annotations.NotNull()
    java.lang.String first_page_url, int from, int last_page, @org.jetbrains.annotations.NotNull()
    java.lang.String last_page_url, @org.jetbrains.annotations.NotNull()
    java.lang.String path, @org.jetbrains.annotations.NotNull()
    java.lang.String per_page, int to, int total) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public PaginationData(int current_page, T data, @org.jetbrains.annotations.NotNull()
    java.lang.String first_page_url, int from, int last_page, @org.jetbrains.annotations.NotNull()
    java.lang.String last_page_url, @org.jetbrains.annotations.NotNull()
    java.lang.String path, @org.jetbrains.annotations.NotNull()
    java.lang.String per_page, int to, int total) {
        super();
    }
    
    public final int component1() {
        return 0;
    }
    
    public final int getCurrent_page() {
        return 0;
    }
    
    public final T component2() {
        return null;
    }
    
    public final T getData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFirst_page_url() {
        return null;
    }
    
    public final int component4() {
        return 0;
    }
    
    public final int getFrom() {
        return 0;
    }
    
    public final int component5() {
        return 0;
    }
    
    public final int getLast_page() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLast_page_url() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPath() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getPer_page() {
        return null;
    }
    
    public final int component9() {
        return 0;
    }
    
    public final int getTo() {
        return 0;
    }
    
    public final int component10() {
        return 0;
    }
    
    public final int getTotal() {
        return 0;
    }
}