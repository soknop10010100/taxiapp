package com.eazy.daiku.ui.scan_qr_code;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000T\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u000f\u001a\u00020\u0010H\u0002J\b\u0010\u0011\u001a\u00020\u0010H\u0002J\b\u0010\u0012\u001a\u00020\u0010H\u0002J\u0010\u0010\u0013\u001a\u00020\u00102\u0006\u0010\u0014\u001a\u00020\u0015H\u0002J\u0012\u0010\u0016\u001a\u00020\u00102\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0014J\b\u0010\u0019\u001a\u00020\u0010H\u0014J\u0010\u0010\u001a\u001a\u00020\u001b2\u0006\u0010\u001c\u001a\u00020\u001dH\u0016J\b\u0010\u001e\u001a\u00020\u0010H\u0014J\b\u0010\u001f\u001a\u00020\u0010H\u0014J\u0012\u0010 \u001a\u00020\u00102\b\u0010!\u001a\u0004\u0018\u00010\"H\u0002J\u001c\u0010#\u001a\u00020\u00102\u0006\u0010$\u001a\u00020\u001b2\n\b\u0002\u0010%\u001a\u0004\u0018\u00010\u0015H\u0002J\b\u0010&\u001a\u00020\u0010H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\t\u001a\u00020\n8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\r\u0010\u000e\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\'"}, d2 = {"Lcom/eazy/daiku/ui/scan_qr_code/ScanQRCodeActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityScanQrcodeBinding;", "codeScanner", "Lcom/budiyev/android/codescanner/CodeScanner;", "easyCallBackImage", "Lpl/aprilapps/easyphotopicker/EasyImage$Callbacks;", "scanQrVm", "Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "getScanQrVm", "()Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "scanQrVm$delegate", "Lkotlin/Lazy;", "doAction", "", "initObserved", "initView", "invokeQrCode", "qrText", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "onPause", "onResume", "onReturnUri", "resultUri", "Landroid/net/Uri;", "showErrorCover", "isShow", "errorMsg", "startCameraQr", "app_wegoRelease"})
public final class ScanQRCodeActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.budiyev.android.codescanner.CodeScanner codeScanner;
    private com.eazy.daiku.databinding.ActivityScanQrcodeBinding binding;
    private final kotlin.Lazy scanQrVm$delegate = null;
    private final pl.aprilapps.easyphotopicker.EasyImage.Callbacks easyCallBackImage = null;
    
    public ScanQRCodeActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.QrCodeVm getScanQrVm() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final void initView() {
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    private final void startCameraQr() {
    }
    
    private final void invokeQrCode(java.lang.String qrText) {
    }
    
    private final void onReturnUri(android.net.Uri resultUri) {
    }
    
    private final void showErrorCover(boolean isShow, java.lang.String errorMsg) {
    }
}