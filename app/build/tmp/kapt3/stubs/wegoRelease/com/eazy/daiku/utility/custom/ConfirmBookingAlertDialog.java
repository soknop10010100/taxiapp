package com.eazy.daiku.utility.custom;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000j\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 &2\u00020\u00012\u00020\u0002:\u0001&B\u0005\u00a2\u0006\u0002\u0010\u0003J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\b\u0010\u0016\u001a\u00020\u0014H\u0002J\b\u0010\u0017\u001a\u00020\u0014H\u0002J\u0010\u0010\u0018\u001a\u00020\u00142\u0006\u0010\u0019\u001a\u00020\u001aH\u0016J$\u0010\u001b\u001a\u00020\u001c2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016J\u0012\u0010#\u001a\u00020\u00142\b\u0010\f\u001a\u0004\u0018\u00010\rH\u0016J\u001a\u0010$\u001a\u00020\u00142\u0006\u0010%\u001a\u00020\u001c2\b\u0010!\u001a\u0004\u0018\u00010\"H\u0016R\u000e\u0010\u0004\u001a\u00020\u0005X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R0\u0010\u000e\u001a$\u0012\u0004\u0012\u00020\u0010\u0012\u0006\u0012\u0004\u0018\u00010\u0011\u0012\u0004\u0012\u00020\u0012\u0012\u0006\u0012\u0004\u0018\u00010\u0013\u0012\u0004\u0012\u00020\u00140\u000fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\'"}, d2 = {"Lcom/eazy/daiku/utility/custom/ConfirmBookingAlertDialog;", "Landroidx/fragment/app/DialogFragment;", "Lcom/google/android/gms/maps/OnMapReadyCallback;", "()V", "binding", "Lcom/eazy/daiku/databinding/ConfirmBookingLayoutBinding;", "bookingTaxiModel", "Lcom/eazy/daiku/data/model/BookingTaxiModel;", "fContext", "Landroidx/fragment/app/FragmentActivity;", "fusedLocationProviderClient", "Lcom/google/android/gms/location/FusedLocationProviderClient;", "googleMap", "Lcom/google/android/gms/maps/GoogleMap;", "selectActionListener", "Lkotlin/Function4;", "Lcom/eazy/daiku/utility/enumerable/ConfirmKey;", "Landroid/app/Dialog;", "", "Lcom/eazy/daiku/data/model/server_model/DestinationInfo;", "", "doAction", "initData", "initView", "onAttach", "context", "Landroid/content/Context;", "onCreateView", "Landroid/view/View;", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onMapReady", "onViewCreated", "view", "Companion", "app_wegoRelease"})
public final class ConfirmBookingAlertDialog extends androidx.fragment.app.DialogFragment implements com.google.android.gms.maps.OnMapReadyCallback {
    private com.eazy.daiku.databinding.ConfirmBookingLayoutBinding binding;
    private androidx.fragment.app.FragmentActivity fContext;
    private com.google.android.gms.location.FusedLocationProviderClient fusedLocationProviderClient;
    private kotlin.jvm.functions.Function4<? super com.eazy.daiku.utility.enumerable.ConfirmKey, ? super android.app.Dialog, ? super java.lang.String, ? super com.eazy.daiku.data.model.server_model.DestinationInfo, kotlin.Unit> selectActionListener;
    private com.eazy.daiku.data.model.BookingTaxiModel bookingTaxiModel;
    private com.google.android.gms.maps.GoogleMap googleMap;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.custom.ConfirmBookingAlertDialog.Companion Companion = null;
    
    public ConfirmBookingAlertDialog() {
        super();
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.utility.custom.ConfirmBookingAlertDialog newInstance(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.model.BookingTaxiModel bookingTaxiModel, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function4<? super com.eazy.daiku.utility.enumerable.ConfirmKey, ? super android.app.Dialog, ? super java.lang.String, ? super com.eazy.daiku.data.model.server_model.DestinationInfo, kotlin.Unit> selectActionListener) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initData() {
    }
    
    private final void doAction() {
    }
    
    @java.lang.Override()
    public void onMapReady(@org.jetbrains.annotations.Nullable()
    com.google.android.gms.maps.GoogleMap googleMap) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J:\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062(\u0010\u0007\u001a$\u0012\u0004\u0012\u00020\t\u0012\u0006\u0012\u0004\u0018\u00010\n\u0012\u0004\u0012\u00020\u000b\u0012\u0006\u0012\u0004\u0018\u00010\f\u0012\u0004\u0012\u00020\r0\bH\u0007\u00a8\u0006\u000e"}, d2 = {"Lcom/eazy/daiku/utility/custom/ConfirmBookingAlertDialog$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/utility/custom/ConfirmBookingAlertDialog;", "bookingTaxiModel", "Lcom/eazy/daiku/data/model/BookingTaxiModel;", "selectActionListener", "Lkotlin/Function4;", "Lcom/eazy/daiku/utility/enumerable/ConfirmKey;", "Landroid/app/Dialog;", "", "Lcom/eazy/daiku/data/model/server_model/DestinationInfo;", "", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.utility.custom.ConfirmBookingAlertDialog newInstance(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.BookingTaxiModel bookingTaxiModel, @org.jetbrains.annotations.NotNull()
        kotlin.jvm.functions.Function4<? super com.eazy.daiku.utility.enumerable.ConfirmKey, ? super android.app.Dialog, ? super java.lang.String, ? super com.eazy.daiku.data.model.server_model.DestinationInfo, kotlin.Unit> selectActionListener) {
            return null;
        }
    }
}