package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\t\u0018\u0000 \u0015*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u00022\u00020\u0003:\u0002\u0015\u0016B3\b\u0002\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0012\u0010\u0006\u001a\u000e\u0012\u0004\u0012\u00028\u0000\u0012\u0004\u0012\u00028\u00010\u0007\u0012\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\u0015\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00028\u0001H\u0002\u00a2\u0006\u0002\u0010\u0010J\'\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0012\u001a\u00028\u00002\u0010\b\u0002\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\tH\u0007\u00a2\u0006\u0002\u0010\u0013J\u0016\u0010\u0014\u001a\u00020\u000e2\u000e\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\tR\u0014\u0010\u000b\u001a\b\u0012\u0004\u0012\u00028\u00000\fX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0016\u0010\b\u001a\n\u0012\u0004\u0012\u00028\u0001\u0018\u00010\tX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0017"}, d2 = {"Lcom/eazy/daiku/utility/BetterActivityResult;", "Input", "Result", "", "caller", "Landroidx/activity/result/ActivityResultCaller;", "contract", "Landroidx/activity/result/contract/ActivityResultContract;", "onActivityResult", "Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;", "(Landroidx/activity/result/ActivityResultCaller;Landroidx/activity/result/contract/ActivityResultContract;Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;)V", "launcher", "Landroidx/activity/result/ActivityResultLauncher;", "callOnActivityResult", "", "result", "(Ljava/lang/Object;)V", "launch", "input", "(Ljava/lang/Object;Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;)V", "setOnActivityResult", "Companion", "OnActivityResult", "app_wegoRelease"})
public final class BetterActivityResult<Input extends java.lang.Object, Result extends java.lang.Object> {
    private com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<Result> onActivityResult;
    private final androidx.activity.result.ActivityResultLauncher<Input> launcher = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.BetterActivityResult.Companion Companion = null;
    
    private BetterActivityResult(androidx.activity.result.ActivityResultCaller caller, androidx.activity.result.contract.ActivityResultContract<Input, Result> contract, com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<Result> onActivityResult) {
        super();
    }
    
    public final void setOnActivityResult(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<Result> onActivityResult) {
    }
    
    /**
     * Same as [.launch] with last parameter set to `null`.
     */
    @kotlin.jvm.JvmOverloads()
    public final void launch(Input input) {
    }
    
    /**
     * Same as [.launch] with last parameter set to `null`.
     */
    @kotlin.jvm.JvmOverloads()
    public final void launch(Input input, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<Result> onActivityResult) {
    }
    
    private final void callOnActivityResult(Result result) {
    }
    
    /**
     * Callback interface
     */
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\bf\u0018\u0000*\u0004\b\u0002\u0010\u00012\u00020\u0002J\u0015\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00028\u0002H&\u00a2\u0006\u0002\u0010\u0006\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;", "O", "", "onActivityResult", "", "result", "(Ljava/lang/Object;)V", "app_wegoRelease"})
    public static abstract interface OnActivityResult<O extends java.lang.Object> {
        
        /**
         * Called after receiving a result from the target activity
         */
        public abstract void onActivityResult(O result);
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u000e\u0012\u0004\u0012\u00020\u0005\u0012\u0004\u0012\u00020\u00060\u00042\u0006\u0010\u0007\u001a\u00020\bJ:\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b0\u0004\"\u0004\b\u0002\u0010\n\"\u0004\b\u0003\u0010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b0\rJJ\u0010\t\u001a\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b0\u0004\"\u0004\b\u0002\u0010\n\"\u0004\b\u0003\u0010\u000b2\u0006\u0010\u0007\u001a\u00020\b2\u0012\u0010\f\u001a\u000e\u0012\u0004\u0012\u0002H\n\u0012\u0004\u0012\u0002H\u000b0\r2\u000e\u0010\u000e\u001a\n\u0012\u0004\u0012\u0002H\u000b\u0018\u00010\u000f\u00a8\u0006\u0010"}, d2 = {"Lcom/eazy/daiku/utility/BetterActivityResult$Companion;", "", "()V", "registerActivityForResult", "Lcom/eazy/daiku/utility/BetterActivityResult;", "Landroid/content/Intent;", "Landroidx/activity/result/ActivityResult;", "caller", "Landroidx/activity/result/ActivityResultCaller;", "registerForActivityResult", "Input", "Result", "contract", "Landroidx/activity/result/contract/ActivityResultContract;", "onActivityResult", "Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;", "app_wegoRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        /**
         * Register activity result using a [ActivityResultContract] and an in-place activity result callback like
         * the default approach. You can still customise callback using [.launch].
         */
        @org.jetbrains.annotations.NotNull()
        public final <Input extends java.lang.Object, Result extends java.lang.Object>com.eazy.daiku.utility.BetterActivityResult<Input, Result> registerForActivityResult(@org.jetbrains.annotations.NotNull()
        androidx.activity.result.ActivityResultCaller caller, @org.jetbrains.annotations.NotNull()
        androidx.activity.result.contract.ActivityResultContract<Input, Result> contract, @org.jetbrains.annotations.Nullable()
        com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<Result> onActivityResult) {
            return null;
        }
        
        /**
         * Same as [.registerForActivityResult] except
         * the last argument is set to `null`.
         */
        @org.jetbrains.annotations.NotNull()
        public final <Input extends java.lang.Object, Result extends java.lang.Object>com.eazy.daiku.utility.BetterActivityResult<Input, Result> registerForActivityResult(@org.jetbrains.annotations.NotNull()
        androidx.activity.result.ActivityResultCaller caller, @org.jetbrains.annotations.NotNull()
        androidx.activity.result.contract.ActivityResultContract<Input, Result> contract) {
            return null;
        }
        
        /**
         * Specialised method for launching new activities.
         */
        @org.jetbrains.annotations.NotNull()
        public final com.eazy.daiku.utility.BetterActivityResult<android.content.Intent, androidx.activity.result.ActivityResult> registerActivityForResult(@org.jetbrains.annotations.NotNull()
        androidx.activity.result.ActivityResultCaller caller) {
            return null;
        }
    }
}