package com.eazy.daiku.data.model.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u000b\u0018\u0000*\u0004\b\u0000\u0010\u00012\u00020\u0002B%\u0012\u0006\u0010\u0003\u001a\u00020\u0004\u0012\u0006\u0010\u0005\u001a\u00028\u0000\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nR\u0011\u0010\u0003\u001a\u00020\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0013\u0010\u0005\u001a\u00028\u0000\u00a2\u0006\n\n\u0002\u0010\u000f\u001a\u0004\b\r\u0010\u000eR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0010\u0010\u0011R\u0011\u0010\b\u001a\u00020\t\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0013\u00a8\u0006\u0014"}, d2 = {"Lcom/eazy/daiku/data/model/base/ResponseErrorBody;", "T", "", "code", "", "error", "message", "", "success", "", "(ILjava/lang/Object;Ljava/lang/String;Z)V", "getCode", "()I", "getError", "()Ljava/lang/Object;", "Ljava/lang/Object;", "getMessage", "()Ljava/lang/String;", "getSuccess", "()Z", "app_taxiRelease"})
public final class ResponseErrorBody<T extends java.lang.Object> {
    private final int code = 0;
    private final T error = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String message = null;
    private final boolean success = false;
    
    public ResponseErrorBody(int code, T error, @org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean success) {
        super();
    }
    
    public final int getCode() {
        return 0;
    }
    
    public final T getError() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMessage() {
        return null;
    }
    
    public final boolean getSuccess() {
        return false;
    }
}