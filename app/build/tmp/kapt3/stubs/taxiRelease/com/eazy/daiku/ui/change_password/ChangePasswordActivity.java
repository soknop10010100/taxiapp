package com.eazy.daiku.ui.change_password;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u0000 \u001b2\u00020\u0001:\u0001\u001bB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0010\u001a\u00020\u0011H\u0002J\b\u0010\u0012\u001a\u00020\u0011H\u0002J\b\u0010\u0013\u001a\u00020\u0011H\u0002J\u0012\u0010\u0014\u001a\u00020\u00112\b\u0010\u0015\u001a\u0004\u0018\u00010\u0016H\u0014J\u0010\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\u001aH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\b\u001a\u00020\t8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\f\u0010\r\u001a\u0004\b\n\u0010\u000bR\u000e\u0010\u000e\u001a\u00020\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001c"}, d2 = {"Lcom/eazy/daiku/ui/change_password/ChangePasswordActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityChangePasswordBinding;", "otpToken", "", "pinCodeOrOtp", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "Lkotlin/Lazy;", "verifyPinEnum", "Lcom/eazy/daiku/utility/enumerable/VerifyPinEnum;", "doAction", "", "initObserved", "intiView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "Companion", "app_taxiRelease"})
public final class ChangePasswordActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityChangePasswordBinding binding;
    private java.lang.String pinCodeOrOtp = "";
    private java.lang.String otpToken = "";
    private com.eazy.daiku.utility.enumerable.VerifyPinEnum verifyPinEnum = com.eazy.daiku.utility.enumerable.VerifyPinEnum.Other;
    private final kotlin.Lazy userInfoVM$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.change_password.ChangePasswordActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String PinCodeKey = "pin_code_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String Otp_token_key = "otp_token_key";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ScreenThatRequestKey = "screen_that_request_key";
    
    public ChangePasswordActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void intiView() {
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/ui/change_password/ChangePasswordActivity$Companion;", "", "()V", "Otp_token_key", "", "PinCodeKey", "ScreenThatRequestKey", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}