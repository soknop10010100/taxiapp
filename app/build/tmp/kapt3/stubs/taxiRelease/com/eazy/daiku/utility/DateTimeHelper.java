package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004J\u001e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\b\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\u00042\u0006\u0010\n\u001a\u00020\u0004J\u000e\u0010\u000b\u001a\u00020\u00062\u0006\u0010\u0005\u001a\u00020\u0004J \u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\u00062\u0006\u0010\u000e\u001a\u00020\u00042\u0006\u0010\u000f\u001a\u00020\u0010H\u0002J\u0010\u0010\u0011\u001a\u0004\u0018\u00010\u00062\u0006\u0010\u0012\u001a\u00020\u0004\u00a8\u0006\u0013"}, d2 = {"Lcom/eazy/daiku/utility/DateTimeHelper;", "", "()V", "dateFm", "", "dateStr", "Ljava/util/Date;", "pattern", "value", "patternValue", "newPattern", "dateParse", "formatToCurrentTimeZone", "d", "format", "tz", "Ljava/util/TimeZone;", "localDateTimeFm", "dateString", "app_taxiRelease"})
public final class DateTimeHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.DateTimeHelper INSTANCE = null;
    
    private DateTimeHelper() {
        super();
    }
    
    /**
     * Functionality to do convert server date time to local date time
     *
     * @param dateString => yyyy-MM-dd HH:mm:ss
     * @return => yyyy-MM-dd HH:mm:ss
     */
    @org.jetbrains.annotations.Nullable()
    public final java.util.Date localDateTimeFm(@org.jetbrains.annotations.NotNull()
    java.lang.String dateString) {
        return null;
    }
    
    /**
     * return date format with local timezone by Locale.getDefault()
     */
    private final java.lang.String formatToCurrentTimeZone(java.util.Date d, java.lang.String format, java.util.TimeZone tz) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String dateFm(@org.jetbrains.annotations.NotNull()
    java.util.Date dateStr, @org.jetbrains.annotations.NotNull()
    java.lang.String pattern) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.Date dateParse(@org.jetbrains.annotations.NotNull()
    java.lang.String dateStr) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String dateFm(@org.jetbrains.annotations.NotNull()
    java.lang.String value, @org.jetbrains.annotations.NotNull()
    java.lang.String patternValue, @org.jetbrains.annotations.NotNull()
    java.lang.String newPattern) {
        return null;
    }
}