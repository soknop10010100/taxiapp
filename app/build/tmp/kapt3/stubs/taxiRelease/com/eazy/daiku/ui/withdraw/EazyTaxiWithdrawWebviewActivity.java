package com.eazy.daiku.ui.withdraw;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u0000 !2\u00020\u0001:\u0002!\"B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0013\u001a\u00020\u0014J\b\u0010\u0015\u001a\u00020\u0014H\u0002J\b\u0010\u0016\u001a\u00020\u0014H\u0002J\b\u0010\u0017\u001a\u00020\u0014H\u0002J\b\u0010\u0018\u001a\u00020\u0014H\u0002J\b\u0010\u0019\u001a\u00020\u0014H\u0002J\u0012\u0010\u001a\u001a\u00020\u00142\b\u0010\u001b\u001a\u0004\u0018\u00010\u001cH\u0014J\u0010\u0010\u001d\u001a\u00020\u001e2\u0006\u0010\u001f\u001a\u00020 H\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u000e\u0010\u0011\u001a\u00020\u0012X\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006#"}, d2 = {"Lcom/eazy/daiku/ui/withdraw/EazyTaxiWithdrawWebviewActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "JAVASCRIPT_OBJ", "", "binding", "Lcom/eazy/daiku/databinding/ActivityEazyTaxiWithdrawWebviewBinding;", "onPaymentSuccess", "verifyOtpUrl", "webChromeClient", "Landroid/webkit/WebChromeClient;", "getWebChromeClient", "()Landroid/webkit/WebChromeClient;", "webViewClient", "Landroid/webkit/WebViewClient;", "getWebViewClient", "()Landroid/webkit/WebViewClient;", "withdrawMoneyRespondModel", "Lcom/eazy/daiku/data/model/WithdrawMoneyRespondModel;", "configWebSetting", "", "initLoadWebView", "initView", "initWebView", "initWithdrawMoneyRespondModel", "injectJavaScriptFunction", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "Companion", "JavaScriptInterface", "app_taxiRelease"})
public final class EazyTaxiWithdrawWebviewActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private final java.lang.String JAVASCRIPT_OBJ = "javascript_obj";
    private com.eazy.daiku.databinding.ActivityEazyTaxiWithdrawWebviewBinding binding;
    private com.eazy.daiku.data.model.WithdrawMoneyRespondModel withdrawMoneyRespondModel;
    private java.lang.String verifyOtpUrl = "";
    private final java.lang.String onPaymentSuccess = "onPaymentSuccess";
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.withdraw.EazyTaxiWithdrawWebviewActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WithdrawMoneyRespondModelKey = "WithdrawMoneyRespondModelKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String WithdrawVerifyOtpUrlKey = "WithdrawVerifyOtpUrlKey";
    @org.jetbrains.annotations.NotNull()
    private final android.webkit.WebChromeClient webChromeClient = null;
    @org.jetbrains.annotations.NotNull()
    private final android.webkit.WebViewClient webViewClient = null;
    
    public EazyTaxiWithdrawWebviewActivity() {
        super();
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void configWebSetting() {
    }
    
    private final void initWithdrawMoneyRespondModel() {
    }
    
    private final void initLoadWebView() {
    }
    
    private final void initWebView() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.webkit.WebChromeClient getWebChromeClient() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.webkit.WebViewClient getWebViewClient() {
        return null;
    }
    
    private final void injectJavaScriptFunction() {
    }
    
    private final void initView() {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0012\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006H\u0007\u00a8\u0006\u0007"}, d2 = {"Lcom/eazy/daiku/ui/withdraw/EazyTaxiWithdrawWebviewActivity$JavaScriptInterface;", "", "(Lcom/eazy/daiku/ui/withdraw/EazyTaxiWithdrawWebviewActivity;)V", "onPaymentSuccess", "", "data", "", "app_taxiRelease"})
    public final class JavaScriptInterface {
        
        public JavaScriptInterface() {
            super();
        }
        
        @android.webkit.JavascriptInterface()
        public final void onPaymentSuccess(@org.jetbrains.annotations.Nullable()
        java.lang.String data) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/ui/withdraw/EazyTaxiWithdrawWebviewActivity$Companion;", "", "()V", "WithdrawMoneyRespondModelKey", "", "WithdrawVerifyOtpUrlKey", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}