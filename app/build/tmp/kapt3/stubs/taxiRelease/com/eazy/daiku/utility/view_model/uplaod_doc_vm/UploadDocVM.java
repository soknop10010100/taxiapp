package com.eazy.daiku.utility.view_model.uplaod_doc_vm;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000d\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0017\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006J\u000e\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"J*\u0010#\u001a\u00020 2\"\u0010$\u001a\u001e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020&0%j\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020&`\'J*\u0010(\u001a\u00020 2\"\u0010$\u001a\u001e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020&0%j\u000e\u0012\u0004\u0012\u00020\"\u0012\u0004\u0012\u00020&`\'R\u0014\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\n\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R \u0010\u000f\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00100\u000b0\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\t0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u0013\u0010\u0014R\u001d\u0010\u0015\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\f0\u000b0\u00168F\u00a2\u0006\u0006\u001a\u0004\b\u0017\u0010\u0018R\u001d\u0010\u0019\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000e0\u000b0\u00168F\u00a2\u0006\u0006\u001a\u0004\b\u001a\u0010\u0018R\u0014\u0010\u0004\u001a\u00020\u0005X\u0096\u0004\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR#\u0010\u001d\u001a\u0014\u0012\u0010\u0012\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00110\u00100\u000b0\b8F\u00a2\u0006\u0006\u001a\u0004\b\u001e\u0010\u0014\u00a8\u0006)"}, d2 = {"Lcom/eazy/daiku/utility/view_model/uplaod_doc_vm/UploadDocVM;", "Lcom/eazy/daiku/utility/base/BaseViewModel;", "context", "Landroid/content/Context;", "repository", "Lcom/eazy/daiku/data/repository/Repository;", "(Landroid/content/Context;Lcom/eazy/daiku/data/repository/Repository;)V", "_loadingUploadDocMutableLiveData", "Landroidx/lifecycle/MutableLiveData;", "", "_reUploadKycDocMutableLiveData", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/User;", "_registerMutableLiveData", "Lcom/eazy/daiku/data/model/server_model/CreateUserRespond;", "_vehicleTypeUploadDocMutableLiveData", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "loadingUploadDocMutableLiveData", "getLoadingUploadDocMutableLiveData", "()Landroidx/lifecycle/MutableLiveData;", "reUploadKycDocMutableLiveData", "Landroidx/lifecycle/LiveData;", "getReUploadKycDocMutableLiveData", "()Landroidx/lifecycle/LiveData;", "registerMutableLiveData", "getRegisterMutableLiveData", "getRepository", "()Lcom/eazy/daiku/data/repository/Repository;", "vehicleTypeUploadDocMutableLiveData", "getVehicleTypeUploadDocMutableLiveData", "fetchVehicleType", "", "vehicleType", "", "reUploadKycDoc", "body", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "submitCreateUser", "app_taxiRelease"})
public final class UploadDocVM extends com.eazy.daiku.utility.base.BaseViewModel {
    private final android.content.Context context = null;
    @org.jetbrains.annotations.NotNull()
    private final com.eazy.daiku.data.repository.Repository repository = null;
    private final androidx.lifecycle.MutableLiveData<java.lang.Boolean> _loadingUploadDocMutableLiveData = null;
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond>>> _vehicleTypeUploadDocMutableLiveData = null;
    
    /**
     * register new user
     */
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.CreateUserRespond>> _registerMutableLiveData = null;
    
    /**
     * re upload kyc
     */
    private final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> _reUploadKycDocMutableLiveData = null;
    
    @javax.inject.Inject()
    public UploadDocVM(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.data.repository.Repository repository) {
        super(null, null);
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.data.repository.Repository getRepository() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.lang.Boolean> getLoadingUploadDocMutableLiveData() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<com.eazy.daiku.data.model.base.ApiResWraper<java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond>>> getVehicleTypeUploadDocMutableLiveData() {
        return null;
    }
    
    public final void fetchVehicleType(@org.jetbrains.annotations.NotNull()
    java.lang.String vehicleType) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.CreateUserRespond>> getRegisterMutableLiveData() {
        return null;
    }
    
    public final void submitCreateUser(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> body) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.LiveData<com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> getReUploadKycDocMutableLiveData() {
        return null;
    }
    
    public final void reUploadKycDoc(@org.jetbrains.annotations.NotNull()
    java.util.HashMap<java.lang.String, java.lang.Object> body) {
    }
}