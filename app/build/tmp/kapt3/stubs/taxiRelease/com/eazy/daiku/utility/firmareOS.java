package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0010\u0003\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0010\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012J\u000e\u0010\u0013\u001a\u00020\u00042\u0006\u0010\u0011\u001a\u00020\u0012J\u0006\u0010\u0014\u001a\u00020\u0004J\u0010\u0010\u0015\u001a\u00020\u00042\b\u0010\u0011\u001a\u0004\u0018\u00010\u0012J\u0006\u0010\u0016\u001a\u00020\u0004R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\f\u0010\r\"\u0004\b\u000e\u0010\u000f\u00a8\u0006\u0017"}, d2 = {"Lcom/eazy/daiku/utility/firmareOS;", "", "()V", "LINE_SEPARATOR", "", "mCrashInfo", "Landroid/app/ApplicationErrorReport$CrashInfo;", "getMCrashInfo", "()Landroid/app/ApplicationErrorReport$CrashInfo;", "setMCrashInfo", "(Landroid/app/ApplicationErrorReport$CrashInfo;)V", "mErrorMessage", "getMErrorMessage", "()Ljava/lang/String;", "setMErrorMessage", "(Ljava/lang/String;)V", "errorMsg", "exception", "", "reportCallStack", "reportDeviceInfo", "reportError", "reportFirmware", "app_taxiRelease"})
public final class firmareOS {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.firmareOS INSTANCE = null;
    @org.jetbrains.annotations.Nullable()
    private static android.app.ApplicationErrorReport.CrashInfo mCrashInfo;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String mErrorMessage = "";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LINE_SEPARATOR = "\n";
    
    private firmareOS() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.ApplicationErrorReport.CrashInfo getMCrashInfo() {
        return null;
    }
    
    public final void setMCrashInfo(@org.jetbrains.annotations.Nullable()
    android.app.ApplicationErrorReport.CrashInfo p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMErrorMessage() {
        return null;
    }
    
    public final void setMErrorMessage(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String errorMsg(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String reportError(@org.jetbrains.annotations.Nullable()
    java.lang.Throwable exception) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String reportCallStack(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable exception) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String reportDeviceInfo() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String reportFirmware() {
        return null;
    }
}