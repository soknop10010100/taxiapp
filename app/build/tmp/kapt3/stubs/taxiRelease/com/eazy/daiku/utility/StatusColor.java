package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u0018\u0010\t\u001a\u0004\u0018\u00010\n2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/utility/StatusColor;", "", "()V", "colorStatus", "", "context", "Landroid/content/Context;", "statusStr", "", "colorStatusTrx", "Landroid/graphics/drawable/Drawable;", "app_taxiRelease"})
public final class StatusColor {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.StatusColor INSTANCE = null;
    
    private StatusColor() {
        super();
    }
    
    public final int colorStatus(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String statusStr) {
        return 0;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.graphics.drawable.Drawable colorStatusTrx(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String statusStr) {
        return null;
    }
}