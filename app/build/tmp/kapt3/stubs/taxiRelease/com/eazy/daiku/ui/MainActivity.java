package com.eazy.daiku.ui;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0006\n\u0002\u0018\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"H\u0002J\b\u0010#\u001a\u00020 H\u0002J\b\u0010$\u001a\u00020 H\u0002J\b\u0010%\u001a\u00020 H\u0002J\b\u0010&\u001a\u00020 H\u0002J\u0012\u0010\'\u001a\u00020 2\b\u0010(\u001a\u0004\u0018\u00010)H\u0014J\b\u0010*\u001a\u00020 H\u0014J\b\u0010+\u001a\u00020 H\u0014J\b\u0010,\u001a\u00020 H\u0014J\b\u0010-\u001a\u00020 H\u0014J\b\u0010.\u001a\u00020 H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u000eX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\u0014\u001a\u0004\b\u0011\u0010\u0012R\u001b\u0010\u0015\u001a\u00020\u00168BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0019\u0010\u0014\u001a\u0004\b\u0017\u0010\u0018R\u001b\u0010\u001a\u001a\u00020\u001b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u001e\u0010\u0014\u001a\u0004\b\u001c\u0010\u001d\u00a8\u0006/"}, d2 = {"Lcom/eazy/daiku/ui/MainActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "bookingTaxiObj", "Lcom/parse/ParseObject;", "homeScreenAdapter", "Lcom/eazy/daiku/utility/adapter/HomeScreenAdapter;", "intentFilter", "Landroid/content/IntentFilter;", "isBooking", "", "mainBinding", "Lcom/eazy/daiku/databinding/ActivityMainBinding;", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "qrCodeVm", "Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "getQrCodeVm", "()Lcom/eazy/daiku/utility/view_model/QrCodeVm;", "qrCodeVm$delegate", "Lkotlin/Lazy;", "userInfoVM", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserInfoVM", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userInfoVM$delegate", "viewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "getViewModel", "()Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "viewModel$delegate", "checkStatusProcessingBooking", "", "code", "", "doAction", "initObserved", "intiData", "intiView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onResume", "onStart", "onStop", "setUpHomeScreenLayout", "app_taxiRelease"})
public final class MainActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityMainBinding mainBinding;
    private final kotlin.Lazy qrCodeVm$delegate = null;
    private final kotlin.Lazy userInfoVM$delegate = null;
    private final kotlin.Lazy viewModel$delegate = null;
    private final android.content.IntentFilter intentFilter = null;
    private com.eazy.daiku.utility.adapter.HomeScreenAdapter homeScreenAdapter;
    private com.parse.ParseObject bookingTaxiObj;
    private boolean isBooking = false;
    private final com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver = null;
    
    public MainActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.QrCodeVm getQrCodeVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserInfoVM() {
        return null;
    }
    
    private final com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel getViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void checkStatusProcessingBooking(java.lang.String code) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onStop() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    private final void intiData() {
    }
    
    private final void initObserved() {
    }
    
    private final void intiView() {
    }
    
    private final void doAction() {
    }
    
    private final void setUpHomeScreenLayout() {
    }
}