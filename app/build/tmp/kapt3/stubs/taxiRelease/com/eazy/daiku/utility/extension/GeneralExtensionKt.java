package com.eazy.daiku.utility.extension;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\f\n\u0000\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\u001a\f\u0010\u0000\u001a\u00020\u0001*\u0004\u0018\u00010\u0002\u00a8\u0006\u0003"}, d2 = {"toText", "", "Landroid/location/Location;", "app_taxiRelease"})
public final class GeneralExtensionKt {
    
    /**
     * Returns the `location` object as a human readable string.
     */
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toText(@org.jetbrains.annotations.Nullable()
    android.location.Location $this$toText) {
        return null;
    }
}