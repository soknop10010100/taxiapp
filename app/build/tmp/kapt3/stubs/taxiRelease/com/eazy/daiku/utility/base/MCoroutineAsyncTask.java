package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000(\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0010\u0011\n\u0002\b\r\b&\u0018\u0000*\u0004\b\u0000\u0010\u0001*\u0004\b\u0001\u0010\u0002*\u0004\b\u0002\u0010\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u0007J%\u0010\u000e\u001a\u00028\u00022\u0016\u0010\u000f\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00018\u00000\u0010\"\u0004\u0018\u00018\u0000H&\u00a2\u0006\u0002\u0010\u0011J#\u0010\u0012\u001a\u00020\f2\u0016\u0010\u000f\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00018\u00000\u0010\"\u0004\u0018\u00018\u0000\u00a2\u0006\u0002\u0010\u0013J\u0017\u0010\u0014\u001a\u00020\f2\b\u0010\u0015\u001a\u0004\u0018\u00018\u0002H&\u00a2\u0006\u0002\u0010\u0016J\u0017\u0010\u0017\u001a\u00020\f2\b\u0010\u0015\u001a\u0004\u0018\u00018\u0002H&\u00a2\u0006\u0002\u0010\u0016J\b\u0010\u0018\u001a\u00020\fH&J%\u0010\u0019\u001a\u00020\f2\u0016\u0010\u001a\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00018\u00010\u0010\"\u0004\u0018\u00018\u0001H&\u00a2\u0006\u0002\u0010\u0013J#\u0010\u001b\u001a\u00020\f2\u0016\u0010\u001c\u001a\f\u0012\b\b\u0001\u0012\u0004\u0018\u00018\u00010\u0010\"\u0004\u0018\u00018\u0001\u00a2\u0006\u0002\u0010\u0013R\u001a\u0010\u0006\u001a\u00020\u0007X\u0084\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\b\"\u0004\b\t\u0010\n\u00a8\u0006\u001d"}, d2 = {"Lcom/eazy/daiku/utility/base/MCoroutineAsyncTask;", "Params", "Progress", "Result", "", "()V", "isCancelled", "", "()Z", "setCancelled", "(Z)V", "cancel", "", "myInterruptIfRunning", "doInBackground", "params", "", "([Ljava/lang/Object;)Ljava/lang/Object;", "execute", "([Ljava/lang/Object;)V", "onCanceled", "result", "(Ljava/lang/Object;)V", "onPostExecute", "onPreExecute", "onProgressUpdate", "progress", "publishProgress", "progresses", "app_taxiRelease"})
public abstract class MCoroutineAsyncTask<Params extends java.lang.Object, Progress extends java.lang.Object, Result extends java.lang.Object> {
    private boolean isCancelled = false;
    
    public MCoroutineAsyncTask() {
        super();
    }
    
    public abstract void onPreExecute();
    
    public abstract Result doInBackground(@org.jetbrains.annotations.NotNull()
    Params... params);
    
    public abstract void onProgressUpdate(@org.jetbrains.annotations.NotNull()
    Progress... progress);
    
    public abstract void onPostExecute(@org.jetbrains.annotations.Nullable()
    Result result);
    
    public abstract void onCanceled(@org.jetbrains.annotations.Nullable()
    Result result);
    
    protected final boolean isCancelled() {
        return false;
    }
    
    protected final void setCancelled(boolean p0) {
    }
    
    public final void publishProgress(@org.jetbrains.annotations.NotNull()
    Progress... progresses) {
    }
    
    public final void execute(@org.jetbrains.annotations.NotNull()
    Params... params) {
    }
    
    public final void cancel(boolean myInterruptIfRunning) {
    }
}