package com.eazy.daiku.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u00a2\u0006\u0002\u0010\nJ\u000b\u0010\u001b\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010\u001c\u001a\u0004\u0018\u00010\u0005H\u00c6\u0003J\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010\u001e\u001a\u0004\u0018\u00010\tH\u00c6\u0003J9\u0010\u001f\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\tH\u00c6\u0001J\u0013\u0010 \u001a\u00020!2\b\u0010\"\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010#\u001a\u00020$H\u00d6\u0001J\t\u0010%\u001a\u00020\u0003H\u00d6\u0001R\u001c\u0010\u0002\u001a\u0004\u0018\u00010\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001c\u0010\b\u001a\u0004\u0018\u00010\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0014\"\u0004\b\u0015\u0010\u0016R\u001c\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001a\u00a8\u0006&"}, d2 = {"Lcom/eazy/daiku/data/model/BookingTaxiModel;", "", "bookingCode", "", "customerInfo", "Lcom/eazy/daiku/data/model/server_model/CustomerInfo;", "destinationInfo", "Lcom/eazy/daiku/data/model/server_model/DestinationInfo;", "bookingParseObj", "Lcom/parse/ParseObject;", "(Ljava/lang/String;Lcom/eazy/daiku/data/model/server_model/CustomerInfo;Lcom/eazy/daiku/data/model/server_model/DestinationInfo;Lcom/parse/ParseObject;)V", "getBookingCode", "()Ljava/lang/String;", "setBookingCode", "(Ljava/lang/String;)V", "getBookingParseObj", "()Lcom/parse/ParseObject;", "setBookingParseObj", "(Lcom/parse/ParseObject;)V", "getCustomerInfo", "()Lcom/eazy/daiku/data/model/server_model/CustomerInfo;", "setCustomerInfo", "(Lcom/eazy/daiku/data/model/server_model/CustomerInfo;)V", "getDestinationInfo", "()Lcom/eazy/daiku/data/model/server_model/DestinationInfo;", "setDestinationInfo", "(Lcom/eazy/daiku/data/model/server_model/DestinationInfo;)V", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_taxiRelease"})
public final class BookingTaxiModel {
    @org.jetbrains.annotations.Nullable()
    private java.lang.String bookingCode;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.server_model.CustomerInfo customerInfo;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.server_model.DestinationInfo destinationInfo;
    @org.jetbrains.annotations.Nullable()
    private com.parse.ParseObject bookingParseObj;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.model.BookingTaxiModel copy(@org.jetbrains.annotations.Nullable()
    java.lang.String bookingCode, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.CustomerInfo customerInfo, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.DestinationInfo destinationInfo, @org.jetbrains.annotations.Nullable()
    com.parse.ParseObject bookingParseObj) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public BookingTaxiModel() {
        super();
    }
    
    public BookingTaxiModel(@org.jetbrains.annotations.Nullable()
    java.lang.String bookingCode, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.CustomerInfo customerInfo, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.DestinationInfo destinationInfo, @org.jetbrains.annotations.Nullable()
    com.parse.ParseObject bookingParseObj) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBookingCode() {
        return null;
    }
    
    public final void setBookingCode(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.CustomerInfo component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.CustomerInfo getCustomerInfo() {
        return null;
    }
    
    public final void setCustomerInfo(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.CustomerInfo p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.DestinationInfo component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.DestinationInfo getDestinationInfo() {
        return null;
    }
    
    public final void setDestinationInfo(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.DestinationInfo p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.parse.ParseObject component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.parse.ParseObject getBookingParseObj() {
        return null;
    }
    
    public final void setBookingParseObj(@org.jetbrains.annotations.Nullable()
    com.parse.ParseObject p0) {
    }
}