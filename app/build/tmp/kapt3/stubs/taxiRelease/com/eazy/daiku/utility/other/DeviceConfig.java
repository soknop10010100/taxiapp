package com.eazy.daiku.utility.other;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lcom/eazy/daiku/utility/other/DeviceConfig;", "", "()V", "Companion", "app_taxiRelease"})
public final class DeviceConfig {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.other.DeviceConfig.Companion Companion = null;
    
    public DeviceConfig() {
        super();
    }
    
    /**
     * @return The device's serial number, visible to the user in `Settings > About phone/tablet/device > Status
     * > Serial number`, or `null` if the serial number couldn't be found
     */
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u0004\u0018\u00010\u0004\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/utility/other/DeviceConfig$Companion;", "", "()V", "getSerialNumber", "", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getSerialNumber() {
            return null;
        }
    }
}