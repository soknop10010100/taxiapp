package com.eazy.daiku.utility.extension;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 2, d1 = {"\u0000\n\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0001\u001a\n\u0010\u0002\u001a\u00020\u0001*\u00020\u0001\u00a8\u0006\u0003"}, d2 = {"toDp", "", "toPx", "app_taxiRelease"})
public final class IntExtensionKt {
    
    public static final int toDp(int $this$toDp) {
        return 0;
    }
    
    public static final int toPx(int $this$toPx) {
        return 0;
    }
}