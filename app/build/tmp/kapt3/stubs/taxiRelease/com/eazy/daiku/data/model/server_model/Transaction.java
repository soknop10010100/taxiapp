package com.eazy.daiku.data.model.server_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u001a\b\u0016\u0018\u00002\u00020\u0001Be\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u00a2\u0006\u0002\u0010\rR \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R \u0010\t\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u000f\"\u0004\b\u0013\u0010\u0011R \u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0014\u0010\u000f\"\u0004\b\u0015\u0010\u0011R \u0010\u0005\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u000f\"\u0004\b\u0017\u0010\u0011R\"\u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001c\u001a\u0004\b\u0018\u0010\u0019\"\u0004\b\u001a\u0010\u001bR \u0010\f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u000f\"\u0004\b\u001e\u0010\u0011R \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R \u0010\b\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b#\u0010\u000f\"\u0004\b$\u0010\u0011\u00a8\u0006%"}, d2 = {"Lcom/eazy/daiku/data/model/server_model/Transaction;", "", "bookingCode", "", "kessTransactionId", "payBy", "total", "", "walletTransactionStatus", "createdAt", "userWithdraw", "Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;", "type", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Ljava/lang/String;Ljava/lang/String;Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;Ljava/lang/String;)V", "getBookingCode", "()Ljava/lang/String;", "setBookingCode", "(Ljava/lang/String;)V", "getCreatedAt", "setCreatedAt", "getKessTransactionId", "setKessTransactionId", "getPayBy", "setPayBy", "getTotal", "()Ljava/lang/Double;", "setTotal", "(Ljava/lang/Double;)V", "Ljava/lang/Double;", "getType", "setType", "getUserWithdraw", "()Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;", "setUserWithdraw", "(Lcom/eazy/daiku/data/model/server_model/WithdrawRespond;)V", "getWalletTransactionStatus", "setWalletTransactionStatus", "app_taxiRelease"})
public class Transaction {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "booking_code")
    private java.lang.String bookingCode;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "kess_transaction_id")
    private java.lang.String kessTransactionId;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "pay_by")
    private java.lang.String payBy;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "total")
    private java.lang.Double total;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "wallet_transaction_status")
    private java.lang.String walletTransactionStatus;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "created_at")
    private java.lang.String createdAt;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "user_withdraw")
    private com.eazy.daiku.data.model.server_model.WithdrawRespond userWithdraw;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "type")
    private java.lang.String type;
    
    public Transaction() {
        super();
    }
    
    public Transaction(@org.jetbrains.annotations.Nullable()
    java.lang.String bookingCode, @org.jetbrains.annotations.Nullable()
    java.lang.String kessTransactionId, @org.jetbrains.annotations.Nullable()
    java.lang.String payBy, @org.jetbrains.annotations.Nullable()
    java.lang.Double total, @org.jetbrains.annotations.Nullable()
    java.lang.String walletTransactionStatus, @org.jetbrains.annotations.Nullable()
    java.lang.String createdAt, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.WithdrawRespond userWithdraw, @org.jetbrains.annotations.Nullable()
    java.lang.String type) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getBookingCode() {
        return null;
    }
    
    public final void setBookingCode(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getKessTransactionId() {
        return null;
    }
    
    public final void setKessTransactionId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPayBy() {
        return null;
    }
    
    public final void setPayBy(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getTotal() {
        return null;
    }
    
    public final void setTotal(@org.jetbrains.annotations.Nullable()
    java.lang.Double p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getWalletTransactionStatus() {
        return null;
    }
    
    public final void setWalletTransactionStatus(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCreatedAt() {
        return null;
    }
    
    public final void setCreatedAt(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.WithdrawRespond getUserWithdraw() {
        return null;
    }
    
    public final void setUserWithdraw(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.WithdrawRespond p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getType() {
        return null;
    }
    
    public final void setType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
}