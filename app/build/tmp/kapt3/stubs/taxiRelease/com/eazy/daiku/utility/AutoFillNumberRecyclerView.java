package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\u0006\u0018\u00002\u00020\u0001:\u0004\u0019\u001a\u001b\u001cB\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0017\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007B\u001f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\u0006\u0010\u0016\u001a\u00020\u0017J\u0016\u0010\u0018\u001a\u00020\u00172\u000e\u0010\u000b\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\r0\fR\u0018\u0010\u000b\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\r\u0018\u00010\fX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001e\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\r0\u000fj\b\u0012\u0004\u0012\u00020\r`\u0010X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0011\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\u0013\"\u0004\b\u0014\u0010\u0015\u00a8\u0006\u001d"}, d2 = {"Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView;", "Landroidx/recyclerview/widget/RecyclerView;", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "attrs", "Landroid/util/AttributeSet;", "(Landroid/content/Context;Landroid/util/AttributeSet;)V", "defstyle", "", "(Landroid/content/Context;Landroid/util/AttributeSet;I)V", "baseListener", "Lcom/eazy/daiku/utility/base/BaseListenerAutoFillNumber;", "Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$Item;", "list", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "spanCount", "getSpanCount", "()I", "setSpanCount", "(I)V", "init", "", "setBaseListener", "Adapter", "GridSpacingItemDecoration", "Item", "ViewHolder", "app_taxiRelease"})
public final class AutoFillNumberRecyclerView extends androidx.recyclerview.widget.RecyclerView {
    private int spanCount = 4;
    private final java.util.ArrayList<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> list = null;
    private com.eazy.daiku.utility.base.BaseListenerAutoFillNumber<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> baseListener;
    
    public AutoFillNumberRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super(null);
    }
    
    public AutoFillNumberRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs) {
        super(null);
    }
    
    public AutoFillNumberRecyclerView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.util.AttributeSet attrs, int defstyle) {
        super(null);
    }
    
    public final int getSpanCount() {
        return 0;
    }
    
    public final void setSpanCount(int p0) {
    }
    
    public final void setBaseListener(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.base.BaseListenerAutoFillNumber<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> baseListener) {
    }
    
    public final void init() {
    }
    
    @android.annotation.SuppressLint(value = {"NotifyDataSetChanged"})
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010!\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\b\'\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001B\u001d\u0012\u0016\u0010\u0003\u001a\u0012\u0012\u0004\u0012\u00020\u00050\u0004j\b\u0012\u0004\u0012\u00020\u0005`\u0006\u00a2\u0006\u0002\u0010\u0007J\u0012\u0010\u000f\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0005\u0018\u00010\u0010H&J\b\u0010\u0011\u001a\u00020\nH\u0016J\u0018\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\u00022\u0006\u0010\u0015\u001a\u00020\nH\u0017J\u0018\u0010\u0016\u001a\u00020\u00022\u0006\u0010\u0017\u001a\u00020\u00182\u0006\u0010\u0019\u001a\u00020\nH\u0016J\u000e\u0010\u001a\u001a\u00020\u00132\u0006\u0010\u001b\u001a\u00020\nR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u001c"}, d2 = {"Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$ViewHolder;", "list", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$Item;", "Lkotlin/collections/ArrayList;", "(Ljava/util/ArrayList;)V", "", "rowIndex", "", "getRowIndex", "()I", "setRowIndex", "(I)V", "getBaseListener", "Lcom/eazy/daiku/utility/base/BaseListenerAutoFillNumber;", "getItemCount", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "selectPositionItem", "selectPosition", "app_taxiRelease"})
    public static abstract class Adapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.utility.AutoFillNumberRecyclerView.ViewHolder> {
        private final java.util.List<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> list = null;
        private int rowIndex = -1;
        
        public Adapter(@org.jetbrains.annotations.NotNull()
        java.util.ArrayList<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> list) {
            super();
        }
        
        public final int getRowIndex() {
            return 0;
        }
        
        public final void setRowIndex(int p0) {
        }
        
        @org.jetbrains.annotations.Nullable()
        public abstract com.eazy.daiku.utility.base.BaseListenerAutoFillNumber<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> getBaseListener();
        
        @org.jetbrains.annotations.NotNull()
        @java.lang.Override()
        public com.eazy.daiku.utility.AutoFillNumberRecyclerView.ViewHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.ViewGroup parent, int viewType) {
            return null;
        }
        
        public final void selectPositionItem(int selectPosition) {
        }
        
        @android.annotation.SuppressLint(value = {"ResourceAsColor"})
        @java.lang.Override()
        public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.AutoFillNumberRecyclerView.ViewHolder holder, int position) {
        }
        
        @java.lang.Override()
        public int getItemCount() {
            return 0;
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u0010J\u000e\u0010\u0011\u001a\u00020\u000e2\u0006\u0010\u0011\u001a\u00020\u0012R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\f\u00a8\u0006\u0013"}, d2 = {"Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$ViewHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "autoContainerNumber", "Lcom/google/android/material/card/MaterialCardView;", "getAutoContainerNumber", "()Lcom/google/android/material/card/MaterialCardView;", "autoFillTextView", "Landroid/widget/TextView;", "getAutoFillTextView", "()Landroid/widget/TextView;", "onBind", "", "item", "Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$Item;", "selectItem", "", "app_taxiRelease"})
    public static final class ViewHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView autoFillTextView = null;
        @org.jetbrains.annotations.NotNull()
        private final com.google.android.material.card.MaterialCardView autoContainerNumber = null;
        
        public ViewHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getAutoFillTextView() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.material.card.MaterialCardView getAutoContainerNumber() {
            return null;
        }
        
        public final void onBind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item item) {
        }
        
        public final void selectItem(boolean selectItem) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\n\u0018\u00002\u00020\u0001B\u0015\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u0004\u001a\u00020\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u000f"}, d2 = {"Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$Item;", "", "number", "", "numberStr", "", "(ILjava/lang/String;)V", "getNumber", "()I", "setNumber", "(I)V", "getNumberStr", "()Ljava/lang/String;", "setNumberStr", "(Ljava/lang/String;)V", "app_taxiRelease"})
    public static final class Item {
        private int number;
        @org.jetbrains.annotations.NotNull()
        private java.lang.String numberStr;
        
        public Item(int number, @org.jetbrains.annotations.NotNull()
        java.lang.String numberStr) {
            super();
        }
        
        public final int getNumber() {
            return 0;
        }
        
        public final void setNumber(int p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getNumberStr() {
            return null;
        }
        
        public final void setNumberStr(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00008\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0003\u0012\u0006\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\u0002\u0010\u0007J(\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u000e\u001a\u00020\u000f2\u0006\u0010\u0010\u001a\u00020\u0011H\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0004\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0002\u001a\u00020\u0003X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$GridSpacingItemDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "spanCount", "", "spacing", "includeEdge", "", "(IIZ)V", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "app_taxiRelease"})
    public static final class GridSpacingItemDecoration extends androidx.recyclerview.widget.RecyclerView.ItemDecoration {
        private final int spanCount = 0;
        private final int spacing = 0;
        private final boolean includeEdge = false;
        
        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            super();
        }
        
        @java.lang.Override()
        public void getItemOffsets(@org.jetbrains.annotations.NotNull()
        android.graphics.Rect outRect, @org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView parent, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.State state) {
        }
    }
}