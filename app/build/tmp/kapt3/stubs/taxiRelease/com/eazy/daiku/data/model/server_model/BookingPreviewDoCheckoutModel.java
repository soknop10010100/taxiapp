package com.eazy.daiku.data.model.server_model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0006\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b#\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u00a5\u0001\u0012\u0018\b\u0002\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u0012\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u0007\u0012\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u0007\u0012\u001c\b\u0002\u0010\n\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u0005\u0012\u001c\b\u0002\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u0005\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000b\u0012\u0018\b\u0002\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u0003j\b\u0012\u0004\u0012\u00020\u000f`\u0005\u00a2\u0006\u0002\u0010\u0010J\u0019\u0010(\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005H\u00c6\u0003J\u000b\u0010)\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010*\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u000b\u0010+\u001a\u0004\u0018\u00010\u0007H\u00c6\u0003J\u001d\u0010,\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u0005H\u00c6\u0003J\u001d\u0010-\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u0005H\u00c6\u0003J\u0010\u0010.\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003\u00a2\u0006\u0002\u0010\u001cJ\u0019\u0010/\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u0003j\b\u0012\u0004\u0012\u00020\u000f`\u0005H\u00c6\u0003J\u00ae\u0001\u00100\u001a\u00020\u00002\u0018\b\u0002\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\n\b\u0002\u0010\u0006\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\u00072\n\b\u0002\u0010\t\u001a\u0004\u0018\u00010\u00072\u001c\b\u0002\u0010\n\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u00052\u001c\b\u0002\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u00052\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u000b2\u0018\b\u0002\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u0003j\b\u0012\u0004\u0012\u00020\u000f`\u0005H\u00c6\u0001\u00a2\u0006\u0002\u00101J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00105\u001a\u000206H\u00d6\u0001J\t\u00107\u001a\u00020\u0007H\u00d6\u0001R.\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R2\u0010\n\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0012\"\u0004\b\u0016\u0010\u0014R \u0010\u0006\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0017\u0010\u0018\"\u0004\b\u0019\u0010\u001aR\"\u0010\r\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010\u001f\u001a\u0004\b\u001b\u0010\u001c\"\u0004\b\u001d\u0010\u001eR \u0010\t\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u0018\"\u0004\b!\u0010\u001aR2\u0010\f\u001a\u0016\u0012\u0004\u0012\u00020\u000b\u0018\u00010\u0003j\n\u0012\u0004\u0012\u00020\u000b\u0018\u0001`\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010\u0012\"\u0004\b#\u0010\u0014R \u0010\b\u001a\u0004\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0018\"\u0004\b%\u0010\u001aR.\u0010\u000e\u001a\u0012\u0012\u0004\u0012\u00020\u000f0\u0003j\b\u0012\u0004\u0012\u00020\u000f`\u00058\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\u0012\"\u0004\b\'\u0010\u0014\u00a8\u00068"}, d2 = {"Lcom/eazy/daiku/data/model/server_model/BookingPreviewDoCheckoutModel;", "", "drivers", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/Drivers;", "Lkotlin/collections/ArrayList;", "fromTitle", "", "toTitle", "qrCodeUrl", "fromLocation", "", "toLocation", "kilometre", "vehicle", "Lcom/eazy/daiku/data/model/server_model/Vehicle;", "(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Double;Ljava/util/ArrayList;)V", "getDrivers", "()Ljava/util/ArrayList;", "setDrivers", "(Ljava/util/ArrayList;)V", "getFromLocation", "setFromLocation", "getFromTitle", "()Ljava/lang/String;", "setFromTitle", "(Ljava/lang/String;)V", "getKilometre", "()Ljava/lang/Double;", "setKilometre", "(Ljava/lang/Double;)V", "Ljava/lang/Double;", "getQrCodeUrl", "setQrCodeUrl", "getToLocation", "setToLocation", "getToTitle", "setToTitle", "getVehicle", "setVehicle", "component1", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "copy", "(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/Double;Ljava/util/ArrayList;)Lcom/eazy/daiku/data/model/server_model/BookingPreviewDoCheckoutModel;", "equals", "", "other", "hashCode", "", "toString", "app_taxiRelease"})
public final class BookingPreviewDoCheckoutModel {
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "drivers")
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> drivers;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "from_title")
    private java.lang.String fromTitle;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "to_title")
    private java.lang.String toTitle;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "qr_code_url")
    private java.lang.String qrCodeUrl;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "from_location")
    private java.util.ArrayList<java.lang.Double> fromLocation;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "to_location")
    private java.util.ArrayList<java.lang.Double> toLocation;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "kilometre")
    private java.lang.Double kilometre;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "vehicle")
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> vehicle;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.data.model.server_model.BookingPreviewDoCheckoutModel copy(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> drivers, @org.jetbrains.annotations.Nullable()
    java.lang.String fromTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String toTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String qrCodeUrl, @org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Double> fromLocation, @org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Double> toLocation, @org.jetbrains.annotations.Nullable()
    java.lang.Double kilometre, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> vehicle) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public BookingPreviewDoCheckoutModel() {
        super();
    }
    
    public BookingPreviewDoCheckoutModel(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> drivers, @org.jetbrains.annotations.Nullable()
    java.lang.String fromTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String toTitle, @org.jetbrains.annotations.Nullable()
    java.lang.String qrCodeUrl, @org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Double> fromLocation, @org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Double> toLocation, @org.jetbrains.annotations.Nullable()
    java.lang.Double kilometre, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> vehicle) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> getDrivers() {
        return null;
    }
    
    public final void setDrivers(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Drivers> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getFromTitle() {
        return null;
    }
    
    public final void setFromTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getToTitle() {
        return null;
    }
    
    public final void setToTitle(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getQrCodeUrl() {
        return null;
    }
    
    public final void setQrCodeUrl(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<java.lang.Double> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<java.lang.Double> getFromLocation() {
        return null;
    }
    
    public final void setFromLocation(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<java.lang.Double> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.ArrayList<java.lang.Double> getToLocation() {
        return null;
    }
    
    public final void setToLocation(@org.jetbrains.annotations.Nullable()
    java.util.ArrayList<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Double getKilometre() {
        return null;
    }
    
    public final void setKilometre(@org.jetbrains.annotations.Nullable()
    java.lang.Double p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> getVehicle() {
        return null;
    }
    
    public final void setVehicle(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.Vehicle> p0) {
    }
}