package com.eazy.daiku.ui.history;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0015H\u0002J\b\u0010\u0017\u001a\u00020\u0015H\u0002J\b\u0010\u0018\u001a\u00020\u0015H\u0002J\u0012\u0010\u0019\u001a\u00020\u00152\b\u0010\u001a\u001a\u0004\u0018\u00010\u001bH\u0014J\u0010\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001fH\u0016R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u000e\u0010\r\u001a\u00020\u000eX\u0082.\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000f\u001a\u00020\u00108BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0013\u0010\f\u001a\u0004\b\u0011\u0010\u0012\u00a8\u0006 "}, d2 = {"Lcom/eazy/daiku/ui/history/HistoryTripActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityHistoryTripBinding;", "customerAdapter", "Lcom/eazy/daiku/utility/pagin/customer_history/CustomerHistoryPaginAdapter;", "customerHistoryVm", "Lcom/eazy/daiku/utility/view_model/CustomerHistoryVm;", "getCustomerHistoryVm", "()Lcom/eazy/daiku/utility/view_model/CustomerHistoryVm;", "customerHistoryVm$delegate", "Lkotlin/Lazy;", "historyAdapter", "Lcom/eazy/daiku/utility/pagin/history/HistoryPaginAdapter;", "historyVm", "Lcom/eazy/daiku/utility/view_model/HistoryVm;", "getHistoryVm", "()Lcom/eazy/daiku/utility/view_model/HistoryVm;", "historyVm$delegate", "doAction", "", "initData", "initObserved", "initView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onOptionsItemSelected", "", "item", "Landroid/view/MenuItem;", "app_taxiRelease"})
public final class HistoryTripActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityHistoryTripBinding binding;
    private com.eazy.daiku.utility.pagin.history.HistoryPaginAdapter historyAdapter;
    private com.eazy.daiku.utility.pagin.customer_history.CustomerHistoryPaginAdapter customerAdapter;
    private final kotlin.Lazy historyVm$delegate = null;
    private final kotlin.Lazy customerHistoryVm$delegate = null;
    
    public HistoryTripActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.HistoryVm getHistoryVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.CustomerHistoryVm getCustomerHistoryVm() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    public boolean onOptionsItemSelected(@org.jetbrains.annotations.NotNull()
    android.view.MenuItem item) {
        return false;
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    private final void initData() {
    }
    
    private final void initView() {
    }
}