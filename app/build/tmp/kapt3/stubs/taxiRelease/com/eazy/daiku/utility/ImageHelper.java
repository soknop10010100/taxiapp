package com.eazy.daiku.utility;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000P\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0012\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bJ\u000e\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\u0004J\u000e\u0010\f\u001a\u00020\u00042\u0006\u0010\r\u001a\u00020\nJ \u0010\u000e\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u000b\u001a\u00020\u00042\u0006\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\u0013J\u0010\u0010\u0014\u001a\u0004\u0018\u00010\u000f2\u0006\u0010\u0015\u001a\u00020\u0004J\u0018\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u000b\u001a\u00020\u0004H\u0002J\u000e\u0010\u0018\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0019\u001a\u00020\u00132\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\u001a\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u0001J\u001e\u0010\u001a\u001a\u00020\u001c2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u00012\u0006\u0010\u001d\u001a\u00020\u001eJ&\u0010\u001a\u001a\u00020\u001c2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u00012\u0006\u0010\u001f\u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u001eJ.\u0010\u001a\u001a\u00020\u001c2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u001b\u001a\u00020\u00012\u0006\u0010\u001f\u001a\u00020\u00132\u0006\u0010 \u001a\u00020\u00132\u0006\u0010\u001d\u001a\u00020\u001eJ\u0018\u0010!\u001a\u0004\u0018\u00010\u00172\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b\u00a8\u0006\""}, d2 = {"Lcom/eazy/daiku/utility/ImageHelper;", "", "()V", "bitmapFromView", "Landroid/graphics/Bitmap;", "context", "Landroid/content/Context;", "view", "Landroid/view/View;", "bitmapToByteArray", "", "bitmap", "byteArrayToBitmap", "byteArray", "convertBase64", "", "compressFile", "Landroid/graphics/Bitmap$CompressFormat;", "quality", "", "detectQrImageToText", "bMap", "fileFromBitmap", "Ljava/io/File;", "getHeightScreen", "getWidthScreen", "loadImage", "resource", "", "imageView", "Landroid/widget/ImageView;", "placeHolder", "errorResource", "uiToFile", "app_taxiRelease"})
public final class ImageHelper {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.ImageHelper INSTANCE = null;
    
    private ImageHelper() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String detectQrImageToText(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bMap) {
        return null;
    }
    
    public final int getWidthScreen(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return 0;
    }
    
    public final int getHeightScreen(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return 0;
    }
    
    /**
     * param
     * Bitmap.CompressFormat.JPEG -> jpeg
     * Bitmap.CompressFormat.PNG -> png
     */
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String convertBase64(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bitmap, @org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap.CompressFormat compressFile, int quality) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final byte[] bitmapToByteArray(@org.jetbrains.annotations.NotNull()
    android.graphics.Bitmap bitmap) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.graphics.Bitmap byteArrayToBitmap(@org.jetbrains.annotations.NotNull()
    byte[] byteArray) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.io.File uiToFile(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.view.View view) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.graphics.Bitmap bitmapFromView(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.view.View view) {
        return null;
    }
    
    private final java.io.File fileFromBitmap(android.content.Context context, android.graphics.Bitmap bitmap) {
        return null;
    }
    
    public final void loadImage(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.Object resource, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView) {
    }
    
    public final void loadImage(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.Object resource, int placeHolder, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView) {
    }
    
    public final void loadImage(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.Object resource, int placeHolder, int errorResource, @org.jetbrains.annotations.NotNull()
    android.widget.ImageView imageView) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.graphics.Bitmap loadImage(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.Object resource) {
        return null;
    }
}