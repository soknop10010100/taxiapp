package com.eazy.daiku.utility.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\t\u001a\u00020\nH\u0016J\u001a\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\b\b\u0002\u0010\u000f\u001a\u00020\u0010H\u0005J\u0019\u0010\u0011\u001a\u0002H\u0012\"\n\b\u0000\u0010\u0012*\u0004\u0018\u00010\u0000H\u0004\u00a2\u0006\u0002\u0010\u0013R\u001e\u0010\u0003\u001a\u00020\u00048\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\u0014"}, d2 = {"Lcom/eazy/daiku/utility/base/BaseFragment;", "Ldagger/android/support/DaggerFragment;", "()V", "factory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "getFactory", "()Landroidx/lifecycle/ViewModelProvider$Factory;", "setFactory", "(Landroidx/lifecycle/ViewModelProvider$Factory;)V", "getUserUserToSharePreference", "Lcom/eazy/daiku/data/model/server_model/User;", "hideShowActionBar", "", "fContext", "Landroidx/fragment/app/FragmentActivity;", "showActionBar", "", "self", "T", "()Lcom/eazy/daiku/utility/base/BaseFragment;", "app_taxiRelease"})
public class BaseFragment extends dagger.android.support.DaggerFragment {
    @javax.inject.Inject()
    public androidx.lifecycle.ViewModelProvider.Factory factory;
    
    public BaseFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.ViewModelProvider.Factory getFactory() {
        return null;
    }
    
    public final void setFactory(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory p0) {
    }
    
    protected final <T extends com.eazy.daiku.utility.base.BaseFragment>T self() {
        return null;
    }
    
    @android.annotation.SuppressLint(value = {"RestrictedApi"})
    protected final void hideShowActionBar(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity fContext, boolean showActionBar) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public com.eazy.daiku.data.model.server_model.User getUserUserToSharePreference() {
        return null;
    }
}