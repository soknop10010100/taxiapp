package com.eazy.daiku.utility.custom;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000e\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\"\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\nJN\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014JF\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J4\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0012\u001a\u00020\u000e2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014J,\u0010\u000b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014JR\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014JJ\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014JT\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J8\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J6\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0019\u001a\u00020\n2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014JB\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014JL\u0010\u000b\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0011\u001a\u0004\u0018\u00010\b2\b\u0010\u0016\u001a\u0004\u0018\u00010\b2\b\u0010\u0018\u001a\u0004\u0018\u00010\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J\"\u0010\u001a\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ,\u0010\u001a\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014J,\u0010\u001b\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dJ$\u0010\u001e\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ\u001e\u0010\u001f\u001a\u00020\f2\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010 \u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\bJ$\u0010!\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\bJ.\u0010!\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014J8\u0010!\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u00142\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dJL\u0010!\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\"\u001a\u0004\u0018\u00010\b2\b\u0010#\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u00142\b\u0010\u0017\u001a\u0004\u0018\u00010\u0014J.\u0010$\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u001c\u001a\u0004\u0018\u00010\u001dJ@\u0010%\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0017\u001a\u0004\u0018\u00010\u00142\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014JF\u0010%\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u0006\u0010\r\u001a\u00020\u000e2\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\u0019\u001a\u00020\n2\u0006\u0010&\u001a\u00020\n2\b\u0010\u0013\u001a\u0004\u0018\u00010\u0014J.\u0010\'\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014J8\u0010\'\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010(\u001a\u0004\u0018\u00010\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014J.\u0010\'\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010)\u001a\u0004\u0018\u00010\bJ8\u0010\'\u001a\u00020\f2\b\u0010\u0005\u001a\u0004\u0018\u00010\u00062\b\u0010\u0010\u001a\u0004\u0018\u00010\b2\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\b\u0010)\u001a\u0004\u0018\u00010\b2\b\u0010\u0015\u001a\u0004\u0018\u00010\u0014J\"\u0010*\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\b\u0010\u0007\u001a\u0004\u0018\u00010\b2\u0006\u0010\t\u001a\u00020\n\u00a8\u0006+"}, d2 = {"Lcom/eazy/daiku/utility/custom/MessageUtils;", "", "()V", "loadingAlertDialog", "Lcn/pedant/SweetAlert/SweetAlertDialog;", "context", "Landroid/content/Context;", "text", "", "isShow", "", "showConfirm", "", "newLogo", "", "color", "title", "cancelStr", "gender", "onConfirm", "Lcn/pedant/SweetAlert/SweetAlertDialog$OnSweetClickListener;", "onSweetClickListener", "confirmStr", "onCancelListener", "onCancel", "showCancelButton", "showError", "showErrorDismiss", "onDismissListener", "Landroid/content/DialogInterface$OnDismissListener;", "showNormal", "showQrCodeDestination", "qrCodeUrl", "showSuccess", "confirm", "cancel", "showSuccessDismiss", "showWarnigIcon", "canceledOnTouchOutside", "showWarning", "onCancelClickListener", "confirmText", "sweetAlertDialog", "app_taxiRelease"})
public final class MessageUtils {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.custom.MessageUtils INSTANCE = null;
    
    private MessageUtils() {
        super();
    }
    
    public final void showError(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text) {
    }
    
    public final void showError(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showErrorDismiss(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    android.content.DialogInterface.OnDismissListener onDismissListener) {
    }
    
    public final void showWarning(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmText) {
    }
    
    public final void showWarning(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmText, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showWarning(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmStr, boolean showCancelButton, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showWarning(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onCancelClickListener, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showSuccess(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text) {
    }
    
    public final void showSuccess(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showNormal(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text) {
    }
    
    public final void showSuccess(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener, @org.jetbrains.annotations.Nullable()
    android.content.DialogInterface.OnDismissListener onDismissListener) {
    }
    
    public final void showSuccess(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String confirm, @org.jetbrains.annotations.Nullable()
    java.lang.String cancel, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onCancelListener) {
    }
    
    public final void showSuccessDismiss(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    android.content.DialogInterface.OnDismissListener onDismissListener) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, int gender, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmStr, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmStr, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onCancel, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onCancel, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    /**
     * show new icon alert
     */
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, int newLogo, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmStr, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, int newLogo, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmStr, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onCancelListener, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    public final void showWarnigIcon(@org.jetbrains.annotations.Nullable()
    android.content.Context context, int newLogo, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onCancelListener, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    public final void showWarnigIcon(@org.jetbrains.annotations.Nullable()
    android.content.Context context, int newLogo, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, boolean showCancelButton, boolean canceledOnTouchOutside, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    /**
     * show new icon alert with new color
     */
    public final void showConfirm(@org.jetbrains.annotations.Nullable()
    android.content.Context context, int newLogo, int color, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, @org.jetbrains.annotations.Nullable()
    java.lang.String confirmStr, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    /**
     * show new icon alert with new color with change confirm yes boy or girl
     */
    public final void showConfirm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int newLogo, int color, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, int gender, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    /**
     * show new icon alert without color with change confirm yes boy or girl
     */
    public final void showConfirm(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int newLogo, @org.jetbrains.annotations.Nullable()
    java.lang.String title, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String cancelStr, int gender, @org.jetbrains.annotations.Nullable()
    cn.pedant.SweetAlert.SweetAlertDialog.OnSweetClickListener onConfirm) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final cn.pedant.SweetAlert.SweetAlertDialog sweetAlertDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String text, boolean isShow) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final cn.pedant.SweetAlert.SweetAlertDialog loadingAlertDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String text, boolean isShow) {
        return null;
    }
    
    public final void showQrCodeDestination(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String qrCodeUrl, @org.jetbrains.annotations.NotNull()
    java.lang.String title) {
    }
}