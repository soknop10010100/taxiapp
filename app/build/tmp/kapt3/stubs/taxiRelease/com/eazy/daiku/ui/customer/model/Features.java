package com.eazy.daiku.ui.customer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0010\u0006\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b<\n\u0002\u0010\u000b\n\u0002\b\u0004\b\u0086\b\u0018\u00002\u00020\u0001B\u00f1\u0001\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u0003\u0012\u0018\b\u0002\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0006j\b\u0012\u0004\u0012\u00020\u0003`\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u0012\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0003\u0012\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u0003\u0012\u0018\b\u0002\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u0007\u0012\u0018\b\u0002\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u0007\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0016\u0012\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00180\u0006j\b\u0012\u0004\u0012\u00020\u0018`\u0007\u00a2\u0006\u0002\u0010\u0019J\u000b\u0010C\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010D\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010E\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0019\u0010F\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u0007H\u00c6\u0003J\u0019\u0010G\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u0007H\u00c6\u0003J\u000b\u0010H\u001a\u0004\u0018\u00010\u0016H\u00c6\u0003J\u0019\u0010I\u001a\u0012\u0012\u0004\u0012\u00020\u00180\u0006j\b\u0012\u0004\u0012\u00020\u0018`\u0007H\u00c6\u0003J\u000b\u0010J\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0019\u0010K\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0006j\b\u0012\u0004\u0012\u00020\u0003`\u0007H\u00c6\u0003J\u0010\u0010L\u001a\u0004\u0018\u00010\tH\u00c6\u0003\u00a2\u0006\u0002\u00109J\u000b\u0010M\u001a\u0004\u0018\u00010\u000bH\u00c6\u0003J\u000b\u0010N\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010O\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010P\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u000b\u0010Q\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u00fa\u0001\u0010R\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0004\u001a\u0004\u0018\u00010\u00032\u0018\b\u0002\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0006j\b\u0012\u0004\u0012\u00020\u0003`\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b2\n\b\u0002\u0010\f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u000f\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00032\n\b\u0002\u0010\u0011\u001a\u0004\u0018\u00010\u00032\u0018\b\u0002\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u00072\u0018\b\u0002\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u00072\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00162\u0018\b\u0002\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00180\u0006j\b\u0012\u0004\u0012\u00020\u0018`\u0007H\u00c6\u0001\u00a2\u0006\u0002\u0010SJ\u0013\u0010T\u001a\u00020U2\b\u0010V\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010W\u001a\u00020\tH\u00d6\u0001J\t\u0010X\u001a\u00020\u0003H\u00d6\u0001R.\u0010\u0012\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\u001b\"\u0004\b\u001c\u0010\u001dR.\u0010\u0014\u001a\u0012\u0012\u0004\u0012\u00020\u00130\u0006j\b\u0012\u0004\u0012\u00020\u0013`\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001b\"\u0004\b\u001f\u0010\u001dR.\u0010\u0017\u001a\u0012\u0012\u0004\u0012\u00020\u00180\u0006j\b\u0012\u0004\u0012\u00020\u0018`\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b \u0010\u001b\"\u0004\b!\u0010\u001dR \u0010\u0015\u001a\u0004\u0018\u00010\u00168\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\"\u0010#\"\u0004\b$\u0010%R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b&\u0010\'\"\u0004\b(\u0010)R \u0010\u0010\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b*\u0010\'\"\u0004\b+\u0010)R \u0010\r\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b,\u0010\'\"\u0004\b-\u0010)R \u0010\u0011\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b.\u0010\'\"\u0004\b/\u0010)R \u0010\u000e\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\'\"\u0004\b1\u0010)R.\u0010\u0005\u001a\u0012\u0012\u0004\u0012\u00020\u00030\u0006j\b\u0012\u0004\u0012\u00020\u0003`\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b2\u0010\u001b\"\u0004\b3\u0010\u001dR \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\"\u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0010\n\u0002\u0010<\u001a\u0004\b8\u00109\"\u0004\b:\u0010;R \u0010\u000f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b=\u0010\'\"\u0004\b>\u0010)R \u0010\f\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b?\u0010\'\"\u0004\b@\u0010)R \u0010\u0004\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\bA\u0010\'\"\u0004\bB\u0010)\u00a8\u0006Y"}, d2 = {"Lcom/eazy/daiku/ui/customer/model/Features;", "", "id", "", "type", "placeType", "Ljava/util/ArrayList;", "Lkotlin/collections/ArrayList;", "relevance", "", "properties", "Lcom/eazy/daiku/ui/customer/model/Properties;", "textEn", "languageEn", "placeNameEn", "text", "language", "placeName", "bbox", "", "center", "geometry", "Lcom/eazy/daiku/ui/customer/model/Geometry;", "context", "Lcom/eazy/daiku/ui/customer/model/Context;", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;Lcom/eazy/daiku/ui/customer/model/Properties;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/eazy/daiku/ui/customer/model/Geometry;Ljava/util/ArrayList;)V", "getBbox", "()Ljava/util/ArrayList;", "setBbox", "(Ljava/util/ArrayList;)V", "getCenter", "setCenter", "getContext", "setContext", "getGeometry", "()Lcom/eazy/daiku/ui/customer/model/Geometry;", "setGeometry", "(Lcom/eazy/daiku/ui/customer/model/Geometry;)V", "getId", "()Ljava/lang/String;", "setId", "(Ljava/lang/String;)V", "getLanguage", "setLanguage", "getLanguageEn", "setLanguageEn", "getPlaceName", "setPlaceName", "getPlaceNameEn", "setPlaceNameEn", "getPlaceType", "setPlaceType", "getProperties", "()Lcom/eazy/daiku/ui/customer/model/Properties;", "setProperties", "(Lcom/eazy/daiku/ui/customer/model/Properties;)V", "getRelevance", "()Ljava/lang/Integer;", "setRelevance", "(Ljava/lang/Integer;)V", "Ljava/lang/Integer;", "getText", "setText", "getTextEn", "setTextEn", "getType", "setType", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;Lcom/eazy/daiku/ui/customer/model/Properties;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/eazy/daiku/ui/customer/model/Geometry;Ljava/util/ArrayList;)Lcom/eazy/daiku/ui/customer/model/Features;", "equals", "", "other", "hashCode", "toString", "app_taxiRelease"})
public final class Features {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "id")
    private java.lang.String id;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "type")
    private java.lang.String type;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "place_type")
    private java.util.ArrayList<java.lang.String> placeType;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "relevance")
    private java.lang.Integer relevance;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "properties")
    private com.eazy.daiku.ui.customer.model.Properties properties;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "text_en")
    private java.lang.String textEn;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "language_en")
    private java.lang.String languageEn;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "place_name_en")
    private java.lang.String placeNameEn;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "text")
    private java.lang.String text;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "language")
    private java.lang.String language;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "place_name")
    private java.lang.String placeName;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "bbox")
    private java.util.ArrayList<java.lang.Double> bbox;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "center")
    private java.util.ArrayList<java.lang.Double> center;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "geometry")
    private com.eazy.daiku.ui.customer.model.Geometry geometry;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "context")
    private java.util.ArrayList<com.eazy.daiku.ui.customer.model.Context> context;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.ui.customer.model.Features copy(@org.jetbrains.annotations.Nullable()
    java.lang.String id, @org.jetbrains.annotations.Nullable()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> placeType, @org.jetbrains.annotations.Nullable()
    java.lang.Integer relevance, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Properties properties, @org.jetbrains.annotations.Nullable()
    java.lang.String textEn, @org.jetbrains.annotations.Nullable()
    java.lang.String languageEn, @org.jetbrains.annotations.Nullable()
    java.lang.String placeNameEn, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String language, @org.jetbrains.annotations.Nullable()
    java.lang.String placeName, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Double> bbox, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Double> center, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Geometry geometry, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.Context> context) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Features() {
        super();
    }
    
    public Features(@org.jetbrains.annotations.Nullable()
    java.lang.String id, @org.jetbrains.annotations.Nullable()
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> placeType, @org.jetbrains.annotations.Nullable()
    java.lang.Integer relevance, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Properties properties, @org.jetbrains.annotations.Nullable()
    java.lang.String textEn, @org.jetbrains.annotations.Nullable()
    java.lang.String languageEn, @org.jetbrains.annotations.Nullable()
    java.lang.String placeNameEn, @org.jetbrains.annotations.Nullable()
    java.lang.String text, @org.jetbrains.annotations.Nullable()
    java.lang.String language, @org.jetbrains.annotations.Nullable()
    java.lang.String placeName, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Double> bbox, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Double> center, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Geometry geometry, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.Context> context) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getId() {
        return null;
    }
    
    public final void setId(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getType() {
        return null;
    }
    
    public final void setType(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getPlaceType() {
        return null;
    }
    
    public final void setPlaceType(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getRelevance() {
        return null;
    }
    
    public final void setRelevance(@org.jetbrains.annotations.Nullable()
    java.lang.Integer p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.Properties component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.Properties getProperties() {
        return null;
    }
    
    public final void setProperties(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Properties p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component6() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getTextEn() {
        return null;
    }
    
    public final void setTextEn(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLanguageEn() {
        return null;
    }
    
    public final void setLanguageEn(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPlaceNameEn() {
        return null;
    }
    
    public final void setPlaceNameEn(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getText() {
        return null;
    }
    
    public final void setText(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component10() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLanguage() {
        return null;
    }
    
    public final void setLanguage(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getPlaceName() {
        return null;
    }
    
    public final void setPlaceName(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Double> component12() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Double> getBbox() {
        return null;
    }
    
    public final void setBbox(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Double> component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.Double> getCenter() {
        return null;
    }
    
    public final void setCenter(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.Double> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.Geometry component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.Geometry getGeometry() {
        return null;
    }
    
    public final void setGeometry(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Geometry p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.ui.customer.model.Context> component15() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<com.eazy.daiku.ui.customer.model.Context> getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.ui.customer.model.Context> p0) {
    }
}