package com.eazy.daiku.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u00b4\u0001\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J=\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ!\u0010\n\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00032\b\b\u0001\u0010\f\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJC\u0010\u000e\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00100\u000f0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJC\u0010\u0011\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00120\u000f0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ!\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00032\b\b\u0001\u0010\u0014\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ=\u0010\u0015\u001a\b\u0012\u0004\u0012\u00020\u00160\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0017\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u000b0\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J1\u0010\u0019\u001a\u0018\u0012\u0014\u0012\u0012\u0012\u0004\u0012\u00020\u001b0\u001aj\b\u0012\u0004\u0012\u00020\u001b`\u001c0\u00032\b\b\u0001\u0010\u0014\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ=\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0017\u0010\u001f\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J=\u0010 \u001a\b\u0012\u0004\u0012\u00020!0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ!\u0010\"\u001a\b\u0012\u0004\u0012\u00020\u000b0\u00032\b\b\u0001\u0010\f\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ=\u0010#\u001a\b\u0012\u0004\u0012\u00020$0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0017\u0010%\u001a\b\u0012\u0004\u0012\u00020&0\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J!\u0010\'\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032\b\b\u0001\u0010\f\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ=\u0010(\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010)\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010*\u001a\b\u0012\u0004\u0012\u00020+0\u00032$\b\u0001\u0010,\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010-\u001a\b\u0012\u0004\u0012\u00020.0\u00032$\b\u0001\u0010,\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJC\u00100\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002010\u000f0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u00102\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u001d\u00103\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002040\u000f0\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J\'\u00105\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u0002060\u000f0\u00032\b\b\u0001\u00107\u001a\u000208H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u00109J=\u0010:\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010;\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0017\u0010<\u001a\b\u0012\u0004\u0012\u0002060\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J\u001b\u0010=\u001a\u00020>2\b\b\u0001\u0010?\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u001b\u0010@\u001a\u00020>2\b\b\u0001\u0010?\u001a\u00020\u0007H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ=\u0010A\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010B\u001a\b\u0012\u0004\u0012\u00020C0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010D\u001a\b\u0012\u0004\u0012\u00020!0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010E\u001a\b\u0012\u0004\u0012\u00020!0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ\u0017\u0010F\u001a\b\u0012\u0004\u0012\u00020!0\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0018J=\u0010G\u001a\b\u0012\u0004\u0012\u00020C0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\tJ=\u0010H\u001a\b\u0012\u0004\u0012\u00020I0\u00032$\b\u0001\u0010\u0005\u001a\u001e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u00010\u0006j\u000e\u0012\u0004\u0012\u00020\u0007\u0012\u0004\u0012\u00020\u0001`\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\t\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006J"}, d2 = {"Lcom/eazy/daiku/data/remote/EazyTaxiApi;", "", "createUser", "Lcom/eazy/daiku/data/model/base/ApiResWraper;", "Lcom/eazy/daiku/data/model/server_model/CreateUserRespond;", "map", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "(Ljava/util/HashMap;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "endTrip", "Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "code", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchCommunityTaxi", "", "Lcom/eazy/daiku/data/model/server_model/CommunityTaxiRespond;", "fetchHistory", "Lcom/eazy/daiku/data/model/server_model/History;", "fetchQrCode", "type", "fetchTransaction", "Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "fetchTripProcessing", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "fetchVehicleType", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "Lkotlin/collections/ArrayList;", "login", "Lcom/google/gson/JsonElement;", "logout", "reUploadKycDoc", "Lcom/eazy/daiku/data/model/server_model/User;", "startTrip", "submitBookingPreviewDoCheckout", "Lcom/eazy/daiku/data/model/server_model/BookingPreviewDoCheckoutModel;", "submitBookingProcessing", "Lcom/example/example/BookingProcessingModel;", "submitCancelBookingTaxi", "submitChangePasswordByOtp", "submitChangePwd", "submitCheckout", "Lcom/eazy/daiku/ui/customer/model/WebPayRespondModel;", "body", "submitCheckoutTaxi", "Lcom/eazy/daiku/ui/customer/model/PreviewCheckoutModel;", "submitCreateUserCustomer", "submitCustomerHistory", "Lcom/eazy/daiku/data/model/server_model/CustomerHistoryModel;", "submitGetTaxiDriver", "submitListAllBank", "Lcom/eazy/daiku/data/model/ListAllBankAccountModel;", "submitListAllLocationKiosk", "Lcom/eazy/daiku/ui/customer/model/ListKioskModel;", "allDevice", "", "(ZLkotlin/coroutines/Continuation;)Ljava/lang/Object;", "submitRequestOtp", "submitReviewBooking", "submitSearchLocationKioskNearBy", "submitSearchMap", "Lcom/eazy/daiku/ui/customer/model/sear_map/SearchMapModel;", "search", "submitSearchMapLocal", "submitVerifyOtp", "submitWithdraw", "Lcom/eazy/daiku/data/model/WithdrawMoneyRespondModel;", "updateAvailableTaxi", "updateUserInfo", "userInfo", "userSubmitWithdraw", "verifyBankAccount", "Lcom/eazy/daiku/data/model/VerifyBankAccountModel;", "app_taxiRelease"})
public abstract interface EazyTaxiApi {
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/login")
    public abstract java.lang.Object login(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/logout")
    public abstract java.lang.Object logout(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/register")
    public abstract java.lang.Object createUser(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.CreateUserRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/auth/me")
    public abstract java.lang.Object userInfo(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/get-terms")
    public abstract java.lang.Object fetchVehicleType(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "slug", encoded = true)
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond>>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/booking/{code}")
    public abstract java.lang.Object fetchQrCode(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Path(value = "code", encoded = true)
    java.lang.String type, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/booking/taxi/start/{code}")
    public abstract java.lang.Object startTrip(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Path(value = "code", encoded = true)
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/booking/taxi/end/{code}")
    public abstract java.lang.Object endTrip(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Path(value = "code", encoded = true)
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/user/booking/processing")
    public abstract java.lang.Object fetchTripProcessing(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.QrCodeRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/user/set-available-status")
    public abstract java.lang.Object updateAvailableTaxi(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/resubmit-kyc")
    public abstract java.lang.Object reUploadKycDoc(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/wallet/getTransaction")
    public abstract java.lang.Object fetchTransaction(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.TransactionRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/booking/get/history")
    public abstract java.lang.Object fetchHistory(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.History>>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/customers/booking/history")
    public abstract java.lang.Object submitCustomerHistory(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.CustomerHistoryModel>>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/user/withdraw")
    public abstract java.lang.Object submitWithdraw(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/v2/user/withdraw")
    public abstract java.lang.Object userSubmitWithdraw(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.WithdrawMoneyRespondModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/change-password")
    public abstract java.lang.Object submitChangePwd(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/requestOTPForgotPassword")
    public abstract java.lang.Object submitRequestOtp(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/verifyOTPForgotPassword")
    public abstract java.lang.Object submitVerifyOtp(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/changePasswordByOTP")
    public abstract java.lang.Object submitChangePasswordByOtp(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/community/group/user")
    public abstract java.lang.Object fetchCommunityTaxi(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.server_model.CommunityTaxiRespond>>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/user/verify/bank-account")
    public abstract java.lang.Object verifyBankAccount(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.VerifyBankAccountModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/me")
    public abstract java.lang.Object updateUserInfo(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.User>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "/api/booking/preview/doCheckout")
    public abstract java.lang.Object submitReviewBooking(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/map/v2/searchMap")
    public abstract java.lang.Object submitSearchMap(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "search")
    java.lang.String search, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/map/v2/searchMapLocal")
    public abstract java.lang.Object submitSearchMapLocal(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Query(value = "search")
    java.lang.String search, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.ui.customer.model.sear_map.SearchMapModel> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/auth/registerAsCustomer")
    public abstract java.lang.Object submitCreateUserCustomer(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.CreateUserRespond>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/list-kiosk")
    public abstract java.lang.Object submitSearchLocationKioskNearBy(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.ListKioskModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/car/search")
    public abstract java.lang.Object submitGetTaxiDriver(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/booking/preview/doCheckout")
    public abstract java.lang.Object submitCheckoutTaxi(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> body, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.PreviewCheckoutModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/list-kiosk")
    public abstract java.lang.Object submitListAllLocationKiosk(@retrofit2.http.Query(value = "all_device")
    boolean allDevice, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.ui.customer.model.ListKioskModel>>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/v2/booking/doCheckoutTaxi")
    public abstract java.lang.Object submitCheckout(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> body, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.ui.customer.model.WebPayRespondModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/user/list-bank")
    public abstract java.lang.Object submitListAllBank(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<java.util.List<com.eazy.daiku.data.model.ListAllBankAccountModel>>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/v2/booking/preview/doCheckout")
    public abstract java.lang.Object submitBookingPreviewDoCheckout(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.eazy.daiku.data.model.server_model.BookingPreviewDoCheckoutModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "api/user/customer/booking/processing")
    public abstract java.lang.Object submitBookingProcessing(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.example.example.BookingProcessingModel>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.POST(value = "api/user/customer/booking/cancelled/{code}")
    public abstract java.lang.Object submitCancelBookingTaxi(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Path(value = "code", encoded = true)
    java.lang.String code, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.eazy.daiku.data.model.base.ApiResWraper<com.google.gson.JsonElement>> continuation);
}