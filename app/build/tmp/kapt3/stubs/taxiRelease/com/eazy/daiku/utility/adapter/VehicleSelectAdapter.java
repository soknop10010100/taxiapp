package com.eazy.daiku.utility.adapter;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0003\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\u0019B\u0005\u00a2\u0006\u0002\u0010\u0003J\u001c\u0010\u0010\u001a\u00020\u00072\f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00060\u000f2\u0006\u0010\f\u001a\u00020\rJ\b\u0010\u0011\u001a\u00020\rH\u0016J\u0018\u0010\u0012\u001a\u00020\u00072\u0006\u0010\u0013\u001a\u00020\u00022\u0006\u0010\u0014\u001a\u00020\rH\u0016J\u0018\u0010\u0015\u001a\u00020\u00022\u0006\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\rH\u0016R&\u0010\u0004\u001a\u000e\u0012\u0004\u0012\u00020\u0006\u0012\u0004\u0012\u00020\u00070\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u000e\u001a\n\u0012\u0004\u0012\u00020\u0006\u0018\u00010\u000fX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u001a"}, d2 = {"Lcom/eazy/daiku/utility/adapter/VehicleSelectAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/eazy/daiku/utility/adapter/VehicleSelectAdapter$VehicleHolder;", "()V", "selectedRow", "Lkotlin/Function1;", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "", "getSelectedRow", "()Lkotlin/jvm/functions/Function1;", "setSelectedRow", "(Lkotlin/jvm/functions/Function1;)V", "selectedVehicleTypeId", "", "vehicleModels", "Ljava/util/ArrayList;", "addVehicle", "getItemCount", "onBindViewHolder", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "VehicleHolder", "app_taxiRelease"})
public final class VehicleSelectAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.eazy.daiku.utility.adapter.VehicleSelectAdapter.VehicleHolder> {
    private java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleModels;
    public kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> selectedRow;
    private int selectedVehicleTypeId = -1;
    
    public VehicleSelectAdapter() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final kotlin.jvm.functions.Function1<com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> getSelectedRow() {
        return null;
    }
    
    public final void setSelectedRow(@org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super com.eazy.daiku.data.model.server_model.VehicleTypeRespond, kotlin.Unit> p0) {
    }
    
    public final void addVehicle(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<com.eazy.daiku.data.model.server_model.VehicleTypeRespond> vehicleModels, int selectedVehicleTypeId) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.eazy.daiku.utility.adapter.VehicleSelectAdapter.VehicleHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.adapter.VehicleSelectAdapter.VehicleHolder holder, int position) {
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u000e\u0010\u0011\u001a\u00020\u00122\u0006\u0010\u0013\u001a\u00020\u0014J\u000e\u0010\u0015\u001a\u00020\u00122\u0006\u0010\u0015\u001a\u00020\u0016R\u0011\u0010\u0005\u001a\u00020\u0006\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0007\u0010\bR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\r\u001a\u00020\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010\u00a8\u0006\u0017"}, d2 = {"Lcom/eazy/daiku/utility/adapter/VehicleSelectAdapter$VehicleHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemView", "Landroid/view/View;", "(Landroid/view/View;)V", "cardVehicleCardView", "Lcom/google/android/material/card/MaterialCardView;", "getCardVehicleCardView", "()Lcom/google/android/material/card/MaterialCardView;", "imageView", "Landroid/widget/ImageView;", "getImageView", "()Landroid/widget/ImageView;", "textView", "Landroid/widget/TextView;", "getTextView", "()Landroid/widget/TextView;", "bind", "", "vehicleModel", "Lcom/eazy/daiku/data/model/server_model/VehicleTypeRespond;", "selectItem", "", "app_taxiRelease"})
    public static final class VehicleHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private final android.widget.ImageView imageView = null;
        @org.jetbrains.annotations.NotNull()
        private final android.widget.TextView textView = null;
        @org.jetbrains.annotations.NotNull()
        private final com.google.android.material.card.MaterialCardView cardVehicleCardView = null;
        
        public VehicleHolder(@org.jetbrains.annotations.NotNull()
        android.view.View itemView) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.ImageView getImageView() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final android.widget.TextView getTextView() {
            return null;
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.google.android.material.card.MaterialCardView getCardVehicleCardView() {
            return null;
        }
        
        public final void bind(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.data.model.server_model.VehicleTypeRespond vehicleModel) {
        }
        
        public final void selectItem(boolean selectItem) {
        }
    }
}