package com.eazy.daiku.data.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u00002\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001JD\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u00032$\b\u0001\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0007j\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0001`\t2\b\b\u0001\u0010\n\u001a\u00020\u000bH\'J4\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u00032$\b\u0001\u0010\u0006\u001a\u001e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u00010\u0007j\u000e\u0012\u0004\u0012\u00020\b\u0012\u0004\u0012\u00020\u0001`\tH\'\u00a8\u0006\u000e"}, d2 = {"Lcom/eazy/daiku/data/remote/TelegramApi;", "", "sendPhoto", "Lretrofit2/Call;", "Lretrofit2/Response;", "Lcom/github/kotlintelegrambot/entities/Message;", "map", "Ljava/util/HashMap;", "", "Lkotlin/collections/HashMap;", "requestBody", "Lokhttp3/RequestBody;", "submitErrorTelegramDefault", "Lcom/eazy/daiku/data/model/server_model/TelegramRespondBody;", "app_taxiRelease"})
public abstract interface TelegramApi {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Headers(value = {"Content-Type:application/x-www-form-urlencoded"})
    @retrofit2.http.POST(value = "sendMessage")
    public abstract retrofit2.Call<com.eazy.daiku.data.model.server_model.TelegramRespondBody> submitErrorTelegramDefault(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map);
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.POST(value = "sendPhoto")
    public abstract retrofit2.Call<retrofit2.Response<com.github.kotlintelegrambot.entities.Message>> sendPhoto(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.QueryMap()
    java.util.HashMap<java.lang.String, java.lang.Object> map, @org.jetbrains.annotations.NotNull()
    @retrofit2.http.Body()
    okhttp3.RequestBody requestBody);
}