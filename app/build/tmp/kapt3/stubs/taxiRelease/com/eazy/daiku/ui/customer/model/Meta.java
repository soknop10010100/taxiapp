package com.eazy.daiku.ui.customer.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0017\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001BC\u0012\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0007\u0012\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t\u0012\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000b\u00a2\u0006\u0002\u0010\fJ\u000b\u0010\u001d\u001a\u0004\u0018\u00010\u0003H\u00c6\u0003J\u0019\u0010\u001e\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u0007H\u00c6\u0003J\u000b\u0010\u001f\u001a\u0004\u0018\u00010\tH\u00c6\u0003J\u000b\u0010 \u001a\u0004\u0018\u00010\u000bH\u00c6\u0003JG\u0010!\u001a\u00020\u00002\n\b\u0002\u0010\u0002\u001a\u0004\u0018\u00010\u00032\u0018\b\u0002\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u00072\n\b\u0002\u0010\b\u001a\u0004\u0018\u00010\t2\n\b\u0002\u0010\n\u001a\u0004\u0018\u00010\u000bH\u00c6\u0001J\u0013\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010%\u001a\u00020&H\u00d6\u0001J\t\u0010\'\u001a\u00020\u0006H\u00d6\u0001R \u0010\b\u001a\u0004\u0018\u00010\t8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R \u0010\n\u001a\u0004\u0018\u00010\u000b8\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R.\u0010\u0004\u001a\u0012\u0012\u0004\u0012\u00020\u00060\u0005j\b\u0012\u0004\u0012\u00020\u0006`\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R \u0010\u0002\u001a\u0004\u0018\u00010\u00038\u0006@\u0006X\u0087\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c\u00a8\u0006("}, d2 = {"Lcom/eazy/daiku/ui/customer/model/Meta;", "", "setting", "Lcom/eazy/daiku/ui/customer/model/Setting;", "outTradeNoSentToKESS", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "customerFees", "Lcom/eazy/daiku/ui/customer/model/CustomerFees;", "iframeStaticCard", "Lcom/eazy/daiku/ui/customer/model/IframeStaticCard;", "(Lcom/eazy/daiku/ui/customer/model/Setting;Ljava/util/ArrayList;Lcom/eazy/daiku/ui/customer/model/CustomerFees;Lcom/eazy/daiku/ui/customer/model/IframeStaticCard;)V", "getCustomerFees", "()Lcom/eazy/daiku/ui/customer/model/CustomerFees;", "setCustomerFees", "(Lcom/eazy/daiku/ui/customer/model/CustomerFees;)V", "getIframeStaticCard", "()Lcom/eazy/daiku/ui/customer/model/IframeStaticCard;", "setIframeStaticCard", "(Lcom/eazy/daiku/ui/customer/model/IframeStaticCard;)V", "getOutTradeNoSentToKESS", "()Ljava/util/ArrayList;", "setOutTradeNoSentToKESS", "(Ljava/util/ArrayList;)V", "getSetting", "()Lcom/eazy/daiku/ui/customer/model/Setting;", "setSetting", "(Lcom/eazy/daiku/ui/customer/model/Setting;)V", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_taxiRelease"})
public final class Meta {
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "setting")
    private com.eazy.daiku.ui.customer.model.Setting setting;
    @org.jetbrains.annotations.NotNull()
    @com.google.gson.annotations.SerializedName(value = "out_trade_no_sent_to_KESS")
    private java.util.ArrayList<java.lang.String> outTradeNoSentToKESS;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "customer_fees")
    private com.eazy.daiku.ui.customer.model.CustomerFees customerFees;
    @org.jetbrains.annotations.Nullable()
    @com.google.gson.annotations.SerializedName(value = "iframe_static_card")
    private com.eazy.daiku.ui.customer.model.IframeStaticCard iframeStaticCard;
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.ui.customer.model.Meta copy(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Setting setting, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> outTradeNoSentToKESS, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.CustomerFees customerFees, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.IframeStaticCard iframeStaticCard) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public Meta() {
        super();
    }
    
    public Meta(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Setting setting, @org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> outTradeNoSentToKESS, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.CustomerFees customerFees, @org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.IframeStaticCard iframeStaticCard) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.Setting component1() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.Setting getSetting() {
        return null;
    }
    
    public final void setSetting(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.Setting p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getOutTradeNoSentToKESS() {
        return null;
    }
    
    public final void setOutTradeNoSentToKESS(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.CustomerFees component3() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.CustomerFees getCustomerFees() {
        return null;
    }
    
    public final void setCustomerFees(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.CustomerFees p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.IframeStaticCard component4() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.ui.customer.model.IframeStaticCard getIframeStaticCard() {
        return null;
    }
    
    public final void setIframeStaticCard(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.ui.customer.model.IframeStaticCard p0) {
    }
}