package com.eazy.daiku.ui.customer.step_booking;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u0000 +2\u00020\u0001:\u0001+B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0019\u001a\u00020\u001aH\u0002J\b\u0010\u001b\u001a\u00020\u001aH\u0003J\u0010\u0010\u001c\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001eH\u0002J\u0010\u0010\u001f\u001a\u00020\u001a2\u0006\u0010 \u001a\u00020!H\u0016J$\u0010\"\u001a\u00020\u001e2\u0006\u0010#\u001a\u00020$2\b\u0010%\u001a\u0004\u0018\u00010&2\b\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\u001a\u0010)\u001a\u00020\u001a2\u0006\u0010\u001d\u001a\u00020\u001e2\b\u0010\'\u001a\u0004\u0018\u00010(H\u0016J\b\u0010*\u001a\u00020\u001aH\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\f\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\r\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000e\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u000f\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0012\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0013\u001a\u00020\u00148BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0017\u0010\u0018\u001a\u0004\b\u0015\u0010\u0016\u00a8\u0006,"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/TripDetailBookingFragment;", "Lcom/eazy/daiku/utility/base/BaseFragment;", "()V", "adminArea", "", "binding", "Lcom/eazy/daiku/databinding/TripDetailBookingLayoutBinding;", "carId", "countryName", "deviceId", "fContext", "Landroidx/fragment/app/FragmentActivity;", "getAddressLine", "lattitude", "longitude", "pathMapScreenShot", "previewCheckoutModel", "Lcom/eazy/daiku/ui/customer/model/PreviewCheckoutModel;", "vechicle", "viewModel", "Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "getViewModel", "()Lcom/eazy/daiku/ui/customer/viewmodel/ProcessCustomerBookingViewModel;", "viewModel$delegate", "Lkotlin/Lazy;", "initAction", "", "initObser", "initView", "view", "Landroid/view/View;", "onAttach", "context", "Landroid/content/Context;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "submitCheckout", "Companion", "app_taxiRelease"})
public final class TripDetailBookingFragment extends com.eazy.daiku.utility.base.BaseFragment {
    private com.eazy.daiku.databinding.TripDetailBookingLayoutBinding binding;
    private androidx.fragment.app.FragmentActivity fContext;
    private com.eazy.daiku.ui.customer.model.PreviewCheckoutModel previewCheckoutModel;
    private java.lang.String carId;
    private java.lang.String deviceId;
    private java.lang.String pathMapScreenShot;
    private java.lang.String adminArea;
    private java.lang.String countryName;
    private java.lang.String getAddressLine;
    private java.lang.String lattitude;
    private java.lang.String longitude;
    private java.lang.String vechicle;
    private final kotlin.Lazy viewModel$delegate = null;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.customer.step_booking.TripDetailBookingFragment.Companion Companion = null;
    
    public TripDetailBookingFragment() {
        super();
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    private final com.eazy.daiku.ui.customer.viewmodel.ProcessCustomerBookingViewModel getViewModel() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.ui.customer.step_booking.TripDetailBookingFragment newInstance(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.ui.customer.model.PreviewCheckoutModel previewCheckoutModel, @org.jetbrains.annotations.NotNull()
    java.lang.String carId, @org.jetbrains.annotations.NotNull()
    java.lang.String deviceId, @org.jetbrains.annotations.NotNull()
    java.lang.String pathMapScreenShot, @org.jetbrains.annotations.NotNull()
    java.lang.String adminArea, @org.jetbrains.annotations.NotNull()
    java.lang.String countryName, @org.jetbrains.annotations.NotNull()
    java.lang.String getAddressLine, @org.jetbrains.annotations.NotNull()
    java.lang.String lattitude, @org.jetbrains.annotations.NotNull()
    java.lang.String longtitude, @org.jetbrains.annotations.NotNull()
    java.lang.String vechicle) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView(android.view.View view) {
    }
    
    @android.annotation.SuppressLint(value = {"FragmentLiveDataObserve"})
    private final void initObser() {
    }
    
    private final void initAction() {
    }
    
    private final void submitCheckout() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\t\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002JX\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\b2\u0006\u0010\n\u001a\u00020\b2\u0006\u0010\u000b\u001a\u00020\b2\u0006\u0010\f\u001a\u00020\b2\u0006\u0010\r\u001a\u00020\b2\u0006\u0010\u000e\u001a\u00020\b2\u0006\u0010\u000f\u001a\u00020\b2\u0006\u0010\u0010\u001a\u00020\bH\u0007\u00a8\u0006\u0011"}, d2 = {"Lcom/eazy/daiku/ui/customer/step_booking/TripDetailBookingFragment$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/ui/customer/step_booking/TripDetailBookingFragment;", "previewCheckoutModel", "Lcom/eazy/daiku/ui/customer/model/PreviewCheckoutModel;", "carId", "", "deviceId", "pathMapScreenShot", "adminArea", "countryName", "getAddressLine", "lattitude", "longtitude", "vechicle", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.ui.customer.step_booking.TripDetailBookingFragment newInstance(@org.jetbrains.annotations.NotNull()
        com.eazy.daiku.ui.customer.model.PreviewCheckoutModel previewCheckoutModel, @org.jetbrains.annotations.NotNull()
        java.lang.String carId, @org.jetbrains.annotations.NotNull()
        java.lang.String deviceId, @org.jetbrains.annotations.NotNull()
        java.lang.String pathMapScreenShot, @org.jetbrains.annotations.NotNull()
        java.lang.String adminArea, @org.jetbrains.annotations.NotNull()
        java.lang.String countryName, @org.jetbrains.annotations.NotNull()
        java.lang.String getAddressLine, @org.jetbrains.annotations.NotNull()
        java.lang.String lattitude, @org.jetbrains.annotations.NotNull()
        java.lang.String longtitude, @org.jetbrains.annotations.NotNull()
        java.lang.String vechicle) {
            return null;
        }
    }
}