package com.eazy.daiku.utility.other;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\b\b\u00c6\u0002\u0018\u00002\u00020\u0001:\u0001\u0013B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\b\u001a\u0004\u0018\u00010\u00042\u0006\u0010\t\u001a\u00020\nJ\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\n2\u0006\u0010\u000e\u001a\u00020\u0004H\u0002J\u000e\u0010\u000f\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\nJ\u0018\u0010\u0010\u001a\u00020\n2\u0006\u0010\r\u001a\u00020\n2\b\b\u0001\u0010\u0011\u001a\u00020\u0004J\u001a\u0010\u0012\u001a\u00020\n2\u0006\u0010\t\u001a\u00020\n2\b\u0010\u0011\u001a\u0004\u0018\u00010\u0004H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lcom/eazy/daiku/utility/other/LocaleManager;", "", "()V", "CHINA", "", "ENGLISH", "KHMER", "LANGUAGE_KEY", "getLanguagePref", "context", "Landroid/content/Context;", "setLanguagePref", "", "mContext", "localeKey", "setLocale", "setNewLocale", "language", "updateResources", "LocaleDef", "app_taxiRelease"})
public final class LocaleManager {
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.utility.other.LocaleManager INSTANCE = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String ENGLISH = "en";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String KHMER = "km";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String CHINA = "zh";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String LANGUAGE_KEY = "language_key";
    
    private LocaleManager() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLanguagePref(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return null;
    }
    
    private final void setLanguagePref(android.content.Context mContext, java.lang.String localeKey) {
    }
    
    /**
     * set current pref locale
     */
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context setLocale(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext) {
        return null;
    }
    
    /**
     * Set new Locale with context
     */
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context setNewLocale(@org.jetbrains.annotations.NotNull()
    android.content.Context mContext, @org.jetbrains.annotations.NotNull()
    @com.eazy.daiku.utility.other.LocaleManager.LocaleDef()
    java.lang.String language) {
        return null;
    }
    
    /**
     * update resource
     */
    private final android.content.Context updateResources(android.content.Context context, java.lang.String language) {
        return null;
    }
    
    @androidx.annotation.StringDef(value = {"en", "km", "zh"})
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u001b\n\u0002\b\u0002\b\u0087\u0002\u0018\u0000 \u00022\u00020\u0001:\u0001\u0002B\u0000\u00a8\u0006\u0003"}, d2 = {"Lcom/eazy/daiku/utility/other/LocaleManager$LocaleDef;", "", "Companion", "app_taxiRelease"})
    @java.lang.annotation.Retention(value = java.lang.annotation.RetentionPolicy.SOURCE)
    public static abstract @interface LocaleDef {
        @org.jetbrains.annotations.NotNull()
        public static final com.eazy.daiku.utility.other.LocaleManager.LocaleDef.Companion Companion = null;
        
        @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0018\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u0006\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\"\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u0010\n\u0002\u0010\n\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\u000b"}, d2 = {"Lcom/eazy/daiku/utility/other/LocaleManager$LocaleDef$Companion;", "", "()V", "SUPPORTED_LOCALES", "", "", "getSUPPORTED_LOCALES", "()[Ljava/lang/String;", "setSUPPORTED_LOCALES", "([Ljava/lang/String;)V", "[Ljava/lang/String;", "app_taxiRelease"})
        public static final class Companion {
            @org.jetbrains.annotations.NotNull()
            private static java.lang.String[] SUPPORTED_LOCALES = {"en", "km", "zh"};
            
            private Companion() {
                super();
            }
            
            @org.jetbrains.annotations.NotNull()
            public final java.lang.String[] getSUPPORTED_LOCALES() {
                return null;
            }
            
            public final void setSUPPORTED_LOCALES(@org.jetbrains.annotations.NotNull()
            java.lang.String[] p0) {
            }
        }
    }
}