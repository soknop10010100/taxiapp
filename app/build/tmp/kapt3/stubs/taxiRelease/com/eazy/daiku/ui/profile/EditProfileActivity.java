package com.eazy.daiku.ui.profile;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0011\n\u0002\u0010\u000e\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0007\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\u0018\u0000 A2\u00020\u0001:\u0001AB\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010,\u001a\u00020-H\u0002J\b\u0010.\u001a\u00020/H\u0002J\b\u00100\u001a\u00020-H\u0002J\b\u00101\u001a\u00020-H\u0002J\b\u00102\u001a\u00020-H\u0002J\"\u00103\u001a\u00020-2\u0006\u00104\u001a\u00020\u00062\u0006\u00105\u001a\u00020\u00062\b\u00106\u001a\u0004\u0018\u000107H\u0014J\u0012\u00108\u001a\u00020-2\b\u00109\u001a\u0004\u0018\u00010:H\u0014J\b\u0010;\u001a\u00020-H\u0002J\u0010\u0010<\u001a\u00020-2\u0006\u0010=\u001a\u00020/H\u0002J\b\u0010>\u001a\u00020-H\u0002J\u0010\u0010?\u001a\u00020-2\u0006\u0010@\u001a\u00020\u0011H\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u0007\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000b\u0010\f\u001a\u0004\b\t\u0010\nR\u001b\u0010\r\u001a\u00020\b8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b\u000f\u0010\f\u001a\u0004\b\u000e\u0010\nR\u0010\u0010\u0010\u001a\u0004\u0018\u00010\u0011X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0015R\u0016\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00140\u0013X\u0082.\u00a2\u0006\u0004\n\u0002\u0010\u0015R+\u0010\u0018\u001a\u00020\u00062\u0006\u0010\u0017\u001a\u00020\u00068B@BX\u0082\u008e\u0002\u00a2\u0006\u0012\n\u0004\b\u001d\u0010\u001e\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001cR\u001b\u0010\u001f\u001a\u00020 8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b#\u0010\f\u001a\u0004\b!\u0010\"R\u0010\u0010$\u001a\u0004\u0018\u00010%X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010&\u001a\u00020\'8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b*\u0010\f\u001a\u0004\b(\u0010)R\u000e\u0010+\u001a\u00020\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006B"}, d2 = {"Lcom/eazy/daiku/ui/profile/EditProfileActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "binding", "Lcom/eazy/daiku/databinding/ActivityEditProfileBinding;", "communityTaxiId", "", "communityTaxiTemporaryRequestOneTimeVm", "Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "getCommunityTaxiTemporaryRequestOneTimeVm", "()Lcom/eazy/daiku/utility/view_model/community/CommunityVm;", "communityTaxiTemporaryRequestOneTimeVm$delegate", "Lkotlin/Lazy;", "communityTaxiVm", "getCommunityTaxiVm", "communityTaxiVm$delegate", "dobDate", "Ljava/util/Date;", "genderKey", "", "", "[Ljava/lang/String;", "genderScreenValue", "<set-?>", "selectGenderIndex", "getSelectGenderIndex", "()I", "setSelectGenderIndex", "(I)V", "selectGenderIndex$delegate", "Lkotlin/properties/ReadWriteProperty;", "uploadDocVm", "Lcom/eazy/daiku/utility/view_model/uplaod_doc_vm/UploadDocVM;", "getUploadDocVm", "()Lcom/eazy/daiku/utility/view_model/uplaod_doc_vm/UploadDocVM;", "uploadDocVm$delegate", "user", "Lcom/eazy/daiku/data/model/server_model/User;", "userCaseVm", "Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "getUserCaseVm", "()Lcom/eazy/daiku/utility/view_model/user_case/UseCaseVm;", "userCaseVm$delegate", "vehicleTermId", "doAction", "", "hasVerifiedKyc", "", "initObserved", "initViewTaxiWithCustomer", "intiView", "onActivityResult", "requestCode", "resultCode", "data", "Landroid/content/Intent;", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "openSelfiePicture", "uiEnable", "verifiedKyc", "updateDataToServer", "updateUIDob", "date", "Companion", "app_taxiRelease"})
public final class EditProfileActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private com.eazy.daiku.databinding.ActivityEditProfileBinding binding;
    private java.lang.String[] genderScreenValue;
    private java.lang.String[] genderKey;
    private final kotlin.Lazy communityTaxiTemporaryRequestOneTimeVm$delegate = null;
    private final kotlin.Lazy communityTaxiVm$delegate = null;
    private final kotlin.Lazy uploadDocVm$delegate = null;
    private final kotlin.Lazy userCaseVm$delegate = null;
    private java.util.Date dobDate;
    private final kotlin.properties.ReadWriteProperty selectGenderIndex$delegate = null;
    private com.eazy.daiku.data.model.server_model.User user;
    private int communityTaxiId = -1;
    private int vehicleTermId = -1;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.profile.EditProfileActivity.Companion Companion = null;
    public static final int RequestSelfieProfileCode = 344;
    
    public EditProfileActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.community.CommunityVm getCommunityTaxiTemporaryRequestOneTimeVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.community.CommunityVm getCommunityTaxiVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.uplaod_doc_vm.UploadDocVM getUploadDocVm() {
        return null;
    }
    
    private final com.eazy.daiku.utility.view_model.user_case.UseCaseVm getUserCaseVm() {
        return null;
    }
    
    private final int getSelectGenderIndex() {
        return 0;
    }
    
    private final void setSelectGenderIndex(int p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onActivityResult(int requestCode, int resultCode, @org.jetbrains.annotations.Nullable()
    android.content.Intent data) {
    }
    
    private final void intiView() {
    }
    
    private final boolean hasVerifiedKyc() {
        return false;
    }
    
    private final void initObserved() {
    }
    
    private final void doAction() {
    }
    
    private final void initViewTaxiWithCustomer() {
    }
    
    private final void openSelfiePicture() {
    }
    
    private final void updateUIDob(java.util.Date date) {
    }
    
    private final void updateDataToServer() {
    }
    
    private final void uiEnable(boolean verifiedKyc) {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/profile/EditProfileActivity$Companion;", "", "()V", "RequestSelfieProfileCode", "", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}