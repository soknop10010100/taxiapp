package com.eazy.daiku.ui.payment_method;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000f\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\t\u0018\u0000 /2\u00020\u0001:\u0001/B\u0005\u00a2\u0006\u0002\u0010\u0002J\n\u0010\u0014\u001a\u0004\u0018\u00010\u0015H\u0002J\b\u0010\u0016\u001a\u00020\u0017H\u0002J\u0010\u0010\u0018\u001a\u00020\u00172\u0006\u0010\u0019\u001a\u00020\u0004H\u0002J\b\u0010\u001a\u001a\u00020\u0017H\u0002J\u0010\u0010\u001b\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001dH\u0002J\u0010\u0010\u001e\u001a\u00020\u00172\u0006\u0010\u001f\u001a\u00020 H\u0016J$\u0010!\u001a\u00020\u001d2\u0006\u0010\"\u001a\u00020#2\b\u0010$\u001a\u0004\u0018\u00010%2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J\u001a\u0010(\u001a\u00020\u00172\u0006\u0010\u001c\u001a\u00020\u001d2\b\u0010&\u001a\u0004\u0018\u00010\'H\u0016J \u0010)\u001a\u00020\u00172\u0006\u0010*\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J \u0010,\u001a\u00020\u00172\u0006\u0010*\u001a\u00020\u00042\u0006\u0010+\u001a\u00020\u00042\u0006\u0010\u0003\u001a\u00020\u0004H\u0002J\u0010\u0010-\u001a\u00020\u00172\u0006\u0010.\u001a\u00020\u0004H\u0003R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\rX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010\u000e\u001a\u00020\u000f8FX\u0086\u0084\u0002\u00a2\u0006\f\n\u0004\b\u0012\u0010\u0013\u001a\u0004\b\u0010\u0010\u0011\u00a8\u00060"}, d2 = {"Lcom/eazy/daiku/ui/payment_method/AddABAAccountNumberFragment;", "Lcom/eazy/daiku/utility/base/BaseFragment;", "()V", "accountNumber", "", "binding", "Lcom/eazy/daiku/databinding/FragmentAddABAAccountNumberBinding;", "fContext", "Landroidx/fragment/app/FragmentActivity;", "navController", "Landroidx/navigation/NavController;", "typeBank", "userId", "", "withdrawViewModel", "Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "getWithdrawViewModel", "()Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "withdrawViewModel$delegate", "Lkotlin/Lazy;", "accessParentFragment", "Lcom/eazy/daiku/utility/bottom_sheet/PaymentMethodBtsFragment;", "doAction", "", "ediTextFormatBankAccount", "bankType", "initObserver", "initView", "view", "Landroid/view/View;", "onAttach", "context", "Landroid/content/Context;", "onCreateView", "inflater", "Landroid/view/LayoutInflater;", "container", "Landroid/view/ViewGroup;", "savedInstanceState", "Landroid/os/Bundle;", "onViewCreated", "saveFirstBankAccount", "id", "ownerName", "saveListBankAccount", "setViewBank", "bic", "Companion", "app_taxiRelease"})
public final class AddABAAccountNumberFragment extends com.eazy.daiku.utility.base.BaseFragment {
    @org.jetbrains.annotations.NotNull()
    private final kotlin.Lazy withdrawViewModel$delegate = null;
    private com.eazy.daiku.databinding.FragmentAddABAAccountNumberBinding binding;
    private androidx.fragment.app.FragmentActivity fContext;
    private androidx.navigation.NavController navController;
    private java.lang.String typeBank = "";
    private java.lang.String accountNumber = "";
    private int userId = -100;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.payment_method.AddABAAccountNumberFragment.Companion Companion = null;
    
    public AddABAAccountNumberFragment() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.view_model.withdraw.WithdrawViewModel getWithdrawViewModel() {
        return null;
    }
    
    @java.lang.Override()
    public void onAttach(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public android.view.View onCreateView(@org.jetbrains.annotations.NotNull()
    android.view.LayoutInflater inflater, @org.jetbrains.annotations.Nullable()
    android.view.ViewGroup container, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
        return null;
    }
    
    @java.lang.Override()
    public void onViewCreated(@org.jetbrains.annotations.NotNull()
    android.view.View view, @org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView(android.view.View view) {
    }
    
    private final void initObserver() {
    }
    
    private final void doAction() {
    }
    
    @android.annotation.SuppressLint(value = {"UseCompatLoadingForDrawables"})
    private final void setViewBank(java.lang.String bic) {
    }
    
    private final void ediTextFormatBankAccount(java.lang.String bankType) {
    }
    
    private final void saveFirstBankAccount(java.lang.String id, java.lang.String ownerName, java.lang.String accountNumber) {
    }
    
    private final void saveListBankAccount(java.lang.String id, java.lang.String ownerName, java.lang.String accountNumber) {
    }
    
    private final com.eazy.daiku.utility.bottom_sheet.PaymentMethodBtsFragment accessParentFragment() {
        return null;
    }
    
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddABAAccountNumberFragment.
     */
    @org.jetbrains.annotations.NotNull()
    @kotlin.jvm.JvmStatic()
    public static final com.eazy.daiku.ui.payment_method.AddABAAccountNumberFragment newInstance() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0012\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\u0007\u00a8\u0006\u0005"}, d2 = {"Lcom/eazy/daiku/ui/payment_method/AddABAAccountNumberFragment$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/ui/payment_method/AddABAAccountNumberFragment;", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AddABAAccountNumberFragment.
         */
        @org.jetbrains.annotations.NotNull()
        @kotlin.jvm.JvmStatic()
        public final com.eazy.daiku.ui.payment_method.AddABAAccountNumberFragment newInstance() {
            return null;
        }
    }
}