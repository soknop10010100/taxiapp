package com.eazy.daiku.ui.withdraw;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000z\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0006\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0005\u0018\u0000 92\u00020\u0001:\u00019B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\'\u001a\u00020(2\u0006\u0010)\u001a\u00020\u00192\u0006\u0010\f\u001a\u00020\u000bH\u0002J\b\u0010*\u001a\u00020(H\u0002J\u0010\u0010+\u001a\u00020(2\u0006\u0010,\u001a\u00020\u0004H\u0003J\b\u0010-\u001a\u00020(H\u0002J\u0010\u0010.\u001a\u00020(2\u0006\u0010/\u001a\u00020\u000bH\u0002J\b\u00100\u001a\u00020(H\u0002J\b\u00101\u001a\u00020(H\u0002J\b\u00102\u001a\u00020(H\u0002J\u0012\u00103\u001a\u00020(2\b\u00104\u001a\u0004\u0018\u000105H\u0014J\b\u00106\u001a\u00020(H\u0014J\b\u00107\u001a\u00020(H\u0014J\u0010\u00108\u001a\u00020(2\u0006\u0010\f\u001a\u00020\u000bH\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n\u0012\u0006\u0012\u0004\u0018\u00010\u00070\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\tX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u000bX\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u000bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0012\u0010\r\u001a\u0004\u0018\u00010\u000eX\u0082\u000e\u00a2\u0006\u0004\n\u0002\u0010\u000fR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\"\u0010\u0012\u001a\u0016\u0012\u0004\u0012\u00020\u0014\u0018\u00010\u0013j\n\u0012\u0004\u0012\u00020\u0014\u0018\u0001`\u0015X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0016\u001a\u00020\u0017X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0018\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001c\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001d\u001a\u00020\u0019X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u001e\u001a\u00020\u001fX\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010 \u001a\u00020\u000eX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001b\u0010!\u001a\u00020\"8BX\u0082\u0084\u0002\u00a2\u0006\f\n\u0004\b%\u0010&\u001a\u0004\b#\u0010$\u00a8\u0006:"}, d2 = {"Lcom/eazy/daiku/ui/withdraw/WithdrawMoneyActivity;", "Lcom/eazy/daiku/utility/base/BaseActivity;", "()V", "_saveBankAccount", "Lcom/eazy/daiku/data/model/SaveBankAccount;", "baseListenerAutoFillNumber", "Lcom/eazy/daiku/utility/base/BaseListenerAutoFillNumber;", "Lcom/eazy/daiku/utility/AutoFillNumberRecyclerView$Item;", "binding", "Lcom/eazy/daiku/databinding/ActivityWithdrawMoneyBinding;", "currencyUSD", "", "haseBankAccount", "idTickBankAccount", "", "Ljava/lang/Integer;", "intentFilter", "Landroid/content/IntentFilter;", "listAllBank", "Ljava/util/ArrayList;", "Lcom/eazy/daiku/data/model/ListAllBankAccountModel;", "Lkotlin/collections/ArrayList;", "myBroadcastReceiver", "Lcom/eazy/daiku/utility/service/MyBroadcastReceiver;", "pendingBalance", "", "textWatcher", "Landroid/text/TextWatcher;", "totalAmount", "totalBalanceAmount", "transactionRespond", "Lcom/eazy/daiku/data/model/server_model/TransactionRespond;", "userId", "withdrawViewModel", "Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "getWithdrawViewModel", "()Lcom/eazy/daiku/utility/view_model/withdraw/WithdrawViewModel;", "withdrawViewModel$delegate", "Lkotlin/Lazy;", "checkEnableButtonWithdraw", "", "amount", "closeKeyBoard", "containerViewBankAccount", "saveBankAccount", "doAction", "initBankAccount", "hasClickChangeBankAccount", "initObserved", "initTransactionRespond", "initView", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onStart", "visibleView", "Companion", "app_taxiRelease"})
public final class WithdrawMoneyActivity extends com.eazy.daiku.utility.base.BaseActivity {
    private final kotlin.Lazy withdrawViewModel$delegate = null;
    private com.eazy.daiku.databinding.ActivityWithdrawMoneyBinding binding;
    private com.eazy.daiku.data.model.SaveBankAccount _saveBankAccount;
    private boolean haseBankAccount = false;
    private com.eazy.daiku.data.model.server_model.TransactionRespond transactionRespond;
    private double totalBalanceAmount = 0.0;
    private double pendingBalance = 0.0;
    private double totalAmount = 0.0;
    private final boolean currencyUSD = true;
    private final android.content.IntentFilter intentFilter = null;
    private int userId = -100;
    private java.util.ArrayList<com.eazy.daiku.data.model.ListAllBankAccountModel> listAllBank;
    private java.lang.Integer idTickBankAccount;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.ui.withdraw.WithdrawMoneyActivity.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String TransactionFileRespondKey = "TransactionFileRespondKey";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String listAllBankAccountModelKey = "listAllBankAccountModelKey";
    private final com.eazy.daiku.utility.base.BaseListenerAutoFillNumber<com.eazy.daiku.utility.AutoFillNumberRecyclerView.Item> baseListenerAutoFillNumber = null;
    private final android.text.TextWatcher textWatcher = null;
    private final com.eazy.daiku.utility.service.MyBroadcastReceiver myBroadcastReceiver = null;
    
    public WithdrawMoneyActivity() {
        super();
    }
    
    private final com.eazy.daiku.utility.view_model.withdraw.WithdrawViewModel getWithdrawViewModel() {
        return null;
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    private final void initView() {
    }
    
    private final void initObserved() {
    }
    
    private final void closeKeyBoard() {
    }
    
    private final void doAction() {
    }
    
    private final void initTransactionRespond() {
    }
    
    private final void initBankAccount(boolean hasClickChangeBankAccount) {
    }
    
    @android.annotation.SuppressLint(value = {"UseCompatLoadingForDrawables"})
    private final void containerViewBankAccount(com.eazy.daiku.data.model.SaveBankAccount saveBankAccount) {
    }
    
    private final void visibleView(boolean haseBankAccount) {
    }
    
    private final void checkEnableButtonWithdraw(double amount, boolean haseBankAccount) {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    @java.lang.Override()
    protected void onDestroy() {
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0006"}, d2 = {"Lcom/eazy/daiku/ui/withdraw/WithdrawMoneyActivity$Companion;", "", "()V", "TransactionFileRespondKey", "", "listAllBankAccountModelKey", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}