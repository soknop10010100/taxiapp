package com.eazy.daiku.data.model;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000Z\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0010\u000e\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0007\u0018\u0000 E2\u00020\u0001:\u0001EB\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010D\u001a\u00020\u001cR\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\n\u0010\u0006\"\u0004\b\u000b\u0010\bR\u001c\u0010\f\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u000f\"\u0004\b\u0010\u0010\u0011R\u001c\u0010\u0012\u001a\u0004\u0018\u00010\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u000f\"\u0004\b\u0014\u0010\u0011R1\u0010\u0015\u001a\"\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0017\u0018\u00010\u0016j\u0010\u0012\u0004\u0012\u00020\r\u0012\u0004\u0012\u00020\u0017\u0018\u0001`\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u000e\u0010\u001b\u001a\u00020\u001cX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u001d\u001a\u0004\u0018\u00010\u001eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001f\u0010 \"\u0004\b!\u0010\"R\u001c\u0010#\u001a\u0004\u0018\u00010\u001eX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010 \"\u0004\b%\u0010\"R\u001c\u0010&\u001a\u0004\u0018\u00010\'X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b(\u0010)\"\u0004\b*\u0010+R\u001a\u0010,\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b-\u0010\u000f\"\u0004\b.\u0010\u0011R\u001a\u0010/\u001a\u00020\rX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b0\u0010\u000f\"\u0004\b1\u0010\u0011R\u001c\u00102\u001a\u0004\u0018\u000103X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b4\u00105\"\u0004\b6\u00107R\u001a\u00108\u001a\u000209X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b:\u0010;\"\u0004\b<\u0010=R\u001c\u0010>\u001a\u0004\u0018\u00010?X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b@\u0010A\"\u0004\bB\u0010C\u00a8\u0006F"}, d2 = {"Lcom/eazy/daiku/data/model/UserDefaultSingleTon;", "", "()V", "bookingTaxiModel", "Lcom/eazy/daiku/data/model/BookingTaxiModel;", "getBookingTaxiModel", "()Lcom/eazy/daiku/data/model/BookingTaxiModel;", "setBookingTaxiModel", "(Lcom/eazy/daiku/data/model/BookingTaxiModel;)V", "bookingTaxiTemporaryObject", "getBookingTaxiTemporaryObject", "setBookingTaxiTemporaryObject", "currentLatitudeUser", "", "getCurrentLatitudeUser", "()Ljava/lang/String;", "setCurrentLatitudeUser", "(Ljava/lang/String;)V", "currentLngtitudeUser", "getCurrentLngtitudeUser", "setCurrentLngtitudeUser", "docKycTemporary", "Ljava/util/HashMap;", "Landroid/graphics/Bitmap;", "Lkotlin/collections/HashMap;", "getDocKycTemporary", "()Ljava/util/HashMap;", "foregroundOnlyBroadcastReceiver", "Lcom/eazy/daiku/utility/service/location/foreground_/ForegroundOnlyBroadcastReceiver;", "globalBaseCoreActivity", "Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "getGlobalBaseCoreActivity", "()Lcom/eazy/daiku/utility/base/BaseCoreActivity;", "setGlobalBaseCoreActivity", "(Lcom/eazy/daiku/utility/base/BaseCoreActivity;)V", "hasMainActivity", "getHasMainActivity", "setHasMainActivity", "lastLocation", "Landroid/location/Location;", "getLastLocation", "()Landroid/location/Location;", "setLastLocation", "(Landroid/location/Location;)V", "localizeLanguage", "getLocalizeLanguage", "setLocalizeLanguage", "networkName", "getNetworkName", "setNetworkName", "qrCodeRespond", "Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "getQrCodeRespond", "()Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;", "setQrCodeRespond", "(Lcom/eazy/daiku/data/model/server_model/QrCodeRespond;)V", "startStopState", "Lcom/eazy/daiku/utility/enumerable/TripEnum;", "getStartStopState", "()Lcom/eazy/daiku/utility/enumerable/TripEnum;", "setStartStopState", "(Lcom/eazy/daiku/utility/enumerable/TripEnum;)V", "user", "Lcom/eazy/daiku/data/model/server_model/User;", "getUser", "()Lcom/eazy/daiku/data/model/server_model/User;", "setUser", "(Lcom/eazy/daiku/data/model/server_model/User;)V", "getForegroundOnlyBroadcastReceiverSingleTon", "Companion", "app_taxiRelease"})
public final class UserDefaultSingleTon {
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.server_model.QrCodeRespond qrCodeRespond;
    @org.jetbrains.annotations.NotNull()
    private com.eazy.daiku.utility.enumerable.TripEnum startStopState = com.eazy.daiku.utility.enumerable.TripEnum.Nothing;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.server_model.User user;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.utility.base.BaseCoreActivity hasMainActivity;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.utility.base.BaseCoreActivity globalBaseCoreActivity;
    @org.jetbrains.annotations.Nullable()
    private final java.util.HashMap<java.lang.String, android.graphics.Bitmap> docKycTemporary = null;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String localizeLanguage = "";
    @org.jetbrains.annotations.NotNull()
    private java.lang.String networkName = "";
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.BookingTaxiModel bookingTaxiModel;
    @org.jetbrains.annotations.Nullable()
    private com.eazy.daiku.data.model.BookingTaxiModel bookingTaxiTemporaryObject;
    @org.jetbrains.annotations.Nullable()
    private android.location.Location lastLocation;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String currentLatitudeUser;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String currentLngtitudeUser;
    private com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyBroadcastReceiver foregroundOnlyBroadcastReceiver;
    @org.jetbrains.annotations.NotNull()
    public static final com.eazy.daiku.data.model.UserDefaultSingleTon.Companion Companion = null;
    private static com.eazy.daiku.data.model.UserDefaultSingleTon ourInstance;
    
    public UserDefaultSingleTon() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.QrCodeRespond getQrCodeRespond() {
        return null;
    }
    
    public final void setQrCodeRespond(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.QrCodeRespond p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.enumerable.TripEnum getStartStopState() {
        return null;
    }
    
    public final void setStartStopState(@org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.enumerable.TripEnum p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.server_model.User getUser() {
        return null;
    }
    
    public final void setUser(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.server_model.User p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.utility.base.BaseCoreActivity getHasMainActivity() {
        return null;
    }
    
    public final void setHasMainActivity(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.base.BaseCoreActivity p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.utility.base.BaseCoreActivity getGlobalBaseCoreActivity() {
        return null;
    }
    
    public final void setGlobalBaseCoreActivity(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.utility.base.BaseCoreActivity p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.util.HashMap<java.lang.String, android.graphics.Bitmap> getDocKycTemporary() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLocalizeLanguage() {
        return null;
    }
    
    public final void setLocalizeLanguage(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getNetworkName() {
        return null;
    }
    
    public final void setNetworkName(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.BookingTaxiModel getBookingTaxiModel() {
        return null;
    }
    
    public final void setBookingTaxiModel(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.BookingTaxiModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.eazy.daiku.data.model.BookingTaxiModel getBookingTaxiTemporaryObject() {
        return null;
    }
    
    public final void setBookingTaxiTemporaryObject(@org.jetbrains.annotations.Nullable()
    com.eazy.daiku.data.model.BookingTaxiModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.location.Location getLastLocation() {
        return null;
    }
    
    public final void setLastLocation(@org.jetbrains.annotations.Nullable()
    android.location.Location p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCurrentLatitudeUser() {
        return null;
    }
    
    public final void setCurrentLatitudeUser(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getCurrentLngtitudeUser() {
        return null;
    }
    
    public final void setCurrentLngtitudeUser(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.eazy.daiku.utility.service.location.foreground_.ForegroundOnlyBroadcastReceiver getForegroundOnlyBroadcastReceiverSingleTon() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public static final com.eazy.daiku.data.model.UserDefaultSingleTon getNewInstance() {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u00048FX\u0087\u0004\u00a2\u0006\f\u0012\u0004\b\u0005\u0010\u0002\u001a\u0004\b\u0006\u0010\u0007R\u0010\u0010\b\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\t"}, d2 = {"Lcom/eazy/daiku/data/model/UserDefaultSingleTon$Companion;", "", "()V", "newInstance", "Lcom/eazy/daiku/data/model/UserDefaultSingleTon;", "getNewInstance$annotations", "getNewInstance", "()Lcom/eazy/daiku/data/model/UserDefaultSingleTon;", "ourInstance", "app_taxiRelease"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @kotlin.jvm.JvmStatic()
        @java.lang.Deprecated()
        public static void getNewInstance$annotations() {
        }
        
        @org.jetbrains.annotations.Nullable()
        public final com.eazy.daiku.data.model.UserDefaultSingleTon getNewInstance() {
            return null;
        }
    }
}