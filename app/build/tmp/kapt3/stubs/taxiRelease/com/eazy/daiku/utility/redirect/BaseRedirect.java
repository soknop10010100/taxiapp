package com.eazy.daiku.utility.redirect;

import java.lang.System;

@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0016\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\bH\u0014J&\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\b2\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\u000b0\nH\u0014J\u0018\u0010\u0003\u001a\u00020\u00042\u0006\u0010\f\u001a\u00020\r2\u0006\u0010\u0007\u001a\u00020\bH\u0014\u00a8\u0006\u000e"}, d2 = {"Lcom/eazy/daiku/utility/redirect/BaseRedirect;", "", "()V", "gotoActivity", "", "activity", "Landroid/app/Activity;", "intent", "Landroid/content/Intent;", "activityResult", "Lcom/eazy/daiku/utility/BetterActivityResult$OnActivityResult;", "Landroidx/activity/result/ActivityResult;", "context", "Landroid/content/Context;", "app_taxiRelease"})
public class BaseRedirect {
    
    public BaseRedirect() {
        super();
    }
    
    protected void gotoActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
    
    protected void gotoActivity(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, @org.jetbrains.annotations.NotNull()
    android.content.Intent intent, @org.jetbrains.annotations.NotNull()
    com.eazy.daiku.utility.BetterActivityResult.OnActivityResult<androidx.activity.result.ActivityResult> activityResult) {
    }
    
    protected void gotoActivity(@org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    android.content.Intent intent) {
    }
}