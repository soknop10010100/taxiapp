package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.step_booking.SelectCarBookingFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      FragmentBuilderModule_SelectCarBookingFragment.SelectCarBookingFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_SelectCarBookingFragment {
  private FragmentBuilderModule_SelectCarBookingFragment() {}

  @Binds
  @IntoMap
  @ClassKey(SelectCarBookingFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SelectCarBookingFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface SelectCarBookingFragmentSubcomponent
      extends AndroidInjector<SelectCarBookingFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SelectCarBookingFragment> {}
  }
}
