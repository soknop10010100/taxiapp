package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.scan_qr_code.ScanQRCodeActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_ScanQRCodeActivity.ScanQRCodeActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_ScanQRCodeActivity {
  private ActivityBuilderModule_ScanQRCodeActivity() {}

  @Binds
  @IntoMap
  @ClassKey(ScanQRCodeActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ScanQRCodeActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface ScanQRCodeActivitySubcomponent extends AndroidInjector<ScanQRCodeActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ScanQRCodeActivity> {}
  }
}
