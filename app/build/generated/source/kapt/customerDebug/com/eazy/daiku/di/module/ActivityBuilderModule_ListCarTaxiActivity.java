package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.step_booking.ListCarTaxiActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_ListCarTaxiActivity.ListCarTaxiActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_ListCarTaxiActivity {
  private ActivityBuilderModule_ListCarTaxiActivity() {}

  @Binds
  @IntoMap
  @ClassKey(ListCarTaxiActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ListCarTaxiActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface ListCarTaxiActivitySubcomponent extends AndroidInjector<ListCarTaxiActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ListCarTaxiActivity> {}
  }
}
