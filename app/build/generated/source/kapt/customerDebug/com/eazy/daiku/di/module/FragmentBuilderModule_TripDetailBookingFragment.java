package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.step_booking.TripDetailBookingFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      FragmentBuilderModule_TripDetailBookingFragment.TripDetailBookingFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_TripDetailBookingFragment {
  private FragmentBuilderModule_TripDetailBookingFragment() {}

  @Binds
  @IntoMap
  @ClassKey(TripDetailBookingFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      TripDetailBookingFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface TripDetailBookingFragmentSubcomponent
      extends AndroidInjector<TripDetailBookingFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<TripDetailBookingFragment> {}
  }
}
