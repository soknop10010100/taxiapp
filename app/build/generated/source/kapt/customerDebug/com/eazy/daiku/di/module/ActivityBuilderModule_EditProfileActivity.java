package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.profile.EditProfileActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_EditProfileActivity.EditProfileActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_EditProfileActivity {
  private ActivityBuilderModule_EditProfileActivity() {}

  @Binds
  @IntoMap
  @ClassKey(EditProfileActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      EditProfileActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface EditProfileActivitySubcomponent extends AndroidInjector<EditProfileActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<EditProfileActivity> {}
  }
}
