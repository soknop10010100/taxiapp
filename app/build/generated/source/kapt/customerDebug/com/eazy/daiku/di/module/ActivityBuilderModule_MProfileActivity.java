package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.profile.MyProfileActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_MProfileActivity.MyProfileActivitySubcomponent.class)
public abstract class ActivityBuilderModule_MProfileActivity {
  private ActivityBuilderModule_MProfileActivity() {}

  @Binds
  @IntoMap
  @ClassKey(MyProfileActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MyProfileActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface MyProfileActivitySubcomponent extends AndroidInjector<MyProfileActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<MyProfileActivity> {}
  }
}
