package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.payment_method.ChangeCardAndBankAccountFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      FragmentBuilderModule_ChangeCardAndBankAccountFragment
          .ChangeCardAndBankAccountFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_ChangeCardAndBankAccountFragment {
  private FragmentBuilderModule_ChangeCardAndBankAccountFragment() {}

  @Binds
  @IntoMap
  @ClassKey(ChangeCardAndBankAccountFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ChangeCardAndBankAccountFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface ChangeCardAndBankAccountFragmentSubcomponent
      extends AndroidInjector<ChangeCardAndBankAccountFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ChangeCardAndBankAccountFragment> {}
  }
}
