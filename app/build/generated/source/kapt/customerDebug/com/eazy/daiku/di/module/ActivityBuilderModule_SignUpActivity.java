package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.signup.SignUpActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_SignUpActivity.SignUpActivitySubcomponent.class)
public abstract class ActivityBuilderModule_SignUpActivity {
  private ActivityBuilderModule_SignUpActivity() {}

  @Binds
  @IntoMap
  @ClassKey(SignUpActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SignUpActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface SignUpActivitySubcomponent extends AndroidInjector<SignUpActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SignUpActivity> {}
  }
}
