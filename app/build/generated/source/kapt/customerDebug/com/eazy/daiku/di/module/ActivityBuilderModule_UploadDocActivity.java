package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.upload_doc.UploadDocActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_UploadDocActivity.UploadDocActivitySubcomponent.class)
public abstract class ActivityBuilderModule_UploadDocActivity {
  private ActivityBuilderModule_UploadDocActivity() {}

  @Binds
  @IntoMap
  @ClassKey(UploadDocActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      UploadDocActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface UploadDocActivitySubcomponent extends AndroidInjector<UploadDocActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<UploadDocActivity> {}
  }
}
