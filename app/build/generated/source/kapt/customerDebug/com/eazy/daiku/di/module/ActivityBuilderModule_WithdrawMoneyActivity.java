package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.withdraw.WithdrawMoneyActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_WithdrawMoneyActivity.WithdrawMoneyActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_WithdrawMoneyActivity {
  private ActivityBuilderModule_WithdrawMoneyActivity() {}

  @Binds
  @IntoMap
  @ClassKey(WithdrawMoneyActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      WithdrawMoneyActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface WithdrawMoneyActivitySubcomponent
      extends AndroidInjector<WithdrawMoneyActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<WithdrawMoneyActivity> {}
  }
}
