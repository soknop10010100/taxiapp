package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.history.HistoryTripActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_HistoryTripActivity.HistoryTripActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_HistoryTripActivity {
  private ActivityBuilderModule_HistoryTripActivity() {}

  @Binds
  @IntoMap
  @ClassKey(HistoryTripActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      HistoryTripActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface HistoryTripActivitySubcomponent extends AndroidInjector<HistoryTripActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<HistoryTripActivity> {}
  }
}
