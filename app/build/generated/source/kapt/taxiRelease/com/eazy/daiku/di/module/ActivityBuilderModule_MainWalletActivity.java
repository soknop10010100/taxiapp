package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.wallet.MainWalletActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_MainWalletActivity.MainWalletActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_MainWalletActivity {
  private ActivityBuilderModule_MainWalletActivity() {}

  @Binds
  @IntoMap
  @ClassKey(MainWalletActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MainWalletActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface MainWalletActivitySubcomponent extends AndroidInjector<MainWalletActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<MainWalletActivity> {}
  }
}
