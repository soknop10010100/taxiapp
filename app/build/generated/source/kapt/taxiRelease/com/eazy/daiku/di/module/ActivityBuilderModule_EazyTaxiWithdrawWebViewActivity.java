package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.withdraw.EazyTaxiWithdrawWebviewActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_EazyTaxiWithdrawWebViewActivity
          .EazyTaxiWithdrawWebviewActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_EazyTaxiWithdrawWebViewActivity {
  private ActivityBuilderModule_EazyTaxiWithdrawWebViewActivity() {}

  @Binds
  @IntoMap
  @ClassKey(EazyTaxiWithdrawWebviewActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      EazyTaxiWithdrawWebviewActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface EazyTaxiWithdrawWebviewActivitySubcomponent
      extends AndroidInjector<EazyTaxiWithdrawWebviewActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<EazyTaxiWithdrawWebviewActivity> {}
  }
}
