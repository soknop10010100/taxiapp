package com.eazy.daiku.di.module;

import com.eazy.daiku.utility.base.SimpleBaseActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_SimpleBaseActivity.SimpleBaseActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_SimpleBaseActivity {
  private ActivityBuilderModule_SimpleBaseActivity() {}

  @Binds
  @IntoMap
  @ClassKey(SimpleBaseActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SimpleBaseActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface SimpleBaseActivitySubcomponent extends AndroidInjector<SimpleBaseActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SimpleBaseActivity> {}
  }
}
