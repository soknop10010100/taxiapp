package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.map.PickUpLocationActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_PrickUpLocationActivity.PickUpLocationActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_PrickUpLocationActivity {
  private ActivityBuilderModule_PrickUpLocationActivity() {}

  @Binds
  @IntoMap
  @ClassKey(PickUpLocationActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      PickUpLocationActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface PickUpLocationActivitySubcomponent
      extends AndroidInjector<PickUpLocationActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<PickUpLocationActivity> {}
  }
}
