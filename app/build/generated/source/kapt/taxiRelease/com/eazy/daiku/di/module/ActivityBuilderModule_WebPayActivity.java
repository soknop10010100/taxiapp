package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.web_payment.WebPayActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_WebPayActivity.WebPayActivitySubcomponent.class)
public abstract class ActivityBuilderModule_WebPayActivity {
  private ActivityBuilderModule_WebPayActivity() {}

  @Binds
  @IntoMap
  @ClassKey(WebPayActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      WebPayActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface WebPayActivitySubcomponent extends AndroidInjector<WebPayActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<WebPayActivity> {}
  }
}
