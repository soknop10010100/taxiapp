package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.payment_method.AddABAAccountNumberFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      FragmentBuilderModule_AddABAAccountNumberFragment.AddABAAccountNumberFragmentSubcomponent
          .class
)
public abstract class FragmentBuilderModule_AddABAAccountNumberFragment {
  private FragmentBuilderModule_AddABAAccountNumberFragment() {}

  @Binds
  @IntoMap
  @ClassKey(AddABAAccountNumberFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      AddABAAccountNumberFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface AddABAAccountNumberFragmentSubcomponent
      extends AndroidInjector<AddABAAccountNumberFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<AddABAAccountNumberFragment> {}
  }
}
