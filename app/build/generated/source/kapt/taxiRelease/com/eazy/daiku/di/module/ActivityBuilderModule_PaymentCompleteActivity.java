package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.step_booking.PaymentCompleteActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_PaymentCompleteActivity.PaymentCompleteActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_PaymentCompleteActivity {
  private ActivityBuilderModule_PaymentCompleteActivity() {}

  @Binds
  @IntoMap
  @ClassKey(PaymentCompleteActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      PaymentCompleteActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface PaymentCompleteActivitySubcomponent
      extends AndroidInjector<PaymentCompleteActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<PaymentCompleteActivity> {}
  }
}
