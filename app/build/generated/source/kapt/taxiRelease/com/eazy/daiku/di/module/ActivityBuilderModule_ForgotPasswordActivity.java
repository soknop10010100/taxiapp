package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.forget_password.VerifyOtpCodeActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_ForgotPasswordActivity.VerifyOtpCodeActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_ForgotPasswordActivity {
  private ActivityBuilderModule_ForgotPasswordActivity() {}

  @Binds
  @IntoMap
  @ClassKey(VerifyOtpCodeActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      VerifyOtpCodeActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface VerifyOtpCodeActivitySubcomponent
      extends AndroidInjector<VerifyOtpCodeActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<VerifyOtpCodeActivity> {}
  }
}
