package com.eazy.daiku.di.module;

import com.eazy.daiku.utility.base.BaseCoreActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_BaseCoreActivity.BaseCoreActivitySubcomponent.class)
public abstract class ActivityBuilderModule_BaseCoreActivity {
  private ActivityBuilderModule_BaseCoreActivity() {}

  @Binds
  @IntoMap
  @ClassKey(BaseCoreActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      BaseCoreActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface BaseCoreActivitySubcomponent extends AndroidInjector<BaseCoreActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<BaseCoreActivity> {}
  }
}
