package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.step_booking.PreviewCheckoutActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_PreviewCheckoutActivity.PreviewCheckoutActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_PreviewCheckoutActivity {
  private ActivityBuilderModule_PreviewCheckoutActivity() {}

  @Binds
  @IntoMap
  @ClassKey(PreviewCheckoutActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      PreviewCheckoutActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface PreviewCheckoutActivitySubcomponent
      extends AndroidInjector<PreviewCheckoutActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<PreviewCheckoutActivity> {}
  }
}
