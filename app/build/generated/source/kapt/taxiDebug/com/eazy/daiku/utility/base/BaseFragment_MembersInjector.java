// Generated by Dagger (https://dagger.dev).
package com.eazy.daiku.utility.base;

import androidx.lifecycle.ViewModelProvider;
import dagger.MembersInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.DaggerFragment_MembersInjector;
import dagger.internal.DaggerGenerated;
import dagger.internal.InjectedFieldSignature;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class BaseFragment_MembersInjector implements MembersInjector<BaseFragment> {
  private final Provider<DispatchingAndroidInjector<Object>> androidInjectorProvider;

  private final Provider<ViewModelProvider.Factory> factoryProvider;

  public BaseFragment_MembersInjector(
      Provider<DispatchingAndroidInjector<Object>> androidInjectorProvider,
      Provider<ViewModelProvider.Factory> factoryProvider) {
    this.androidInjectorProvider = androidInjectorProvider;
    this.factoryProvider = factoryProvider;
  }

  public static MembersInjector<BaseFragment> create(
      Provider<DispatchingAndroidInjector<Object>> androidInjectorProvider,
      Provider<ViewModelProvider.Factory> factoryProvider) {
    return new BaseFragment_MembersInjector(androidInjectorProvider, factoryProvider);
  }

  @Override
  public void injectMembers(BaseFragment instance) {
    DaggerFragment_MembersInjector.injectAndroidInjector(instance, androidInjectorProvider.get());
    injectFactory(instance, factoryProvider.get());
  }

  @InjectedFieldSignature("com.eazy.daiku.utility.base.BaseFragment.factory")
  public static void injectFactory(BaseFragment instance, ViewModelProvider.Factory factory) {
    instance.factory = factory;
  }
}
