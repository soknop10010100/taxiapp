package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.splash_screen.SplashScreenActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_SplashScreenActivity.SplashScreenActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_SplashScreenActivity {
  private ActivityBuilderModule_SplashScreenActivity() {}

  @Binds
  @IntoMap
  @ClassKey(SplashScreenActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SplashScreenActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface SplashScreenActivitySubcomponent extends AndroidInjector<SplashScreenActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SplashScreenActivity> {}
  }
}
