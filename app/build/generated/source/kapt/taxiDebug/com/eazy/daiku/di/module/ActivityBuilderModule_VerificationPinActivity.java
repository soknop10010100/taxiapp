package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.verification_pin_code.VerificationPinActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_VerificationPinActivity.VerificationPinActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_VerificationPinActivity {
  private ActivityBuilderModule_VerificationPinActivity() {}

  @Binds
  @IntoMap
  @ClassKey(VerificationPinActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      VerificationPinActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface VerificationPinActivitySubcomponent
      extends AndroidInjector<VerificationPinActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<VerificationPinActivity> {}
  }
}
