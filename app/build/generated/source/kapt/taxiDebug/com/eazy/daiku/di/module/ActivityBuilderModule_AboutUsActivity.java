package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.about_us.AboutUsActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_AboutUsActivity.AboutUsActivitySubcomponent.class)
public abstract class ActivityBuilderModule_AboutUsActivity {
  private ActivityBuilderModule_AboutUsActivity() {}

  @Binds
  @IntoMap
  @ClassKey(AboutUsActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      AboutUsActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface AboutUsActivitySubcomponent extends AndroidInjector<AboutUsActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<AboutUsActivity> {}
  }
}
