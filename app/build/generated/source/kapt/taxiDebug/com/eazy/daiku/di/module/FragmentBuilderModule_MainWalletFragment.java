package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.home.MainWalletFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = FragmentBuilderModule_MainWalletFragment.MainWalletFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_MainWalletFragment {
  private FragmentBuilderModule_MainWalletFragment() {}

  @Binds
  @IntoMap
  @ClassKey(MainWalletFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MainWalletFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface MainWalletFragmentSubcomponent extends AndroidInjector<MainWalletFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<MainWalletFragment> {}
  }
}
