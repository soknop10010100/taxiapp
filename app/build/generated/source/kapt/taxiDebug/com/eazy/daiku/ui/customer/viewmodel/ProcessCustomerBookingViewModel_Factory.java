// Generated by Dagger (https://dagger.dev).
package com.eazy.daiku.ui.customer.viewmodel;

import android.content.Context;
import com.eazy.daiku.data.repository.Repository;
import dagger.internal.DaggerGenerated;
import dagger.internal.Factory;
import javax.inject.Provider;

@DaggerGenerated
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class ProcessCustomerBookingViewModel_Factory implements Factory<ProcessCustomerBookingViewModel> {
  private final Provider<Context> contextProvider;

  private final Provider<Repository> repositoryProvider;

  public ProcessCustomerBookingViewModel_Factory(Provider<Context> contextProvider,
      Provider<Repository> repositoryProvider) {
    this.contextProvider = contextProvider;
    this.repositoryProvider = repositoryProvider;
  }

  @Override
  public ProcessCustomerBookingViewModel get() {
    return newInstance(contextProvider.get(), repositoryProvider.get());
  }

  public static ProcessCustomerBookingViewModel_Factory create(Provider<Context> contextProvider,
      Provider<Repository> repositoryProvider) {
    return new ProcessCustomerBookingViewModel_Factory(contextProvider, repositoryProvider);
  }

  public static ProcessCustomerBookingViewModel newInstance(Context context,
      Repository repository) {
    return new ProcessCustomerBookingViewModel(context, repository);
  }
}
