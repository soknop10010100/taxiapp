package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.web_view.WebViewEazyTaxiActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_WebViewEazyTaxiActivity.WebViewEazyTaxiActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_WebViewEazyTaxiActivity {
  private ActivityBuilderModule_WebViewEazyTaxiActivity() {}

  @Binds
  @IntoMap
  @ClassKey(WebViewEazyTaxiActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      WebViewEazyTaxiActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface WebViewEazyTaxiActivitySubcomponent
      extends AndroidInjector<WebViewEazyTaxiActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<WebViewEazyTaxiActivity> {}
  }
}
