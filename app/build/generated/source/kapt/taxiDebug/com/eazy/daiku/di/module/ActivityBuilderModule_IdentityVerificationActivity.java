package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.identity_verification.IdentityVerificationActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_IdentityVerificationActivity.IdentityVerificationActivitySubcomponent
          .class
)
public abstract class ActivityBuilderModule_IdentityVerificationActivity {
  private ActivityBuilderModule_IdentityVerificationActivity() {}

  @Binds
  @IntoMap
  @ClassKey(IdentityVerificationActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      IdentityVerificationActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface IdentityVerificationActivitySubcomponent
      extends AndroidInjector<IdentityVerificationActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<IdentityVerificationActivity> {}
  }
}
