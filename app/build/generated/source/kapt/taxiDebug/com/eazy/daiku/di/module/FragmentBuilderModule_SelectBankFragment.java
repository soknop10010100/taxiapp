package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.payment_method.SelectBankFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = FragmentBuilderModule_SelectBankFragment.SelectBankFragmentSubcomponent.class
)
public abstract class FragmentBuilderModule_SelectBankFragment {
  private FragmentBuilderModule_SelectBankFragment() {}

  @Binds
  @IntoMap
  @ClassKey(SelectBankFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SelectBankFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface SelectBankFragmentSubcomponent extends AndroidInjector<SelectBankFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SelectBankFragment> {}
  }
}
