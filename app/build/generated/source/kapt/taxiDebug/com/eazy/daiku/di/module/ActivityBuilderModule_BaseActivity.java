package com.eazy.daiku.di.module;

import com.eazy.daiku.utility.base.BaseActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_BaseActivity.BaseActivitySubcomponent.class)
public abstract class ActivityBuilderModule_BaseActivity {
  private ActivityBuilderModule_BaseActivity() {}

  @Binds
  @IntoMap
  @ClassKey(BaseActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      BaseActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface BaseActivitySubcomponent extends AndroidInjector<BaseActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<BaseActivity> {}
  }
}
