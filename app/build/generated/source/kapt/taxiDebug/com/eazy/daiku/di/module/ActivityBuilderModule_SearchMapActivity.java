package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.map.SearchMapActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = ActivityBuilderModule_SearchMapActivity.SearchMapActivitySubcomponent.class)
public abstract class ActivityBuilderModule_SearchMapActivity {
  private ActivityBuilderModule_SearchMapActivity() {}

  @Binds
  @IntoMap
  @ClassKey(SearchMapActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      SearchMapActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface SearchMapActivitySubcomponent extends AndroidInjector<SearchMapActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<SearchMapActivity> {}
  }
}
