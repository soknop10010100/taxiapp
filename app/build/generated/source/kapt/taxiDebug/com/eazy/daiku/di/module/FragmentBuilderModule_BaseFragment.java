package com.eazy.daiku.di.module;

import com.eazy.daiku.utility.base.BaseFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = FragmentBuilderModule_BaseFragment.BaseFragmentSubcomponent.class)
public abstract class FragmentBuilderModule_BaseFragment {
  private FragmentBuilderModule_BaseFragment() {}

  @Binds
  @IntoMap
  @ClassKey(BaseFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      BaseFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface BaseFragmentSubcomponent extends AndroidInjector<BaseFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<BaseFragment> {}
  }
}
