package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.forget_password.ForgetPasswordActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_ForgetPasswordActivity.ForgetPasswordActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_ForgetPasswordActivity {
  private ActivityBuilderModule_ForgetPasswordActivity() {}

  @Binds
  @IntoMap
  @ClassKey(ForgetPasswordActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ForgetPasswordActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface ForgetPasswordActivitySubcomponent
      extends AndroidInjector<ForgetPasswordActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ForgetPasswordActivity> {}
  }
}
