package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.map.MapPreviewActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents = ActivityBuilderModule_MapPreviewActivity.MapPreviewActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_MapPreviewActivity {
  private ActivityBuilderModule_MapPreviewActivity() {}

  @Binds
  @IntoMap
  @ClassKey(MapPreviewActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      MapPreviewActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface MapPreviewActivitySubcomponent extends AndroidInjector<MapPreviewActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<MapPreviewActivity> {}
  }
}
