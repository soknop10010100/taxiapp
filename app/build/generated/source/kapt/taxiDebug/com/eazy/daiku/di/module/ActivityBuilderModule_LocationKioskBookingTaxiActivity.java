package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.step_booking.LocationKioskBookingTaxiActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_LocationKioskBookingTaxiActivity
          .LocationKioskBookingTaxiActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_LocationKioskBookingTaxiActivity {
  private ActivityBuilderModule_LocationKioskBookingTaxiActivity() {}

  @Binds
  @IntoMap
  @ClassKey(LocationKioskBookingTaxiActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      LocationKioskBookingTaxiActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface LocationKioskBookingTaxiActivitySubcomponent
      extends AndroidInjector<LocationKioskBookingTaxiActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<LocationKioskBookingTaxiActivity> {}
  }
}
