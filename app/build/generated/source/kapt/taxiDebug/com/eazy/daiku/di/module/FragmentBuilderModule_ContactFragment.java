package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.home.ContactFragment;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(subcomponents = FragmentBuilderModule_ContactFragment.ContactFragmentSubcomponent.class)
public abstract class FragmentBuilderModule_ContactFragment {
  private FragmentBuilderModule_ContactFragment() {}

  @Binds
  @IntoMap
  @ClassKey(ContactFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ContactFragmentSubcomponent.Factory builder);

  @Subcomponent
  public interface ContactFragmentSubcomponent extends AndroidInjector<ContactFragment> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ContactFragment> {}
  }
}
