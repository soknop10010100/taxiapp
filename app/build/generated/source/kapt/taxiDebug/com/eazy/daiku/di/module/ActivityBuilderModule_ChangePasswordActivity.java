package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.change_password.ChangePasswordActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_ChangePasswordActivity.ChangePasswordActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_ChangePasswordActivity {
  private ActivityBuilderModule_ChangePasswordActivity() {}

  @Binds
  @IntoMap
  @ClassKey(ChangePasswordActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      ChangePasswordActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface ChangePasswordActivitySubcomponent
      extends AndroidInjector<ChangePasswordActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<ChangePasswordActivity> {}
  }
}
