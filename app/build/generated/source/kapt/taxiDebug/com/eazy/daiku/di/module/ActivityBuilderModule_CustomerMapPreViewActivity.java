package com.eazy.daiku.di.module;

import com.eazy.daiku.ui.customer.map.CustomerMapPreViewActivity;
import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;

@Module(
  subcomponents =
      ActivityBuilderModule_CustomerMapPreViewActivity.CustomerMapPreViewActivitySubcomponent.class
)
public abstract class ActivityBuilderModule_CustomerMapPreViewActivity {
  private ActivityBuilderModule_CustomerMapPreViewActivity() {}

  @Binds
  @IntoMap
  @ClassKey(CustomerMapPreViewActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      CustomerMapPreViewActivitySubcomponent.Factory builder);

  @Subcomponent
  public interface CustomerMapPreViewActivitySubcomponent
      extends AndroidInjector<CustomerMapPreViewActivity> {
    @Subcomponent.Factory
    interface Factory extends AndroidInjector.Factory<CustomerMapPreViewActivity> {}
  }
}
