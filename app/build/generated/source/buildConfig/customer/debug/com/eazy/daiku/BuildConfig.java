/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.eazy.daiku;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.eazy.daikou.customer";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "customer";
  public static final int VERSION_CODE = 2;
  public static final String VERSION_NAME = "1.1";
  // Field from product flavor: customer
  public static final String APP_NAME = "Eazy Customer";
  // Field from build type: debug
  public static final String BASE_TELEGRAM = "https://api.telegram.org/bot5418353614:AAFj5AVjjb9hD580RW3O0ZcloPJetxM6Fsw/";
  // Field from build type: debug
  public static final String BASE_URL = "https://dev.eazybooking.asia/";
  // Field from build type: debug
  public static final String BUILD_VERSION = "1.3";
  // Field from product flavor: customer
  public static final boolean IS_CUSTOMER = true;
  // Field from build type: debug
  public static final boolean IS_DEBUG = true;
  // Field from product flavor: customer
  public static final boolean IS_TAXI = false;
  // Field from product flavor: customer
  public static final boolean IS_WEGO_TAXI = false;
}
