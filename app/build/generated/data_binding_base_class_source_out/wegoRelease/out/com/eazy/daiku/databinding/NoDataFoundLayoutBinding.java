// Generated by view binder compiler. Do not edit!
package com.eazy.daiku.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.eazy.daiku.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class NoDataFoundLayoutBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final TextView messageView;

  @NonNull
  public final RelativeLayout retryView;

  private NoDataFoundLayoutBinding(@NonNull RelativeLayout rootView, @NonNull TextView messageView,
      @NonNull RelativeLayout retryView) {
    this.rootView = rootView;
    this.messageView = messageView;
    this.retryView = retryView;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static NoDataFoundLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static NoDataFoundLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.no_data_found_layout, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static NoDataFoundLayoutBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.message_view;
      TextView messageView = ViewBindings.findChildViewById(rootView, id);
      if (messageView == null) {
        break missingId;
      }

      RelativeLayout retryView = (RelativeLayout) rootView;

      return new NoDataFoundLayoutBinding((RelativeLayout) rootView, messageView, retryView);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
