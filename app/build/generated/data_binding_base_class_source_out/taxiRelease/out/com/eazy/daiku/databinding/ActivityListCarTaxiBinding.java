// Generated by view binder compiler. Do not edit!
package com.eazy.daiku.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.eazy.daiku.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityListCarTaxiBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final FrameLayout containerListCarTaxiFragment;

  private ActivityListCarTaxiBinding(@NonNull LinearLayout rootView,
      @NonNull FrameLayout containerListCarTaxiFragment) {
    this.rootView = rootView;
    this.containerListCarTaxiFragment = containerListCarTaxiFragment;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityListCarTaxiBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityListCarTaxiBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_list_car_taxi, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityListCarTaxiBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.container_list_car_taxi_fragment;
      FrameLayout containerListCarTaxiFragment = ViewBindings.findChildViewById(rootView, id);
      if (containerListCarTaxiFragment == null) {
        break missingId;
      }

      return new ActivityListCarTaxiBinding((LinearLayout) rootView, containerListCarTaxiFragment);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
