// Generated by view binder compiler. Do not edit!
package com.eazy.daiku.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.eazy.daiku.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityAboutUsBinding implements ViewBinding {
  @NonNull
  private final LinearLayout rootView;

  @NonNull
  public final RelativeLayout actionPrivacyPolicyLayout;

  @NonNull
  public final RelativeLayout actionTermsOfServiceLayout;

  @NonNull
  public final TextView actionUpdateTv;

  @NonNull
  public final TextView buildVersionTv;

  @NonNull
  public final ImageView img;

  @NonNull
  public final LinearLayout layoutLogo;

  @NonNull
  public final TextView layoutNameCompany;

  @NonNull
  public final LinearLayout layoutNameLogo;

  @NonNull
  public final LinearLayout layoutTermService;

  @NonNull
  public final LinearLayout term;

  @NonNull
  public final TextView termOfServiceTv;

  @NonNull
  public final TextView versionNameTv;

  private ActivityAboutUsBinding(@NonNull LinearLayout rootView,
      @NonNull RelativeLayout actionPrivacyPolicyLayout,
      @NonNull RelativeLayout actionTermsOfServiceLayout, @NonNull TextView actionUpdateTv,
      @NonNull TextView buildVersionTv, @NonNull ImageView img, @NonNull LinearLayout layoutLogo,
      @NonNull TextView layoutNameCompany, @NonNull LinearLayout layoutNameLogo,
      @NonNull LinearLayout layoutTermService, @NonNull LinearLayout term,
      @NonNull TextView termOfServiceTv, @NonNull TextView versionNameTv) {
    this.rootView = rootView;
    this.actionPrivacyPolicyLayout = actionPrivacyPolicyLayout;
    this.actionTermsOfServiceLayout = actionTermsOfServiceLayout;
    this.actionUpdateTv = actionUpdateTv;
    this.buildVersionTv = buildVersionTv;
    this.img = img;
    this.layoutLogo = layoutLogo;
    this.layoutNameCompany = layoutNameCompany;
    this.layoutNameLogo = layoutNameLogo;
    this.layoutTermService = layoutTermService;
    this.term = term;
    this.termOfServiceTv = termOfServiceTv;
    this.versionNameTv = versionNameTv;
  }

  @Override
  @NonNull
  public LinearLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityAboutUsBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityAboutUsBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_about_us, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityAboutUsBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.action_privacy_policy_layout;
      RelativeLayout actionPrivacyPolicyLayout = ViewBindings.findChildViewById(rootView, id);
      if (actionPrivacyPolicyLayout == null) {
        break missingId;
      }

      id = R.id.action_terms_of_service_layout;
      RelativeLayout actionTermsOfServiceLayout = ViewBindings.findChildViewById(rootView, id);
      if (actionTermsOfServiceLayout == null) {
        break missingId;
      }

      id = R.id.actionUpdateTv;
      TextView actionUpdateTv = ViewBindings.findChildViewById(rootView, id);
      if (actionUpdateTv == null) {
        break missingId;
      }

      id = R.id.buildVersionTv;
      TextView buildVersionTv = ViewBindings.findChildViewById(rootView, id);
      if (buildVersionTv == null) {
        break missingId;
      }

      id = R.id.img;
      ImageView img = ViewBindings.findChildViewById(rootView, id);
      if (img == null) {
        break missingId;
      }

      id = R.id.layout_logo;
      LinearLayout layoutLogo = ViewBindings.findChildViewById(rootView, id);
      if (layoutLogo == null) {
        break missingId;
      }

      id = R.id.layout_name_company;
      TextView layoutNameCompany = ViewBindings.findChildViewById(rootView, id);
      if (layoutNameCompany == null) {
        break missingId;
      }

      id = R.id.layout_name_logo;
      LinearLayout layoutNameLogo = ViewBindings.findChildViewById(rootView, id);
      if (layoutNameLogo == null) {
        break missingId;
      }

      id = R.id.layout_term_service;
      LinearLayout layoutTermService = ViewBindings.findChildViewById(rootView, id);
      if (layoutTermService == null) {
        break missingId;
      }

      id = R.id.term;
      LinearLayout term = ViewBindings.findChildViewById(rootView, id);
      if (term == null) {
        break missingId;
      }

      id = R.id.term_of_service_tv;
      TextView termOfServiceTv = ViewBindings.findChildViewById(rootView, id);
      if (termOfServiceTv == null) {
        break missingId;
      }

      id = R.id.versionNameTv;
      TextView versionNameTv = ViewBindings.findChildViewById(rootView, id);
      if (versionNameTv == null) {
        break missingId;
      }

      return new ActivityAboutUsBinding((LinearLayout) rootView, actionPrivacyPolicyLayout,
          actionTermsOfServiceLayout, actionUpdateTv, buildVersionTv, img, layoutLogo,
          layoutNameCompany, layoutNameLogo, layoutTermService, term, termOfServiceTv,
          versionNameTv);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
