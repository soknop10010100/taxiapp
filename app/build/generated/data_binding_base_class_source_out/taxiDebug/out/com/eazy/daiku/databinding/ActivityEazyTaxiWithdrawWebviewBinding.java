// Generated by view binder compiler. Do not edit!
package com.eazy.daiku.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.eazy.daiku.R;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.progressindicator.LinearProgressIndicator;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ActivityEazyTaxiWithdrawWebviewBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final AppbarLayoutBinding containerBar;

  @NonNull
  public final RelativeLayout containerLoadingWeb;

  @NonNull
  public final MaterialCardView contanerLogo;

  @NonNull
  public final LinearProgressIndicator progressBarWebView;

  @NonNull
  public final WebView webView;

  private ActivityEazyTaxiWithdrawWebviewBinding(@NonNull RelativeLayout rootView,
      @NonNull AppbarLayoutBinding containerBar, @NonNull RelativeLayout containerLoadingWeb,
      @NonNull MaterialCardView contanerLogo, @NonNull LinearProgressIndicator progressBarWebView,
      @NonNull WebView webView) {
    this.rootView = rootView;
    this.containerBar = containerBar;
    this.containerLoadingWeb = containerLoadingWeb;
    this.contanerLogo = contanerLogo;
    this.progressBarWebView = progressBarWebView;
    this.webView = webView;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ActivityEazyTaxiWithdrawWebviewBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ActivityEazyTaxiWithdrawWebviewBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.activity_eazy_taxi_withdraw_webview, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ActivityEazyTaxiWithdrawWebviewBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.container_bar;
      View containerBar = ViewBindings.findChildViewById(rootView, id);
      if (containerBar == null) {
        break missingId;
      }
      AppbarLayoutBinding binding_containerBar = AppbarLayoutBinding.bind(containerBar);

      id = R.id.container_loading_web;
      RelativeLayout containerLoadingWeb = ViewBindings.findChildViewById(rootView, id);
      if (containerLoadingWeb == null) {
        break missingId;
      }

      id = R.id.contaner_logo;
      MaterialCardView contanerLogo = ViewBindings.findChildViewById(rootView, id);
      if (contanerLogo == null) {
        break missingId;
      }

      id = R.id.progress_bar_web_view;
      LinearProgressIndicator progressBarWebView = ViewBindings.findChildViewById(rootView, id);
      if (progressBarWebView == null) {
        break missingId;
      }

      id = R.id.webView;
      WebView webView = ViewBindings.findChildViewById(rootView, id);
      if (webView == null) {
        break missingId;
      }

      return new ActivityEazyTaxiWithdrawWebviewBinding((RelativeLayout) rootView,
          binding_containerBar, containerLoadingWeb, contanerLogo, progressBarWebView, webView);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
