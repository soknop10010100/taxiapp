// Generated by view binder compiler. Do not edit!
package com.eazy.daiku.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewbinding.ViewBinding;
import androidx.viewbinding.ViewBindings;
import com.eazy.daiku.R;
import com.google.android.material.card.MaterialCardView;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class QrHolderErrorLayoutBinding implements ViewBinding {
  @NonNull
  private final RelativeLayout rootView;

  @NonNull
  public final TextView errorMsgTv;

  @NonNull
  public final MaterialCardView rescanMtcardView;

  private QrHolderErrorLayoutBinding(@NonNull RelativeLayout rootView, @NonNull TextView errorMsgTv,
      @NonNull MaterialCardView rescanMtcardView) {
    this.rootView = rootView;
    this.errorMsgTv = errorMsgTv;
    this.rescanMtcardView = rescanMtcardView;
  }

  @Override
  @NonNull
  public RelativeLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static QrHolderErrorLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static QrHolderErrorLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.qr_holder_error_layout, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static QrHolderErrorLayoutBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    int id;
    missingId: {
      id = R.id.error_msg_tv;
      TextView errorMsgTv = ViewBindings.findChildViewById(rootView, id);
      if (errorMsgTv == null) {
        break missingId;
      }

      id = R.id.rescan_mtcard_view;
      MaterialCardView rescanMtcardView = ViewBindings.findChildViewById(rootView, id);
      if (rescanMtcardView == null) {
        break missingId;
      }

      return new QrHolderErrorLayoutBinding((RelativeLayout) rootView, errorMsgTv,
          rescanMtcardView);
    }
    String missingId = rootView.getResources().getResourceName(id);
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
